"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const typedi_1 = require("typedi");
const tipoViaturaId_1 = require("../domain/tipoViatura/tipoViaturaId");
const TipoViaturaMap_1 = require("../mappers/TipoViaturaMap");
let TipoViaturaRepo = class TipoViaturaRepo {
    constructor(tipoViaturaSchema) {
        this.tipoViaturaSchema = tipoViaturaSchema;
    }
    createBaseQuery() {
        return {
            where: {},
        };
    }
    async exists(tipoViaturaId) {
        const idX = tipoViaturaId instanceof tipoViaturaId_1.TipoViaturaId ? tipoViaturaId.id.toValue() : tipoViaturaId;
        const query = { string: idX };
        const tipoViaturaDocument = await this.tipoViaturaSchema.findOne(query);
        return !!tipoViaturaDocument === true;
    }
    async save(tipoViatura) {
        const query = { domainId: tipoViatura.id.toString() };
        const tipoViaturaDocument = await this.tipoViaturaSchema.findOne(query);
        try {
            if (tipoViaturaDocument === null) {
                const rawTipoViatura = TipoViaturaMap_1.TipoViaturaMap.toPersistence(tipoViatura);
                const tipoViaturaCreated = await this.tipoViaturaSchema.create(rawTipoViatura);
                return TipoViaturaMap_1.TipoViaturaMap.toDomain(tipoViaturaCreated);
            }
            else {
                // tipoViaturaDocument.domainId = tipoViatura.props.domainId;
                // tipoViaturaDocument.nome = tipoViatura.nome;
                // tipoViaturaDocument.autonomia = tipoViatura.autonomia;
                // tipoViaturaDocument.custo = tipoViatura.custo;
                // tipoViaturaDocument.velocidadeMedia = tipoViatura.velocidadeMedia;
                // tipoViaturaDocument.combustivel = tipoViatura.combustivel;
                // tipoViaturaDocument.consumo = tipoViatura.consumo;
                // tipoViaturaDocument.emissoes = tipoViatura.emissoes;
                //             await tipoViaturaDocument.save();
                return tipoViatura;
            }
        }
        catch (err) {
            throw err;
        }
    }
    async findByDomainId(tipoViaturaId) {
        const query = { domainId: tipoViaturaId };
        const tipoViaturaRecord = await this.tipoViaturaSchema.findOne(query);
        if (tipoViaturaRecord != null) {
            return TipoViaturaMap_1.TipoViaturaMap.toDomain(tipoViaturaRecord);
        }
        else
            return null;
    }
    async findAll() {
        const tipoViaturaRecord = await this.tipoViaturaSchema.find({});
        if (tipoViaturaRecord != null) {
            return TipoViaturaMap_1.TipoViaturaMap.toDomainArray(tipoViaturaRecord);
        }
        else
            return null;
    }
    ;
    async findAllSorted(ord) {
        const tipoViaturaRecord = await this.tipoViaturaSchema.find({}).sort(ord);
        if (tipoViaturaRecord != null) {
            return TipoViaturaMap_1.TipoViaturaMap.toDomainArray(tipoViaturaRecord);
        }
        else
            return null;
    }
    ;
};
TipoViaturaRepo = __decorate([
    typedi_1.Service(),
    __param(0, typedi_1.Inject('tipoViaturaSchema')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], TipoViaturaRepo);
exports.default = TipoViaturaRepo;
//# sourceMappingURL=tipoViaturaRepo.js.map