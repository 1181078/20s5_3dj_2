"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const typedi_1 = require("typedi");
const percursoId_1 = require("../domain/percurso/percursoId");
const PercursoMap_1 = require("../mappers/PercursoMap");
let PercursoRepo = class PercursoRepo {
    constructor(percursoSchema) {
        this.percursoSchema = percursoSchema;
    }
    createBaseQuery() {
        return {
            where: {},
        };
    }
    async exists(percursoId) {
        const idX = percursoId instanceof percursoId_1.PercursoId ? percursoId.id.toValue() : percursoId;
        const query = { string: idX };
        const percursoDocument = await this.percursoSchema.findOne(query);
        return !!percursoDocument === true;
    }
    async save(percurso) {
        const query = { domainId: percurso.id.toString() };
        const percursoDocument = await this.percursoSchema.findOne(query);
        try {
            if (percursoDocument === null) {
                const rawPercurso = PercursoMap_1.PercursoMap.toPersistence(percurso);
                const percursoCreated = await this.percursoSchema.create(rawPercurso);
                return PercursoMap_1.PercursoMap.toDomain(percursoCreated);
            }
            else {
                // percursoDocument.domainId = percurso.props.domainId;
                // percursoDocument.nosPercurso = percurso.nosPercurso;
                // await percursoDocument.save();
                return percurso;
            }
        }
        catch (err) {
            throw err;
        }
    }
    async findByDomainId(percursoId) {
        const query = { domainId: percursoId };
        const percursoRecord = await this.percursoSchema.findOne(query);
        if (percursoRecord != null) {
            return PercursoMap_1.PercursoMap.toDomain(percursoRecord);
        }
        else
            return null;
    }
    async findAll() {
        const percursoRecord = await this.percursoSchema.find({});
        if (percursoRecord != null) {
            return PercursoMap_1.PercursoMap.toDomainArray(percursoRecord);
        }
        else
            return null;
    }
    ;
};
PercursoRepo = __decorate([
    typedi_1.Service(),
    __param(0, typedi_1.Inject('percursoSchema')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], PercursoRepo);
exports.default = PercursoRepo;
//# sourceMappingURL=percursoRepo.js.map