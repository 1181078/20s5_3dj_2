"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const typedi_1 = require("typedi");
const linhaId_1 = require("../domain/linha/linhaId");
const LinhaMap_1 = require("../mappers/LinhaMap");
let LinhaRepo = class LinhaRepo {
    constructor(linhaSchema) {
        this.linhaSchema = linhaSchema;
    }
    createBaseQuery() {
        return {
            where: {},
        };
    }
    async exists(linhaId) {
        const idX = linhaId instanceof linhaId_1.LinhaId ? linhaId.id.toValue() : linhaId;
        const query = { string: idX };
        const linhaDocument = await this.linhaSchema.findOne(query);
        return !!linhaDocument === true;
    }
    async save(linha) {
        const query = { domainId: linha.id.toString() };
        const linhaDocument = await this.linhaSchema.findOne(query);
        try {
            if (linhaDocument === null) {
                const rawLinha = LinhaMap_1.LinhaMap.toPersistence(linha);
                const linhaCreated = await this.linhaSchema.create(rawLinha);
                return LinhaMap_1.LinhaMap.toDomain(linhaCreated);
            }
            else {
                // linhaDocument.domainId = linha.props.domainId;
                // linhaDocument.nome = linha.props.nome;
                // linhaDocument.cor = linha.props.cor;
                // linhaDocument.percursoLinha = linha.props.percursoLinha;
                //await linhaDocument.save();
                return linha;
            }
        }
        catch (err) {
            throw err;
        }
    }
    async findByDomainId(linhaId) {
        const query = { domainId: linhaId };
        const noRecord = await this.linhaSchema.findOne(query);
        if (noRecord != null) {
            return LinhaMap_1.LinhaMap.toDomain(noRecord);
        }
        else
            return null;
    }
    async findAll() {
        const noRecord = await this.linhaSchema.find({});
        if (noRecord != null) {
            return LinhaMap_1.LinhaMap.toDomainArray(noRecord);
        }
        else
            return null;
    }
    ;
    async findAllSorted(ord) {
        const noRecord = await this.linhaSchema.find({}).sort(ord);
        if (noRecord != null) {
            return LinhaMap_1.LinhaMap.toDomainArray(noRecord);
        }
        else
            return null;
    }
    ;
};
LinhaRepo = __decorate([
    typedi_1.Service(),
    __param(0, typedi_1.Inject('linhaSchema')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], LinhaRepo);
exports.default = LinhaRepo;
//# sourceMappingURL=linhaRepo.js.map