"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const typedi_1 = require("typedi");
const noId_1 = require("../domain/no/noId");
const NoMap_1 = require("../mappers/NoMap");
let NoRepo = class NoRepo {
    constructor(noSchema) {
        this.noSchema = noSchema;
    }
    createBaseQuery() {
        return {
            where: {},
        };
    }
    async exists(noId) {
        const idX = noId instanceof noId_1.NoId ? noId.id.toValue() : noId;
        const query = { string: idX };
        const noDocument = await this.noSchema.findOne(query);
        return !!noDocument === true;
    }
    async save(no) {
        const query = { domainId: no.id.toString() };
        const noDocument = await this.noSchema.findOne(query);
        try {
            if (noDocument === null) {
                const rawNo = NoMap_1.NoMap.toPersistence(no);
                const noCreated = await this.noSchema.create(rawNo);
                return NoMap_1.NoMap.toDomain(noCreated);
            }
            else {
                // noDocument.id = no.id;
                // noDocument.domainId = no.domainId;
                // noDocument.nome = no.nome;
                // noDocument.latitude = no.latitude;
                // noDocument.longitude = no.longitude;
                // noDocument.nomeCurto = no.nomeCurto;
                // noDocument.isEstacaoRecolha = no.isEstacaoRecolha;
                // noDocument.isPontoRendicao = no.isPontoRendicao;
                // noDocument.tempoViagem = no.isTempoViagem;
                //await noDocument.save();
                return no;
            }
        }
        catch (err) {
            throw err;
        }
    }
    async findByDomainId(noId) {
        const query = { domainId: noId };
        const noRecord = await this.noSchema.findOne(query);
        if (noRecord != null) {
            return NoMap_1.NoMap.toDomain(noRecord);
        }
        else
            return null;
    }
    async findAll() {
        const noRecord = await this.noSchema.find({});
        if (noRecord != null) {
            return NoMap_1.NoMap.toDomainArray(noRecord);
        }
        else
            return null;
    }
    ;
    async findAllSorted(ord) {
        const noRecord = await this.noSchema.find({}).sort(ord);
        if (noRecord != null) {
            return NoMap_1.NoMap.toDomainArray(noRecord);
        }
        else
            return null;
    }
    ;
};
NoRepo = __decorate([
    typedi_1.Service(),
    __param(0, typedi_1.Inject('noSchema')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], NoRepo);
exports.default = NoRepo;
//# sourceMappingURL=noRepo.js.map