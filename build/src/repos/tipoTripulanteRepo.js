"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const typedi_1 = require("typedi");
const tipoTripulanteId_1 = require("../domain/tipoTripulante/tipoTripulanteId");
const TipoTripulanteMap_1 = require("../mappers/TipoTripulanteMap");
let TipoTripulanteRepo = class TipoTripulanteRepo {
    constructor(tipoTripulanteSchema) {
        this.tipoTripulanteSchema = tipoTripulanteSchema;
    }
    createBaseQuery() {
        return {
            where: {},
        };
    }
    async exists(tipoTripulanteId) {
        const idX = tipoTripulanteId instanceof tipoTripulanteId_1.TipoTripulanteId ? tipoTripulanteId.id.toValue() : tipoTripulanteId;
        const query = { string: idX };
        const tipoTripulanteDocument = await this.tipoTripulanteSchema.findOne(query);
        return !!tipoTripulanteDocument === true;
    }
    async save(tipoTripulante) {
        const query = { domainId: tipoTripulante.id.toString() };
        const tipoTripulanteDocument = await this.tipoTripulanteSchema.findOne(query);
        try {
            if (tipoTripulanteDocument === null) {
                const rawTipoTripulante = TipoTripulanteMap_1.TipoTripulanteMap.toPersistence(tipoTripulante);
                const tipoTripulanteCreated = await this.tipoTripulanteSchema.create(rawTipoTripulante);
                return TipoTripulanteMap_1.TipoTripulanteMap.toDomain(tipoTripulanteCreated);
            }
            else {
                // //const newDocument = Object.assign({domainId: tipoTripulante.props.domainId, desc: tipoTripulante.desc}, tipoTripulanteDocument);
                // tipoTripulanteDocument.domainId = tipoTripulante.props.domainId;
                // tipoTripulanteDocument.desc = tipoTripulante.desc;
                // await tipoTripulanteDocument.save();
                return tipoTripulante;
            }
        }
        catch (err) {
            throw err;
        }
    }
    async findByDomainId(tipoTripulanteId) {
        const query = { domainId: tipoTripulanteId };
        const tipoTripulanteRecord = await this.tipoTripulanteSchema.findOne(query);
        if (tipoTripulanteRecord != null) {
            return TipoTripulanteMap_1.TipoTripulanteMap.toDomain(tipoTripulanteRecord);
        }
        else
            return null;
    }
    async findAll() {
        const tipoTripulanteRecord = await this.tipoTripulanteSchema.find({});
        if (tipoTripulanteRecord != null) {
            return TipoTripulanteMap_1.TipoTripulanteMap.toDomainArray(tipoTripulanteRecord);
        }
        else
            return null;
    }
    ;
    async findAllSorted(ord) {
        const tipoTripulanteRecord = await this.tipoTripulanteSchema.find({}).sort(ord);
        if (tipoTripulanteRecord != null) {
            return TipoTripulanteMap_1.TipoTripulanteMap.toDomainArray(tipoTripulanteRecord);
        }
        else
            return null;
    }
    ;
};
TipoTripulanteRepo = __decorate([
    typedi_1.Service(),
    __param(0, typedi_1.Inject('tipoTripulanteSchema')),
    __metadata("design:paramtypes", [mongoose_1.Model])
], TipoTripulanteRepo);
exports.default = TipoTripulanteRepo;
//# sourceMappingURL=tipoTripulanteRepo.js.map