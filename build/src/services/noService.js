"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../config.js"));
const Result_1 = require("../core/logic/Result");
const NoMap_1 = require("../mappers/NoMap");
const no_1 = require("../domain/no/no");
let NoService = class NoService {
    constructor(noRepo) {
        this.noRepo = noRepo;
    }
    async createNo(noDTO) {
        try {
            const noOrError = await no_1.No.create(noDTO);
            if (noOrError.isFailure) {
                return Result_1.Result.fail(noOrError.errorValue());
            }
            const noRes = noOrError.getValue();
            await this.noRepo.save(noRes);
            const noDTOResult = NoMap_1.NoMap.toDTO(noRes);
            return Result_1.Result.ok(noDTOResult);
        }
        catch (e) {
            throw e;
        }
    }
    async updateNo(noDTO) {
        try {
            const no = await this.noRepo.findByDomainId(noDTO.domainId);
            if (no === null) {
                return Result_1.Result.fail("Nó não encontrado");
            }
            else {
                no.nome = noDTO.nome;
                no.latitude = noDTO.latitude;
                no.longitude = noDTO.longitude;
                no.nomeCurto = noDTO.nomeCurto;
                no.isEstacaoRecolha = noDTO.isEstacaoRecolha;
                no.isPontoRendicao = noDTO.isPontoRendicao;
                no.isTempoViagem = noDTO.tempoViagem;
                await this.noRepo.save(no);
                const noDTOResult = NoMap_1.NoMap.toDTO(no);
                return Result_1.Result.ok(noDTOResult);
            }
        }
        catch (e) {
            throw e;
        }
    }
    async getNoById(id) {
        try {
            const no = await this.noRepo.findByDomainId(id);
            const noDTOResult = NoMap_1.NoMap.toDTO(no);
            return Result_1.Result.ok(noDTOResult);
        }
        catch (e) {
            throw e;
        }
    }
    async listNos() {
        try {
            const no = await this.noRepo.findAll();
            var nos = new Array(no.length);
            for (var i = 0; i < no.length; i++) {
                const noOrError = NoMap_1.NoMap.toDTO(no[i]);
                nos[i] = noOrError;
            }
            return Result_1.Result.ok(nos);
        }
        catch (e) {
            throw e;
        }
    }
    async listNosSorted(ord) {
        try {
            const no = await this.noRepo.findAllSorted(ord);
            var nos = new Array(no.length);
            for (var i = 0; i < no.length; i++) {
                const noOrError = NoMap_1.NoMap.toDTO(no[i]);
                nos[i] = noOrError;
            }
            return Result_1.Result.ok(nos);
        }
        catch (e) {
            throw e;
        }
    }
};
NoService = __decorate([
    typedi_1.Service(),
    __param(0, typedi_1.Inject(config_js_1.default.repos.no.name)),
    __metadata("design:paramtypes", [Object])
], NoService);
exports.default = NoService;
//# sourceMappingURL=noService.js.map