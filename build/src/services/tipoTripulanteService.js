"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../config.js"));
const Result_1 = require("../core/logic/Result");
const TipoTripulanteMap_1 = require("../mappers/TipoTripulanteMap");
const tipoTripulante_1 = require("../domain/tipoTripulante/tipoTripulante");
let TipoTripulanteService = class TipoTripulanteService {
    constructor(tipoTripulanteRepo) {
        this.tipoTripulanteRepo = tipoTripulanteRepo;
    }
    async createTipoTripulante(tipoTripulanteDTO) {
        try {
            const tipoTripulanteOrError = await tipoTripulante_1.TipoTripulante.create(tipoTripulanteDTO);
            if (tipoTripulanteOrError.isFailure) {
                return Result_1.Result.fail(tipoTripulanteOrError.errorValue());
            }
            const tipoTripulanteRes = tipoTripulanteOrError.getValue();
            await this.tipoTripulanteRepo.save(tipoTripulanteRes);
            const tipoTripulanteDTOResult = TipoTripulanteMap_1.TipoTripulanteMap.toDTO(tipoTripulanteRes);
            return Result_1.Result.ok(tipoTripulanteDTOResult);
        }
        catch (e) {
            throw e;
        }
    }
    async updateTipoTripulante(tipoTripulanteDTO) {
        try {
            const tipo = await this.tipoTripulanteRepo.findByDomainId(tipoTripulanteDTO.domainId);
            if (tipo === null) {
                return Result_1.Result.fail("Tipo de Tripulante não encontrado");
            }
            else {
                tipo.desc = tipoTripulanteDTO.desc;
                await this.tipoTripulanteRepo.save(tipo);
                const tipoDTOResult = TipoTripulanteMap_1.TipoTripulanteMap.toDTO(tipo);
                return Result_1.Result.ok(tipoDTOResult);
            }
        }
        catch (e) {
            throw e;
        }
    }
    async getTipoTripulanteById(id) {
        try {
            const tipoTripulanteRes = await this.tipoTripulanteRepo.findByDomainId(id);
            const tipoTripulanteDTOResult = TipoTripulanteMap_1.TipoTripulanteMap.toDTO(tipoTripulanteRes);
            return Result_1.Result.ok(tipoTripulanteDTOResult);
        }
        catch (e) {
            throw e;
        }
    }
    async listTiposTripulante() {
        try {
            const per = await this.tipoTripulanteRepo.findAll();
            var tipos = new Array(per.length);
            for (var i = 0; i < per.length; i++) {
                const tipoOrError = TipoTripulanteMap_1.TipoTripulanteMap.toDTO(per[i]);
                tipos[i] = tipoOrError;
            }
            return Result_1.Result.ok(tipos);
        }
        catch (e) {
            throw e;
        }
    }
    async listTiposTripulanteSorted(ord) {
        try {
            const per = await this.tipoTripulanteRepo.findAllSorted(ord);
            var tipos = new Array(per.length);
            for (var i = 0; i < per.length; i++) {
                const tipoOrError = TipoTripulanteMap_1.TipoTripulanteMap.toDTO(per[i]);
                tipos[i] = tipoOrError;
            }
            return Result_1.Result.ok(tipos);
        }
        catch (e) {
            throw e;
        }
    }
};
TipoTripulanteService = __decorate([
    typedi_1.Service(),
    __param(0, typedi_1.Inject(config_js_1.default.repos.tipoTripulante.name)),
    __metadata("design:paramtypes", [Object])
], TipoTripulanteService);
exports.default = TipoTripulanteService;
//# sourceMappingURL=tipoTripulanteService.js.map