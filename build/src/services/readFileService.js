"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const tipoViatura_1 = require("../domain/tipoViatura/tipoViatura");
const tipoViaturaRepo_1 = __importDefault(require("../repos/tipoViaturaRepo"));
const linha_1 = require("../domain/linha/linha");
const linhaRepo_1 = __importDefault(require("../repos/linhaRepo"));
const percurso_1 = require("../domain/percurso/percurso");
const percursoRepo_1 = __importDefault(require("../repos/percursoRepo"));
const noRepo_1 = __importDefault(require("../repos/noRepo"));
const no_1 = require("../domain/no/no");
const xml2js = require('xml2js');
let readFileService = class readFileService {
    constructor() {
    }
    async readFile(data) {
        try {
            xml2js.parseString(data, async function (err, result) {
                if (err) {
                    throw err;
                }
                else {
                    const tipoViaturaRepo = typedi_1.Container.get(tipoViaturaRepo_1.default);
                    const linhasRepo = typedi_1.Container.get(linhaRepo_1.default);
                    const percursoRepo = typedi_1.Container.get(percursoRepo_1.default);
                    const nosRepo = typedi_1.Container.get(noRepo_1.default);
                    //Para Tipos de Viatura
                    const vehicleTypes = result.GlDocumentInfo.world[0].GlDocument[0].GlDocumentNetwork[0].Network[0].VehicleTypes[0].VehicleType;
                    for (let i = 0; i < vehicleTypes.length; i++) {
                        const atual = vehicleTypes[i];
                        const newViatura = {
                            domainId: atual.$.key,
                            nome: atual.$.Name,
                            autonomia: atual.$.Autonomy,
                            custo: atual.$.Cost,
                            velocidadeMedia: atual.$.AverageSpeed,
                            combustivel: atual.$.EnergySource,
                            consumo: atual.$.Consumption,
                            emissoes: atual.$.Emissions,
                        };
                        const tipoViaturaOrError = tipoViatura_1.TipoViatura.create(newViatura);
                        tipoViaturaRepo.save(tipoViaturaOrError.getValue());
                    }
                    //Para Linhas
                    const linhas = result.GlDocumentInfo.world[0].GlDocument[0].GlDocumentNetwork[0].Network[0].Lines[0].Line;
                    for (let i = 0; i < linhas.length; i++) {
                        const atual = linhas[i];
                        const caminhosLinha = atual.LinePaths[0].LinePath;
                        let arrayLista = new Array();
                        for (let j = 0; j < caminhosLinha.length; j++) {
                            const atualCaminho = caminhosLinha[j];
                            const newLinha = {
                                value: {
                                    percursoLinhaId: atualCaminho.$.key,
                                    percurso: atualCaminho.$.Path,
                                    orientation: atualCaminho.$.Orientation
                                }
                            };
                            arrayLista[j] = newLinha;
                        }
                        const newLinha = {
                            domainId: atual.$.key,
                            nome: atual.$.Name,
                            cor: atual.$.Color,
                            percursoLinha: arrayLista
                        };
                        const newLinhaOrError = linha_1.Linha.create(newLinha);
                        linhasRepo.save(newLinhaOrError.getValue());
                    }
                    //Para percursos
                    const percursos = result.GlDocumentInfo.world[0].GlDocument[0].GlDocumentNetwork[0].Network[0].Paths[0].Path;
                    for (let i = 0; i < percursos.length; i++) {
                        const atual = percursos[i];
                        const percursosNos = atual.PathNodes[0].PathNode;
                        let arrayNos = new Array();
                        for (let j = 0; j < percursosNos.length; j++) {
                            const atualNo = percursosNos[j];
                            const newNoPercurso = {
                                value: {
                                    noId: atualNo.$.Node,
                                    duracao: atualNo.$.Duration,
                                    distancia: atualNo.$.Distance
                                }
                            };
                            arrayNos[j] = newNoPercurso;
                        }
                        const newPath = {
                            domainId: atual.$.key,
                            nosPercurso: arrayNos
                        };
                        const newPercursoOrError = percurso_1.Percurso.create(newPath);
                        percursoRepo.save(newPercursoOrError.getValue());
                    }
                    //Para Nós
                    const nos = result.GlDocumentInfo.world[0].GlDocument[0].GlDocumentNetwork[0].Network[0].Nodes[0].Node;
                    for (let i = 0; i < nos.length; i++) {
                        const atual = nos[i];
                        const tViagemGrupo = atual.CrewTravelTimes[0].CrewTravelTime;
                        let arrayViagem = new Array();
                        if (tViagemGrupo != undefined) {
                            for (let j = 0; j < tViagemGrupo.length; j++) {
                                const atualCaminho = tViagemGrupo[j];
                                const newTempo = {
                                    value: {
                                        noId: atualCaminho.$.Node,
                                        duracao: atualCaminho.$.Duration,
                                    }
                                };
                                arrayViagem[j] = newTempo;
                            }
                        }
                        var isDepot;
                        if (atual.$.IsDepot == "False") {
                            isDepot = false;
                        }
                        else {
                            isDepot = true;
                        }
                        var isRelief;
                        if (atual.$.IsReliefPoint == "False") {
                            isRelief = false;
                        }
                        else {
                            isRelief = true;
                        }
                        const newNo = {
                            domainId: atual.$.key,
                            nome: atual.$.Name,
                            latitude: atual.$.Latitude,
                            longitude: atual.$.Longitude,
                            nomeCurto: atual.$.ShortName,
                            isEstacaoRecolha: isDepot,
                            isPontoRendicao: isRelief,
                            tempoViagem: arrayViagem
                        };
                        const newNoOrError = no_1.No.create(newNo);
                        nosRepo.save(newNoOrError.getValue());
                    }
                }
            });
            return true;
        }
        catch (e) {
            throw e;
        }
    }
};
readFileService = __decorate([
    typedi_1.Service("readFileService"),
    __metadata("design:paramtypes", [])
], readFileService);
exports.default = readFileService;
//# sourceMappingURL=readFileService.js.map