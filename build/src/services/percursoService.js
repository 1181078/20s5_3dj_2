"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../config.js"));
const Result_1 = require("../core/logic/Result");
const PercursoMap_1 = require("../mappers/PercursoMap");
const percurso_1 = require("../domain/percurso/percurso");
let PercursoService = class PercursoService {
    constructor(percursoRepo) {
        this.percursoRepo = percursoRepo;
    }
    async createPercurso(percursoDTO) {
        try {
            const percursoOrError = await percurso_1.Percurso.create(percursoDTO);
            if (percursoOrError.isFailure) {
                return Result_1.Result.fail(percursoOrError.errorValue());
            }
            const percursoRes = percursoOrError.getValue();
            await this.percursoRepo.save(percursoRes);
            const percursoDTOResult = PercursoMap_1.PercursoMap.toDTO(percursoRes);
            return Result_1.Result.ok(percursoDTOResult);
        }
        catch (e) {
            throw e;
        }
    }
    async updatePercurso(percursoDTO) {
        try {
            const per = await this.percursoRepo.findByDomainId(percursoDTO.domainId);
            if (per === null) {
                return Result_1.Result.fail("Percurso não encontrado");
            }
            else {
                per.nosPercurso = percursoDTO.nosPercurso;
                await this.percursoRepo.save(per);
                const perDTOResult = PercursoMap_1.PercursoMap.toDTO(per);
                return Result_1.Result.ok(perDTOResult);
            }
        }
        catch (e) {
            throw e;
        }
    }
    async getPercursoById(id) {
        const per = await this.percursoRepo.findByDomainId(id);
        try {
            const per = await this.percursoRepo.findByDomainId(id);
            const perDTOResult = PercursoMap_1.PercursoMap.toDTO(per);
            return Result_1.Result.ok(perDTOResult);
        }
        catch (e) {
            throw e;
        }
    }
    async listPercursos() {
        try {
            const per = await this.percursoRepo.findAll();
            var pers = new Array(per.length);
            for (var i = 0; i < per.length; i++) {
                const perOrError = PercursoMap_1.PercursoMap.toDTO(per[i]);
                pers[i] = perOrError;
            }
            return Result_1.Result.ok(pers);
        }
        catch (e) {
            throw e;
        }
    }
};
PercursoService = __decorate([
    typedi_1.Service(),
    __param(0, typedi_1.Inject(config_js_1.default.repos.percurso.name)),
    __metadata("design:paramtypes", [Object])
], PercursoService);
exports.default = PercursoService;
//# sourceMappingURL=percursoService.js.map