"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../config.js"));
const Result_1 = require("../core/logic/Result");
const TipoViaturaMap_1 = require("../mappers/TipoViaturaMap");
const tipoViatura_1 = require("../domain/tipoViatura/tipoViatura");
let TipoViaturaService = class TipoViaturaService {
    constructor(tipoViaturaRepo) {
        this.tipoViaturaRepo = tipoViaturaRepo;
    }
    async createTipoViatura(tipoViaturaDTO) {
        try {
            const tipoViaturaOrError = await tipoViatura_1.TipoViatura.create(tipoViaturaDTO);
            if (tipoViaturaOrError.isFailure) {
                return Result_1.Result.fail(tipoViaturaOrError.errorValue());
            }
            const tipoViaturaRes = tipoViaturaOrError.getValue();
            await this.tipoViaturaRepo.save(tipoViaturaRes);
            const tipoViaturaDTOResult = TipoViaturaMap_1.TipoViaturaMap.toDTO(tipoViaturaRes);
            return Result_1.Result.ok(tipoViaturaDTOResult);
        }
        catch (e) {
            throw e;
        }
    }
    async updateTipoViatura(tipoViaturaDTO) {
        try {
            const tipo = await this.tipoViaturaRepo.findByDomainId(tipoViaturaDTO.domainId);
            if (tipo === null) {
                return Result_1.Result.fail("Tipo de Viatura não encontrado");
            }
            else {
                tipo.nome = tipoViaturaDTO.nome;
                tipo.autonomia = tipoViaturaDTO.autonomia;
                tipo.custo = tipoViaturaDTO.custo;
                tipo.velocidadeMedia = tipoViaturaDTO.velocidadeMedia;
                tipo.combustivel = tipoViaturaDTO.combustivel;
                tipo.consumo = tipoViaturaDTO.consumo;
                tipo.emissoes = tipoViaturaDTO.emissoes;
                await this.tipoViaturaRepo.save(tipo);
                const tipoDTOResult = TipoViaturaMap_1.TipoViaturaMap.toDTO(tipo);
                return Result_1.Result.ok(tipoDTOResult);
            }
        }
        catch (e) {
            throw e;
        }
    }
    async getTipoViaturaById(id) {
        try {
            const tipoViaturaRes = await this.tipoViaturaRepo.findByDomainId(id);
            const tipoViaturaDTOResult = TipoViaturaMap_1.TipoViaturaMap.toDTO(tipoViaturaRes);
            return Result_1.Result.ok(tipoViaturaDTOResult);
        }
        catch (e) {
            throw e;
        }
    }
    async listTiposViatura() {
        try {
            const per = await this.tipoViaturaRepo.findAll();
            var tipos = new Array(per.length);
            for (var i = 0; i < per.length; i++) {
                const tipoOrError = TipoViaturaMap_1.TipoViaturaMap.toDTO(per[i]);
                tipos[i] = tipoOrError;
            }
            return Result_1.Result.ok(tipos);
        }
        catch (e) {
            throw e;
        }
    }
    async listTiposViaturaSorted(ord) {
        try {
            const per = await this.tipoViaturaRepo.findAllSorted(ord);
            var tipos = new Array(per.length);
            for (var i = 0; i < per.length; i++) {
                const tipoOrError = TipoViaturaMap_1.TipoViaturaMap.toDTO(per[i]);
                tipos[i] = tipoOrError;
            }
            return Result_1.Result.ok(tipos);
        }
        catch (e) {
            throw e;
        }
    }
};
TipoViaturaService = __decorate([
    typedi_1.Service(),
    __param(0, typedi_1.Inject(config_js_1.default.repos.tipoViatura.name)),
    __metadata("design:paramtypes", [Object])
], TipoViaturaService);
exports.default = TipoViaturaService;
//# sourceMappingURL=tipoViaturaService.js.map