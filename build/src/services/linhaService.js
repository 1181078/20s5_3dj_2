"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../config.js"));
const Result_1 = require("../core/logic/Result");
const LinhaMap_1 = require("../mappers/LinhaMap");
const linha_1 = require("../domain/linha/linha");
let LinhaService = class LinhaService {
    constructor(linhaRepo) {
        this.linhaRepo = linhaRepo;
    }
    async createLinha(linhaDTO) {
        try {
            const linhaOrError = await linha_1.Linha.create(linhaDTO);
            if (linhaOrError.isFailure) {
                return Result_1.Result.fail(linhaOrError.errorValue());
            }
            const percursoRes = linhaOrError.getValue();
            await this.linhaRepo.save(percursoRes);
            const linhaDTOResult = LinhaMap_1.LinhaMap.toDTO(percursoRes);
            return Result_1.Result.ok(linhaDTOResult);
        }
        catch (e) {
            throw e;
        }
    }
    async updateLinha(linhaDTO) {
        try {
            const per = await this.linhaRepo.findByDomainId(linhaDTO.domainId);
            if (per === null) {
                return Result_1.Result.fail("Linha não encontrada");
            }
            else {
                per.percursoLinha = linhaDTO.percursoLinha;
                await this.linhaRepo.save(per);
                const perDTOResult = LinhaMap_1.LinhaMap.toDTO(per);
                return Result_1.Result.ok(perDTOResult);
            }
        }
        catch (e) {
            throw e;
        }
    }
    async getLinhaById(id) {
        try {
            const linha = await this.linhaRepo.findByDomainId(id);
            const linhaDTOResult = LinhaMap_1.LinhaMap.toDTO(linha);
            return Result_1.Result.ok(linhaDTOResult);
        }
        catch (e) {
            throw e;
        }
    }
    async listLinhas() {
        try {
            const linha = await this.linhaRepo.findAll();
            var linhas = new Array(linha.length);
            for (var i = 0; i < linha.length; i++) {
                const noOrError = LinhaMap_1.LinhaMap.toDTO(linha[i]);
                linhas[i] = noOrError;
            }
            return Result_1.Result.ok(linhas);
        }
        catch (e) {
            throw e;
        }
    }
    async listLinhasSorted(ord) {
        try {
            const linha = await this.linhaRepo.findAllSorted(ord);
            var linhas = new Array(linha.length);
            for (var i = 0; i < linha.length; i++) {
                const noOrError = LinhaMap_1.LinhaMap.toDTO(linha[i]);
                linhas[i] = noOrError;
            }
            return Result_1.Result.ok(linhas);
        }
        catch (e) {
            throw e;
        }
    }
};
LinhaService = __decorate([
    typedi_1.Service(),
    __param(0, typedi_1.Inject(config_js_1.default.repos.linha.name)),
    __metadata("design:paramtypes", [Object])
], LinhaService);
exports.default = LinhaService;
//# sourceMappingURL=linhaService.js.map