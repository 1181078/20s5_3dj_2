"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const PercursoSchema = new mongoose_1.default.Schema({
    domainId: { type: String, unique: true },
    nosPercurso: { type: Array },
}, {
    timestamps: true
});
exports.default = mongoose_1.default.model('Percurso', PercursoSchema);
//# sourceMappingURL=percursoSchema.js.map