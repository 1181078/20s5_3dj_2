"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const TipoTripulanteSchema = new mongoose_1.default.Schema({
    domainId: { type: String, unique: true },
    desc: { type: String, unique: false }
}, {
    timestamps: true
});
exports.default = mongoose_1.default.model('TipoTripulante', TipoTripulanteSchema);
//# sourceMappingURL=tipoTripulanteSchema.js.map