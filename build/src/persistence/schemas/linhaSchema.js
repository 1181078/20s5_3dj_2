"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const LinhaSchema = new mongoose_1.default.Schema({
    domainId: { type: String, unique: true },
    nome: { type: String, unique: true },
    cor: { type: String },
    percursoLinha: { type: Array },
}, {
    timestamps: true
});
exports.default = mongoose_1.default.model('Linha', LinhaSchema);
//# sourceMappingURL=linhaSchema.js.map