"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const TipoViaturaSchema = new mongoose_1.default.Schema({
    domainId: { type: String, unique: true },
    nome: { type: String, unique: true },
    autonomia: { type: Number },
    custo: { type: Number },
    velocidadeMedia: { type: Number },
    combustivel: { type: Number },
    consumo: { type: Number },
    emissoes: { type: Number }
}, {
    timestamps: true
});
exports.default = mongoose_1.default.model('TipoViatura', TipoViaturaSchema);
//# sourceMappingURL=tipoViaturaSchema.js.map