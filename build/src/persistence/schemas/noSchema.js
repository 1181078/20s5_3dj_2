"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const NoSchema = new mongoose_1.default.Schema({
    domainId: { type: String, unique: true },
    nome: { type: String, unique: true },
    latitude: { type: Number, unique: false },
    longitude: { type: Number, unique: false },
    nomeCurto: { type: String, unique: true },
    isEstacaoRecolha: { type: Boolean },
    isPontoRendicao: { type: Boolean },
    tempoViagem: { type: Array }
}, {
    timestamps: true
});
exports.default = mongoose_1.default.model('No', NoSchema);
//# sourceMappingURL=noSchema.js.map