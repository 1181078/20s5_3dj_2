"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../config.js"));
let TipoTripulanteController = class TipoTripulanteController {
    constructor(tipoTripulanteServiceInstance) {
        this.tipoTripulanteServiceInstance = tipoTripulanteServiceInstance;
    }
    async createTipoTripulante(req, res, next) {
        try {
            const tipoOrError = await this.tipoTripulanteServiceInstance.createTipoTripulante(req.body);
            if (tipoOrError.isFailure) {
                return res.status(400).send();
            }
            const tipoDTO = tipoOrError.getValue();
            return res.status(201).json(tipoDTO).send();
        }
        catch (err) {
            return next(err);
        }
    }
    async updateTipoTripulante(req, res, next) {
        try {
            const tipoOrError = await this.tipoTripulanteServiceInstance.updateTipoTripulante(req.body);
            if (tipoOrError.isFailure) {
                return res.status(404).send();
            }
            const tipoDTO = tipoOrError.getValue();
            return res.json(tipoDTO).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
    ;
    async getTipoTripulanteById(req, res, next) {
        try {
            const tipoOrError = await this.tipoTripulanteServiceInstance.getTipoTripulanteById(req.params.id);
            if (tipoOrError.isFailure) {
                return res.status(404).send();
            }
            const tipoDTO = tipoOrError.getValue();
            return res.json({ domainId: tipoDTO.domainId, descricao: tipoDTO.desc }).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
    ;
    async listTiposTripulante(req, res, next) {
        try {
            const tipoList = await this.tipoTripulanteServiceInstance.listTiposTripulante();
            if (tipoList.isFailure) {
                return res.status(404).send();
            }
            const tipoDTOList = tipoList.getValue();
            return res.json(tipoDTOList).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
    async listTiposTripulanteSorted(req, res, next) {
        try {
            const tipoList = await this.tipoTripulanteServiceInstance.listTiposTripulanteSorted(req.params.ord);
            if (tipoList.isFailure) {
                return res.status(404).send();
            }
            const tipoDTOList = tipoList.getValue();
            return res.json(tipoDTOList).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
};
TipoTripulanteController = __decorate([
    __param(0, typedi_1.Inject(config_js_1.default.services.tipoTripulante.name)),
    __metadata("design:paramtypes", [Object])
], TipoTripulanteController);
exports.default = TipoTripulanteController;
//# sourceMappingURL=tipoTripulanteController.js.map