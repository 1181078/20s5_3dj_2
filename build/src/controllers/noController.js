"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../config.js"));
let NoController = class NoController {
    constructor(noServiceInstance) {
        this.noServiceInstance = noServiceInstance;
    }
    async createNo(req, res, next) {
        try {
            const noOrError = await this.noServiceInstance.createNo(req.body);
            if (noOrError.isFailure) {
                return res.status(400).send();
            }
            const noDTO = noOrError.getValue();
            return res.status(201).json(noDTO).send();
        }
        catch (err) {
            return next(err);
        }
    }
    async updateNo(req, res, next) {
        try {
            const noOrError = await this.noServiceInstance.updateNo(req.body);
            if (noOrError.isFailure) {
                return res.status(404).send();
            }
            const tipoDTO = noOrError.getValue();
            return res.json(tipoDTO).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
    async getNoById(req, res, next) {
        try {
            const noOrError = await this.noServiceInstance.getNoById(req.params.id);
            if (noOrError.isFailure) {
                return res.status(404).send();
            }
            const noDTO = noOrError.getValue();
            return res.json(noDTO).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
    ;
    async listNos(req, res, next) {
        try {
            const noList = await this.noServiceInstance.listNos();
            if (noList.isFailure) {
                return res.status(404).send();
            }
            const noDTOList = noList.getValue();
            return res.json(noDTOList).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
    async listNosSorted(req, res, next) {
        try {
            const noList = await this.noServiceInstance.listNosSorted(req.params.ord);
            if (noList.isFailure) {
                return res.status(404).send();
            }
            const noDTOList = noList.getValue();
            return res.json(noDTOList).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
};
NoController = __decorate([
    __param(0, typedi_1.Inject(config_js_1.default.services.no.name)),
    __metadata("design:paramtypes", [Object])
], NoController);
exports.default = NoController;
//# sourceMappingURL=noController.js.map