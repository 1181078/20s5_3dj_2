"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../config.js"));
let LinhaController = class LinhaController {
    constructor(linhaServiceInstance) {
        this.linhaServiceInstance = linhaServiceInstance;
    }
    async createLinha(req, res, next) {
        try {
            const linhaOrError = await this.linhaServiceInstance.createLinha(req.body);
            if (linhaOrError.isFailure) {
                return res.status(400).send();
            }
            const tipoDTO = linhaOrError.getValue();
            return res.status(201).json(tipoDTO).send();
        }
        catch (err) {
            return next(err);
        }
    }
    async updateLinha(req, res, next) {
        try {
            const linhaOrError = await this.linhaServiceInstance.updateLinha(req.body);
            if (linhaOrError.isFailure) {
                return res.status(404).send();
            }
            const tipoDTO = linhaOrError.getValue();
            return res.json(tipoDTO).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
    ;
    async getLinhaById(req, res, next) {
        try {
            const noOrError = await this.linhaServiceInstance.getLinhaById(req.params.id);
            if (noOrError.isFailure) {
                return res.status(404).send();
            }
            const linhaDTO = noOrError.getValue();
            return res.json(linhaDTO).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
    ;
    async listLinhas(req, res, next) {
        try {
            const linhaList = await this.linhaServiceInstance.listLinhas();
            if (linhaList.isFailure) {
                return res.status(404).send();
            }
            const linhaDTOList = linhaList.getValue();
            return res.json(linhaDTOList).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
    async listLinhasSorted(req, res, next) {
        try {
            const linhaList = await this.linhaServiceInstance.listLinhasSorted(req.params.ord);
            if (linhaList.isFailure) {
                return res.status(404).send();
            }
            const linhaDTOList = linhaList.getValue();
            return res.json(linhaDTOList).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
};
LinhaController = __decorate([
    __param(0, typedi_1.Inject(config_js_1.default.services.linha.name)),
    __metadata("design:paramtypes", [Object])
], LinhaController);
exports.default = LinhaController;
//# sourceMappingURL=linhaController.js.map