"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../config.js"));
const readFileService_1 = __importDefault(require("../services/readFileService"));
const fs = require('fs');
const formidable = require('formidable');
let readFileController = class readFileController {
    constructor(readFileServiceInstance) {
        this.readFileServiceInstance = readFileServiceInstance;
    }
    async readFile(req, res, next) {
        try {
            let form = new formidable.IncomingForm();
            let awaitImport = this.awaitImport;
            let data;
            form.parse(req, function (error, fields, files) {
                if (error) {
                    throw error;
                }
                fs.readFile(files.lapr5file.path, function (error, data) {
                    if (error) {
                        throw error;
                    }
                    awaitImport(data);
                });
            });
            return res.status(201).json("Sucesso!").send();
        }
        catch (err) {
            return next(err);
        }
    }
    async awaitImport(data) {
        const readFileServiceInstance = typedi_1.Container.get(readFileService_1.default);
        const result = await readFileServiceInstance.readFile(data);
        return result;
    }
};
readFileController = __decorate([
    __param(0, typedi_1.Inject(config_js_1.default.services.readFile.name)),
    __metadata("design:paramtypes", [Object])
], readFileController);
exports.default = readFileController;
//# sourceMappingURL=readFileController.js.map