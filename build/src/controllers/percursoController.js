"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../config.js"));
let PercursoController = class PercursoController {
    constructor(percursoServiceInstance) {
        this.percursoServiceInstance = percursoServiceInstance;
    }
    async createPercurso(req, res, next) {
        try {
            const percursoOrError = await this.percursoServiceInstance.createPercurso(req.body);
            if (percursoOrError.isFailure) {
                return res.status(400).send();
            }
            const tipoDTO = percursoOrError.getValue();
            return res.status(201).json(tipoDTO).send();
        }
        catch (err) {
            return next(err);
        }
    }
    async updatePercurso(req, res, next) {
        try {
            const percursoOrError = await this.percursoServiceInstance.updatePercurso(req.body);
            if (percursoOrError.isFailure) {
                return res.status(404).send();
            }
            const tipoDTO = percursoOrError.getValue();
            return res.json(tipoDTO).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
    ;
    async getPercursoById(req, res, next) {
        try {
            const percursoOrError = await this.percursoServiceInstance.getPercursoById(req.params.id);
            if (percursoOrError.isFailure) {
                return res.status(404).send();
            }
            const percursoDTO = percursoOrError.getValue();
            return res.json(percursoDTO).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
    ;
    async listPercursos(req, res, next) {
        try {
            const percursoList = await this.percursoServiceInstance.listPercursos();
            if (percursoList.isFailure) {
                return res.status(404).send();
            }
            const percursoDTOList = percursoList.getValue();
            return res.json(percursoDTOList).status(200);
        }
        catch (e) {
            return next(e);
        }
    }
};
PercursoController = __decorate([
    __param(0, typedi_1.Inject(config_js_1.default.services.percurso.name)),
    __metadata("design:paramtypes", [Object])
], PercursoController);
exports.default = PercursoController;
//# sourceMappingURL=percursoController.js.map