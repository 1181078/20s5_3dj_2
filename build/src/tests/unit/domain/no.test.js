"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const no_1 = require("../../../domain/no/no");
const tempoViagemGrupo_1 = require("../../../domain/no/tempoViagemGrupo");
const UniqueEntityID_1 = require("../../../core/domain/UniqueEntityID");
describe("Tipo de Tripulante Tests", () => {
    it("Validate Percurso values", () => {
        const id = new UniqueEntityID_1.UniqueEntityID(1);
        var tempoViagemm = tempoViagemGrupo_1.TempoViagemGrupo.create({
            noId: "noId1",
            duracao: 4,
        }).getValue();
        const goodNo = no_1.No.create({
            domainId: "id1",
            nome: "nome",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: true,
            isPontoRendicao: false,
            tempoViagem: [tempoViagemm]
        }, id);
        const badNo = no_1.No.create({
            domainId: "id1",
            nome: "",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: true,
            isPontoRendicao: false,
            tempoViagem: [tempoViagemm]
        }, id);
        const badNo2 = no_1.No.create({
            domainId: "id1",
            nome: "as",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: null,
            isPontoRendicao: false,
            tempoViagem: [tempoViagemm]
        }, id);
        const badNo3 = no_1.No.create({
            domainId: "id1",
            nome: "as",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: true,
            isPontoRendicao: null,
            tempoViagem: [tempoViagemm]
        }, id);
        const badNo4 = no_1.No.create({
            domainId: "id1",
            nome: "as",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: null,
            isPontoRendicao: null,
            tempoViagem: [tempoViagemm]
        }, id);
        chai_1.expect(goodNo.isSuccess).to.equal(true);
        chai_1.expect(badNo.isFailure).to.equal(true);
        chai_1.expect(badNo2.isFailure).to.equal(true);
        chai_1.expect(badNo3.isFailure).to.equal(true);
        chai_1.expect(badNo4.isFailure).to.equal(true);
        var no1 = no_1.No.create({
            domainId: "id1",
            nome: "nome",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: true,
            isPontoRendicao: false,
            tempoViagem: [tempoViagemm]
        }, id).getValue();
        //testar get domainID
        chai_1.expect(no1.props.domainId).equal("id1");
        //testar get nome
        chai_1.expect(no1.nome).equal("nome");
        //testar get latitude
        chai_1.expect(no1.latitude).equal(40);
        //testar get longitude
        chai_1.expect(no1.longitude).equal(23);
        //testar get nomeCurto
        chai_1.expect(no1.nomeCurto).equal("Pedro");
        //testar get isEstacaoRecolha
        chai_1.expect(no1.isEstacaoRecolha).equal(true);
        //testar get isPontoRendicao
        chai_1.expect(no1.isPontoRendicao).equal(false);
        //testar get isTempoViagem
        sinon.assert.match(no1.isTempoViagem, [tempoViagemm]);
        var no2 = no_1.No.create({
            domainId: "id1",
            nome: "nome",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: true,
            isPontoRendicao: false,
            tempoViagem: [tempoViagemm]
        }, id).getValue();
        var tempoViagemm2 = tempoViagemGrupo_1.TempoViagemGrupo.create({
            noId: "noId1",
            duracao: 4,
        }).getValue();
        //testar set nome
        no2.nome = "fromage";
        chai_1.expect(no2.nome).equal("fromage");
        //testar set latitude
        no2.latitude = 21;
        chai_1.expect(no2.latitude).equal(21);
        //testar set longitude
        no2.longitude = 11;
        chai_1.expect(no2.longitude).equal(11);
        //testar set nomeCurto
        no2.nomeCurto = "curto";
        chai_1.expect(no2.nomeCurto).equal("curto");
        //testar set isEstacaoRecolha
        no2.isEstacaoRecolha = false;
        chai_1.expect(no2.isEstacaoRecolha).equal(false);
        //testar set isPontoRendicao
        no2.isEstacaoRecolha = true;
        chai_1.expect(no2.isEstacaoRecolha).equal(true);
        //testar set isTempoViagem
        no2.isTempoViagem = [tempoViagemm2];
        sinon.assert.match(no2.isTempoViagem, [tempoViagemm2]);
    });
});
//# sourceMappingURL=no.test.js.map