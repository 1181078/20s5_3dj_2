"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const percurso_1 = require("../../../domain/percurso/percurso");
const nosPercurso_1 = require("../../../domain/percurso/nosPercurso");
const UniqueEntityID_1 = require("../../../core/domain/UniqueEntityID");
describe("Percurso Tests", () => {
    it("Validate Percurso values", () => {
        const id = new UniqueEntityID_1.UniqueEntityID();
        const nosPercursos = nosPercurso_1.NosPercurso.create({
            noId: "100",
            duracao: 100,
            distancia: 900
        }).getValue();
        const nosPercursosxyz = nosPercurso_1.NosPercurso.create({
            noId: "100",
            duracao: 100,
            distancia: 900
        }).getValue();
        let body = { "domainId": "1", "nosPercurso": [nosPercursos] };
        const goodPercurso = percurso_1.Percurso.create(body, id);
        console.log(goodPercurso);
        chai_1.expect(goodPercurso.isSuccess).to.equal(true);
        const nosPercursos1 = nosPercurso_1.NosPercurso.create({ noId: "100", duracao: 100, distancia: 900 }).getValue();
        var percurso = percurso_1.Percurso.create(body).getValue();
        //testar get domainID
        sinon.assert.match(percurso.props.domainId, "1");
        //testar get nosPercurso
        sinon.assert.match(percurso.nosPercurso, [nosPercursos]);
        const nosPercursos2 = nosPercurso_1.NosPercurso.create({ noId: "100", duracao: 100, distancia: 900 }).getValue();
        const nosPercursos3 = nosPercurso_1.NosPercurso.create({ noId: "102", duracao: 102, distancia: 910 }).getValue();
        var percurso2 = percurso_1.Percurso.create(body).getValue();
        //testar set nosPercurso
        percurso2.nosPercurso = [nosPercursos3];
        sinon.assert.match(percurso2.nosPercurso, [nosPercursos3]);
    });
});
//# sourceMappingURL=percurso.test.js.map