"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const tipoViatura_1 = require("../../../domain/tipoViatura/tipoViatura");
const UniqueEntityID_1 = require("../../../core/domain/UniqueEntityID");
describe("Tipo de Viatura Tests", () => {
    it("Validate Tipo de Viatura values", () => {
        const id = new UniqueEntityID_1.UniqueEntityID(1);
        const goodTipoViatura = tipoViatura_1.TipoViatura.create({
            domainId: "id1",
            nome: "nome1",
            autonomia: 50000,
            custo: 20000,
            velocidadeMedia: 450,
            combustivel: 45,
            consumo: 21,
            emissoes: 33
        }, id);
        const badTipoViatura = tipoViatura_1.TipoViatura.create({
            domainId: "id2",
            nome: "",
            autonomia: 50000,
            custo: 20000,
            velocidadeMedia: 450,
            combustivel: 45,
            consumo: 21,
            emissoes: 33
        }, id);
        chai_1.expect(goodTipoViatura.isSuccess).to.equal(true);
        chai_1.expect(badTipoViatura.isFailure).to.equal(true);
        var tipoViatura1 = tipoViatura_1.TipoViatura.create({
            domainId: "id3",
            nome: "name",
            autonomia: 50000,
            custo: 20000,
            velocidadeMedia: 450,
            combustivel: 45,
            consumo: 21,
            emissoes: 33
        }, id).getValue();
        //testar get domainID
        chai_1.expect(tipoViatura1.props.domainId).equal("id3");
        //testar get nome
        chai_1.expect(tipoViatura1.nome).equal("name");
        //testar get autonomia
        chai_1.expect(tipoViatura1.autonomia).equal(50000);
        //testar get custo
        chai_1.expect(tipoViatura1.custo).equal(20000);
        //testar get velocidadeMedia
        chai_1.expect(tipoViatura1.velocidadeMedia).equal(450);
        //testar get combustivel
        chai_1.expect(tipoViatura1.combustivel).equal(45);
        //testar get consumo
        chai_1.expect(tipoViatura1.consumo).equal(21);
        //testar get emissoes
        chai_1.expect(tipoViatura1.emissoes).equal(33);
        var tipoViatura2 = tipoViatura_1.TipoViatura.create({
            domainId: "id4",
            nome: "name",
            autonomia: 50000,
            custo: 20000,
            velocidadeMedia: 450,
            combustivel: 45,
            consumo: 21,
            emissoes: 33
        }, id).getValue();
        //testar set nosPercurso
        tipoViatura2.nome = "nome1";
        chai_1.expect(tipoViatura2.nome).equal("nome1");
        tipoViatura2.autonomia = 40000;
        chai_1.expect(tipoViatura2.autonomia).equal(40000);
        tipoViatura2.custo = 22000;
        chai_1.expect(tipoViatura2.custo).equal(22000);
        tipoViatura2.velocidadeMedia = 27000;
        chai_1.expect(tipoViatura2.velocidadeMedia).equal(27000);
        tipoViatura2.combustivel = 48;
        chai_1.expect(tipoViatura2.combustivel).equal(48);
        tipoViatura2.consumo = 20;
        chai_1.expect(tipoViatura2.consumo).equal(20);
        tipoViatura2.emissoes = 19;
        chai_1.expect(tipoViatura2.emissoes).equal(19);
    });
});
//# sourceMappingURL=tipoViatura.test.js.map