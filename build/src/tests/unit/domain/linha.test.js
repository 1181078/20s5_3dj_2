"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const sinon = __importStar(require("sinon"));
const linha_1 = require("../../../domain/linha/linha");
const percursoLinha_1 = require("../../../domain/linha/percursoLinha");
const UniqueEntityID_1 = require("../../../core/domain/UniqueEntityID");
describe("Linha Tests", () => {
    it("Validate Linha values", () => {
        const id = new UniqueEntityID_1.UniqueEntityID(1);
        const percursoLinhaa = percursoLinha_1.PercursoLinha.create({ percursoLinhaId: "pl1", percurso: "percurso", orientation: "Go" }).getValue();
        let body = { "domainId": "1", "nome": "nome", "cor": "cor", "percursoLinha": [percursoLinhaa] };
        const goodLinha = linha_1.Linha.create(body, id);
        // const badLinha = Linha.create({ domainId: "1", percursoLinha: percursoLinhaa, nome: "nome", cor: "cor" }, id);
        chai_1.expect(goodLinha.isSuccess).to.equal(true);
        // expect(badLinha.isFailure).to.equal(true);
        var linha = linha_1.Linha.create(body).getValue();
        //testar get domainID
        chai_1.assert.equal(linha.props.domainId, "1");
        //testar get nome
        sinon.assert.match(linha.percursoLinha, [percursoLinhaa]);
        //testar get nome
        chai_1.assert.equal(linha.nome, "nome");
        //testar get cor
        chai_1.assert.equal(linha.cor, "cor");
        var linha2 = linha_1.Linha.create(body).getValue();
        const percursoLinhaa2 = percursoLinha_1.PercursoLinha.create({ percursoLinhaId: "pl:1", percurso: "percurso", orientation: "Go" }).getValue();
        //testar set percursoLinha
        linha2.percursoLinha = [percursoLinhaa2];
        sinon.assert.match(linha2.percursoLinha, [percursoLinhaa2]);
        //testar set nome
        linha2.nome = "the name";
        chai_1.assert.equal(linha2.nome, "the name");
        //testar set cor
        linha2.nome = "a cor";
        chai_1.assert.equal(linha2.nome, "a cor");
    });
});
//# sourceMappingURL=linha.test.js.map