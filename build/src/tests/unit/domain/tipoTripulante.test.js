"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const tipoTripulante_1 = require("../../../domain/tipoTripulante/tipoTripulante");
const UniqueEntityID_1 = require("../../../core/domain/UniqueEntityID");
describe("Tipo de Tripulante Tests", () => {
    it("Validate Percurso values", () => {
        const id = new UniqueEntityID_1.UniqueEntityID(1);
        const goodTipoTripulante = tipoTripulante_1.TipoTripulante.create({
            domainId: "1",
            desc: "descricao"
        }, id);
        const badTipoTripulante = tipoTripulante_1.TipoTripulante.create({
            domainId: "1",
            desc: ""
        }, id);
        chai_1.expect(goodTipoTripulante.isSuccess).to.equal(true);
        chai_1.expect(badTipoTripulante.isFailure).to.equal(true);
        var tipoTripulante = tipoTripulante_1.TipoTripulante.create({ domainId: "1",
            desc: "descricao" }).getValue();
        //testar get domainID
        chai_1.expect(tipoTripulante.props.domainId).equal("1");
        //testar get nosPercurso
        chai_1.expect(tipoTripulante.desc).equal("descricao");
        var tipoTripulante2 = tipoTripulante_1.TipoTripulante.create({
            domainId: "1",
            desc: "la description"
        }).getValue();
        //testar set nosPercurso
        tipoTripulante2.desc = "vive la france";
        chai_1.expect(tipoTripulante2.desc).equal("vive la france");
    });
});
//# sourceMappingURL=tipoTripulante.test.js.map