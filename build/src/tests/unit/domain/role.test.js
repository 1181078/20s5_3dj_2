"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const role_1 = require("../../../domain/role/role");
const UniqueEntityID_1 = require("../../../core/domain/UniqueEntityID");
describe("Role Tests", () => {
    it("Validate role values", () => {
        const id = new UniqueEntityID_1.UniqueEntityID(1);
        const goodRole = role_1.Role.create({ id: "id1", name: "Role" });
        const badRole = role_1.Role.create({ id: "id2", name: "" });
        chai_1.expect(goodRole.isSuccess).to.equal(true);
        chai_1.expect(badRole.isFailure).to.equal(true);
        var role = role_1.Role.create({
            id: "id3",
            name: "role_"
        }).getValue();
        //testar get name
        chai_1.expect(role.name).equal("role_");
        var role2 = role_1.Role.create({
            id: "idrole2",
            name: "la description"
        }).getValue();
        //testar set role
        role2.name = "role2";
        chai_1.expect(role2.name).equal("role2");
    });
});
//# sourceMappingURL=role.test.js.map