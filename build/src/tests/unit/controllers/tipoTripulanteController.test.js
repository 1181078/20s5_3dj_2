"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../../../config.js"));
const Result_1 = require("../../../core/logic/Result");
const tipoTripulanteController_1 = __importDefault(require("../../../controllers/tipoTripulanteController"));
describe('tipo tripulante controller', function () {
    beforeEach(function () {
    });
    let tipoTripulanteSchemaClass = require("../../../persistence/schemas/tipoTripulanteSchema").default;
    typedi_1.Container.set('tipoTripulanteSchema', tipoTripulanteSchemaClass);
    let tipoTripulanteRepoClass = require("../../../repos/tipoTripulanteRepo").default;
    let tipoTripulanteRepoInstance = typedi_1.Container.get(tipoTripulanteRepoClass);
    typedi_1.Container.set(config_js_1.default.repos.tipoTripulante.name, tipoTripulanteRepoInstance);
    let tipoTripulanteServiceClass = require("../../../services/tipoTripulanteService").default;
    let tipoTripulanteServiceInstance = typedi_1.Container.get(tipoTripulanteServiceClass);
    typedi_1.Container.set(config_js_1.default.services.tipoTripulante.name, tipoTripulanteServiceInstance);
    it('createTipoTripulante: returns json with description value', async function () {
        let body = { "domainId": "12345", "desc": 'test_desc' };
        let req = {};
        req.body = body;
        let res = {
            json: sinon.spy(),
            status: function (s) { this.statusCode = s; return this; }
        };
        let next = () => { };
        tipoTripulanteServiceInstance = typedi_1.Container.get(config_js_1.default.services.tipoTripulante.name);
        sinon.stub(tipoTripulanteServiceInstance, "createTipoTripulante").returns(Result_1.Result.ok({ "domainId": req.body.domainId, "desc": req.body.desc }));
        const ctrl = new tipoTripulanteController_1.default(tipoTripulanteServiceInstance);
        await ctrl.createTipoTripulante(req, res, next);
        sinon.assert.calledOnce(res.json);
        sinon.assert.calledWith(res.json, sinon.match(body));
    });
    it('listTipoTripulantes: returns json with array of types', async function () {
        let body = { "domainId": "12345", "desc": 'test_desc' };
        let body2 = { "domainId": "112233", "desc": 'newtest_desc' };
        let req = {};
        let req2 = {};
        let req3 = {};
        req.body = body;
        req2.body = body2;
        let res = {};
        let res2 = {};
        let res3 = {
            json: sinon.spy()
        };
        let next = () => { };
        tipoTripulanteServiceInstance = typedi_1.Container.get(config_js_1.default.services.tipoTripulante.name);
        sinon.stub(tipoTripulanteServiceInstance, "listTiposTripulante").returns(Result_1.Result.ok([{ "domainId": req.body.domainId, "desc": req.body.desc }, { "domainId": req2.body.domainId, "desc": req2.body.desc }]));
        const ctrl = new tipoTripulanteController_1.default(tipoTripulanteServiceInstance);
        await ctrl.createTipoTripulante(req, res, next);
        await ctrl.createTipoTripulante(req2, res2, next);
        await ctrl.listTiposTripulante(req3, res3, next);
        sinon.assert.calledOnce(res3.json);
        sinon.assert.calledWith(res3.json, sinon.match([{ "domainId": "12345", "desc": "test_desc" }, { "domainId": "112233", "desc": 'newtest_desc' }]));
    });
    it('listTipoTripulantes: returns json with sorted array of types', async function () {
        let body = { "domainId": "12345", "desc": 'test_desc' };
        let body2 = { "domainId": "112233", "desc": 'newtest_desc' };
        let req = {};
        let req2 = {};
        let req3 = {};
        req.body = body;
        req2.body = body2;
        req3.params = { "ord": "desc" };
        ;
        let res = {};
        let res2 = {};
        let res3 = {
            json: sinon.spy()
        };
        let next = () => { };
        tipoTripulanteServiceInstance = typedi_1.Container.get(config_js_1.default.services.tipoTripulante.name);
        sinon.stub(tipoTripulanteServiceInstance, "listTiposTripulanteSorted").returns(Result_1.Result.ok([{ "domainId": req2.body.domainId, "desc": req2.body.desc }, { "domainId": req.body.domainId, "desc": req.body.desc }]));
        const ctrl = new tipoTripulanteController_1.default(tipoTripulanteServiceInstance);
        await ctrl.createTipoTripulante(req, res, next);
        await ctrl.createTipoTripulante(req2, res2, next);
        await ctrl.listTiposTripulanteSorted(req3, res3, next);
        sinon.assert.calledOnce(res3.json);
        sinon.assert.calledWith(res3.json, sinon.match([{ "domainId": "112233", "desc": 'newtest_desc' }, { "domainId": "12345", "desc": "test_desc" }]));
    });
});
// import * as sinon from 'sinon';
// import { mocked } from 'ts-jest/utils'
// import { Response, Request, NextFunction } from 'express';
// import { Container } from 'typedi';
// import config from "../../../../config";
// import { Result } from '../../../core/logic/Result';
// import ITipoTripulanteService from "../../../services/IServices/ITipoTripulanteService";
// import tipoTripulanteService from "../../../services/tipoTripulanteService";
// import tipoTripulanteRepo from "../../../repos/tipoTripulanteRepo";
// import TipoTripulanteController from "../../../controllers/tipoTripulanteController";
// import ITipoTripulanteController from "../../../controllers/IControllers/ITipoTripulanteController";
// import ITipoTripulanteDTO from '../../../dto/ITipoTripulanteDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';
// jest.mock("../../../services/tipoTripulanteService", () => {
// 	const serv = {
// 		createTipoTripulante: jest.fn()
// 	  };
// 	  return { TipoTripulanteService: jest.fn(() => serv) };
// 	});
// describe('tipo tripulante controller', () => {
// 	// const MockedServ = mocked(tipoTripulanteService, true);
// 	// ;
// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;
// 	// });
// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;
// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
// 	// 	const controller = new TipoTripulanteController(MockedServ.prototype);
// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);
// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;
// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };
// 	// 	// mockedFoo.prototype.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);
// 	// 	// const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createTipoTripulante.mock.calls).toHaveLength(1);
// 	//});
// 	// const m = {name: "TipoTripulanteController", path: "../controllers/tipoTripulanteController"};
// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	require('../../../app');
// 	// 	console.log(Container);
// 	// 	const ctrl = Container.get(config.controller.tipoTripulante.name) as ITipoTripulanteController;
// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;
// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
// 	// 	 ctrl.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);
// 	// 	 const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);
// 	// }
// 	// 	);});
//# sourceMappingURL=tipoTripulanteController.test.js.map