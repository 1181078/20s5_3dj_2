"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const typedi_1 = require("typedi");
const config_1 = __importDefault(require("../../../../config"));
const Result_1 = require("../../../core/logic/Result");
const tipoViaturaController_1 = __importDefault(require("../../../controllers/tipoViaturaController"));
describe('tipo viatura controller', function () {
    beforeEach(function () {
    });
    let tipoViaturaSchemaClass = require("../../../persistence/schemas/tipoViaturaSchema").default;
    typedi_1.Container.set('tipoViaturaSchema', tipoViaturaSchemaClass);
    let tipoViaturaRepoClass = require("../../../repos/tipoViaturaRepo").default;
    let tipoViaturaRepoInstance = typedi_1.Container.get(tipoViaturaRepoClass);
    typedi_1.Container.set(config_1.default.repos.tipoViatura.name, tipoViaturaRepoInstance);
    let tipoViaturaServiceClass = require("../../../services/tipoViaturaService").default;
    let tipoViaturaServiceInstance = typedi_1.Container.get(tipoViaturaServiceClass);
    typedi_1.Container.set(config_1.default.services.tipoViatura.name, tipoViaturaServiceInstance);
    it('createTipoViatura: returns json with type components', async function () {
        let body = {
            "domainId": "v:123",
            "nome": "iviat123",
            "autonomia": 151,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        let req = {};
        req.body = body;
        let res = {
            json: sinon.spy(),
            status: function (s) { this.statusCode = s; return this; }
        };
        let next = () => { };
        tipoViaturaServiceInstance = typedi_1.Container.get(config_1.default.services.tipoViatura.name);
        sinon.stub(tipoViaturaServiceInstance, "createTipoViatura").returns(Result_1.Result.ok({
            "domainId": req.body.domainId,
            "nome": req.body.nome,
            "autonomia": req.body.autonomia,
            "custo": req.body.custo,
            "velocidadeMedia": req.body.velocidadeMedia,
            "combustivel": req.body.combustivel,
            "consumo": req.body.consumo,
            "emissoes": req.body.emissoes
        }));
        const ctrl = new tipoViaturaController_1.default(tipoViaturaServiceInstance);
        await ctrl.createTipoViatura(req, res, next);
        sinon.assert.calledOnce(res.json);
        sinon.assert.calledWith(res.json, sinon.match(body));
    });
    it('listTipoViaturas: returns json with array of types', async function () {
        let body = {
            "domainId": "v:123",
            "nome": "iviat123",
            "autonomia": 151,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        let body2 = {
            "domainId": "v:321",
            "nome": "iviat321",
            "autonomia": 231,
            "custo": 423,
            "velocidadeMedia": 200,
            "combustivel": 50,
            "consumo": 70,
            "emissoes": 200
        };
        let req = {};
        let req2 = {};
        let req3 = {};
        req.body = body;
        req2.body = body2;
        let res = {};
        let res2 = {};
        let res3 = {
            json: sinon.spy()
        };
        let next = () => { };
        tipoViaturaServiceInstance = typedi_1.Container.get(config_1.default.services.tipoViatura.name);
        sinon.stub(tipoViaturaServiceInstance, "listTiposViatura").returns(Result_1.Result.ok([{
                "domainId": req.body.domainId,
                "nome": req.body.nome,
                "autonomia": req.body.autonomia,
                "custo": req.body.custo,
                "velocidadeMedia": req.body.velocidadeMedia,
                "combustivel": req.body.combustivel,
                "consumo": req.body.consumo,
                "emissoes": req.body.emissoes
            },
            {
                "domainId": req2.body.domainId,
                "nome": req2.body.nome,
                "autonomia": req2.body.autonomia,
                "custo": req2.body.custo,
                "velocidadeMedia": req2.body.velocidadeMedia,
                "combustivel": req2.body.combustivel,
                "consumo": req2.body.consumo,
                "emissoes": req2.body.emissoes
            }]));
        const ctrl = new tipoViaturaController_1.default(tipoViaturaServiceInstance);
        await ctrl.createTipoViatura(req, res, next);
        await ctrl.createTipoViatura(req2, res2, next);
        await ctrl.listTiposViatura(req3, res3, next);
        sinon.assert.calledOnce(res3.json);
        sinon.assert.calledWith(res3.json, sinon.match([{
                "domainId": "v:123",
                "nome": "iviat123",
                "autonomia": 151,
                "custo": 23,
                "velocidadeMedia": 100,
                "combustivel": 23,
                "consumo": 120,
                "emissoes": 330
            },
            {
                "domainId": "v:321",
                "nome": "iviat321",
                "autonomia": 231,
                "custo": 423,
                "velocidadeMedia": 200,
                "combustivel": 50,
                "consumo": 70,
                "emissoes": 200
            }]));
    });
    it('listTipoViaturas: returns json with sorted array of types', async function () {
        let body = {
            "domainId": "v:123",
            "nome": "iviat123",
            "autonomia": 151,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        let body2 = {
            "domainId": "v:321",
            "nome": "iviat321",
            "autonomia": 231,
            "custo": 423,
            "velocidadeMedia": 200,
            "combustivel": 50,
            "consumo": 70,
            "emissoes": 200
        };
        let req = {};
        let req2 = {};
        let req3 = {};
        req.body = body;
        req2.body = body2;
        req3.params = { "ord": "autonomia" };
        ;
        let res = {};
        let res2 = {};
        let res3 = {
            json: sinon.spy()
        };
        let next = () => { };
        tipoViaturaServiceInstance = typedi_1.Container.get(config_1.default.services.tipoViatura.name);
        sinon.stub(tipoViaturaServiceInstance, "listTiposViaturaSorted").returns(Result_1.Result.ok([{
                "domainId": req.body.domainId,
                "nome": req.body.nome,
                "autonomia": req.body.autonomia,
                "custo": req.body.custo,
                "velocidadeMedia": req.body.velocidadeMedia,
                "combustivel": req.body.combustivel,
                "consumo": req.body.consumo,
                "emissoes": req.body.emissoes
            },
            {
                "domainId": req2.body.domainId,
                "nome": req2.body.nome,
                "autonomia": req2.body.autonomia,
                "custo": req2.body.custo,
                "velocidadeMedia": req2.body.velocidadeMedia,
                "combustivel": req2.body.combustivel,
                "consumo": req2.body.consumo,
                "emissoes": req2.body.emissoes
            }]));
        const ctrl = new tipoViaturaController_1.default(tipoViaturaServiceInstance);
        await ctrl.createTipoViatura(req, res, next);
        await ctrl.createTipoViatura(req2, res2, next);
        await ctrl.listTiposViaturaSorted(req3, res3, next);
        sinon.assert.calledOnce(res3.json);
        sinon.assert.calledWith(res3.json, sinon.match([{
                "domainId": "v:123",
                "nome": "iviat123",
                "autonomia": 151,
                "custo": 23,
                "velocidadeMedia": 100,
                "combustivel": 23,
                "consumo": 120,
                "emissoes": 330
            },
            {
                "domainId": "v:321",
                "nome": "iviat321",
                "autonomia": 231,
                "custo": 423,
                "velocidadeMedia": 200,
                "combustivel": 50,
                "consumo": 70,
                "emissoes": 200
            }]));
    });
});
// import * as sinon from 'sinon';
// import { mocked } from 'ts-jest/utils'
// import { Response, Request, NextFunction } from 'express';
// import { Container } from 'typedi';
// import config from "../../../../config";
// import { Result } from '../../../core/logic/Result';
// import ITipoTripulanteService from "../../../services/IServices/ITipoTripulanteService";
// import tipoTripulanteService from "../../../services/tipoTripulanteService";
// import tipoTripulanteRepo from "../../../repos/tipoTripulanteRepo";
// import TipoTripulanteController from "../../../controllers/tipoTripulanteController";
// import ITipoTripulanteController from "../../../controllers/IControllers/ITipoTripulanteController";
// import ITipoTripulanteDTO from '../../../dto/ITipoTripulanteDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';
// jest.mock("../../../services/tipoTripulanteService", () => {
// 	const serv = {
// 		createTipoTripulante: jest.fn()
// 	  };
// 	  return { TipoTripulanteService: jest.fn(() => serv) };
// 	});
// describe('tipo tripulante controller', () => {
// 	// const MockedServ = mocked(tipoTripulanteService, true);
// 	// ;
// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;
// 	// });
// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;
// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
// 	// 	const controller = new TipoTripulanteController(MockedServ.prototype);
// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);
// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;
// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };
// 	// 	// mockedFoo.prototype.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);
// 	// 	// const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createTipoTripulante.mock.calls).toHaveLength(1);
// 	//});
// 	// const m = {name: "TipoTripulanteController", path: "../controllers/tipoTripulanteController"};
// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	require('../../../app');
// 	// 	console.log(Container);
// 	// 	const ctrl = Container.get(config.controller.tipoTripulante.name) as ITipoTripulanteController;
// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;
// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
// 	// 	 ctrl.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);
// 	// 	 const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);
// 	// }
// 	// 	);});
//# sourceMappingURL=tipoViaturaController.test.js.map