"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const typedi_1 = require("typedi");
const config_1 = __importDefault(require("../../../../config"));
const Result_1 = require("../../../core/logic/Result");
const linhaController_1 = __importDefault(require("../../../controllers/linhaController"));
describe('Linha controller', function () {
    beforeEach(function () {
    });
    let linhaSchemaClass = require("../../../persistence/schemas/linhaSchema").default;
    typedi_1.Container.set('linhaSchema', linhaSchemaClass);
    let linhaRepoClass = require("../../../repos/linhaRepo").default;
    let linhaRepoInstance = typedi_1.Container.get(linhaRepoClass);
    typedi_1.Container.set(config_1.default.repos.linha.name, linhaRepoInstance);
    let linhaServiceClass = require("../../../services/linhaService").default;
    let linhaServiceInstance = typedi_1.Container.get(linhaServiceClass);
    typedi_1.Container.set(config_1.default.services.linha.name, linhaServiceInstance);
    it('createlinha: returns json with linha components', async function () {
        let body = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": [{
                    "percursoLinhaId": "71123",
                    "percurso": "a",
                    "orientation": "b"
                }, {
                    "percursoLinhaId": "1234",
                    "percurso": "a4",
                    "orientation": "b4"
                }]
        };
        let req = {};
        req.body = body;
        let res = {
            json: sinon.spy(),
            status: function (s) { this.statusCode = s; return this; }
        };
        let next = () => { };
        linhaServiceInstance = typedi_1.Container.get(config_1.default.services.linha.name);
        sinon.stub(linhaServiceInstance, "createLinha").returns(Result_1.Result.ok({
            "domainId": req.body.domainId,
            "nome": req.body.nome,
            "cor": req.body.cor,
            "percursoLinha": req.body.percursoLinha
        }));
        const ctrl = new linhaController_1.default(linhaServiceInstance);
        await ctrl.createLinha(req, res, next);
        sinon.assert.calledOnce(res.json);
        sinon.assert.calledWith(res.json, sinon.match(body));
    });
    it('listLinhas: returns json with array of linhas', async function () {
        let body = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": [{
                    "percursoLinhaId": "71123",
                    "percurso": "a",
                    "orientation": "b"
                }, {
                    "percursoLinhaId": "1234",
                    "percurso": "a4",
                    "orientation": "b4"
                }]
        };
        let body2 = {
            "domainId": "nl:11",
            "nome": "n11",
            "cor": "bbb",
            "percursoLinha": [{
                    "percursoLinhaId": "121",
                    "percurso": "a3",
                    "orientation": "b"
                }, {
                    "percursoLinhaId": "1231114",
                    "percurso": "a5",
                    "orientation": "b4"
                }]
        };
        let req = {};
        let req2 = {};
        let req3 = {};
        req.body = body;
        req2.body = body2;
        let res = {};
        let res2 = {};
        let res3 = {
            json: sinon.spy()
        };
        let next = () => { };
        linhaServiceInstance = typedi_1.Container.get(config_1.default.services.linha.name);
        sinon.stub(linhaServiceInstance, "listLinhas").returns(Result_1.Result.ok([{
                "domainId": req.body.domainId,
                "nome": req.body.nome,
                "cor": req.body.cor,
                "percursoLinha": req.body.percursoLinha
            },
            {
                "domainId": req2.body.domainId,
                "nome": req2.body.nome,
                "cor": req2.body.cor,
                "percursoLinha": req2.body.percursoLinha
            }]));
        const ctrl = new linhaController_1.default(linhaServiceInstance);
        await ctrl.createLinha(req, res, next);
        await ctrl.createLinha(req2, res2, next);
        await ctrl.listLinhas(req3, res3, next);
        sinon.assert.calledOnce(res3.json);
        sinon.assert.calledWith(res3.json, sinon.match([{
                "domainId": "nl:7512341ee",
                "nome": "newlineTest7532341",
                "cor": "rrr",
                "percursoLinha": [{
                        "percursoLinhaId": "71123",
                        "percurso": "a",
                        "orientation": "b"
                    }, {
                        "percursoLinhaId": "1234",
                        "percurso": "a4",
                        "orientation": "b4"
                    }]
            }, {
                "domainId": "nl:11",
                "nome": "n11",
                "cor": "bbb",
                "percursoLinha": [{
                        "percursoLinhaId": "121",
                        "percurso": "a3",
                        "orientation": "b"
                    }, {
                        "percursoLinhaId": "1231114",
                        "percurso": "a5",
                        "orientation": "b4"
                    }]
            }]));
    });
    it('listLinha: returns json with sorted array of types', async function () {
        let body = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": [{
                    "percursoLinhaId": "71123",
                    "percurso": "a",
                    "orientation": "b"
                }, {
                    "percursoLinhaId": "1234",
                    "percurso": "a4",
                    "orientation": "b4"
                }]
        };
        let body2 = {
            "domainId": "nl:11",
            "nome": "n11",
            "cor": "bbb",
            "percursoLinha": [{
                    "percursoLinhaId": "121",
                    "percurso": "a3",
                    "orientation": "b"
                }, {
                    "percursoLinhaId": "1231114",
                    "percurso": "a5",
                    "orientation": "b4"
                }]
        };
        let req = {};
        let req2 = {};
        let req3 = {};
        req.body = body;
        req2.body = body2;
        req3.params = { "ord": "desc" };
        ;
        let res = {};
        let res2 = {};
        let res3 = {
            json: sinon.spy()
        };
        let next = () => { };
        linhaServiceInstance = typedi_1.Container.get(config_1.default.services.linha.name);
        sinon.stub(linhaServiceInstance, "listLinhasSorted").returns(Result_1.Result.ok([{
                "domainId": req.body.domainId,
                "nome": req.body.nome,
                "cor": req.body.cor,
                "percursoLinha": req.body.percursoLinha
            },
            {
                "domainId": req2.body.domainId,
                "nome": req2.body.nome,
                "cor": req2.body.cor,
                "percursoLinha": req2.body.percursoLinha
            }]));
        const ctrl = new linhaController_1.default(linhaServiceInstance);
        await ctrl.createLinha(req, res, next);
        await ctrl.createLinha(req2, res2, next);
        await ctrl.listLinhasSorted(req3, res3, next);
        sinon.assert.calledOnce(res3.json);
        sinon.assert.calledWith(res3.json, sinon.match([{
                "domainId": "nl:7512341ee",
                "nome": "newlineTest7532341",
                "cor": "rrr",
                "percursoLinha": [{
                        "percursoLinhaId": "71123",
                        "percurso": "a",
                        "orientation": "b"
                    }, {
                        "percursoLinhaId": "1234",
                        "percurso": "a4",
                        "orientation": "b4"
                    }]
            }, {
                "domainId": "nl:11",
                "nome": "n11",
                "cor": "bbb",
                "percursoLinha": [{
                        "percursoLinhaId": "121",
                        "percurso": "a3",
                        "orientation": "b"
                    }, {
                        "percursoLinhaId": "1231114",
                        "percurso": "a5",
                        "orientation": "b4"
                    }]
            }]));
    });
});
// import * as silinhan from 'silinhan';
// import { mocked } from 'ts-jest/utils'
// import { Response, Request, NextFunction } from 'express';
// import { Container } from 'linhadi';
// import config from "../../../../config";
// import { Result } from '../../../core/logic/Result';
// import ILinhaService from "../../../services/IServices/ILinhaService";
// import linhaService from "../../../services/linhaService";
// import linhaRepo from "../../../repos/linhaRepo";
// import LinhaController from "../../../controllers/linhaController";
// import ILinhaController from "../../../controllers/IControllers/ILinhaController";
// import ILinhaDTO from '../../../dto/ILinhaDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';
// jest.mock("../../../services/linhaService", () => {
// 	const serv = {
// 		createLinha: jest.fn()
// 	  };
// 	  return { LinhaService: jest.fn(() => serv) };
// 	});
// describe('tipo tripulante controller', () => {
// 	// const MockedServ = mocked(linhaService, true);
// 	// ;
// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;
// 	// });
// 	// it('createLinha: returns json with description value', async function () {
// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;
// 	// 	let res: Partial<Response> = {
// 	// 		json: silinhan.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
// 	// 	const controller = new LinhaController(MockedServ.protolinha);
// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);
// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;
// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: silinhan.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };
// 	// 	// mockedFoo.protolinha.createLinha(<Request>req, <Response>res, <NextFunction>next);
// 	// 	// const exp: ILinhaDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.protolinha.createLinha.mock.calls).toHaveLength(1);
// 	//});
// 	// const m = {name: "LinhaController", path: "../controllers/linhaController"};
// 	// it('createLinha: returns json with description value', async function () {
// 	// 	require('../../../app');
// 	// 	console.log(Container);
// 	// 	const ctrl = Container.get(config.controller.linha.name) as ILinhaController;
// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;
// 	//  	let res: Partial<Response> = {
// 	//  		json: silinhan.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
// 	// 	 ctrl.createLinha(<Request>req, <Response>res, <NextFunction>next);
// 	// 	 const exp: ILinhaDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);
// 	// }
// 	// 	);});
//# sourceMappingURL=linhaController.test.js.map