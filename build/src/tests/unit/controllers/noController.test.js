"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const typedi_1 = require("typedi");
const config_1 = __importDefault(require("../../../../config"));
const Result_1 = require("../../../core/logic/Result");
const noController_1 = __importDefault(require("../../../controllers/noController"));
describe('No controller', function () {
    beforeEach(function () {
    });
    let noSchemaClass = require("../../../persistence/schemas/noSchema").default;
    typedi_1.Container.set('noSchema', noSchemaClass);
    let noRepoClass = require("../../../repos/noRepo").default;
    let noRepoInstance = typedi_1.Container.get(noRepoClass);
    typedi_1.Container.set(config_1.default.repos.no.name, noRepoInstance);
    let noServiceClass = require("../../../services/noService").default;
    let noServiceInstance = typedi_1.Container.get(noServiceClass);
    typedi_1.Container.set(config_1.default.services.no.name, noServiceInstance);
    it('createno: returns json with no components', async function () {
        let body = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [{
                    "noId": "no1",
                    "duracao": "123"
                }]
        };
        let req = {};
        req.body = body;
        let res = {
            json: sinon.spy(),
            status: function (s) { this.statusCode = s; return this; }
        };
        let next = () => { };
        noServiceInstance = typedi_1.Container.get(config_1.default.services.no.name);
        sinon.stub(noServiceInstance, "createNo").returns(Result_1.Result.ok({
            "domainId": req.body.domainId,
            "nome": req.body.nome,
            "latitude": req.body.latitude,
            "longitude": req.body.longitude,
            "nomeCurto": req.body.nomeCurto,
            "isEstacaoRecolha": req.body.isEstacaoRecolha,
            "isPontoRendicao": req.body.isPontoRendicao,
            "tempoViagem": req.body.tempoViagem
        }));
        const ctrl = new noController_1.default(noServiceInstance);
        await ctrl.createNo(req, res, next);
        sinon.assert.calledOnce(res.json);
        sinon.assert.calledWith(res.json, sinon.match(body));
    });
    it('listNos: returns json with array of nos', async function () {
        let body = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [{
                    "noId": "no1",
                    "duracao": "123"
                }]
        };
        let body2 = {
            "domainId": "no2",
            "nome": "no222",
            "latitude": 41,
            "longitude": 63,
            "nomeCurto": "gome2",
            "isEstacaoRecolha": true,
            "isPontoRendicao": true,
            "tempoViagem": [{
                    "noId": "no2",
                    "duracao": "1223"
                }]
        };
        let req = {};
        let req2 = {};
        let req3 = {};
        req.body = body;
        req2.body = body2;
        let res = {};
        let res2 = {};
        let res3 = {
            json: sinon.spy()
        };
        let next = () => { };
        noServiceInstance = typedi_1.Container.get(config_1.default.services.no.name);
        sinon.stub(noServiceInstance, "listNos").returns(Result_1.Result.ok([{
                "domainId": req.body.domainId,
                "nome": req.body.nome,
                "latitude": req.body.latitude,
                "longitude": req.body.longitude,
                "nomeCurto": req.body.nomeCurto,
                "isEstacaoRecolha": req.body.isEstacaoRecolha,
                "isPontoRendicao": req.body.isPontoRendicao,
                "tempoViagem": req.body.tempoViagem
            },
            {
                "domainId": req2.body.domainId,
                "nome": req2.body.nome,
                "latitude": req2.body.latitude,
                "longitude": req2.body.longitude,
                "nomeCurto": req2.body.nomeCurto,
                "isEstacaoRecolha": req2.body.isEstacaoRecolha,
                "isPontoRendicao": req2.body.isPontoRendicao,
                "tempoViagem": req2.body.tempoViagem
            }]));
        const ctrl = new noController_1.default(noServiceInstance);
        await ctrl.createNo(req, res, next);
        await ctrl.createNo(req2, res2, next);
        await ctrl.listNos(req3, res3, next);
        sinon.assert.calledOnce(res3.json);
        sinon.assert.calledWith(res3.json, sinon.match([{
                "domainId": "no1",
                "nome": "ano8",
                "latitude": 40,
                "longitude": 64,
                "nomeCurto": "gome",
                "isEstacaoRecolha": true,
                "isPontoRendicao": false,
                "tempoViagem": [{
                        "noId": "no1",
                        "duracao": "123"
                    }]
            }, {
                "domainId": "no2",
                "nome": "no222",
                "latitude": 41,
                "longitude": 63,
                "nomeCurto": "gome2",
                "isEstacaoRecolha": true,
                "isPontoRendicao": true,
                "tempoViagem": [{
                        "noId": "no2",
                        "duracao": "1223"
                    }]
            }]));
    });
    it('listNos: returns json with sorted array of nos', async function () {
        let body = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [{
                    "noId": "no1",
                    "duracao": "123"
                }]
        };
        let body2 = {
            "domainId": "no2",
            "nome": "no222",
            "latitude": 41,
            "longitude": 63,
            "nomeCurto": "gome2",
            "isEstacaoRecolha": true,
            "isPontoRendicao": true,
            "tempoViagem": [{
                    "noId": "no2",
                    "duracao": "1223"
                }]
        };
        let req = {};
        let req2 = {};
        let req3 = {};
        req.body = body;
        req2.body = body2;
        req3.params = { "ord": "nome" };
        ;
        let res = {};
        let res2 = {};
        let res3 = {
            json: sinon.spy()
        };
        let next = () => { };
        noServiceInstance = typedi_1.Container.get(config_1.default.services.no.name);
        sinon.stub(noServiceInstance, "listNosSorted").returns(Result_1.Result.ok([{
                "domainId": req.body.domainId,
                "nome": req.body.nome,
                "latitude": req.body.latitude,
                "longitude": req.body.longitude,
                "nomeCurto": req.body.nomeCurto,
                "isEstacaoRecolha": req.body.isEstacaoRecolha,
                "isPontoRendicao": req.body.isPontoRendicao,
                "tempoViagem": req.body.tempoViagem
            },
            {
                "domainId": req2.body.domainId,
                "nome": req2.body.nome,
                "latitude": req2.body.latitude,
                "longitude": req2.body.longitude,
                "nomeCurto": req2.body.nomeCurto,
                "isEstacaoRecolha": req2.body.isEstacaoRecolha,
                "isPontoRendicao": req2.body.isPontoRendicao,
                "tempoViagem": req2.body.tempoViagem
            }]));
        const ctrl = new noController_1.default(noServiceInstance);
        await ctrl.createNo(req, res, next);
        await ctrl.createNo(req2, res2, next);
        await ctrl.listNosSorted(req3, res3, next);
        sinon.assert.calledOnce(res3.json);
        sinon.assert.calledWith(res3.json, sinon.match([{
                "domainId": "no1",
                "nome": "ano8",
                "latitude": 40,
                "longitude": 64,
                "nomeCurto": "gome",
                "isEstacaoRecolha": true,
                "isPontoRendicao": false,
                "tempoViagem": [{
                        "noId": "no1",
                        "duracao": "123"
                    }]
            }, {
                "domainId": "no2",
                "nome": "no222",
                "latitude": 41,
                "longitude": 63,
                "nomeCurto": "gome2",
                "isEstacaoRecolha": true,
                "isPontoRendicao": true,
                "tempoViagem": [{
                        "noId": "no2",
                        "duracao": "1223"
                    }]
            }]));
    });
});
// import * as sinon from 'sinon';
// import { mocked } from 'ts-jest/utils'
// import { Response, Request, NextFunction } from 'express';
// import { Container } from 'nodi';
// import config from "../../../../config";
// import { Result } from '../../../core/logic/Result';
// import ITipoTripulanteService from "../../../services/IServices/ITipoTripulanteService";
// import tipoTripulanteService from "../../../services/tipoTripulanteService";
// import tipoTripulanteRepo from "../../../repos/tipoTripulanteRepo";
// import TipoTripulanteController from "../../../controllers/tipoTripulanteController";
// import ITipoTripulanteController from "../../../controllers/IControllers/ITipoTripulanteController";
// import ITipoTripulanteDTO from '../../../dto/ITipoTripulanteDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';
// jest.mock("../../../services/tipoTripulanteService", () => {
// 	const serv = {
// 		createTipoTripulante: jest.fn()
// 	  };
// 	  return { TipoTripulanteService: jest.fn(() => serv) };
// 	});
// describe('tipo tripulante controller', () => {
// 	// const MockedServ = mocked(tipoTripulanteService, true);
// 	// ;
// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;
// 	// });
// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;
// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
// 	// 	const controller = new TipoTripulanteController(MockedServ.protono);
// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);
// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;
// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };
// 	// 	// mockedFoo.protono.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);
// 	// 	// const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.protono.createTipoTripulante.mock.calls).toHaveLength(1);
// 	//});
// 	// const m = {name: "TipoTripulanteController", path: "../controllers/tipoTripulanteController"};
// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	require('../../../app');
// 	// 	console.log(Container);
// 	// 	const ctrl = Container.get(config.controller.tipoTripulante.name) as ITipoTripulanteController;
// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;
// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
// 	// 	 ctrl.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);
// 	// 	 const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);
// 	// }
// 	// 	);});
//# sourceMappingURL=noController.test.js.map