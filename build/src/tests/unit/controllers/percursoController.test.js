"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const typedi_1 = require("typedi");
const config_1 = __importDefault(require("../../../../config"));
const Result_1 = require("../../../core/logic/Result");
const percursoController_1 = __importDefault(require("../../../controllers/percursoController"));
describe('Percurso controller', function () {
    beforeEach(function () {
    });
    let percursoSchemaClass = require("../../../persistence/schemas/percursoSchema").default;
    typedi_1.Container.set('percursoSchema', percursoSchemaClass);
    let percursoRepoClass = require("../../../repos/percursoRepo").default;
    let percursoRepoInstance = typedi_1.Container.get(percursoRepoClass);
    typedi_1.Container.set(config_1.default.repos.percurso.name, percursoRepoInstance);
    let percursoServiceClass = require("../../../services/percursoService").default;
    let percursoServiceInstance = typedi_1.Container.get(percursoServiceClass);
    typedi_1.Container.set(config_1.default.services.percurso.name, percursoServiceInstance);
    it('createpercurso: returns json with percurso components', async function () {
        let body = {
            "domainId": "Path:1",
            "nosPercurso": [
                { "noId": "no3", "duracao": 34, "distancia": 45 },
                { "noId": "no4", "duracao": 34, "distancia": 45 }
            ]
        };
        let req = {};
        req.body = body;
        let res = {
            json: sinon.spy(),
            status: function (s) { this.statusCode = s; return this; }
        };
        let next = () => { };
        percursoServiceInstance = typedi_1.Container.get(config_1.default.services.percurso.name);
        sinon.stub(percursoServiceInstance, "createPercurso").returns(Result_1.Result.ok({
            "domainId": req.body.domainId,
            "nosPercurso": req.body.nosPercurso
        }));
        const ctrl = new percursoController_1.default(percursoServiceInstance);
        await ctrl.createPercurso(req, res, next);
        sinon.assert.calledOnce(res.json);
        sinon.assert.calledWith(res.json, sinon.match(body));
    });
    it('listPercursos: returns json with array of percursos', async function () {
        let body = {
            "domainId": "Path:1",
            "nosPercurso": []
        };
        let body2 = {
            "domainId": "Path:2",
            "nosPercurso": []
        };
        let req = {};
        let req2 = {};
        let req3 = {};
        req.body = body;
        req2.body = body2;
        let res = {};
        let res2 = {};
        let res3 = {
            json: sinon.spy()
        };
        let next = () => { };
        percursoServiceInstance = typedi_1.Container.get(config_1.default.services.percurso.name);
        sinon.stub(percursoServiceInstance, "listPercursos").returns(Result_1.Result.ok([{
                "domainId": req.body.domainId,
                "nosPercurso": req.body.nosPercurso
            },
            {
                "domainId": req2.body.domainId,
                "nosPercurso": req2.body.nosPercurso
            }]));
        const ctrl = new percursoController_1.default(percursoServiceInstance);
        await ctrl.createPercurso(req, res, next);
        await ctrl.createPercurso(req2, res2, next);
        await ctrl.listPercursos(req3, res3, next);
        sinon.assert.calledOnce(res3.json);
        sinon.assert.calledWith(res3.json, sinon.match([{
                "domainId": "Path:1",
                "nosPercurso": []
            }, {
                "domainId": "Path:2",
                "nosPercurso": []
            }]));
    });
});
// import * as sipercurson from 'sipercurson';
// import { mocked } from 'ts-jest/utils'
// import { Response, Request, NextFunction } from 'express';
// import { Container } from 'percursodi';
// import config from "../../../../config";
// import { Result } from '../../../core/logic/Result';
// import ITipoTripulanteService from "../../../services/IServices/ITipoTripulanteService";
// import tipoTripulanteService from "../../../services/tipoTripulanteService";
// import tipoTripulanteRepo from "../../../repos/tipoTripulanteRepo";
// import TipoTripulanteController from "../../../controllers/tipoTripulanteController";
// import ITipoTripulanteController from "../../../controllers/IControllers/ITipoTripulanteController";
// import ITipoTripulanteDTO from '../../../dto/ITipoTripulanteDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';
// jest.mock("../../../services/tipoTripulanteService", () => {
// 	const serv = {
// 		createTipoTripulante: jest.fn()
// 	  };
// 	  return { TipoTripulanteService: jest.fn(() => serv) };
// 	});
// describe('tipo tripulante controller', () => {
// 	// const MockedServ = mocked(tipoTripulanteService, true);
// 	// ;
// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;
// 	// });
// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;
// 	// 	let res: Partial<Response> = {
// 	// 		json: sipercurson.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
// 	// 	const controller = new TipoTripulanteController(MockedServ.protopercurso);
// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);
// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;
// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sipercurson.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };
// 	// 	// mockedFoo.protopercurso.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);
// 	// 	// const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.protopercurso.createTipoTripulante.mock.calls).toHaveLength(1);
// 	//});
// 	// const m = {name: "TipoTripulanteController", path: "../controllers/tipoTripulanteController"};
// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	require('../../../app');
// 	// 	console.log(Container);
// 	// 	const ctrl = Container.get(config.controller.tipoTripulante.name) as ITipoTripulanteController;
// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;
// 	//  	let res: Partial<Response> = {
// 	//  		json: sipercurson.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
// 	// 	 ctrl.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);
// 	// 	 const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);
// 	// }
// 	// 	);});
//# sourceMappingURL=percursoController.test.js.map