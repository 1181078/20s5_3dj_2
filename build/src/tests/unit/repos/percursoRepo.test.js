"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const typedi_1 = require("typedi");
const Result_1 = require("../../../core/logic/Result");
const percursoRepo_1 = __importDefault(require("../../../repos/percursoRepo"));
const percurso_1 = require("../../../domain/percurso/percurso");
const nosPercurso_1 = require("../../../domain/percurso/nosPercurso");
describe('tipo tripulante repo', function () {
    beforeEach(function () {
    });
    let percursoSchemaClass = require("../../../persistence/schemas/percursoSchema").default;
    typedi_1.Container.set('percursoSchema', percursoSchemaClass);
    it('save: returns json with description value', async function () {
        const bodyT = [nosPercurso_1.NosPercurso.create({ noId: "no1", duracao: 34, distancia: 45 }).getValue(), nosPercurso_1.NosPercurso.create({ noId: "no7", duracao: 34, distancia: 45 }).getValue()];
        const body = {
            "domainId": "Path:1",
            "nosPercurso": bodyT
        };
        const body2 = {
            "domainId": "Path:1",
            "nosPercurso": bodyT
        };
        const resu = percurso_1.Percurso.create(body).getValue();
        const newS = typedi_1.Container.get("percursoSchema");
        sinon.stub(newS, "findOne").returns(Result_1.Result.ok(body2));
        sinon.stub(newS, "create").returns(Result_1.Result.ok(resu));
        const repo = new percursoRepo_1.default(newS);
        const res = await repo.save(resu);
        sinon.assert.match(res, resu);
    });
    it('listPercursos: returns json with array of types', async function () {
        const bodyT = [nosPercurso_1.NosPercurso.create({ noId: "no1", duracao: 34, distancia: 45 }).getValue(), nosPercurso_1.NosPercurso.create({ noId: "no7", duracao: 34, distancia: 45 }).getValue()];
        const bodyT2 = [nosPercurso_1.NosPercurso.create({ noId: "no2", duracao: 34, distancia: 45 }).getValue()];
        const bodyT3 = [nosPercurso_1.NosPercurso.create({ noId: "no3", duracao: 34, distancia: 45 }).getValue()];
        const body = {
            "domainId": "Path:1",
            "nosPercurso": bodyT
        };
        const body2 = {
            "domainId": "Path:2",
            "nosPercurso": bodyT2
        };
        const body3 = {
            "domainId": "Path:3",
            "nosPercurso": bodyT3
        };
        const bodyb = {
            "domainId": "Path:1",
            "nosPercurso": bodyT
        };
        const body2b = {
            "domainId": "Path:2",
            "nosPercurso": bodyT2
        };
        const body3b = {
            "domainId": "Path:3",
            "nosPercurso": bodyT3
        };
        const resu = percurso_1.Percurso.create(body).getValue();
        const resu2 = percurso_1.Percurso.create(body2).getValue();
        const resu3 = percurso_1.Percurso.create(body3).getValue();
        const arr = [bodyb, body2b, body3b];
        const resf = [resu, resu2, resu3];
        const newS = typedi_1.Container.get("percursoSchema");
        sinon.stub(newS, "find").returns(Result_1.Result.ok(arr));
        const repo = new percursoRepo_1.default(newS);
        const res = await repo.findAll();
        sinon.assert.match(resf, resf);
    });
    it('listPercursos: returns json with sorted array of types', async function () {
        const bodyT = [nosPercurso_1.NosPercurso.create({ noId: "no1", duracao: 34, distancia: 45 }).getValue(), nosPercurso_1.NosPercurso.create({ noId: "no7", duracao: 34, distancia: 45 }).getValue()];
        const bodyT2 = [nosPercurso_1.NosPercurso.create({ noId: "no2", duracao: 34, distancia: 45 }).getValue()];
        const bodyT3 = [nosPercurso_1.NosPercurso.create({ noId: "no3", duracao: 34, distancia: 45 }).getValue()];
        const body = {
            "domainId": "Path:1",
            "nosPercurso": bodyT
        };
        const body2 = {
            "domainId": "Path:2",
            "nosPercurso": bodyT2
        };
        const body3 = {
            "domainId": "Path:3",
            "nosPercurso": bodyT3
        };
        const bodyb = {
            "domainId": "Path:1",
            "nosPercurso": bodyT
        };
        const body2b = {
            "domainId": "Path:2",
            "nosPercurso": bodyT2
        };
        const body3b = {
            "domainId": "Path:3",
            "nosPercurso": bodyT3
        };
        const resu = percurso_1.Percurso.create(body).getValue();
        const resu2 = percurso_1.Percurso.create(body2).getValue();
        const resu3 = percurso_1.Percurso.create(body3).getValue();
        const arr = [bodyb, body2b, body3b];
        const resf = [resu2, resu3, resu];
        const newS = typedi_1.Container.get("percursoSchema");
        //sinon.stub(newS, "findAllSorted").returns( Result.ok<IPercursoPersistence>(arr));
        const repo = new percursoRepo_1.default(newS);
        const res = await repo.findAll();
        sinon.assert.match(resf, resf);
    });
});
// import * as sinon from 'sinon';
// import { mocked } from 'ts-jest/utils'
// import { Response, Request, NextFunction } from 'express';
// import { Container } from 'typedi';
// import config from "../../../../config";
// import { Result } from '../../../core/logic/Result';
// import IPercursoService from "../../../services/IServices/IPercursoService";
// import percursoService from "../../../services/percursoService";
// import percursoRepo from "../../../repos/percursoRepo";
// import PercursoController from "../../../controllers/percursoController";
// import IPercursoController from "../../../controllers/IControllers/IPercursoController";
// import IPercursoDTO from '../../../dto/IPercursoDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';
// jest.mock("../../../services/percursoService", () => {
// 	const serv = {
// 		createPercurso: jest.fn()
// 	  };
// 	  return { PercursoService: jest.fn(() => serv) };
// 	});
// describe('tipo tripulante controller', () => {
// 	// const MockedServ = mocked(percursoService, true);
// 	// ;
// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;
// 	// });
// 	// it('createPercurso: returns json with description value', async function () {
// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;
// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
// 	// 	const controller = new PercursoController(MockedServ.prototype);
// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);
// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;
// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };
// 	// 	// mockedFoo.prototype.createPercurso(<Request>req, <Response>res, <NextFunction>next);
// 	// 	// const exp: IPercursoDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createPercurso.mock.calls).toHaveLength(1);
// 	//});
// 	// const m = {name: "PercursoController", path: "../controllers/percursoController"};
// 	// it('createPercurso: returns json with description value', async function () {
// 	// 	require('../../../app');
// 	// 	console.log(Container);
// 	// 	const ctrl = Container.get(config.controller.percurso.name) as IPercursoController;
// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;
// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
// 	// 	 ctrl.createPercurso(<Request>req, <Response>res, <NextFunction>next);
// 	// 	 const exp: IPercursoDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);
// 	// }
// 	// 	);});
//# sourceMappingURL=percursoRepo.test.js.map