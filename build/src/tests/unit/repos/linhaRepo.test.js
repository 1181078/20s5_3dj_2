"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const typedi_1 = require("typedi");
const Result_1 = require("../../../core/logic/Result");
const linhaRepo_1 = __importDefault(require("../../../repos/linhaRepo"));
const linha_1 = require("../../../domain/linha/linha");
const percursoLinha_1 = require("../../../domain/linha/percursoLinha");
describe('tipo tripulante repo', function () {
    beforeEach(function () {
    });
    let linhaSchemaClass = require("../../../persistence/schemas/linhaSchema").default;
    typedi_1.Container.set('linhaSchema', linhaSchemaClass);
    it('save: returns json with description value', async function () {
        const bodyT = [percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "71123", "percurso": "a", "orientation": "b" }).getValue(),
            percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "1234", "percurso": "a4", "orientation": "b4" }).getValue()];
        const body = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT
        };
        const bodyb = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT
        };
        const resu = linha_1.Linha.create(body).getValue();
        const newS = typedi_1.Container.get("linhaSchema");
        sinon.stub(newS, "findOne").returns(Result_1.Result.ok(bodyb));
        sinon.stub(newS, "create").returns(Result_1.Result.ok(resu));
        const repo = new linhaRepo_1.default(newS);
        const res = await repo.save(resu);
        sinon.assert.match(res, resu);
    });
    it('listLinhas: returns json with array of types', async function () {
        const bodyT = [percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "71123", "percurso": "a", "orientation": "b" }).getValue(),
            percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "1234", "percurso": "a4", "orientation": "b4" }).getValue()];
        const body = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT
        };
        const bodyb = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT
        };
        const bodyT2 = [percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "71123", "percurso": "a", "orientation": "b" }).getValue(),
            percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "1234", "percurso": "a4", "orientation": "b4" }).getValue()];
        const body2 = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT2
        };
        const body2b = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT2
        };
        const bodyT3 = [percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "71123", "percurso": "a", "orientation": "b" }).getValue(),
            percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "1234", "percurso": "a4", "orientation": "b4" }).getValue()];
        const body3 = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT3
        };
        const body3b = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT3
        };
        const resu = linha_1.Linha.create(body).getValue();
        const resu2 = linha_1.Linha.create(body2).getValue();
        const resu3 = linha_1.Linha.create(body3).getValue();
        const arr = [bodyb, body2b, body3b];
        const resf = [resu, resu2, resu3];
        const newS = typedi_1.Container.get("linhaSchema");
        sinon.stub(newS, "find").returns(Result_1.Result.ok(arr));
        const repo = new linhaRepo_1.default(newS);
        const res = await repo.findAll();
        sinon.assert.match(resf, resf);
    });
    it('listLinhas: returns json with sorted array of types', async function () {
        const bodyT = [percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "71123", "percurso": "a", "orientation": "b" }).getValue(),
            percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "1234", "percurso": "a4", "orientation": "b4" }).getValue()];
        const body = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT
        };
        const bodyb = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT
        };
        const bodyT2 = [percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "71123", "percurso": "a", "orientation": "b" }).getValue(),
            percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "1234", "percurso": "a4", "orientation": "b4" }).getValue()];
        const body2 = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT2
        };
        const body2b = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT2
        };
        const bodyT3 = [percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "71123", "percurso": "a", "orientation": "b" }).getValue(),
            percursoLinha_1.PercursoLinha.create({ "percursoLinhaId": "1234", "percurso": "a4", "orientation": "b4" }).getValue()];
        const body3 = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT3
        };
        const body3b = {
            "domainId": "nl:7512341ee",
            "nome": "newlineTest7532341",
            "cor": "rrr",
            "percursoLinha": bodyT3
        };
        const resu = linha_1.Linha.create(body).getValue();
        const resu2 = linha_1.Linha.create(body2).getValue();
        const resu3 = linha_1.Linha.create(body3).getValue();
        const arr = [bodyb, body2b, body3b];
        const resf = [resu2, resu3, resu];
        const newS = typedi_1.Container.get("linhaSchema");
        //sinon.stub(newS, "findAllSorted").returns( Result.ok<ILinhaPersistence>(arr));
        const repo = new linhaRepo_1.default(newS);
        const res = await repo.findAll();
        sinon.assert.match(resf, resf);
    });
});
// import * as sinon from 'sinon';
// import { mocked } from 'ts-jest/utils'
// import { Response, Request, NextFunction } from 'express';
// import { Container } from 'typedi';
// import config from "../../../../config";
// import { Result } from '../../../core/logic/Result';
// import ILinhaService from "../../../services/IServices/ILinhaService";
// import linhaService from "../../../services/linhaService";
// import linhaRepo from "../../../repos/linhaRepo";
// import LinhaController from "../../../controllers/linhaController";
// import ILinhaController from "../../../controllers/IControllers/ILinhaController";
// import ILinhaDTO from '../../../dto/ILinhaDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';
// jest.mock("../../../services/linhaService", () => {
// 	const serv = {
// 		createLinha: jest.fn()
// 	  };
// 	  return { LinhaService: jest.fn(() => serv) };
// 	});
// describe('tipo tripulante controller', () => {
// 	// const MockedServ = mocked(linhaService, true);
// 	// ;
// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;
// 	// });
// 	// it('createLinha: returns json with description value', async function () {
// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;
// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
// 	// 	const controller = new LinhaController(MockedServ.prototype);
// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);
// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;
// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };
// 	// 	// mockedFoo.prototype.createLinha(<Request>req, <Response>res, <NextFunction>next);
// 	// 	// const exp: ILinhaDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createLinha.mock.calls).toHaveLength(1);
// 	//});
// 	// const m = {name: "LinhaController", path: "../controllers/linhaController"};
// 	// it('createLinha: returns json with description value', async function () {
// 	// 	require('../../../app');
// 	// 	console.log(Container);
// 	// 	const ctrl = Container.get(config.controller.linha.name) as ILinhaController;
// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;
// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
// 	// 	 ctrl.createLinha(<Request>req, <Response>res, <NextFunction>next);
// 	// 	 const exp: ILinhaDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);
// 	// }
// 	// 	);});
//# sourceMappingURL=linhaRepo.test.js.map