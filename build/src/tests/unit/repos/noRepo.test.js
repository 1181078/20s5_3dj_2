"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const typedi_1 = require("typedi");
const Result_1 = require("../../../core/logic/Result");
const noRepo_1 = __importDefault(require("../../../repos/noRepo"));
const no_1 = require("../../../domain/no/no");
const tempoViagemGrupo_1 = require("../../../domain/no/tempoViagemGrupo");
describe('tipo tripulante repo', function () {
    beforeEach(function () {
    });
    let noSchemaClass = require("../../../persistence/schemas/noSchema").default;
    typedi_1.Container.set('noSchema', noSchemaClass);
    it('save: returns json with description value', async function () {
        const bodyT = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no1",
            "duracao": 123
        });
        const body = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT.getValue()]
        };
        const bodyb = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT.getValue()]
        };
        const resu = no_1.No.create(body).getValue();
        const newS = typedi_1.Container.get("noSchema");
        sinon.stub(newS, "findOne").returns(Result_1.Result.ok(bodyb));
        sinon.stub(newS, "create").returns(Result_1.Result.ok(resu));
        const repo = new noRepo_1.default(newS);
        const res = await repo.save(resu);
        sinon.assert.match(res, resu);
    });
    it('listNos: returns json with array of types', async function () {
        const bodyT = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no1",
            "duracao": 123
        });
        const body = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT.getValue()]
        };
        const bodyb = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT.getValue()]
        };
        const bodyT2 = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no2",
            "duracao": 123
        });
        const body2 = {
            "domainId": "no2",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT2.getValue()]
        };
        const body2b = {
            "domainId": "no2",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT2.getValue()]
        };
        const bodyT3 = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no1",
            "duracao": 123
        });
        const body3 = {
            "domainId": "no3",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT3.getValue()]
        };
        const body3b = {
            "domainId": "no3",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT3.getValue()]
        };
        const resu = no_1.No.create(body).getValue();
        const resu2 = no_1.No.create(body2).getValue();
        const resu3 = no_1.No.create(body3).getValue();
        const arr = [bodyb, body2b, body3b];
        const resf = [resu, resu2, resu3];
        const newS = typedi_1.Container.get("noSchema");
        sinon.stub(newS, "find").returns(Result_1.Result.ok(arr));
        const repo = new noRepo_1.default(newS);
        const res = await repo.findAll();
        sinon.assert.match(resf, resf);
    });
    it('listNos: returns json with sorted array of types', async function () {
        const bodyT = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no1",
            "duracao": 123
        });
        const body = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT.getValue()]
        };
        const bodyb = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT.getValue()]
        };
        const bodyT2 = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no2",
            "duracao": 123
        });
        const body2 = {
            "domainId": "no2",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT2.getValue()]
        };
        const body2b = {
            "domainId": "no2",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT2.getValue()]
        };
        const bodyT3 = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no1",
            "duracao": 123
        });
        const body3 = {
            "domainId": "no3",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT3.getValue()]
        };
        const body3b = {
            "domainId": "no3",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT3.getValue()]
        };
        const resu = no_1.No.create(body).getValue();
        const resu2 = no_1.No.create(body2).getValue();
        const resu3 = no_1.No.create(body3).getValue();
        const arr = [bodyb, body2b, body3b];
        const resf = [resu2, resu3, resu];
        const newS = typedi_1.Container.get("noSchema");
        //sinon.stub(newS, "findAllSorted").returns( Result.ok<INoPersistence>(arr));
        const repo = new noRepo_1.default(newS);
        const res = await repo.findAll();
        sinon.assert.match(resf, resf);
    });
});
// import * as sinon from 'sinon';
// import { mocked } from 'ts-jest/utils'
// import { Response, Request, NextFunction } from 'express';
// import { Container } from 'typedi';
// import config from "../../../../config";
// import { Result } from '../../../core/logic/Result';
// import INoService from "../../../services/IServices/INoService";
// import noService from "../../../services/noService";
// import noRepo from "../../../repos/noRepo";
// import NoController from "../../../controllers/noController";
// import INoController from "../../../controllers/IControllers/INoController";
// import INoDTO from '../../../dto/INoDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';
// jest.mock("../../../services/noService", () => {
// 	const serv = {
// 		createNo: jest.fn()
// 	  };
// 	  return { NoService: jest.fn(() => serv) };
// 	});
// describe('tipo tripulante controller', () => {
// 	// const MockedServ = mocked(noService, true);
// 	// ;
// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;
// 	// });
// 	// it('createNo: returns json with description value', async function () {
// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;
// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
// 	// 	const controller = new NoController(MockedServ.prototype);
// 	// 	console.log(controller.listNos(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);
// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;
// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };
// 	// 	// mockedFoo.prototype.createNo(<Request>req, <Response>res, <NextFunction>next);
// 	// 	// const exp: INoDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createNo.mock.calls).toHaveLength(1);
// 	//});
// 	// const m = {name: "NoController", path: "../controllers/noController"};
// 	// it('createNo: returns json with description value', async function () {
// 	// 	require('../../../app');
// 	// 	console.log(Container);
// 	// 	const ctrl = Container.get(config.controller.no.name) as INoController;
// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;
// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
// 	// 	 ctrl.createNo(<Request>req, <Response>res, <NextFunction>next);
// 	// 	 const exp: INoDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);
// 	// }
// 	// 	);});
//# sourceMappingURL=noRepo.test.js.map