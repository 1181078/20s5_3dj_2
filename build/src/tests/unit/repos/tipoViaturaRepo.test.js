"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const typedi_1 = require("typedi");
const Result_1 = require("../../../core/logic/Result");
const tipoViaturaRepo_1 = __importDefault(require("../../../repos/tipoViaturaRepo"));
const tipoViatura_1 = require("../../../domain/tipoViatura/tipoViatura");
describe('tipo viatura repo', function () {
    beforeEach(function () {
    });
    let tipoViaturaSchemaClass = require("../../../persistence/schemas/tipoViaturaSchema").default;
    typedi_1.Container.set('tipoViaturaSchema', tipoViaturaSchemaClass);
    it('save: returns json with description value', async function () {
        const body = {
            "domainId": "v:123",
            "nome": "civiat123",
            "autonomia": 151,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const body2 = {
            "domainId": "v:123",
            "nome": "civiat123",
            "autonomia": 151,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const resu = tipoViatura_1.TipoViatura.create(body).getValue();
        const newS = typedi_1.Container.get("tipoViaturaSchema");
        sinon.stub(newS, "findOne").returns(Result_1.Result.ok(body2));
        sinon.stub(newS, "create").returns(Result_1.Result.ok(resu));
        const repo = new tipoViaturaRepo_1.default(newS);
        const res = await repo.save(resu);
        sinon.assert.match(res, resu);
    });
    it('listTipoViaturas: returns json with array of types', async function () {
        const body = {
            "domainId": "v:123",
            "nome": "civiat123",
            "autonomia": 151,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const body2 = {
            "domainId": "v:456",
            "nome": "civiat456",
            "autonomia": 120,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const body3 = {
            "domainId": "v:789",
            "nome": "civiat789",
            "autonomia": 130,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const bodyb = {
            "domainId": "v:123",
            "nome": "civiat123",
            "autonomia": 151,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const body2b = {
            "domainId": "v:456",
            "nome": "civiat456",
            "autonomia": 120,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const body3b = {
            "domainId": "v:789",
            "nome": "civiat789",
            "autonomia": 130,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const resu = tipoViatura_1.TipoViatura.create(body).getValue();
        const resu2 = tipoViatura_1.TipoViatura.create(body2).getValue();
        const resu3 = tipoViatura_1.TipoViatura.create(body3).getValue();
        const arr = [bodyb, body2b, body3b];
        const resf = [resu, resu2, resu3];
        const newS = typedi_1.Container.get("tipoViaturaSchema");
        sinon.stub(newS, "find").returns(Result_1.Result.ok(arr));
        const repo = new tipoViaturaRepo_1.default(newS);
        const res = await repo.findAll();
        sinon.assert.match(resf, resf);
    });
    it('listTipoViaturas: returns json with sorted array of types', async function () {
        const body = {
            "domainId": "v:123",
            "nome": "civiat123",
            "autonomia": 151,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const body2 = {
            "domainId": "v:456",
            "nome": "civiat456",
            "autonomia": 120,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const body3 = {
            "domainId": "v:789",
            "nome": "civiat789",
            "autonomia": 130,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const bodyb = {
            "domainId": "v:123",
            "nome": "civiat123",
            "autonomia": 151,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const body2b = {
            "domainId": "v:456",
            "nome": "civiat456",
            "autonomia": 120,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const body3b = {
            "domainId": "v:789",
            "nome": "civiat789",
            "autonomia": 130,
            "custo": 23,
            "velocidadeMedia": 100,
            "combustivel": 23,
            "consumo": 120,
            "emissoes": 330
        };
        const resu = tipoViatura_1.TipoViatura.create(body).getValue();
        const resu2 = tipoViatura_1.TipoViatura.create(body2).getValue();
        const resu3 = tipoViatura_1.TipoViatura.create(body3).getValue();
        const arr = [bodyb, body2b, body3b];
        const resf = [resu2, resu3, resu];
        const newS = typedi_1.Container.get("tipoViaturaSchema");
        //sinon.stub(newS, "findAllSorted").returns( Result.ok<ITipoViaturaPersistence>(arr));
        const repo = new tipoViaturaRepo_1.default(newS);
        const res = await repo.findAll();
        sinon.assert.match(resf, resf);
    });
    // it('listTipoViaturas: returns json with array of types', async function () {
    //     const body = {"domainId":"12345", "desc":'test_desc' } as ITipoViaturaDTO;
    //     const body2 = {"domainId":"123456", "desc":'test_desc2' } as ITipoViaturaDTO;
    //     const body3 = {"domainId":"123457", "desc":'test_desc3' } as ITipoViaturaDTO;
    // 	const resu = TipoViatura.create(body).getValue();
    // 	const resu2 = TipoViatura.create(body2).getValue();
    // 	const resu3 = TipoViatura.create(body3).getValue();
    // 	tipoViaturaRepoInstance = Container.get(config.repos.tipoViatura.name);
    // 	sinon.stub(tipoViaturaRepoInstance, "findAll").returns( [resu, resu2, resu3]);
    // 	const serv = new TipoViaturaService(tipoViaturaRepoInstance as ITipoViaturaRepo);
    // 	serv.createTipoViatura(body);
    // 	serv.createTipoViatura(body2);
    // 	serv.createTipoViatura(body3);
    // 	const res = await (await serv.listTiposViatura()).getValue();
    // 	sinon.assert.match(res, [body, body2, body3]);
    // });
    // it('listTipoViaturas: returns json with sorted array of types', async function () {
    //     const body = {"domainId":"12345", "desc":'btest_desc' } as ITipoViaturaDTO;
    //     const body2 = {"domainId":"123456", "desc":'atest_desc2' } as ITipoViaturaDTO;
    //     const body3 = {"domainId":"123457", "desc":'ctest_desc3' } as ITipoViaturaDTO;
    // 	const resu = TipoViatura.create(body).getValue();
    // 	const resu2 = TipoViatura.create(body2).getValue();
    // 	const resu3 = TipoViatura.create(body3).getValue();
    // 	tipoViaturaRepoInstance = Container.get(config.repos.tipoViatura.name);
    // 	sinon.stub(tipoViaturaRepoInstance, "findAllSorted").returns( [resu2, resu, resu3]);
    // 	const serv = new TipoViaturaService(tipoViaturaRepoInstance as ITipoViaturaRepo);
    // 	serv.createTipoViatura(body);
    // 	serv.createTipoViatura(body2);
    // 	serv.createTipoViatura(body3);
    // 	const res = await (await serv.listTiposViaturaSorted("desc")).getValue();
    // 	sinon.assert.match(res, [body2, body, body3]);
    // });
});
// import * as sinon from 'sinon';
// import { mocked } from 'ts-jest/utils'
// import { Response, Request, NextFunction } from 'express';
// import { Container } from 'typedi';
// import config from "../../../../config";
// import { Result } from '../../../core/logic/Result';
// import ITipoViaturaService from "../../../services/IServices/ITipoViaturaService";
// import tipoViaturaService from "../../../services/tipoViaturaService";
// import tipoViaturaRepo from "../../../repos/tipoViaturaRepo";
// import TipoViaturaController from "../../../controllers/tipoViaturaController";
// import ITipoViaturaController from "../../../controllers/IControllers/ITipoViaturaController";
// import ITipoViaturaDTO from '../../../dto/ITipoViaturaDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';
// jest.mock("../../../services/tipoViaturaService", () => {
// 	const serv = {
// 		createTipoViatura: jest.fn()
// 	  };
// 	  return { TipoViaturaService: jest.fn(() => serv) };
// 	});
// describe('tipo viatura controller', () => {
// 	// const MockedServ = mocked(tipoViaturaService, true);
// 	// ;
// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;
// 	// });
// 	// it('createTipoViatura: returns json with description value', async function () {
// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;
// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
// 	// 	const controller = new TipoViaturaController(MockedServ.prototype);
// 	// 	console.log(controller.listTiposViatura(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);
// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;
// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };
// 	// 	// mockedFoo.prototype.createTipoViatura(<Request>req, <Response>res, <NextFunction>next);
// 	// 	// const exp: ITipoViaturaDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createTipoViatura.mock.calls).toHaveLength(1);
// 	//});
// 	// const m = {name: "TipoViaturaController", path: "../controllers/tipoViaturaController"};
// 	// it('createTipoViatura: returns json with description value', async function () {
// 	// 	require('../../../app');
// 	// 	console.log(Container);
// 	// 	const ctrl = Container.get(config.controller.tipoViatura.name) as ITipoViaturaController;
// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;
// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
// 	// 	 ctrl.createTipoViatura(<Request>req, <Response>res, <NextFunction>next);
// 	// 	 const exp: ITipoViaturaDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);
// 	// }
// 	// 	);});
//# sourceMappingURL=tipoViaturaRepo.test.js.map