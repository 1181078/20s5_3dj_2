"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const typedi_1 = require("typedi");
const config_1 = __importDefault(require("../../../../config"));
const Result_1 = require("../../../core/logic/Result");
const tipoTripulanteService_1 = __importDefault(require("../../../services/tipoTripulanteService"));
const tipoTripulante_1 = require("../../../domain/tipoTripulante/tipoTripulante");
describe('tipo tripulante service', function () {
    beforeEach(function () {
    });
    let tipoTripulanteSchemaClass = require("../../../persistence/schemas/tipoTripulanteSchema").default;
    typedi_1.Container.set('tipoTripulanteSchema', tipoTripulanteSchemaClass);
    let tipoTripulanteRepoClass = require("../../../repos/tipoTripulanteRepo").default;
    let tipoTripulanteRepoInstance = typedi_1.Container.get(tipoTripulanteRepoClass);
    typedi_1.Container.set(config_1.default.repos.tipoTripulante.name, tipoTripulanteRepoInstance);
    it('createTipoTripulante: returns json with description value', async function () {
        const body = { "domainId": "12345", "desc": 'test_desc' };
        const resu = tipoTripulante_1.TipoTripulante.create(body).getValue();
        tipoTripulanteRepoInstance = typedi_1.Container.get(config_1.default.repos.tipoTripulante.name);
        sinon.stub(tipoTripulanteRepoInstance, "save").returns(Result_1.Result.ok(resu));
        const serv = new tipoTripulanteService_1.default(tipoTripulanteRepoInstance);
        const res = await (await serv.createTipoTripulante(body)).getValue();
        sinon.assert.match(res, body);
    });
    it('listTipoTripulantes: returns json with array of types', async function () {
        const body = { "domainId": "12345", "desc": 'test_desc' };
        const body2 = { "domainId": "123456", "desc": 'test_desc2' };
        const body3 = { "domainId": "123457", "desc": 'test_desc3' };
        const resu = tipoTripulante_1.TipoTripulante.create(body).getValue();
        const resu2 = tipoTripulante_1.TipoTripulante.create(body2).getValue();
        const resu3 = tipoTripulante_1.TipoTripulante.create(body3).getValue();
        tipoTripulanteRepoInstance = typedi_1.Container.get(config_1.default.repos.tipoTripulante.name);
        sinon.stub(tipoTripulanteRepoInstance, "findAll").returns([resu, resu2, resu3]);
        const serv = new tipoTripulanteService_1.default(tipoTripulanteRepoInstance);
        serv.createTipoTripulante(body);
        serv.createTipoTripulante(body2);
        serv.createTipoTripulante(body3);
        const res = await (await serv.listTiposTripulante()).getValue();
        sinon.assert.match(res, [body, body2, body3]);
    });
    it('listTipoTripulantes: returns json with sorted array of types', async function () {
        const body = { "domainId": "12345", "desc": 'btest_desc' };
        const body2 = { "domainId": "123456", "desc": 'atest_desc2' };
        const body3 = { "domainId": "123457", "desc": 'ctest_desc3' };
        const resu = tipoTripulante_1.TipoTripulante.create(body).getValue();
        const resu2 = tipoTripulante_1.TipoTripulante.create(body2).getValue();
        const resu3 = tipoTripulante_1.TipoTripulante.create(body3).getValue();
        tipoTripulanteRepoInstance = typedi_1.Container.get(config_1.default.repos.tipoTripulante.name);
        sinon.stub(tipoTripulanteRepoInstance, "findAllSorted").returns([resu2, resu, resu3]);
        const serv = new tipoTripulanteService_1.default(tipoTripulanteRepoInstance);
        serv.createTipoTripulante(body);
        serv.createTipoTripulante(body2);
        serv.createTipoTripulante(body3);
        const res = await (await serv.listTiposTripulanteSorted("desc")).getValue();
        sinon.assert.match(res, [body2, body, body3]);
    });
});
// import * as sinon from 'sinon';
// import { mocked } from 'ts-jest/utils'
// import { Response, Request, NextFunction } from 'express';
// import { Container } from 'typedi';
// import config from "../../../../config";
// import { Result } from '../../../core/logic/Result';
// import ITipoTripulanteService from "../../../services/IServices/ITipoTripulanteService";
// import tipoTripulanteService from "../../../services/tipoTripulanteService";
// import tipoTripulanteRepo from "../../../repos/tipoTripulanteRepo";
// import TipoTripulanteController from "../../../controllers/tipoTripulanteController";
// import ITipoTripulanteController from "../../../controllers/IControllers/ITipoTripulanteController";
// import ITipoTripulanteDTO from '../../../dto/ITipoTripulanteDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';
// jest.mock("../../../services/tipoTripulanteService", () => {
// 	const serv = {
// 		createTipoTripulante: jest.fn()
// 	  };
// 	  return { TipoTripulanteService: jest.fn(() => serv) };
// 	});
// describe('tipo tripulante controller', () => {
// 	// const MockedServ = mocked(tipoTripulanteService, true);
// 	// ;
// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;
// 	// });
// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;
// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
// 	// 	const controller = new TipoTripulanteController(MockedServ.prototype);
// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);
// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;
// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };
// 	// 	// mockedFoo.prototype.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);
// 	// 	// const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createTipoTripulante.mock.calls).toHaveLength(1);
// 	//});
// 	// const m = {name: "TipoTripulanteController", path: "../controllers/tipoTripulanteController"};
// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	require('../../../app');
// 	// 	console.log(Container);
// 	// 	const ctrl = Container.get(config.controller.tipoTripulante.name) as ITipoTripulanteController;
// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;
// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
// 	// 	 ctrl.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);
// 	// 	 const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);
// 	// }
// 	// 	);});
//# sourceMappingURL=tipoTripulanteService.test.js.map