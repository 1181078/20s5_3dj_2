"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const typedi_1 = require("typedi");
const config_1 = __importDefault(require("../../../../config"));
const Result_1 = require("../../../core/logic/Result");
const percursoService_1 = __importDefault(require("../../../services/percursoService"));
const percurso_1 = require("../../../domain/percurso/percurso");
const nosPercurso_1 = require("../../../domain/percurso/nosPercurso");
describe('tipo tripulante service', function () {
    beforeEach(function () {
    });
    let percursoSchemaClass = require("../../../persistence/schemas/percursoSchema").default;
    typedi_1.Container.set('percursoSchema', percursoSchemaClass);
    let percursoRepoClass = require("../../../repos/percursoRepo").default;
    let percursoRepoInstance = typedi_1.Container.get(percursoRepoClass);
    typedi_1.Container.set(config_1.default.repos.percurso.name, percursoRepoInstance);
    it('createPercurso: returns json with description value', async function () {
        const body = {
            "domainId": "Path:1",
            "nosPercurso": []
        };
        const resu = percurso_1.Percurso.create(body).getValue();
        percursoRepoInstance = typedi_1.Container.get(config_1.default.repos.percurso.name);
        sinon.stub(percursoRepoInstance, "save").returns(Result_1.Result.ok(resu));
        const serv = new percursoService_1.default(percursoRepoInstance);
        const res = await (await serv.createPercurso(body)).getValue();
        sinon.assert.match(res, body);
    });
    it('listPercursos: returns json with array of types', async function () {
        const bodyT = [nosPercurso_1.NosPercurso.create({ noId: "no1", duracao: 34, distancia: 45 }).getValue(), nosPercurso_1.NosPercurso.create({ noId: "no7", duracao: 34, distancia: 45 }).getValue()];
        const bodyT2 = [nosPercurso_1.NosPercurso.create({ noId: "no2", duracao: 34, distancia: 45 }).getValue()];
        const bodyT3 = [nosPercurso_1.NosPercurso.create({ noId: "no3", duracao: 34, distancia: 45 }).getValue()];
        const body = {
            "domainId": "Path:1",
            "nosPercurso": bodyT
        };
        const body2 = {
            "domainId": "Path:2",
            "nosPercurso": bodyT2
        };
        const body3 = {
            "domainId": "Path:3",
            "nosPercurso": bodyT3
        };
        const resu = percurso_1.Percurso.create(body).getValue();
        const resu2 = percurso_1.Percurso.create(body2).getValue();
        const resu3 = percurso_1.Percurso.create(body3).getValue();
        percursoRepoInstance = typedi_1.Container.get(config_1.default.repos.percurso.name);
        sinon.stub(percursoRepoInstance, "findAll").returns([resu, resu2, resu3]);
        const serv = new percursoService_1.default(percursoRepoInstance);
        serv.createPercurso(body);
        serv.createPercurso(body2);
        serv.createPercurso(body3);
        const res = await (await serv.listPercursos()).getValue();
        sinon.assert.match(res, [body, body2, body3]);
    });
});
// import * as sinon from 'sinon';
// import { mocked } from 'ts-jest/utils'
// import { Response, Request, NextFunction } from 'express';
// import { Container } from 'typedi';
// import config from "../../../../config";
// import { Result } from '../../../core/logic/Result';
// import IPercursoService from "../../../services/IServices/IPercursoService";
// import percursoService from "../../../services/percursoService";
// import percursoRepo from "../../../repos/percursoRepo";
// import PercursoController from "../../../controllers/percursoController";
// import IPercursoController from "../../../controllers/IControllers/IPercursoController";
// import IPercursoDTO from '../../../dto/IPercursoDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';
// jest.mock("../../../services/percursoService", () => {
// 	const serv = {
// 		createPercurso: jest.fn()
// 	  };
// 	  return { PercursoService: jest.fn(() => serv) };
// 	});
// describe('tipo tripulante controller', () => {
// 	// const MockedServ = mocked(percursoService, true);
// 	// ;
// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;
// 	// });
// 	// it('createPercurso: returns json with description value', async function () {
// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;
// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
// 	// 	const controller = new PercursoController(MockedServ.prototype);
// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);
// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;
// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };
// 	// 	// mockedFoo.prototype.createPercurso(<Request>req, <Response>res, <NextFunction>next);
// 	// 	// const exp: IPercursoDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createPercurso.mock.calls).toHaveLength(1);
// 	//});
// 	// const m = {name: "PercursoController", path: "../controllers/percursoController"};
// 	// it('createPercurso: returns json with description value', async function () {
// 	// 	require('../../../app');
// 	// 	console.log(Container);
// 	// 	const ctrl = Container.get(config.controller.percurso.name) as IPercursoController;
// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;
// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
// 	// 	 ctrl.createPercurso(<Request>req, <Response>res, <NextFunction>next);
// 	// 	 const exp: IPercursoDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);
// 	// }
// 	// 	);});
//# sourceMappingURL=percursoService.test.js.map