"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon = __importStar(require("sinon"));
const typedi_1 = require("typedi");
const config_1 = __importDefault(require("../../../../config"));
const Result_1 = require("../../../core/logic/Result");
const noService_1 = __importDefault(require("../../../services/noService"));
const no_1 = require("../../../domain/no/no");
const tempoViagemGrupo_1 = require("../../../domain/no/tempoViagemGrupo");
describe('tipo tripulante service', function () {
    beforeEach(function () {
    });
    let noSchemaClass = require("../../../persistence/schemas/noSchema").default;
    typedi_1.Container.set('noSchema', noSchemaClass);
    let noRepoClass = require("../../../repos/noRepo").default;
    let noRepoInstance = typedi_1.Container.get(noRepoClass);
    typedi_1.Container.set(config_1.default.repos.no.name, noRepoInstance);
    it('createNo: returns json with description value', async function () {
        const bodyT = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no1",
            "duracao": 123
        });
        const body = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT.getValue()]
        };
        const resu = no_1.No.create(body).getValue();
        noRepoInstance = typedi_1.Container.get(config_1.default.repos.no.name);
        sinon.stub(noRepoInstance, "save").returns(Result_1.Result.ok(resu));
        const serv = new noService_1.default(noRepoInstance);
        const res = await (await serv.createNo(body)).getValue();
        sinon.assert.match(res, body);
    });
    it('listNos: returns json with array of types', async function () {
        const bodyT = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no1",
            "duracao": 123
        });
        const body = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT.getValue()]
        };
        const bodyT2 = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no2",
            "duracao": 123
        });
        const body2 = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT2.getValue()]
        };
        const bodyT3 = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no1",
            "duracao": 123
        });
        const body3 = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT3.getValue()]
        };
        const resu = no_1.No.create(body).getValue();
        const resu2 = no_1.No.create(body2).getValue();
        const resu3 = no_1.No.create(body3).getValue();
        noRepoInstance = typedi_1.Container.get(config_1.default.repos.no.name);
        sinon.stub(noRepoInstance, "findAll").returns([resu, resu2, resu3]);
        const serv = new noService_1.default(noRepoInstance);
        serv.createNo(body);
        serv.createNo(body2);
        serv.createNo(body3);
        const res = await (await serv.listNos()).getValue();
        sinon.assert.match(res, [body, body2, body3]);
    });
    it('listNos: returns json with sorted array of types', async function () {
        const bodyT = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no1",
            "duracao": 123
        });
        const body = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT.getValue()]
        };
        const bodyT2 = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no2",
            "duracao": 123
        });
        const body2 = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT2.getValue()]
        };
        const bodyT3 = tempoViagemGrupo_1.TempoViagemGrupo.create({
            "noId": "no1",
            "duracao": 123
        });
        const body3 = {
            "domainId": "no1",
            "nome": "ano8",
            "latitude": 40,
            "longitude": 64,
            "nomeCurto": "gome",
            "isEstacaoRecolha": true,
            "isPontoRendicao": false,
            "tempoViagem": [bodyT3.getValue()]
        };
        const resu = no_1.No.create(body).getValue();
        const resu2 = no_1.No.create(body2).getValue();
        const resu3 = no_1.No.create(body3).getValue();
        noRepoInstance = typedi_1.Container.get(config_1.default.repos.no.name);
        sinon.stub(noRepoInstance, "findAllSorted").returns([resu2, resu, resu3]);
        const serv = new noService_1.default(noRepoInstance);
        serv.createNo(body);
        serv.createNo(body2);
        serv.createNo(body3);
        const res = await (await serv.listNosSorted("desc")).getValue();
        sinon.assert.match(res, [body2, body, body3]);
    });
});
// import * as sinon from 'sinon';
// import { mocked } from 'ts-jest/utils'
// import { Response, Request, NextFunction } from 'express';
// import { Container } from 'typedi';
// import config from "../../../../config";
// import { Result } from '../../../core/logic/Result';
// import INoService from "../../../services/IServices/INoService";
// import noService from "../../../services/noService";
// import noRepo from "../../../repos/noRepo";
// import NoController from "../../../controllers/noController";
// import INoController from "../../../controllers/IControllers/INoController";
// import INoDTO from '../../../dto/INoDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';
// jest.mock("../../../services/noService", () => {
// 	const serv = {
// 		createNo: jest.fn()
// 	  };
// 	  return { NoService: jest.fn(() => serv) };
// 	});
// describe('tipo tripulante controller', () => {
// 	// const MockedServ = mocked(noService, true);
// 	// ;
// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;
// 	// });
// 	// it('createNo: returns json with description value', async function () {
// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;
// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
// 	// 	const controller = new NoController(MockedServ.prototype);
// 	// 	console.log(controller.listNos(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);
// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;
// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };
// 	// 	// mockedFoo.prototype.createNo(<Request>req, <Response>res, <NextFunction>next);
// 	// 	// const exp: INoDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createNo.mock.calls).toHaveLength(1);
// 	//});
// 	// const m = {name: "NoController", path: "../controllers/noController"};
// 	// it('createNo: returns json with description value', async function () {
// 	// 	require('../../../app');
// 	// 	console.log(Container);
// 	// 	const ctrl = Container.get(config.controller.no.name) as INoController;
// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;
// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
// 	// 	 ctrl.createNo(<Request>req, <Response>res, <NextFunction>next);
// 	// 	 const exp: INoDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);
// 	// }
// 	// 	);});
//# sourceMappingURL=noService.test.js.map