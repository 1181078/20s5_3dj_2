"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const celebrate_1 = require("celebrate");
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../../config.js"));
const route = express_1.Router();
exports.default = (app) => {
    app.use('/tipoTripulantes', route);
    const ctrl = typedi_1.Container.get(config_js_1.default.controller.tipoTripulante.name);
    route.post('/add', celebrate_1.celebrate({
        body: celebrate_1.Joi.object({
            domainId: celebrate_1.Joi.string().required(),
            desc: celebrate_1.Joi.string().required()
        })
    }), (req, res, next) => ctrl.createTipoTripulante(req, res, next));
    route.put('', (req, res, next) => ctrl.updateTipoTripulante(req, res, next));
    route.get('', (req, res, next) => ctrl.listTiposTripulante(req, res, next));
    route.get('/sortedBy/:ord', (req, res, next) => ctrl.listTiposTripulanteSorted(req, res, next));
    route.get('/:id', (req, res, next) => ctrl.getTipoTripulanteById(req, res, next));
};
//# sourceMappingURL=tipoTripulante.js.map