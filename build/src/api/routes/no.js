"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const celebrate_1 = require("celebrate");
const typedi_1 = require("typedi");
const config_1 = __importDefault(require("../../../config"));
const route = express_1.Router();
exports.default = (app) => {
    app.use('/nos', route);
    const ctrl = typedi_1.Container.get(config_1.default.controller.no.name);
    route.post('', celebrate_1.celebrate({
        body: celebrate_1.Joi.object({
            domainId: celebrate_1.Joi.string().required(),
            nome: celebrate_1.Joi.string().required(),
            latitude: celebrate_1.Joi.number().min(-90).max(90).required(),
            longitude: celebrate_1.Joi.number().min(-180).max(180).required(),
            nomeCurto: celebrate_1.Joi.string().min(4).max(5).required(),
            isEstacaoRecolha: celebrate_1.Joi.boolean().required(),
            isPontoRendicao: celebrate_1.Joi.boolean().required(),
            tempoViagem: celebrate_1.Joi.array().items(celebrate_1.Joi.object({
                noId: celebrate_1.Joi.string(),
                duracao: celebrate_1.Joi.string()
            }))
        })
    }), (req, res, next) => ctrl.createNo(req, res, next));
    route.put('', (req, res, next) => ctrl.updateNo(req, res, next));
    route.get('', (req, res, next) => ctrl.listNos(req, res, next));
    route.get('/sortedBy/:ord', (req, res, next) => ctrl.listNosSorted(req, res, next));
    route.get('/:id', (req, res, next) => ctrl.getNoById(req, res, next));
};
//# sourceMappingURL=no.js.map