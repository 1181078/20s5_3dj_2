"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../../config.js"));
const route = express_1.Router();
exports.default = (app) => {
    app.use('/readFile', route);
    const ctrl = typedi_1.Container.get(config_js_1.default.controller.readFile.name);
    //Decidir se por post ou get
    route.post('/import', (req, res, next) => ctrl.readFile(req, res, next));
    //route.put('', (req, res, next) => ctrl.updatePercurso(req, res, next));
};
//# sourceMappingURL=readFile.js.map