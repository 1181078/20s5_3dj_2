"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const celebrate_1 = require("celebrate");
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../../config.js"));
const route = express_1.Router();
exports.default = (app) => {
    app.use('/linhas', route);
    const ctrl = typedi_1.Container.get(config_js_1.default.controller.linha.name);
    route.post('', celebrate_1.celebrate({
        body: celebrate_1.Joi.object({
            domainId: celebrate_1.Joi.string().required(),
            nome: celebrate_1.Joi.string().required(),
            cor: celebrate_1.Joi.string().required(),
            percursoLinha: celebrate_1.Joi.array().items(celebrate_1.Joi.object({
                percursoLinhaId: celebrate_1.Joi.string(), percurso: celebrate_1.Joi.string(), orientation: celebrate_1.Joi.string()
            }))
        })
    }), (req, res, next) => ctrl.createLinha(req, res, next));
    route.put('', (req, res, next) => ctrl.updateLinha(req, res, next));
    route.get('', (req, res, next) => ctrl.listLinhas(req, res, next));
    route.get('/sortedBy/:ord', (req, res, next) => ctrl.listLinhasSorted(req, res, next));
    route.get('/:id', (req, res, next) => ctrl.getLinhaById(req, res, next));
};
//# sourceMappingURL=linha.js.map