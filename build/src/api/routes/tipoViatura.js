"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const celebrate_1 = require("celebrate");
const typedi_1 = require("typedi");
const route = express_1.Router();
const config_js_1 = __importDefault(require("../../../config.js"));
exports.default = (app) => {
    app.use('/tipoViaturas', route);
    const ctrl = typedi_1.Container.get(config_js_1.default.controller.tipoViatura.name);
    route.post('', celebrate_1.celebrate({
        body: celebrate_1.Joi.object({
            domainId: celebrate_1.Joi.string().required(),
            nome: celebrate_1.Joi.string().required(),
            autonomia: celebrate_1.Joi.number().min(0).required(),
            custo: celebrate_1.Joi.number().min(0).required(),
            velocidadeMedia: celebrate_1.Joi.number().min(0).required(),
            combustivel: celebrate_1.Joi.number().min(0).required(),
            consumo: celebrate_1.Joi.number().min(0).required(),
            emissoes: celebrate_1.Joi.number().min(0).required()
        })
    }), (req, res, next) => ctrl.createTipoViatura(req, res, next));
    route.put('', (req, res, next) => ctrl.updateTipoViatura(req, res, next));
    route.get('', (req, res, next) => ctrl.listTiposViatura(req, res, next));
    route.get('/sortedBy/:ord', (req, res, next) => ctrl.listTiposViaturaSorted(req, res, next));
    route.get('/:id', (req, res, next) => ctrl.getTipoViaturaById(req, res, next));
};
//# sourceMappingURL=tipoViatura.js.map