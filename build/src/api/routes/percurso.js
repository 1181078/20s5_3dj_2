"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const celebrate_1 = require("celebrate");
const typedi_1 = require("typedi");
const config_js_1 = __importDefault(require("../../../config.js"));
const route = express_1.Router();
exports.default = (app) => {
    app.use('/percursos', route);
    const ctrl = typedi_1.Container.get(config_js_1.default.controller.percurso.name);
    route.post('', celebrate_1.celebrate({
        body: celebrate_1.Joi.object({
            domainId: celebrate_1.Joi.string().required(),
            nosPercurso: celebrate_1.Joi.array().items(celebrate_1.Joi.object({
                noId: celebrate_1.Joi.string(), duracao: celebrate_1.Joi.number(), distancia: celebrate_1.Joi.number()
            }))
        })
    }), (req, res, next) => ctrl.createPercurso(req, res, next));
    route.put('', (req, res, next) => ctrl.updatePercurso(req, res, next));
    route.get('', (req, res, next) => ctrl.listPercursos(req, res, next));
    route.get('/:id', (req, res, next) => ctrl.getPercursoById(req, res, next));
};
//# sourceMappingURL=percurso.js.map