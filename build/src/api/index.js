"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const authRoute_1 = __importDefault(require("./routes/authRoute"));
const userRoute_1 = __importDefault(require("./routes/userRoute"));
const roleRoute_1 = __importDefault(require("./routes/roleRoute"));
const tipoTripulante_1 = __importDefault(require("./routes/tipoTripulante"));
const tipoViatura_1 = __importDefault(require("./routes/tipoViatura"));
const percurso_1 = __importDefault(require("./routes/percurso"));
const no_1 = __importDefault(require("./routes/no"));
const linha_1 = __importDefault(require("./routes/linha"));
const readFile_1 = __importDefault(require("./routes/readFile"));
// guaranteed to get dependencies
exports.default = () => {
    const app = express_1.Router();
    authRoute_1.default(app);
    userRoute_1.default(app);
    roleRoute_1.default(app);
    tipoTripulante_1.default(app);
    tipoViatura_1.default(app);
    percurso_1.default(app);
    no_1.default(app);
    linha_1.default(app);
    readFile_1.default(app);
    return app;
};
//# sourceMappingURL=index.js.map