"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipoViaturaMap = void 0;
const tipoViatura_1 = require("../domain/tipoViatura/tipoViatura");
const UniqueEntityID_1 = require("../core/domain/UniqueEntityID");
const Mapper_1 = require("../core/infra/Mapper");
class TipoViaturaMap extends Mapper_1.Mapper {
    static toDTO(tipoViatura) {
        return {
            domainId: tipoViatura.props.domainId,
            nome: tipoViatura.nome,
            autonomia: tipoViatura.autonomia,
            custo: tipoViatura.custo,
            velocidadeMedia: tipoViatura.velocidadeMedia,
            combustivel: tipoViatura.combustivel,
            consumo: tipoViatura.consumo,
            emissoes: tipoViatura.emissoes
        };
    }
    static toDomain(tipoViatura) {
        const tipoOrError = tipoViatura_1.TipoViatura.create(tipoViatura, new UniqueEntityID_1.UniqueEntityID(tipoViatura.domainId));
        tipoOrError.isFailure ? console.log(tipoOrError.error) : '';
        return tipoOrError.isSuccess ? tipoOrError.getValue() : null;
    }
    static toDomainArray(tipoViatura) {
        var tipos = new Array(tipoViatura.length);
        for (var i = 0; i < tipoViatura.length; i++) {
            const tipoOrError = tipoViatura_1.TipoViatura.create(tipoViatura[i], new UniqueEntityID_1.UniqueEntityID(tipoViatura[i].domainId));
            tipoOrError.isFailure ? console.log(tipoOrError.error) : '';
            tipoOrError.isSuccess ? tipos[i] = tipoOrError.getValue() : null;
        }
        return tipos;
    }
    static toPersistence(tipoViatura) {
        return {
            id: tipoViatura.id.toString(),
            domainId: tipoViatura.props.domainId,
            nome: tipoViatura.nome,
            autonomia: tipoViatura.autonomia,
            custo: tipoViatura.custo,
            velocidadeMedia: tipoViatura.velocidadeMedia,
            combustivel: tipoViatura.combustivel,
            consumo: tipoViatura.consumo,
            emissoes: tipoViatura.emissoes
        };
    }
}
exports.TipoViaturaMap = TipoViaturaMap;
//# sourceMappingURL=TipoViaturaMap.js.map