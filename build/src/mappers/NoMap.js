"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NoMap = void 0;
const no_1 = require("../domain/no/no");
const UniqueEntityID_1 = require("../core/domain/UniqueEntityID");
const Mapper_1 = require("../core/infra/Mapper");
class NoMap extends Mapper_1.Mapper {
    static toDTO(no) {
        return {
            domainId: no.domainId,
            nome: no.nome,
            latitude: no.latitude,
            longitude: no.longitude,
            nomeCurto: no.nomeCurto,
            isEstacaoRecolha: no.isEstacaoRecolha,
            isPontoRendicao: no.isPontoRendicao,
            tempoViagem: no.isTempoViagem
        };
    }
    static toDomain(no) {
        const noOrError = no_1.No.create(no, new UniqueEntityID_1.UniqueEntityID(no.domainId));
        noOrError.isFailure ? console.log(noOrError.error) : '';
        return noOrError.isSuccess ? noOrError.getValue() : null;
    }
    static toDomainArray(no) {
        var pers = new Array(no.length);
        for (var i = 0; i < no.length; i++) {
            const noOrError = no_1.No.create(no[i], new UniqueEntityID_1.UniqueEntityID(no[i].domainId));
            noOrError.isFailure ? console.log(noOrError.error) : '';
            noOrError.isSuccess ? pers[i] = noOrError.getValue() : null;
        }
        return pers;
    }
    static toPersistence(no) {
        return {
            id: no.id.toString(),
            domainId: no.domainId,
            nome: no.nome,
            latitude: no.latitude,
            longitude: no.longitude,
            nomeCurto: no.nomeCurto,
            isEstacaoRecolha: no.isEstacaoRecolha,
            isPontoRendicao: no.isPontoRendicao,
            tempoViagem: no.isTempoViagem
        };
    }
}
exports.NoMap = NoMap;
//# sourceMappingURL=NoMap.js.map