"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LinhaMap = void 0;
const linha_1 = require("../domain/linha/linha");
const UniqueEntityID_1 = require("../core/domain/UniqueEntityID");
const Mapper_1 = require("../core/infra/Mapper");
class LinhaMap extends Mapper_1.Mapper {
    static toDTO(linha) {
        return {
            domainId: linha.domainId,
            nome: linha.props.nome,
            cor: linha.props.cor,
            percursoLinha: linha.props.percursoLinha
        };
    }
    static toDomain(linha) {
        const linhaOrError = linha_1.Linha.create(linha, new UniqueEntityID_1.UniqueEntityID(linha.domainId));
        linhaOrError.isFailure ? console.log(linhaOrError.error) : '';
        return linhaOrError.isSuccess ? linhaOrError.getValue() : null;
    }
    static toDomainArray(linha) {
        var pers = new Array(linha.length);
        for (var i = 0; i < linha.length; i++) {
            const linhaOrError = linha_1.Linha.create(linha[i], new UniqueEntityID_1.UniqueEntityID(linha[i].domainId));
            linhaOrError.isFailure ? console.log(linhaOrError.error) : '';
            linhaOrError.isSuccess ? pers[i] = linhaOrError.getValue() : null;
        }
        return pers;
    }
    static toPersistence(linha) {
        return {
            id: linha.id.toString(),
            domainId: linha.props.domainId,
            nome: linha.props.nome,
            cor: linha.props.cor,
            percursoLinha: linha.props.percursoLinha
        };
    }
}
exports.LinhaMap = LinhaMap;
//# sourceMappingURL=LinhaMap.js.map