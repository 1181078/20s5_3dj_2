"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipoTripulanteMap = void 0;
const tipoTripulante_1 = require("../domain/tipoTripulante/tipoTripulante");
const UniqueEntityID_1 = require("../core/domain/UniqueEntityID");
const Mapper_1 = require("../core/infra/Mapper");
class TipoTripulanteMap extends Mapper_1.Mapper {
    static toDTO(tipoTripulante) {
        return {
            domainId: tipoTripulante.props.domainId,
            desc: tipoTripulante.desc,
        };
    }
    static toDomain(tipoTripulante) {
        const tipoOrError = tipoTripulante_1.TipoTripulante.create(tipoTripulante, new UniqueEntityID_1.UniqueEntityID(tipoTripulante.domainId));
        tipoOrError.isFailure ? console.log(tipoOrError.error) : '';
        return tipoOrError.isSuccess ? tipoOrError.getValue() : null;
    }
    static toDomainArray(tipoTripulante) {
        var tipos = new Array(tipoTripulante.length);
        for (var i = 0; i < tipoTripulante.length; i++) {
            const tipoOrError = tipoTripulante_1.TipoTripulante.create(tipoTripulante[i], new UniqueEntityID_1.UniqueEntityID(tipoTripulante[i].domainId));
            tipoOrError.isFailure ? console.log(tipoOrError.error) : '';
            tipoOrError.isSuccess ? tipos[i] = tipoOrError.getValue() : null;
        }
        return tipos;
    }
    static toPersistence(tipoTripulante) {
        return {
            id: tipoTripulante.id.toString(),
            domainId: tipoTripulante.props.domainId,
            desc: tipoTripulante.desc
        };
    }
}
exports.TipoTripulanteMap = TipoTripulanteMap;
//# sourceMappingURL=TipoTripulanteMap.js.map