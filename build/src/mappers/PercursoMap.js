"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PercursoMap = void 0;
const percurso_1 = require("../domain/percurso/percurso");
const UniqueEntityID_1 = require("../core/domain/UniqueEntityID");
const Mapper_1 = require("../core/infra/Mapper");
class PercursoMap extends Mapper_1.Mapper {
    static toDTO(percurso) {
        return {
            domainId: percurso.props.domainId,
            nosPercurso: percurso.props.nosPercurso,
        };
    }
    static toDomain(percurso) {
        const percursoOrError = percurso_1.Percurso.create(percurso, new UniqueEntityID_1.UniqueEntityID(percurso.domainId));
        percursoOrError.isFailure ? console.log(percursoOrError.error) : '';
        return percursoOrError.isSuccess ? percursoOrError.getValue() : null;
    }
    static toPersistence(percurso) {
        return {
            id: percurso.id.toString(),
            domainId: percurso.props.domainId,
            nosPercurso: percurso.props.nosPercurso
        };
    }
    static toDomainArray(percurso) {
        var pers = new Array(percurso.length);
        for (var i = 0; i < percurso.length; i++) {
            const percursoOrError = percurso_1.Percurso.create(percurso[i], new UniqueEntityID_1.UniqueEntityID(percurso[i].domainId));
            percursoOrError.isFailure ? console.log(percursoOrError.error) : '';
            percursoOrError.isSuccess ? pers[i] = percursoOrError.getValue() : null;
        }
        return pers;
    }
}
exports.PercursoMap = PercursoMap;
//# sourceMappingURL=PercursoMap.js.map