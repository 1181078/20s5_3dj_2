"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PercursoLinha = void 0;
const ValueObject_1 = require("../../core/domain/ValueObject");
const Result_1 = require("../../core/logic/Result");
const Guard_1 = require("../../core/logic/Guard");
class PercursoLinha extends ValueObject_1.ValueObject {
    get value() {
        return this.props.value;
    }
    constructor(props) {
        super(props);
    }
    static create(percursoLinhas) {
        const guardResult = Guard_1.Guard.againstNullOrUndefined(percursoLinhas, 'percursoLinhas');
        if (!guardResult.succeeded) {
            return Result_1.Result.fail(guardResult.message);
        }
        else {
            return Result_1.Result.ok(new PercursoLinha({ value: percursoLinhas }));
        }
    }
}
exports.PercursoLinha = PercursoLinha;
//# sourceMappingURL=percursoLinha.js.map