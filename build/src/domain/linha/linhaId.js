"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LinhaId = void 0;
const Entity_1 = require("../../core/domain/Entity");
class LinhaId extends Entity_1.Entity {
    get id() {
        return this._id;
    }
    static create(id) {
        return new LinhaId(id);
    }
}
exports.LinhaId = LinhaId;
//# sourceMappingURL=linhaId.js.map