"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Linha = void 0;
const AggregateRoot_1 = require("../../core/domain/AggregateRoot");
const Result_1 = require("../../core/logic/Result");
const linhaId_1 = require("./linhaId");
class Linha extends AggregateRoot_1.AggregateRoot {
    get id() {
        return this._id;
    }
    get linhaId() {
        return linhaId_1.LinhaId.create(this.id);
    }
    get percursoLinha() {
        return this.props.percursoLinha;
    }
    set percursoLinha(value) {
        this.props.percursoLinha = value;
    }
    get domainId() {
        return this.props.domainId;
    }
    set domainId(value) {
        this.props.domainId = value;
    }
    get nome() {
        return this.props.nome;
    }
    set nome(value) {
        this.props.nome = value;
    }
    get cor() {
        return this.props.cor;
    }
    set cor(value) {
        this.props.cor = value;
    }
    constructor(props, id) {
        super(props, id);
    }
    static create(props, id) {
        const domainId = props.domainId;
        const per = props.percursoLinha;
        const nome = props.nome;
        const cor = props.cor;
        const newProps = {
            domainId: domainId,
            nome: nome,
            cor: cor,
            percursoLinha: per
        };
        if (!!per === false) {
            return Result_1.Result.fail('Must provide information');
        }
        else {
            const linha = new Linha(newProps, id);
            return Result_1.Result.ok(linha);
        }
    }
}
exports.Linha = Linha;
//# sourceMappingURL=linha.js.map