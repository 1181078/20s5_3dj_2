"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Percurso = void 0;
const AggregateRoot_1 = require("../../core/domain/AggregateRoot");
const Result_1 = require("../../core/logic/Result");
const percursoId_1 = require("./percursoId");
class Percurso extends AggregateRoot_1.AggregateRoot {
    get id() {
        return this._id;
    }
    get percursoId() {
        return percursoId_1.PercursoId.create(this.id);
    }
    get nosPercurso() {
        return this.props.nosPercurso;
    }
    set nosPercurso(value) {
        this.props.nosPercurso = value;
    }
    constructor(props, id) {
        super(props, id);
    }
    static create(props, id) {
        const domainId = props.domainId;
        const nosPer = props.nosPercurso;
        const newProps = {
            domainId: domainId,
            nosPercurso: nosPer
        };
        if (!!nosPer === false) {
            return Result_1.Result.fail('Must provide information');
        }
        else {
            const per = new Percurso(newProps, id);
            return Result_1.Result.ok(per);
        }
    }
}
exports.Percurso = Percurso;
//# sourceMappingURL=percurso.js.map