"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NosPercurso = void 0;
const ValueObject_1 = require("../../core/domain/ValueObject");
const Result_1 = require("../../core/logic/Result");
const Guard_1 = require("../../core/logic/Guard");
class NosPercurso extends ValueObject_1.ValueObject {
    get value() {
        return this.props.value;
    }
    constructor(props) {
        super(props);
    }
    static create(nos) {
        const guardResult = Guard_1.Guard.againstNullOrUndefined(nos, 'nos');
        if (!guardResult.succeeded) {
            return Result_1.Result.fail(guardResult.message);
        }
        else {
            return Result_1.Result.ok(new NosPercurso({ value: nos }));
        }
    }
}
exports.NosPercurso = NosPercurso;
//# sourceMappingURL=nosPercurso.js.map