"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PercursoId = void 0;
const Entity_1 = require("../../core/domain/Entity");
class PercursoId extends Entity_1.Entity {
    get id() {
        return this._id;
    }
    static create(id) {
        return new PercursoId(id);
    }
}
exports.PercursoId = PercursoId;
//# sourceMappingURL=percursoId.js.map