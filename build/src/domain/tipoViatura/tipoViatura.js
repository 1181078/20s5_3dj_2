"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipoViatura = void 0;
const AggregateRoot_1 = require("../../core/domain/AggregateRoot");
const Result_1 = require("../../core/logic/Result");
const tipoViaturaId_1 = require("./tipoViaturaId");
class TipoViatura extends AggregateRoot_1.AggregateRoot {
    get id() {
        return this._id;
    }
    get tipoViaturaId() {
        return tipoViaturaId_1.TipoViaturaId.create(this.id);
    }
    get autonomia() {
        return this.props.autonomia;
    }
    set autonomia(value) {
        this.props.autonomia = value;
    }
    get nome() {
        return this.props.nome;
    }
    set nome(value) {
        this.props.nome = value;
    }
    get custo() {
        return this.props.custo;
    }
    set custo(value) {
        this.props.custo = value;
    }
    get velocidadeMedia() {
        return this.props.velocidadeMedia;
    }
    set velocidadeMedia(value) {
        this.props.velocidadeMedia = value;
    }
    get combustivel() {
        return this.props.combustivel;
    }
    set combustivel(value) {
        this.props.combustivel = value;
    }
    get consumo() {
        return this.props.consumo;
    }
    set consumo(value) {
        this.props.consumo = value;
    }
    get emissoes() {
        return this.props.emissoes;
    }
    set emissoes(value) {
        this.props.emissoes = value;
    }
    constructor(props, id) {
        super(props, id);
    }
    static create(ITipoViaturaDTO, id) {
        const dId = ITipoViaturaDTO.domainId;
        const nome = ITipoViaturaDTO.nome;
        const autonomia = ITipoViaturaDTO.autonomia;
        const custo = ITipoViaturaDTO.custo;
        const velocidadeMedia = ITipoViaturaDTO.velocidadeMedia;
        const combustivel = ITipoViaturaDTO.combustivel;
        const consumo = ITipoViaturaDTO.consumo;
        const emissoes = ITipoViaturaDTO.emissoes;
        if (!!nome === false || nome.length === 0) {
            return Result_1.Result.fail('Must provide a name');
        }
        else {
            const tipo = new TipoViatura({ domainId: dId, nome: nome, autonomia: autonomia, custo: custo, velocidadeMedia: velocidadeMedia, combustivel: combustivel, consumo: consumo, emissoes: emissoes }, id);
            return Result_1.Result.ok(tipo);
        }
    }
}
exports.TipoViatura = TipoViatura;
//# sourceMappingURL=tipoViatura.js.map