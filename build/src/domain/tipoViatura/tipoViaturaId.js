"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipoViaturaId = void 0;
const Entity_1 = require("../../core/domain/Entity");
class TipoViaturaId extends Entity_1.Entity {
    get id() {
        return this._id;
    }
    static create(id) {
        return new TipoViaturaId(id);
    }
}
exports.TipoViaturaId = TipoViaturaId;
//# sourceMappingURL=tipoViaturaId.js.map