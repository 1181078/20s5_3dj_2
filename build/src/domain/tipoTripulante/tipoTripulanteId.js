"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipoTripulanteId = void 0;
const Entity_1 = require("../../core/domain/Entity");
class TipoTripulanteId extends Entity_1.Entity {
    get id() {
        return this._id;
    }
    static create(id) {
        return new TipoTripulanteId(id);
    }
}
exports.TipoTripulanteId = TipoTripulanteId;
//# sourceMappingURL=tipoTripulanteId.js.map