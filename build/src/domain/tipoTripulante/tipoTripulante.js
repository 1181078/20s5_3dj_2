"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TipoTripulante = void 0;
const AggregateRoot_1 = require("../../core/domain/AggregateRoot");
const Result_1 = require("../../core/logic/Result");
const tipoTripulanteId_1 = require("./tipoTripulanteId");
class TipoTripulante extends AggregateRoot_1.AggregateRoot {
    get id() {
        return this._id;
    }
    get tipoTripulanteId() {
        return tipoTripulanteId_1.TipoTripulanteId.create(this.id);
    }
    get desc() {
        return this.props.desc;
    }
    set desc(value) {
        this.props.desc = value;
    }
    constructor(props, id) {
        super(props, id);
    }
    static create(tipoTripulanteDTO, id) {
        const desc = tipoTripulanteDTO.desc;
        const domainId = tipoTripulanteDTO.domainId;
        if (!!desc === false || desc.length === 0) {
            return Result_1.Result.fail('Must provide a description');
        }
        else {
            const tipo = new TipoTripulante({ domainId: domainId, desc: desc }, id);
            return Result_1.Result.ok(tipo);
        }
    }
}
exports.TipoTripulante = TipoTripulante;
//# sourceMappingURL=tipoTripulante.js.map