"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleId = void 0;
const Entity_1 = require("../../core/domain/Entity");
class RoleId extends Entity_1.Entity {
    get id() {
        return this._id;
    }
    constructor(id) {
        super(null, id);
    }
}
exports.RoleId = RoleId;
//# sourceMappingURL=roleId.js.map