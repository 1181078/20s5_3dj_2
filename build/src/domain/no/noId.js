"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NoId = void 0;
const Entity_1 = require("../../core/domain/Entity");
class NoId extends Entity_1.Entity {
    get id() {
        return this._id;
    }
    static create(id) {
        return new NoId(id);
    }
}
exports.NoId = NoId;
//# sourceMappingURL=noId.js.map