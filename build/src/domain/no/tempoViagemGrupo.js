"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TempoViagemGrupo = void 0;
const ValueObject_1 = require("../../core/domain/ValueObject");
const Result_1 = require("../../core/logic/Result");
const Guard_1 = require("../../core/logic/Guard");
class TempoViagemGrupo extends ValueObject_1.ValueObject {
    get value() {
        return this.props.value;
    }
    get duracao() {
        return this.props.value.duracao;
    }
    constructor(props) {
        super(props);
    }
    static create(tempo) {
        const guardResult = Guard_1.Guard.againstNullOrUndefined(tempo, 'nos');
        if (!guardResult.succeeded) {
            return Result_1.Result.fail(guardResult.message);
        }
        else {
            return Result_1.Result.ok(new TempoViagemGrupo({ value: tempo }));
        }
    }
}
exports.TempoViagemGrupo = TempoViagemGrupo;
//# sourceMappingURL=tempoViagemGrupo.js.map