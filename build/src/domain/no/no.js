"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.No = void 0;
const AggregateRoot_1 = require("../../core/domain/AggregateRoot");
const Result_1 = require("../../core/logic/Result");
const noId_1 = require("./noId");
class No extends AggregateRoot_1.AggregateRoot {
    get id() {
        return this._id;
    }
    get noId() {
        return noId_1.NoId.create(this.id);
    }
    get domainId() {
        return this.props.domainId;
    }
    set domainId(value) {
        this.props.domainId = value;
    }
    get latitude() {
        return this.props.latitude;
    }
    set latitude(value) {
        this.props.latitude = value;
    }
    get nome() {
        return this.props.nome;
    }
    set nome(value) {
        this.props.nome = value;
    }
    get longitude() {
        return this.props.longitude;
    }
    set longitude(value) {
        this.props.longitude = value;
    }
    get nomeCurto() {
        return this.props.nomeCurto;
    }
    set nomeCurto(value) {
        this.props.nomeCurto = value;
    }
    get isEstacaoRecolha() {
        return this.props.isEstacaoRecolha;
    }
    set isEstacaoRecolha(value) {
        this.props.isEstacaoRecolha = value;
    }
    get isPontoRendicao() {
        return this.props.isPontoRendicao;
    }
    set isPontoRendicao(value) {
        this.props.isPontoRendicao = value;
    }
    get isTempoViagem() {
        return this.props.tempoViagem;
    }
    set isTempoViagem(value) {
        this.props.tempoViagem = value;
    }
    constructor(props, id) {
        super(props, id);
    }
    static create(noDTO, id) {
        const domainId = noDTO.domainId;
        const nome = noDTO.nome;
        const latitude = noDTO.latitude;
        const longitude = noDTO.longitude;
        const nomeCurto = noDTO.nomeCurto;
        const isEstacaoRecolha = noDTO.isEstacaoRecolha;
        const isPontoRendicao = noDTO.isPontoRendicao;
        const tempo = noDTO.tempoViagem;
        const newProps = {
            domainId: domainId,
            nome: nome,
            latitude: latitude,
            longitude: longitude,
            nomeCurto: nomeCurto,
            isEstacaoRecolha: isEstacaoRecolha,
            isPontoRendicao: isPontoRendicao,
            tempoViagem: tempo
        };
        if (isEstacaoRecolha === null) {
            return Result_1.Result.fail('isEstacaoRecolha cant be null');
        }
        else if (isPontoRendicao === null) {
            return Result_1.Result.fail('isPontoRendicao cant be null');
        }
        else if (!!nome === false || nome.length == 0) {
            return Result_1.Result.fail('Must provide nodes');
        }
        else {
            const per = new No({ domainId: domainId, nome: nome, latitude: latitude, longitude: longitude, nomeCurto: nomeCurto, isEstacaoRecolha: isEstacaoRecolha, isPontoRendicao: isPontoRendicao, tempoViagem: tempo }, id);
            return Result_1.Result.ok(per);
        }
    }
}
exports.No = No;
//# sourceMappingURL=no.js.map