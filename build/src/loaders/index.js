"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("./express"));
const dependencyInjector_1 = __importDefault(require("./dependencyInjector"));
const mongoose_1 = __importDefault(require("./mongoose"));
const logger_1 = __importDefault(require("./logger"));
const config_js_1 = __importDefault(require("../../config.js"));
exports.default = async ({ expressApp }) => {
    const mongoConnection = await mongoose_1.default();
    logger_1.default.info('✌️ DB loaded and connected!');
    const noSchema = {
        name: 'noSchema',
        schema: '../persistence/schemas/noSchema',
    };
    const percursoSchema = {
        name: 'percursoSchema',
        schema: '../persistence/schemas/percursoSchema',
    };
    const tipoViaturaSchema = {
        name: 'tipoViaturaSchema',
        schema: '../persistence/schemas/tipoViaturaSchema',
    };
    const tipoTripulanteSchema = {
        name: 'tipoTripulanteSchema',
        schema: '../persistence/schemas/tipoTripulanteSchema',
    };
    const linhaSchema = {
        name: 'linhaSchema',
        schema: '../persistence/schemas/linhaSchema',
    };
    const userSchema = {
        // compare with the approach followed in repos and services
        name: 'userSchema',
        schema: '../persistence/schemas/userSchema',
    };
    const roleSchema = {
        // compare with the approach followed in repos and services
        name: 'roleSchema',
        schema: '../persistence/schemas/roleSchema',
    };
    const roleController = {
        name: config_js_1.default.controller.role.name,
        path: config_js_1.default.controller.role.path
    };
    const roleRepo = {
        name: config_js_1.default.repos.role.name,
        path: config_js_1.default.repos.role.path
    };
    const userRepo = {
        name: config_js_1.default.repos.user.name,
        path: config_js_1.default.repos.user.path
    };
    const roleService = {
        name: config_js_1.default.services.role.name,
        path: config_js_1.default.services.role.path
    };
    const readFileController = {
        name: config_js_1.default.controller.readFile.name,
        path: config_js_1.default.controller.readFile.path
    };
    const noController = {
        name: config_js_1.default.controller.no.name,
        path: config_js_1.default.controller.no.path
    };
    const percursoController = {
        name: config_js_1.default.controller.percurso.name,
        path: config_js_1.default.controller.percurso.path
    };
    const tipoViaturaController = {
        name: config_js_1.default.controller.tipoViatura.name,
        path: config_js_1.default.controller.tipoViatura.path
    };
    const tipoTripulanteController = {
        name: config_js_1.default.controller.tipoTripulante.name,
        path: config_js_1.default.controller.tipoTripulante.path
    };
    const linhaController = {
        name: config_js_1.default.controller.linha.name,
        path: config_js_1.default.controller.linha.path
    };
    const noRepo = {
        name: config_js_1.default.repos.no.name,
        path: config_js_1.default.repos.no.path
    };
    const percursoRepo = {
        name: config_js_1.default.repos.percurso.name,
        path: config_js_1.default.repos.percurso.path
    };
    const tipoViaturaRepo = {
        name: config_js_1.default.repos.tipoViatura.name,
        path: config_js_1.default.repos.tipoViatura.path
    };
    const tipoTripulanteRepo = {
        name: config_js_1.default.repos.tipoTripulante.name,
        path: config_js_1.default.repos.tipoTripulante.path
    };
    const linhaRepo = {
        name: config_js_1.default.repos.linha.name,
        path: config_js_1.default.repos.linha.path
    };
    const noService = {
        name: config_js_1.default.services.no.name,
        path: config_js_1.default.services.no.path
    };
    const percursoService = {
        name: config_js_1.default.services.percurso.name,
        path: config_js_1.default.services.percurso.path
    };
    const tipoViaturaService = {
        name: config_js_1.default.services.tipoViatura.name,
        path: config_js_1.default.services.tipoViatura.path
    };
    const tipoTripulanteService = {
        name: config_js_1.default.services.tipoTripulante.name,
        path: config_js_1.default.services.tipoTripulante.path
    };
    const linhaService = {
        name: config_js_1.default.services.linha.name,
        path: config_js_1.default.services.linha.path
    };
    const readFileService = {
        name: config_js_1.default.services.readFile.name,
        path: config_js_1.default.services.readFile.path
    };
    await dependencyInjector_1.default({
        mongoConnection,
        schemas: [
            noSchema,
            percursoSchema,
            tipoViaturaSchema,
            tipoTripulanteSchema,
            linhaSchema,
            roleSchema,
            userSchema
        ],
        controllers: [
            noController,
            percursoController,
            tipoViaturaController,
            tipoTripulanteController,
            linhaController,
            readFileController,
            roleController
        ],
        repos: [
            noRepo,
            percursoRepo,
            tipoViaturaRepo,
            tipoTripulanteRepo,
            linhaRepo,
            roleRepo,
            userRepo
        ],
        services: [
            noService,
            percursoService,
            tipoViaturaService,
            tipoTripulanteService,
            linhaService,
            readFileService,
            roleService
        ]
    });
    logger_1.default.info('✌️ Schemas, Controllers, Repositories, Services, etc. loaded');
    await express_1.default({ app: expressApp });
    logger_1.default.info('✌️ Express loaded');
};
//# sourceMappingURL=index.js.map