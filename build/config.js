require('./src/app');
// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
module.exports = {
    /**
     * Your favorite port
     */
    port: parseInt(process.env.PORT, 10),
    /**
     * That long string from mlab
     */
    databaseURL: process.env.MONGODB_URI,
    /**
     * Your secret sauce
     */
    jwtSecret: process.env.JWT_SECRET,
    /**
     * Used by winston logger
     */
    logs: {
        level: process.env.LOG_LEVEL || 'silly',
    },
    /**
     * API configs
     */
    api: {
        prefix: '/api',
    },
    controller: {
        tipoViatura: {
            name: "TipoViaturaController",
            path: "../controllers/tipoViaturaController"
        },
        tipoTripulante: {
            name: "TipoTripulanteController",
            path: "../controllers/tipoTripulanteController"
        },
        percurso: {
            name: "PercursoController",
            path: "../controllers/percursoController"
        },
        no: {
            name: "NoController",
            path: "../controllers/noController"
        },
        linha: {
            name: "linhaController",
            path: "../controllers/linhaController"
        },
        readFile: {
            name: "readFileController",
            path: "../controllers/readFileController"
        },
        role: {
            name: "RoleController",
            path: "../controllers/roleController"
        }
    },
    repos: {
        tipoViatura: {
            name: "TipoViaturaRepo",
            path: "../repos/tipoViaturaRepo"
        },
        tipoTripulante: {
            name: "TipoTripulanteRepo",
            path: "../repos/tipoTripulanteRepo"
        },
        percurso: {
            name: "PercursoRepo",
            path: "../repos/percursoRepo"
        },
        no: {
            name: "NoRepo",
            path: "../repos/noRepo"
        },
        linha: {
            name: "linhaRepo",
            path: "../repos/linhaRepo"
        },
        role: {
            name: "RoleRepo",
            path: "../repos/roleRepo"
        },
        user: {
            name: "UserRepo",
            path: "../repos/userRepo"
        }
    },
    services: {
        tipoViatura: {
            name: "TipoViaturaService",
            path: "../services/tipoViaturaService"
        },
        tipoTripulante: {
            name: "TipoTripulanteService",
            path: "../services/tipoTripulanteService"
        },
        percurso: {
            name: "PercursoService",
            path: "../services/percursoService"
        },
        no: {
            name: "NoService",
            path: "../services/noService"
        },
        linha: {
            name: "linhaService",
            path: "../services/linhaService"
        },
        readFile: {
            name: "readFileService",
            path: "../services/readFileService"
        },
        role: {
            name: "RoleService",
            path: "../services/roleService"
        }
    },
};
//# sourceMappingURL=config.js.map