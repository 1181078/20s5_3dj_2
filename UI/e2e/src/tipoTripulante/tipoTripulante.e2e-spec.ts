import { AppPage } from './tipoTripulante.po';
import { browser, logging } from 'protractor';

describe('workspace-project TipoTripulante', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  it('Should have tipos', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getTiposDeTripulante().click();

    expect(page.getTitleText()).toEqual('Tipos Tripulante');

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should add tipo', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getTiposDeTripulante().click();

    page.getDomainId().sendKeys("autocarroTeste");

    page.getDescription().sendKeys("autocarroTeste");
  
    page.getAddButton().click();

    expect(page.getLastAddedTipos().getText()).toEqual("autocarroTeste");

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should redirect to tipo detail', () => {
    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getTiposDeTripulante().click();

    page.getLastAddedTiposLink().click();

    expect(page.navigateToWhere()).toEqual('http://localhost:4200/tiposTripulante/autocarroTeste');

    page.getLogout().click();

    //browser.sleep(3000);
  
  });

  it('Should redirect back', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getTiposDeTripulante().click();

    page.getLastAddedTiposLink().click();

    page.getGoBackButton().click();

    expect(page.navigateToWhere()).toEqual('http://localhost:4200/tiposTripulante');

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should update desc', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getTiposDeTripulante().click();
    
    page.getLastAddedTiposLink().click();

    page.getUpdateDesc().sendKeys("newAutocarroTeste");

    page.getSaveButton().click();
    
    page.getTiposDeTripulante().click();
    
    expect(page.getLastAddedTiposLink().getText()).toEqual("autocarroTeste autocarroTeste");

    page.getLogout().click();

    //browser.sleep(3000);
  });

  it('Should remove', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getTiposDeTripulante().click();

    page.getLastRemoveButton().click();

    page.getLogout().click();

    //browser.sleep(3000);

  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
