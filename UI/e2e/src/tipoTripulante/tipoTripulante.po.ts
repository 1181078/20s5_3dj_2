import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  navigateToWhere() {
    return browser.getCurrentUrl() as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }

  getLogin() {
    return element(by.id('login'));
  }

  getEmailLogin() {
    return element(by.id('eMail'));
  }

  getPasswordLogin() {
    return element(by.id('passWord'));
  }

  getLoginButton() {
    return element(by.id('LoginButton'));
  }

  getTiposDeTripulante() {
    return element(by.id('tt'));
  }

  getDomainId() {
    return element(by.id('tipoTripulanteDomainId'));
  }

  getDescription() {
    return element(by.id('tipoTripulanteDesc'));
  }

  getAddButton() {
    return element(by.id('add'));
  }

  getLastRemoveButton() {
    return element.all(by.id('remove')).last();
  }

  getLastAddedTipos(){
    return element.all(by.id('tipoTripul')).last();
  }

  getLastAddedTiposLink(){
    return element.all(by.id('ttClickable')).last();
  }

  getUpdateDesc(){
    return element(by.id('descUpdate'));
  }

  getGoBackButton(){
    return element(by.id('goBack'));
  }

  getSaveButton(){
    return element(by.id('save'));
  }

  getLogout() {
    return element(by.id('logout'));
  }

}
