import { AppPage } from './importFile.po';
import { browser, logging } from 'protractor';

describe('workspace-project ImportFile', async() => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  it('Should drop files as admin', async() => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getRefreshButton().click();

    page.getLoginButton().click();
    
    page.getLoginButton().click();

    page.getImportarFicheiro().click();
    
    // page.getDropZone().sendKeys("test.txt");

    expect(page.getTitleText()).toEqual('Importar Ficheiro');

    // expect(page.getResText()).toEqual('test.txt');

    page.getLogout().click();

    //browser.sleep(3000);

  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
