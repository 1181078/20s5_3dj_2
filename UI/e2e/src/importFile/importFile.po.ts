import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  navigateToWhere() {
    return browser.getCurrentUrl() as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }

  getResText() {
    return element(by.css('h5')).getText() as Promise<string>;
  }
  
  getLogin() {
    return element(by.id('login'));
  }

  getEmailLogin() {
    return element(by.id('eMail'));
  }

  getPasswordLogin() {
    return element(by.id('passWord'));
  }

  getLoginButton() {
    return element(by.id('LoginButton'));
  }

  getImportarFicheiro() {
    return element(by.id('imp'));
  }

  getDropZone() {
    return element(by.id('drop'));
  }

  getRefreshButton() {
    return element(by.id('refr'));
  }

  getLogout() {
    return element(by.id('logout'));
  }

}
