import { AppPage } from './home.po';
import { browser, logging } from 'protractor';

describe('workspace-project Home', async () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  it('Should go to home as admin', async () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();

    page.getLoginButton().click();

    page.getInicio().click();

    expect(page.getTitleText()).toEqual('Termos e Condições');

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should go to home as gestor', async () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("gestor");

    page.getPasswordLogin().sendKeys("gestor");

    page.getLoginButton().click();
    
    page.getLoginButton().click();

    page.getInicio().click();

    expect(page.getTitleText()).toEqual('Termos e Condições');

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should go to home as cliente', async () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("client");

    page.getPasswordLogin().sendKeys("client");

    page.getLoginButton().click();
    
    page.getLoginButton().click();

    page.getInicio().click();

    expect(page.getTitleText()).toEqual('Termos e Condições');

    page.getLogout().click();

    //browser.sleep(3000);

  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
