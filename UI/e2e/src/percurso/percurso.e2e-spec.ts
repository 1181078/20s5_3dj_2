import { AppPage } from './percurso.po';
import { browser, logging } from 'protractor';

describe('workspace-project Percurso', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  it('Should have percursos', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getPercursos().click();

    expect(page.getTitleText()).toEqual('Percursos');

    page.getLogout().click();

    //browser.sleep(3000);
  });

  it('Should add percurso', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getPercursos().click();

    page.getDomainId().sendKeys("percursoTest");
  
    page.getAddButton().click();

    // expect(page.getLastAddedTipos().getText()).toEqual("percursoTest");

    page.getLogout().click();

    //browser.sleep(3000);
  });

  it('Should redirect to percurso detail', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getPercursos().click();

    page.getLastAddedTiposLink().click();

    // expect(page.navigateToWhere()).toEqual('http://localhost:4200/percursos/percursoTest');
  
    page.getLogout().click();

    //browser.sleep(3000);
  });

  it('Should redirect back', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getPercursos().click();

    page.getLastAddedTiposLink().click();

    page.getGoBackButton().click();

    expect(page.navigateToWhere()).toEqual('http://localhost:4200/percursos');

    page.getLogout().click();

    //browser.sleep(3000);
  });

  it('Should update desc', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getPercursos().click();
    
    page.getLastAddedTiposLink().click();

    page.getSaveButton().click();
    
    page.getPercursos().click();
    
    expect(page.getLastAddedTiposLink().getText()).toEqual("percursoTest");

    page.getLogout().click();

    //browser.sleep(3000);
  });

  it('Should remove', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getPercursos().click();

    page.getLastRemoveButton().click();

    page.getLogout().click();

    //browser.sleep(3000);
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
