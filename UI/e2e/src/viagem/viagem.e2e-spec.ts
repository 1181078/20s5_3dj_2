// import { AppPage } from './tipoTripulante.po';
// import { browser, logging } from 'protractor';

// describe('workspace-project Viagem', () => {
//   let page: AppPage;

//   beforeEach(() => {
//     page = new AppPage();
//   });


//   it('Should have viagens', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getViagens().click();

//     expect(page.getTitleText()).toEqual('Viagens');

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   it('Should add viagens', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getViagens().click();

//     page.getDomainId().sendKeys("viagemTeste");
  
//     page.getAddButton().click();

//     expect(page.getLastAdded().getText()).toEqual("viagemTeste");

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   it('Should redirect to viagem detail', () => {
//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getViagens().click();

//     page.getLastAddedLink().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/viagens/viagemTeste');

//     page.getLogout().click();

//     //browser.sleep(3000);
  
//   });

//   it('Should redirect back', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getViagens().click();

//     page.getLastAddedLink().click();

//     page.getGoBackButton().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/viagens');

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   it('Should update desc', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getViagens().click();
    
//     page.getLastAddedLink().click();

//     page.getSaveButton().click();
    
//     page.getViagens().click();
    
//     expect(page.getLastAddedLink().getText()).toEqual("viagemTeste");

//     page.getLogout().click();

//     //browser.sleep(3000);
//   });

//   it('Should remove', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getViagens().click();

//     page.getLastRemoveButton().click();

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });


//   afterEach(async () => {
//     // Assert that there are no errors emitted from the browser
//     const logs = await browser.manage().logs().get(logging.Type.BROWSER);
//     expect(logs).not.toContain(jasmine.objectContaining({
//       level: logging.Level.SEVERE,
//     } as logging.Entry));
//   });
// });
