import { AppPage } from './map.po';
import { browser, logging } from 'protractor';

describe('workspace-project Map', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  it('Should have Map Menu', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("client");

    page.getPasswordLogin().sendKeys("client");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getMap().click();

    expect(page.getMenuTitle().getText()).toEqual("MENU");

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should have Menu', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("client");

    page.getPasswordLogin().sendKeys("client");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getMap().click();

    expect(page.getTable().isDisplayed()).toBeTruthy();

    page.getLogout().click();

    //browser.sleep(3000);
  
  });

  it('Should have Buttons', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("client");

    page.getPasswordLogin().sendKeys("client");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getMap().click();

    // expect(page.getNosButton().getText()).toEqual("Mostrar Nos");
    // expect(page.getShowPercursosButton().getText()).toEqual("Mostrar Percursos");
    // expect(page.getHidePercursosButton().getText()).toEqual("Esconder Percursos");
    // expect(page.getShowLinhasButton().getText()).toEqual("Mostrar Linhas");
    // expect(page.getHideLinhasButton().getText()).toEqual("Esconder Linhas");

    page.getLogout().click();

    //browser.sleep(3000);

  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
