import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  navigateToWhere() {
    return browser.getCurrentUrl() as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }

  getLogin() {
    return element(by.id('login'));
  }

  getEmailLogin() {
    return element(by.id('eMail'));
  }

  getPasswordLogin() {
    return element(by.id('passWord'));
  }

  getLoginButton() {
    return element(by.id('LoginButton'));
  }

  getMap() {
    return element(by.id('mapa'));
  }

  getMapElem() {
    return element(by.id('map'));
  }

  getMenuTitle() {
    return element(by.id('tm'));
  }

  getTable() {
    return element(by.id('tMenu'));
  }

  getNosButton() {
    return element(by.id('sN'));
  }
  getShowPercursosButton() {
    return element(by.id('sP'));
  }
  getHidePercursosButton() {
    return element(by.id('hP'));
  }
  getShowLinhasButton() {
    return element(by.id('sL'));
  }
  getHideLinhasButton() {
    return element(by.id('hL'));
  }

  getLogout() {
    return element(by.id('logout'));
  }
  
}
