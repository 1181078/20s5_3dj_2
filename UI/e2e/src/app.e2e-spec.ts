// import { AppPage } from './app.po';
// import { browser, logging } from 'protractor';

// describe('workspace-project App', async () => {
//   let page: AppPage;

//   beforeEach(() => {
//     page = new AppPage();
//   });

//   it('should display OPT', async () => {
//     page.navigateTo();
//     expect(page.getTitleText()).toEqual('OPT');
//   });

//   it('Should redirect to login',  async () => {
//     page.navigateTo();

//     page.getLogin().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/login');

//   });

//   it('Should redirect to SignUp', async () => {
//     page.navigateTo();
    
//     page.getSignUp().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/signup');
//   });

//   it('Should login as admin', async () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();

//     page.getLoginButton().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/dashboard');

//   });

//   it('Should redirect to TiposTripulante', async () => {

//     page.getTiposTripulante().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/tiposTripulante');

//   });

//   it('Should redirect to TiposViatura', async () => {

//     page.getTiposViatura().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/tiposViatura');

//   });

//   it('Should redirect to Percursos', async () => {

//     page.getPercursos().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/percursos');

//   });

//   it('Should redirect to Linhas', async () => {

//     page.getLinhas().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/linhas');

//   });

//   it('Should redirect to Nos', async () => {

//     page.getNos().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/nos');

//   });

//   it('Should redirect to ImportFile', async () => {

//     page.getImportarFicheiro().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/importFile');

//   });

//   it('Should redirect to Home', async () => {

//     page.getInicio().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/home');

//   });

//   it('Should redirect to Mapa as Admin', async () => {

//     page.getMapa().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/map');

//   });

//   it('Should redirect to Perfil as Admin', async () => {

//     page.getPerfil().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/me');

//   });
    
//   it('Should redirect to ', () => {

//   });

//   it('Should login as gestor', async () => {

//     page.getLogout().click();

//     //browser.sleep(3000);

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("gestor");

//     page.getPasswordLogin().sendKeys("gestor");

//     await page.getRefreshButton().click();

//     await page.getLoginButton().click();

//     await page.getLoginButton().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/me');

//   });

//   it('Should redirect to GC', async () => {

//     page.getGC().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/gestaoCaminhos');

//   });

//   it('Should redirect to Perfil as Gestor', async () => {

//     page.getPerfil().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/me');

//   });

//   it('Should login as cliente',async  () => {

//     page.getLogout().click();

//     //browser.sleep(3000);

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("client");

//     page.getPasswordLogin().sendKeys("client");

//     page.getLoginButton().click();
    
//     page.getLoginButton().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/map');

//   });

//   it('Should redirect to Perfil as Cliente', async () => {

//     page.getPerfil().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/me');

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   afterEach(async () => {
//     // Assert that there are no errors emitted from the browser
//     const logs = await browser.manage().logs().get(logging.Type.BROWSER);
//     expect(logs).not.toContain(jasmine.objectContaining({
//       level: logging.Level.SEVERE,
//     } as logging.Entry));
//   });
// });
