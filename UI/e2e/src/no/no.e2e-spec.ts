import { AppPage } from './no.po';
import { browser, logging } from 'protractor';

describe('workspace-project No', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  it('Should have tipos', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();

    page.getLoginButton().click();

    page.getNos().click();

    expect(page.getTitleText()).toEqual('Nós');

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should add tipo', () => {
    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getNos().click();

    page.getDomainId().sendKeys("noTeste");

    page.getNome().sendKeys("noTeste");
    
    page.getLatitude().sendKeys(10);
    
    page.getLongitude().sendKeys(12);
    
    page.getNomeCurto().sendKeys("noTo");
  
    page.getAddButton().click();

    expect(page.getLastAddedTipos().getText()).toEqual("noTeste");

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should redirect to tipo detail', () => {
    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getNos().click();

    page.getLastAddedTiposLink().click();

    expect(page.navigateToWhere()).toEqual('http://localhost:4200/nos/noTeste');

    page.getLogout().click();

    //browser.sleep(3000);
  
  });

  it('Should redirect back', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getNos().click();

    page.getLastAddedTiposLink().click();

    page.getGoBackButton().click();

    expect(page.navigateToWhere()).toEqual('http://localhost:4200/nos');

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should update desc', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getNos().click();
    
    page.getLastAddedTiposLink().click();

    page.getUpdateNome().sendKeys("newNoTeste");
    
    page.getUpdateLatitude().sendKeys(41);
    
    page.getUpdateLongitude().sendKeys(23);
    
    page.getUpdateNomeCurto().sendKeys("newPo");

    page.getSaveButton().click();
    
    page.getNos().click();
    
    expect(page.getLastAddedTiposLink().getText()).toEqual("noTeste noTeste");

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should remove', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getNos().click();

    page.getLastRemoveButton().click();

    page.getLogout().click();

    //browser.sleep(3000);

  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
