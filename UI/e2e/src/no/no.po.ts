import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  navigateToWhere() {
    return browser.getCurrentUrl() as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }

  getLogin() {
    return element(by.id('login'));
  }

  getEmailLogin() {
    return element(by.id('eMail'));
  }

  getPasswordLogin() {
    return element(by.id('passWord'));
  }

  getLoginButton() {
    return element(by.id('LoginButton'));
  }

  getNos() {
    return element(by.id('nos'));
  }

  getDomainId() {
    return element(by.id('noDomainId'));
  }

  getNome() {
    return element(by.id('noNome'));
  }

  getLatitude() {
    return element(by.id('noLat'));
  }

  getLongitude() {
    return element(by.id('noLon'));
  }

  getNomeCurto() {
    return element(by.id('noNomeCurto'));
  }

  getIsEstacaoRecolha() {
    return element(by.id('noER'));
  }

  getIsPontoRendicao() {
    return element(by.id('noPR'));
  }

  getAddButton() {
    return element(by.id('add'));
  }

  getLastRemoveButton() {
    return element.all(by.id('remove')).last();
  }

  getLastAddedTipos(){
    return element.all(by.id('no')).last();
  }

  getLastAddedTiposLink(){
    return element.all(by.id('noClickable')).last();
  }

  getUpdateNome(){
    return element(by.id('no.nome'));
  }
  
  getUpdateLatitude(){
    return element(by.id('no.latitude'));
  }
  
  getUpdateLongitude(){
    return element(by.id('no.longitude'));
  }
  
  getUpdateNomeCurto(){
    return element(by.id('no.nomeCurto'));
  }
  
  getUpdateEstacaoRecolha(){
    return element(by.id('no.isEstacaoRecolha'));
  }
  
  getUpdatePontoRendicao(){
    return element(by.id('no.isPontoRendicao'));
  }

  getGoBackButton(){
    return element(by.id('goBack'));
  }

  getSaveButton(){
    return element(by.id('save'));
  }

  getLogout() {
    return element(by.id('logout'));
  }

}
