import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  navigateToWhere() {
    return browser.getCurrentUrl() as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }

  getLogin() {
    return element(by.id('login'));
  }

  getEmailLogin() {
    return element(by.id('eMail'));
  }

  getPasswordLogin() {
    return element(by.id('passWord'));
  }

  getLoginButton() {
    return element(by.id('LoginButton'));
  }

  getTiposDeViatura() {
    return element(by.id('tv'));
  }

  getDomainId() {
    return element(by.id('tipoViaturaDomainId'));
  }

  getNome() {
    return element(by.id('tipoViaturaNome'));
  }
  getAutonomia() {
    return element(by.id('tipoViaturaAutonomia'));
  }
  getCusto() {
    return element(by.id('tipoViaturaCusto'));
  }
  getVelMedia() {
    return element(by.id('tipoViaturaVelocidadeMedia'));
  }
  getCombustivel() {
    return element(by.id('tipoViaturaCombustivel'));
  }
  getConsumo() {
    return element(by.id('tipoViaturaConsumo'));
  }
  getEmissoes() {
    return element(by.id('tipoViaturaEmissoes'));
  }

  getAddButton() {
    return element(by.id('add'));
  }

  getLastRemoveButton() {
    return element.all(by.id('remove')).last();
  }

  getLastAddedTipos(){
    return element.all(by.id('tipoViat')).last();
  }

  getLastAddedTiposLink(){
    return element.all(by.id('tvClickable')).last();
  }

  getUpdateNome(){
    return element(by.id('nomeUpdate'));
  }
  
  getUpdateAutonomia(){
    return element(by.id('autonomiaUpdate'));
  }
  
  getUpdateCusto(){
    return element(by.id('custoUpdate'));
  }
  
  getUpdateVelocidade(){
    return element(by.id('velUpdate'));
  }
  
  getUpdateCombustivel(){
    return element(by.id('combustUpdate'));
  }
  
  getUpdateConsumo(){
    return element(by.id('consumoUpdate'));
  }
  
  getUpdateEmissoes(){
    return element(by.id('emissoesUpdate'));
  }

  getGoBackButton(){
    return element(by.id('goBack'));
  }

  getSaveButton(){
    return element(by.id('save'));
  }

  getLogout() {
    return element(by.id('logout'));
  }

}
