import { AppPage } from './tipoViatura.po';
import { browser, logging } from 'protractor';

describe('workspace-project TipoViatura', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  it('Should have tipos', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getTiposDeViatura().click();

    expect(page.getTitleText()).toEqual('Tipos Viatura');

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should add tipo', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getTiposDeViatura().click();

    page.getDomainId().sendKeys("autocarroTeste");

    page.getNome().sendKeys("autocarroTeste");
    
    page.getAutonomia().sendKeys(10);
    
    page.getCombustivel().sendKeys(120);
    
    page.getConsumo().sendKeys(11);
    
    page.getCusto().sendKeys(99);
    
    page.getEmissoes().sendKeys(89);
    
    page.getVelMedia().sendKeys(60);
  
    page.getAddButton().click();

    expect(page.getLastAddedTipos().getText()).toEqual("autocarroTeste");

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should redirect to tipo detail', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getTiposDeViatura().click();

    page.getLastAddedTiposLink().click();

    expect(page.navigateToWhere()).toEqual('http://localhost:4200/tiposViatura/autocarroTeste');

    page.getLogout().click();

    //browser.sleep(3000);
  
  });

  it('Should redirect back', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getTiposDeViatura().click();

    page.getLastAddedTiposLink().click();

    page.getGoBackButton().click();

    expect(page.navigateToWhere()).toEqual('http://localhost:4200/tiposViatura');

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should update desc', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getTiposDeViatura().click();
    
    page.getLastAddedTiposLink().click();

    page.getUpdateNome().sendKeys("newAutocarroTeste");
    
    page.getUpdateAutonomia().sendKeys(10);
    
    page.getUpdateCombustivel().sendKeys(120);
    
    page.getUpdateConsumo().sendKeys(11);
    
    page.getUpdateCusto().sendKeys(99);
    
    page.getUpdateEmissoes().sendKeys(89);
    
    page.getUpdateVelocidade().sendKeys(60);

    page.getSaveButton().click();
    
    page.getTiposDeViatura().click();
    
    expect(page.getLastAddedTiposLink().getText()).toEqual("autocarroTeste autocarroTeste");

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should remove', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getTiposDeViatura().click();

    page.getLastRemoveButton().click();

    page.getLogout().click();

    //browser.sleep(3000);

  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
