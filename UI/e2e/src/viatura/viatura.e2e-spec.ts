// import { AppPage } from './tipoTripulante.po';
// import { browser, logging } from 'protractor';

// describe('workspace-project Viatura', () => {
//   let page: AppPage;

//   beforeEach(() => {
//     page = new AppPage();
//   });


//   it('Should have viaturas', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getViaturas().click();

//     expect(page.getTitleText()).toEqual('Viaturas');

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   it('Should add viatura', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getViaturas().click();

//     page.getMatricula().sendKeys("11-11-11");

//     page.getVIN().sendKeys("12345");
  
//     page.getAddButton().click();

//     expect(page.getLastAdded().getText()).toEqual("11-11-11");

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   it('Should redirect to viatura detail', () => {
//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getViaturas().click();

//     page.getLastAddedLink().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/viaturas/11-11-11');

//     page.getLogout().click();

//     //browser.sleep(3000);
  
//   });

//   it('Should redirect back', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getViaturas().click();

//     page.getLastAddedLink().click();

//     page.getGoBackButton().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/viaturas');

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   it('Should update desc', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getViaturas().click();
    
//     page.getLastAddedLink().click();

//     page.getSaveButton().click();
    
//     page.getViaturas().click();
    
//     expect(page.getLastAddedLink().getText()).toEqual("11-11-11");

//     page.getLogout().click();

//     //browser.sleep(3000);
//   });

//   it('Should remove', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getViaturas().click();

//     page.getLastRemoveButton().click();

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });


//   afterEach(async () => {
//     // Assert that there are no errors emitted from the browser
//     const logs = await browser.manage().logs().get(logging.Type.BROWSER);
//     expect(logs).not.toContain(jasmine.objectContaining({
//       level: logging.Level.SEVERE,
//     } as logging.Entry));
//   });
// });
