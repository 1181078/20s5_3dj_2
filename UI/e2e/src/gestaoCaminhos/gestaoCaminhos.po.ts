import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  navigateToWhere() {
    return browser.getCurrentUrl() as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }

  getLogin() {
    return element(by.id('login'));
  }

  getEmailLogin() {
    return element(by.id('eMail'));
  }

  getPasswordLogin() {
    return element(by.id('passWord'));
  }

  getLoginButton() {
    return element(by.id('LoginButton'));
  }

  getGestaoCaminhos() {
    return element(by.id('ges'));
  }

  getTempo() {
    return element(by.id('1'));
  }

  getNoDePartida() {
    return element(by.id('orig'));
  }

  getNoDestino() {
    return element(by.id('dest'));
  }

  getAlgoritmo() {
    return element(by.id('alg'));
  }

  getEnterButton() {
    return element(by.id('enter'));
  }

  getVerResultados() {
    return element(by.id('verResultados'));
  }

  getCaminho(){
      return element(by.id('cam'));
  }

  getCusto(){
      return element(by.id('cust'));
  }

  getRefreshButton() {
    return element(by.id('refr'));
  }

  getLogout() {
    return element(by.id('logout'));
  }

}