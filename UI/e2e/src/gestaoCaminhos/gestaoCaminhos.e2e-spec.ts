import { AppPage } from './gestaoCaminhos.po';
import { browser, logging } from 'protractor';

describe('workspace-project GestaoCaminhos', async () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  it('Should have gestao', async () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("gestor");

    page.getPasswordLogin().sendKeys("gestor");

    page.getRefreshButton().click();
    
    page.getLoginButton().click();

    page.getLoginButton().click();

    page.getGestaoCaminhos().click();

    expect(page.getTitleText()).toEqual('Gestão de Caminhos');

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should enter gestao', async () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("gestor");

    page.getPasswordLogin().sendKeys("gestor");

    page.getRefreshButton().click();
    
    page.getLoginButton().click();

    page.getLoginButton().click();

    page.getGestaoCaminhos().click();

    page.getTempo().sendKeys(1);

    page.getNoDePartida().sendKeys("PARED");
    
    page.getNoDestino().sendKeys("LORDL");
    
    page.getAlgoritmo().sendKeys("A*");
    
    page.getEnterButton().click();
    
    page.getVerResultados().click();

    expect(page.getCaminho().getText()).toEqual("[('PARED','LORDL',6)]");

    expect(page.getCusto().getText()).toEqual('48419')

    page.getLogout().click();

    //browser.sleep(3000);

  });



  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
