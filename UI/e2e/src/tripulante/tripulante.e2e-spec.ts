// import { AppPage } from './tripulante.po';
// import { browser, logging } from 'protractor';

// describe('workspace-project tripulante', () => {
//   let page: AppPage;

//   beforeEach(() => {
//     page = new AppPage();
//   });


//   it('Should have tipos', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getTripulantes().click();

//     expect(page.getTitleText()).toEqual('Tripulantes');

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   it('Should add tripulante', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getTripulantes().click();

//     page.getNumMecan().sendKeys("10111214");
    
//     page.getNome().sendKeys("PessoaTeste");

//     page.getCC().sendKeys("123456789");

//     page.getNIF().sendKeys("987654321");
  
//     page.getAddButton().click();

//     expect(page.getLastAdded().getText()).toEqual("10111214 PessoaTeste");

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   it('Should redirect to tripulante detail', () => {
//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getTripulantes().click();

//     page.getLastAddedLink().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/tripulantes/10111214');

//     page.getLogout().click();

//     //browser.sleep(3000);
  
//   });

//   it('Should redirect back', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getTripulantes().click();

//     page.getLastAddedLink().click();

//     page.getGoBackButton().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/tripulantes');

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   it('Should update desc', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getTripulantes().click();
    
//     page.getLastAddedLink().click();

//     page.getUpdateNome().sendKeys("newPessoa");
//     page.getUpdateCC().sendKeys("24681012");
//     page.getUpdateNIF().sendKeys("01357911");

//     page.getSaveButton().click();
    
//     page.getTripulantes().click();
    
//     expect(page.getLastAddedLink().getText()).toEqual("10111214 newPessoa");

//     page.getLogout().click();

//     //browser.sleep(3000);
//   });

//   it('Should remove', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getTripulantes().click();

//     page.getLastRemoveButton().click();

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });


//   afterEach(async () => {
//     // Assert that there are no errors emitted from the browser
//     const logs = await browser.manage().logs().get(logging.Type.BROWSER);
//     expect(logs).not.toContain(jasmine.objectContaining({
//       level: logging.Level.SEVERE,
//     } as logging.Entry));
//   });
// });
