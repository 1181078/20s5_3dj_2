import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  navigateToWhere() {
    return browser.getCurrentUrl() as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }

  getLogin() {
    return element(by.id('login'));
  }

  getEmailLogin() {
    return element(by.id('eMail'));
  }

  getPasswordLogin() {
    return element(by.id('passWord'));
  }

  getLoginButton() {
    return element(by.id('LoginButton'));
  }

  getTripulantes() {
    return element(by.id('t'));
  }

  getNumMecan() {
    return element(by.id('tripulanteNumMecanografico'));
  }

  getNome() {
    return element(by.id('tripulanteNome'));
  }

  getCC() {
    return element(by.id('tripulanteCC'));
  }

  getNIF() {
    return element(by.id('tripulanteNIF'));
  }

  getAddButton() {
    return element(by.id('add'));
  }

  getLastRemoveButton() {
    return element.all(by.id('remove')).last();
  }

  getLastAdded(){
    return element.all(by.id('tripul')).last();
  }

  getLastAddedLink(){
    return element.all(by.id('tClickable')).last();
  }

  getUpdateNome(){
    return element(by.id('nomeUpdate'));
  }

  getUpdateCC(){
    return element(by.id('CCUpdate'));
  }

  getUpdateNIF(){
    return element(by.id('NIFUpdate'));
  }

  getGoBackButton(){
    return element(by.id('goBack'));
  }

  getSaveButton(){
    return element(by.id('save'));
  }

  getLogout() {
    return element(by.id('logout'));
  }

}
