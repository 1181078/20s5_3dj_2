// import { AppPage } from './tipoTripulante.po';
// import { browser, logging } from 'protractor';

// describe('workspace-project Blocos de Trabalho', () => {
//   let page: AppPage;

//   beforeEach(() => {
//     page = new AppPage();
//   });


//   it('Should have blocos', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getBlocos().click();

//     expect(page.getTitleText()).toEqual('Blocos de Trabalho');

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   it('Should add bloco', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getBlocos().click();

//     page.getDomainId().sendKeys("blocoTeste");
  
//     page.getAddButton().click();

//     expect(page.getLastAdded().getText()).toEqual("blocoTeste");

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   it('Should redirect to bloco detail', () => {
//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getBlocos().click();

//     page.getLastAddedLink().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/blocosTrabalho/blocoTeste');

//     page.getLogout().click();

//     //browser.sleep(3000);
  
//   });

//   it('Should redirect back', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getBlocos().click();

//     page.getLastAddedLink().click();

//     page.getGoBackButton().click();

//     expect(page.navigateToWhere()).toEqual('http://localhost:4200/blocosTrabalho');

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });

//   it('Should update desc', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getBlocos().click();
    
//     page.getLastAddedLink().click();

//     page.getSaveButton().click();
    
//     page.getBlocos().click();
    
//     expect(page.getLastAddedLink().getText()).toEqual("blocoTeste");

//     page.getLogout().click();

//     //browser.sleep(3000);
//   });

//   it('Should remove', () => {

//     page.navigateTo();

//     page.getLogin().click();

//     page.getEmailLogin().sendKeys("admin");

//     page.getPasswordLogin().sendKeys("admin");

//     page.getLoginButton().click();
//     page.getLoginButton().click();

//     page.getBlocos().click();

//     page.getLastRemoveButton().click();

//     page.getLogout().click();

//     //browser.sleep(3000);

//   });


//   afterEach(async () => {
//     // Assert that there are no errors emitted from the browser
//     const logs = await browser.manage().logs().get(logging.Type.BROWSER);
//     expect(logs).not.toContain(jasmine.objectContaining({
//       level: logging.Level.SEVERE,
//     } as logging.Entry));
//   });
// });
