import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  navigateToWhere() {
    return browser.getCurrentUrl() as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }

  getLogin() {
    return element(by.id('login'));
  }

  getEmailLogin() {
    return element(by.id('eMail'));
  }

  getPasswordLogin() {
    return element(by.id('passWord'));
  }

  getLoginButton() {
    return element(by.id('LoginButton'));
  }

  getBlocos() {
    return element(by.id('bloc'));
  }

  getDomainId() {
    return element(by.id('blocoTrabalhoDomainId'));
  }

  getAddButton() {
    return element(by.id('add'));
  }

  getLastRemoveButton() {
    return element.all(by.id('remove')).last();
  }

  getLastAdded(){
    return element.all(by.id('blocks')).last();
  }

  getLastAddedLink(){
    return element.all(by.id('blClickable')).last();
  }

  getGoBackButton(){
    return element(by.id('goBack'));
  }

  getSaveButton(){
    return element(by.id('save'));
  }

  getLogout() {
    return element(by.id('logout'));
  }

}
