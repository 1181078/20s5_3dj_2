import { AppPage } from './linha.po';
import { browser, logging } from 'protractor';

describe('workspace-project Linha', async() => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  // it('Should have linhas', async  () => {

  //   page.navigateTo();

  //   page.getLogin().click();

  //   page.getEmailLogin().sendKeys("admin");

  //   page.getPasswordLogin().sendKeys("admin");

  //   page.getLoginButton().click();
  //   page.getLoginButton().click();

  //   page.getLinhas().click();

  //   expect(page.getTitleText()).toEqual('Linhas');

  //   page.getLogout().click();

  //   //browser.sleep(3000);
  // });

  // it('Should add percurso', async () => {

  //   page.navigateTo();

  //   page.getLogin().click();

  //   page.getEmailLogin().sendKeys("admin");

  //   page.getPasswordLogin().sendKeys("admin");

  //   page.getLoginButton().click();
  //   page.getLoginButton().click();

  //   page.getLinhas().click();

  //   page.getDomainId().sendKeys("linhaTest");
    
  //   page.getNomeLinha().sendKeys("linhaTest");
    
  //   page.getCorLinha().sendKeys("RBG(X,Y,Z)");
  
  //   //page.getAddButton().click();

  //   //expect(page.getLastAddedLinha().getText()).toEqual("linhaTest");

  //   page.getLogout().click();

  //   //browser.sleep(3000);

  // });

  // it('Should redirect to percurso detail', async () => {

  //   page.navigateTo();

  //   page.getLogin().click();

  //   page.getEmailLogin().sendKeys("admin");

  //   page.getPasswordLogin().sendKeys("admin");

  //   page.getLoginButton().click();
  //   page.getLoginButton().click();

  //   page.getLinhas().click();

  //   //page.getLastAddedTiposLink().click();

  //   //expect(page.navigateToWhere()).toEqual('http://localhost:4200/linhas/linhaTest');

  //   page.getLogout().click();

  //   //browser.sleep(3000);
  
  // });

  // it('Should redirect back', async () => {

  //   page.navigateTo();

  //   page.getLogin().click();

  //   page.getEmailLogin().sendKeys("admin");

  //   page.getPasswordLogin().sendKeys("admin");

  //   page.getLoginButton().click();
  //   page.getLoginButton().click();

  //   page.getLinhas().click();

  //   page.getLastAddedTiposLink().click();

  //   page.getGoBackButton().click();

  //   expect(page.navigateToWhere()).toEqual('http://localhost:4200/linhas');

  //   page.getLogout().click();

  //   //browser.sleep(3000);

  // });

  // it('Should update desc', async () => {

  //   page.navigateTo();

  //   page.getLogin().click();

  //   page.getEmailLogin().sendKeys("admin");

  //   page.getPasswordLogin().sendKeys("admin");

  //   page.getLoginButton().click();
  //   page.getLoginButton().click();

  //   page.getLinhas().click();
    
  //   page.getLastAddedTiposLink().click();

  //   page.getNomeLinhaUpdate().sendKeys("linhaTest linhaTest");

  //   //page.getSaveButton().click();
    
  //   page.getLinhas().click();
    
  //   //expect(page.getLastAddedTiposLink().getText()).toEqual("newLinhaTest");

  //   page.getLogout().click();

  //   //browser.sleep(3000);

  // });

  // it('Should remove', async () => {

  //   page.navigateTo();

  //   page.getLogin().click();

  //   page.getEmailLogin().sendKeys("admin");

  //   page.getPasswordLogin().sendKeys("admin");

  //   page.getLoginButton().click();
  //   page.getLoginButton().click();

  //   page.getLinhas().click();

  //   //page.getLastRemoveButton().click();

  //   page.getLogout().click();

  //   //browser.sleep(3000);

  // });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
