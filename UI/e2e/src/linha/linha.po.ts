import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  navigateToWhere() {
    return browser.getCurrentUrl() as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h2')).getText() as Promise<string>;
  }

  getLogin() {
    return element(by.id('login'));
  }

  getEmailLogin() {
    return element(by.id('eMail'));
  }

  getPasswordLogin() {
    return element(by.id('passWord'));
  }

  getLoginButton() {
    return element(by.id('LoginButton'));
  }

  getLinhas() {
    return element(by.id('lin'));
  }

  getDomainId() {
    return element(by.id('linhaDomainId'));
  }

  getNomeLinha() {
    return element(by.id('linhaNome'));
  }

  getCorLinha() {
    return element(by.id('linhaCor'));
  }

  getAddButton() {
    return element(by.id('add'));
  }

  getLastRemoveButton() {
    return element.all(by.id('remove')).last();
  }

  getLastAddedLinha(){
    return element.all(by.id('linhaa')).last();
  }

  getLastAddedTiposLink(){
    return element.all(by.id('lClickable')).last();
  }

  getNomeLinhaUpdate() {
    return element(by.id('linhaNomeUpdate'));
  }

  getCorLinhaUpdate() {
    return element(by.id('linhaCorUpdate'));
  }

  getGoBackButton(){
    return element(by.id('goBack'));
  }

  getSaveButton(){
    return element(by.id('save'));
  }

  getRefreshButton() {
    return element(by.id('refr'));
  }

  getLogout() {
    return element(by.id('logout'));
  }

}
