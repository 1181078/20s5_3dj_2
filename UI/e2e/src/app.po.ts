import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  navigateToWhere() {
    return browser.getCurrentUrl() as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h1')).getText() as Promise<string>;
  }
  
  getLogin() {
    return element(by.id('login'));
  }
  
  getSignUp() {
    return element(by.id('signup'));
  }

  getImportarFicheiro() {
    return element(by.id('imp'));
  }

  getTiposTripulante() {
    return element(by.id('tt'));
  }
  
  getTiposViatura() {
    return element(by.id('tv'));
  }

  getPercursos() {
    return element(by.id('per'));
  }
  
  getNos() {
    return element(by.id('nos'));
  }
  
  getLinhas() {
    return element(by.id('lin'));
  }

  getDashboard() {
    return element(by.id('dash'));
  }
  
  getGC() {
    return element(by.id('ges'));
  }

  getMapa() {
    return element(by.id('mapa'));
  }
  
  getInicio() {
    return element(by.id('ini'));
  }

  getPerfil() {
    return element(by.id('perf'));
  }

  getLogout() {
    return element(by.id('logout'));
  }

  getEmailLogin() {
    return element(by.id('eMail'));
  }

  getPasswordLogin() {
    return element(by.id('passWord'));
  }

  getLoginButton() {
    return element(by.id('LoginButton'));
  }

  getRefreshButton() {
    return element(by.id('refr'));
  }

}
