import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  navigateToWhere() {
    return browser.getCurrentUrl() as Promise<any>;
  }

  getTitleText() {
    return element(by.css('h1')).getText() as Promise<string>;
  }
  
  getLogin() {
    return element(by.id('login'));
  }
  
  getSignUp() {
    return element(by.id('signup'));
  }

  getPerfil() {
    return element(by.id('perf'));
  }

  getPerfilEmail() {
    return element(by.id('emailPerfil'));
  }

  getPerfilFuncao() {
    return element(by.id('funcaoPerfil'));
  }

  getLogout() {
    return element(by.id('logout'));
  }

  getEmailLogin() {
    return element(by.id('eMail'));
  }

  getPasswordLogin() {
    return element(by.id('passWord'));
  }

  getLoginButton() {
    return element(by.id('LoginButton'));
  }

  getTV() {
    return element(by.id('tv'));
  }

  getRefreshButton() {
    return element(by.id('refr'));
  }

}
