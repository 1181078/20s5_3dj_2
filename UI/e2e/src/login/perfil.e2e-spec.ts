import { AppPage } from './login.po';
import { browser, logging } from 'protractor';

describe('workspace-project Perfil', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  it('Should see admin credential', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getPerfil().click();

    expect(page.getPerfilFuncao().getText()).toEqual('Função: Admin');

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should see gestor credential', () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("gestor");

    page.getPasswordLogin().sendKeys("gestor");

    page.getLoginButton().click();
    page.getLoginButton().click();

    page.getPerfil().click();

    expect(page.getPerfilFuncao().getText()).toEqual('Função: Gestor');

    page.getLogout().click();

    //browser.sleep(3000);

  });

  // it('Should see cliente credential', () => {

  //   page.navigateTo();

  //   page.getLogin().click();

  //   page.getEmailLogin().sendKeys("client");

  //   page.getPasswordLogin().sendKeys("client");

  //   page.getLoginButton().click();
  //   page.getLoginButton().click();

  //   page.getPerfil().click();

  //   expect(page.getPerfilFuncao().getText()).toEqual('Função: Cliente');

  //   page.getLogout().click();

  //   //browser.sleep(3000);

  // });


  


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
