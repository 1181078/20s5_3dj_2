import { AppPage } from './login.po';
import { browser, logging, protractor } from 'protractor';

describe('workspace-project login', async () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  it('Should login as admin', async () => {
    
    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("admin");

    page.getPasswordLogin().sendKeys("admin");

    page.getRefreshButton().click();
    
    page.getLoginButton().click();
    page.getLoginButton().click();

    expect(page.navigateToWhere()).toEqual('http://localhost:4200/dashboard');
    
    page.getLogout().click();

    //browser.sleep(3000);

  });


  
  it('Should login as gestor', async () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("gestor");

    page.getPasswordLogin().sendKeys("gestor");

    page.getRefreshButton().click();

    page.getLoginButton().click();
    page.getLoginButton().click();

    expect(page.navigateToWhere()).toEqual('http://localhost:4200/me');

    page.getLogout().click();

    //browser.sleep(3000);

  });

  it('Should login as cliente', async () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("client");

    page.getPasswordLogin().sendKeys("client");

    page.getLoginButton().click();
    page.getLoginButton().click();

    expect(page.navigateToWhere()).toEqual('http://localhost:4200/map');

    page.getLogout().click();

    //browser.sleep(3000);
  });

  
  it('Should not login', async () => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("gestor");

    page.getPasswordLogin().sendKeys("notgestor");

    //page.getLoginButton().click();
    // page.getLoginButton().click();

    expect(page.navigateToWhere()).toEqual('http://localhost:4200/login');

  });

  it('Should not login 2 ',  async() => {

    page.navigateTo();

    page.getLogin().click();

    page.getEmailLogin().sendKeys("client");

    page.getPasswordLogin().sendKeys("notclient");

    //page.getLoginButton().click();
    // page.getLoginButton().click();

    expect(page.navigateToWhere()).toEqual('http://localhost:4200/login');

  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
