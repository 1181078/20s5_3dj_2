import { AppPage } from './pageNotFound.po';
import { browser, logging } from 'protractor';

describe('workspace-project pageNotFound', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });


  // it('Should go to PNF as admin', () => {

  //   page.navigateTo();

  //   page.getLogin().click();

  //   page.getEmailLogin().sendKeys("admin");

  //   page.getPasswordLogin().sendKeys("admin");

  //   page.getLoginButton().click();
  //   page.getLoginButton().click();

  //   page.getPNF();

  //   expect(page.getTitleText()).toEqual('page-not-found');

  //   page.getLogout().click();

  //   //browser.sleep(3000);

  // });

  // it('Should go to home as gestor', () => {

  //   page.navigateTo();

  //   page.getLogin().click();

  //   page.getEmailLogin().sendKeys("gestor");

  //   page.getPasswordLogin().sendKeys("gestor");

  //   page.getLoginButton().click();
  //   page.getLoginButton().click();

  //   page.getPNF();

  //   expect(page.getTitleText()).toEqual('page-not-found');

  //   page.getLogout().click();

  //   //browser.sleep(3000);

  // });



  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
