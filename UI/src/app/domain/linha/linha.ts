export interface Linha {
  domainId: string;
  nome: string;
  cor: string;
  percursoLinha : { percursoLinhaId: String, percurso: String, orientation: String }[];
  }