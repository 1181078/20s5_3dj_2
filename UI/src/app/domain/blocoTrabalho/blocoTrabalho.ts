export interface BlocoTrabalho {
  domainId: string;
  tempoInicio: number;
  tempoFim: number;
  noInicio: string;
  noFim: string;
  viagens: string[];
}