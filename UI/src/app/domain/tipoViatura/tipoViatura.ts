export interface TipoViatura {
  domainId: string;
  nome: string;
  autonomia: number;
  custo: number;
  velocidadeMedia: number;
  combustivel: number;
  consumo: number;
  emissoes: number;
  }