export interface Viagem {
  domainId: string;
  vazio: boolean;
  lineId: string;
  pathId: string;
  gerado: boolean;
  tempos: number[];
}