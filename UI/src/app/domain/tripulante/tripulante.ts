export interface Tripulante {
  numMecanografico: number;
  nome: string;
  dataNascimento: Date;
  numCartaoCidadao: number;
  NIF: number;
  tipoTripulante: string;
  dataEntrada: Date;
  dataSaida: Date;
}