export interface ServicoTripulante {
  domainId: string;
  nome: string;
  cor: string;
  blocks: string[];
}