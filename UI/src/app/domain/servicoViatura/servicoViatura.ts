export interface ServicoViatura {
  domainId: string;
  nome: string;
  cor: string;
  blocks: string[];
}