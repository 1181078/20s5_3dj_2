export interface No {
  domainId: string;
  nome: string;
  latitude: number;
  longitude: number;
  nomeCurto: string;
  isEstacaoRecolha: boolean;
  isPontoRendicao: boolean;
  tempoViagem: { noId: String, duracao: Number}[];
  }