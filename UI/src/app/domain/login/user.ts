import { Role } from './role';

export class User {
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    role: Role;
}
