import { NosPercurso } from "../../../../../MDR/src/domain/percurso/nosPercurso";

export interface Percurso {
  domainId: string;
  nosPercurso: {noId: String, duracao: Number, distancia: Number}[];
  }