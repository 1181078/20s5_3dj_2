export interface Viatura {
  matricula: string;
  VIN: string;
  tipoViatura: string;
  dataEntrada: Date;
}