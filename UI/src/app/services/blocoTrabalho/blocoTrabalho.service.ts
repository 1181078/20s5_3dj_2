import { Injectable } from '@angular/core';
import { BlocoTrabalho } from '../../domain/blocoTrabalho/blocoTrabalho';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { domain } from 'process';


@Injectable({
  providedIn: 'root'
})
export class BlocoTrabalhoService {

  constructor(
    private http: HttpClient, private messageService: MessageService) { }

  public log(message: string) {
    this.messageService.add(`BlocoTrabalhoService: ${message}`);
  }

  private blocosTrabalhoUrl = 'https://localhost:7001/api/blocosTrabalho';
  
  

  getBlocosTrabalho(): Observable<BlocoTrabalho[]>  {
    return this.http.get<BlocoTrabalho[]>(this.blocosTrabalhoUrl)
      .pipe(
        tap(_ => this.log('fetched blocosTrabalho')),
        catchError(this.handleError<BlocoTrabalho[]>('getBlocosTrabalho', []))
      );
  } 

  getBlocoTrabalho(domainId: string): Observable<BlocoTrabalho> 
  {
    return this.getBlocosTrabalho().pipe(
      map((tipos: BlocoTrabalho[]) => tipos.find(tipo => tipo.domainId === domainId)));
  }
 

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updateBlocoTrabalho(blocoTrabalho: BlocoTrabalho): Observable<any> {
    return this.http.put(this.blocosTrabalhoUrl, blocoTrabalho, this.httpOptions).pipe(
      tap(_ => this.log(`updated blocoTrabalho domainId=${blocoTrabalho.domainId}`)),
      catchError(this.handleError<any>('updateBlocoTrabalho'))
    );
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  addBlocoTrabalho(blocoTrabalho: BlocoTrabalho): Observable<BlocoTrabalho> {
    console.log(blocoTrabalho);
    return this.http.post<BlocoTrabalho>(this.blocosTrabalhoUrl, blocoTrabalho, this.httpOptions).pipe(
      tap((newBlocoTrabalho: BlocoTrabalho) => this.log(`added blocoTrabalho w/ domainId=${newBlocoTrabalho.domainId}`)),
      catchError(this.handleError<BlocoTrabalho>('addBlocoTrabalho'))
    );
  }

  deleteBlocoTrabalho(blocoTrabalho: BlocoTrabalho | number): Observable<BlocoTrabalho> {
    const domainId = typeof blocoTrabalho === 'number' ? blocoTrabalho : blocoTrabalho.domainId;
    const url = `${this.blocosTrabalhoUrl}/${domainId}/hard`;
  
    return this.http.delete<BlocoTrabalho>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted blocoTrabalho domainId=${domainId}`)),
      catchError(this.handleError<BlocoTrabalho>('deleteBlocoTrabalho'))
    );
  }

  searchBlocosTrabalho(term: string): Observable<BlocoTrabalho[]> {
    if (!term.trim()) {
      // if blocoTrabalhot search term, return empty blocoTrabalho array.
      return of([]);
    }
    return this.http.get<BlocoTrabalho[]>(`${this.blocosTrabalhoUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found blocosTrabalho matching "${term}"`) :
         this.log(`blocoTrabalho blocosTrabalho matching "${term}"`)),
      catchError(this.handleError<BlocoTrabalho[]>('searchsBlocoTrabalho', []))
    );
  }

}
