import { Injectable } from '@angular/core';
import { Viatura } from '../../domain/viatura/viatura';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { domain } from 'process';

@Injectable({
  providedIn: 'root'
})
export class ViaturaService {

  constructor(
    private http: HttpClient, private messageService: MessageService) { }

  private log(message: string) {
    this.messageService.add(`ViaturaService: ${message}`);
  }

  private viaturasUrl = 'https://localhost:7001/api/viaturas';

  getViaturas(): Observable<Viatura[]> {
    return this.http.get<Viatura[]>(this.viaturasUrl)
      .pipe(
        tap(_ => this.log('fetched viaturas')),
        catchError(this.handleError<Viatura[]>('getViaturas', []))
      );
  } 

  getViatura(matricula: string): Observable<Viatura> 
  {
    return this.getViaturas().pipe(
      map((tipos: Viatura[]) => tipos.find(tipo => tipo.matricula === matricula)));
  }
 

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updateViatura(viatura: Viatura): Observable<any> {
    return this.http.put(this.viaturasUrl, viatura, this.httpOptions).pipe(
      tap(_ => this.log(`updated viatura domainId=${viatura.matricula}`)),
      catchError(this.handleError<any>('updateViatura'))
    );
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  addViatura(viatura: Viatura): Observable<Viatura> {
    return this.http.post<Viatura>(this.viaturasUrl, viatura, this.httpOptions).pipe(
      tap((newViatura: Viatura) => this.log(`added viatura w/ domainId=${newViatura.matricula}`)),
      catchError(this.handleError<Viatura>('addViatura'))
    );
  }


  deleteViatura(viatura: Viatura): Observable<any> {
    const domainId = viatura.matricula;
    const url = `${this.viaturasUrl}/${domainId}/hard`;
  
    return this.http.delete<Viatura>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted viatura domainId=${domainId}`)),
      catchError(this.handleError<Viatura>('deleteViatura'))
    );
  }

  searchViaturas(term: string): Observable<Viatura[]> {
    if (!term.trim()) {
      // if not search term, return empty viatura array.
      return of([]);
    }
    return this.http.get<Viatura[]>(`${this.viaturasUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found viaturas matching "${term}"`) :
         this.log(`no viaturas matching "${term}"`)),
      catchError(this.handleError<Viatura[]>('searchsViatura', []))
    );
  }

}
