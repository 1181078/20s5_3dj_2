import { Injectable } from '@angular/core';
import { Tripulante } from '../../domain/tripulante/tripulante';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { domain } from 'process';

@Injectable({
  providedIn: 'root'
})
export class TripulanteService {

  constructor(
    private http: HttpClient) { }

  private tripulantesUrl = 'https://localhost:7001/api/tripulantes';

  getTripulantes(): Observable<Tripulante[]> {
    return this.http.get<Tripulante[]>(this.tripulantesUrl)
      .pipe(
        catchError(this.handleError<Tripulante[]>('getTripulantes', []))
      );
  } 

  getTripulante(numMecano: number): Observable<Tripulante> {
    return this.getTripulantes().pipe(
      map((tipos: Tripulante[]) => tipos.find(tipo => tipo.numMecanografico === numMecano)));
  }



  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updateTripulante(tripulante: Tripulante): Observable<any> {
    return this.http.put(this.tripulantesUrl, tripulante, this.httpOptions).pipe(
      catchError(this.handleError<any>('updateTripulante'))
    );
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  addTripulante(tripulante: Tripulante): Observable<Tripulante> {
    console.log(tripulante);
    return this.http.post<Tripulante>(this.tripulantesUrl, tripulante, this.httpOptions).pipe(
      catchError(this.handleError<Tripulante>('addTripulante'))
    );
  }

  deleteTripulante(tripulante: Tripulante): Observable<any> {
    const numMecanografico = tripulante.numMecanografico;
    const url = `${this.tripulantesUrl}/${numMecanografico}/hard`;
  
    return this.http.delete<Tripulante>(url, this.httpOptions).pipe(
      catchError(this.handleError<Tripulante>('deleteTripulante'))
    );
  }

}
