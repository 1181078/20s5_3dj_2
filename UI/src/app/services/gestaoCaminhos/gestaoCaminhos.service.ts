import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { No } from '../../domain/nos/no';


@Injectable({
    providedIn: 'root'
})
export class GestaoCaminhosService {

    constructor(
        private http: HttpClient) { }



    private aStarURL = 'http://localhost:3000/api/gestaoCaminhos/aStar';
    private allURL = 'http://localhost:3000/api/gestaoCaminhos/todosMelhor';
    private bfURL = 'http://localhost:3000/api/gestaoCaminhos/bestfs';

    httpOptions = {
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Methods': 'POST',
        'Access-Control-Allow-Origin': '*',
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };

    aStar(time: number, orig: string, dest: string): Observable<string> {

        var formData = { "time": time, "orig": orig, "dest": dest };

        return this.http.post<any>(this.aStarURL, formData);

    }

    todas(time: number, orig: string, dest: string): Observable<string> {

        var formData = { "time": time.valueOf(), "orig": orig, "dest": dest };

        return this.http.post<any>(this.allURL, formData);

    }

    bestF(time: number, orig: string, dest: string): Observable<string> {

        var formData = { "time": time.valueOf(), "orig": orig, "dest": dest };

        return this.http.post<any>(this.bfURL, formData);

    }


    private nosUrl = 'http://localhost:3000/api/nos/pontoOuEstacao';

    getNos(): Observable<No[]> {
        return this.http.get<No[]>(this.nosUrl);
    }





}
