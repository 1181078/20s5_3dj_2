import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { No } from '../../domain/nos/no';


@Injectable({
    providedIn: 'root'
})
export class SequenciamentoMotoristasService {

    constructor(
        private http: HttpClient) { }

    private URL = 'http://localhost:3000/api/gestaoCaminhos/AG';

    httpOptions = {
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Methods': 'POST',
        'Access-Control-Allow-Origin': '*',
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };

    ag(nvd:number, ng: number, np: number, pc: number, pm: number, pp: number, tl: number, mv: number, sv: number ): Observable<string> {


        var formData = { "nvd": nvd, "ngeracoes": ng, "dpopulacao": np, "pcruzamento": pc, "pmutacao": pm, "ppassagem": pp, "tlim": tl, "amin": mv, "stabval": sv};

        return this.http.post<any>(this.URL, formData);

    }


    // private nosUrl = 'http://localhost:3000/api/nos/pontoOuEstacao';

    // getNos(): Observable<No[]> {
    //     return this.http.get<No[]>(this.nosUrl);
    // }





}
