import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})
export class ImportFileService {

    constructor(
        private http: HttpClient, private messageService: MessageService) { }

    public log(message: string) {
        this.messageService.add(`ImportFileService: ${message}`);
    }

    private importFilesUrl = 'http://localhost:3000/api/readFile/import';
    private importFilesMDVURL = 'http://localhost:7000/api/ReadFile';

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    importFile(file: File): void {
        //this.http.post<File>(this.importFilesUrl, file, this.httpOptions);

        let formData: FormData = new FormData();
        formData.append('lapr5file', file);

        this.http.post(this.importFilesUrl, formData).subscribe(
            data => {
                console.log(data);
            },
            error => {
                console.log(error);
            }
        );
        this.http.post(this.importFilesMDVURL, formData).subscribe(
            data => {
                console.log(data);
            },
            error => {
                console.log(error);
            }
        );

        this.log(file.name + " imported");
    }





}
