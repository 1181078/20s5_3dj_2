import { Injectable } from '@angular/core';
import { Viagem } from '../../domain/viagem/viagem';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { domain } from 'process';

@Injectable({
  providedIn: 'root'
})
export class ViagemService {

  constructor(
    private http: HttpClient, private messageService: MessageService) { }

  private log(message: string) {
    this.messageService.add(`ViagemService: ${message}`);
  }

  private viagensUrl = 'https://localhost:7001/api/viagens';

  getViagens(): Observable<Viagem[]> {
    return this.http.get<Viagem[]>(this.viagensUrl)
      .pipe(
        tap(_ => this.log('fetched viagens')),
        catchError(this.handleError<Viagem[]>('getViagens', []))
      );
  } 

  getViagem(domainId: string): Observable<Viagem> 
  {
    return this.getViagens().pipe(
      map((tipos: Viagem[]) => tipos.find(tipo => tipo.domainId === domainId)));
  }
 

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updateViagem(viagem: Viagem): Observable<any> {
    return this.http.put(this.viagensUrl, viagem, this.httpOptions).pipe(
      tap(_ => this.log(`updated viagem domainId=${viagem.domainId}`)),
      catchError(this.handleError<any>('updateViagem'))
    );
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  addViagem(viagem: Viagem): Observable<Viagem> {
    return this.http.post<Viagem>(this.viagensUrl, viagem, this.httpOptions).pipe(
      tap((newViagem: Viagem) => this.log(`added viagem w/ domainId=${newViagem.domainId}`)),
      catchError(this.handleError<Viagem>('addViagem'))
    );
  }


  deleteViagem(viagem: Viagem): Observable<any> {
    const domainId = viagem.domainId;
    const url = `${this.viagensUrl}/${domainId}/hard`;
  
    return this.http.delete<Viagem>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted viagem domainId=${domainId}`)),
      catchError(this.handleError<Viagem>('deleteViagem'))
    );
  }

  searchViagens(term: string): Observable<Viagem[]> {
    if (!term.trim()) {
      // if not search term, return empty viagem array.
      return of([]);
    }
    return this.http.get<Viagem[]>(`${this.viagensUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found viagens matching "${term}"`) :
         this.log(`no viagens matching "${term}"`)),
      catchError(this.handleError<Viagem[]>('searchsViagem', []))
    );
  }

}
