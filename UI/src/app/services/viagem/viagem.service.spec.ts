// import { HttpClientModule } from '@angular/common/http';
// import { TestBed, async, ComponentFixture } from '@angular/core/testing';
// import { FormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
// import { RouterTestingModule } from '@angular/router/testing';
// import { AngularDualListBoxModule } from 'angular-dual-listbox';
// import { AppRoutingModule } from '../app-routing.module';
// import { AppComponent } from '../app.component';

// import { NgModule } from '@angular/core';


// import { TiposTripulanteComponent } from '../tipoTripulante/tiposTripulante/tiposTripulante.component';
// import { TipoTripulanteDetailComponent } from '../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
// import { ViagensComponent } from '../viagem/viagens/viagens.component';
// import { ViagemDetailComponent } from '../viagem/viagem-detail/viagem-detail.component';
// import { PercursosComponent } from '../percurso/percursos/percursos.component';
// import { PercursoDetailComponent } from '../percurso/percurso-detail/percurso-detail.component';
// import { NosComponent } from '../no/nos/nos.component';
// import { NoDetailComponent } from '../no/no-detail/no-detail.component';
// import { LinhasComponent } from '../linha/linhas/linhas.component';
// import { LinhaDetailComponent } from '../linha/linha-detail/linha-detail.component';
// import { MessagesComponent } from '../messages/messages.component';
// import { DashboardComponent } from '../dashboard/dashboard.component';
// import { ViagemSearchComponent } from '../viagem/viagem-search/viagem-search.component';
// import { LoginComponent } from '../login/login.component';
// import { MapComponent } from '../map/map.component';
// import { DragDirective } from '../importFile/dragDrop.directive';
// import { PageNotFoundComponent } from '../pageNotFound/page-not-found.component';
// import { ImportFileComponent } from '../importFile/importFile.component';
// import { UserDirective } from '../login/directives/user.directive';
// import { UserRoleDirective } from '../login/directives/user-role.directive';
// import { AuthService } from '../login/services/auth.service';
// import { HomeComponent } from '../home/home.component';
// import { PerfilComponent } from '../login/perfil/perfil.component';
// import { SignupComponent } from '../login/signup/signup.component';
// import { GestaoCaminhosComponent } from '../gestaoCaminhos/gestaoCaminhos.component';
// import { Viagem } from './viagem';
// import { ViagemService } from './viagem.service';
// import { TripulantesComponent } from 'src/app/components/tripulante/tripulantes/tripulantes.component';
// import { ViaturasComponent } from 'src/app/components/viatura/viatura/viaturas.component';
// import { TripulanteDetailComponent } from 'src/app/components/tripulante/tripulante-detail/tripulante-detail.component';
// import { ViaturaDetailComponent } from 'src/app/components/viatura/viatura-detail/viatura-detail.component';
// import { ViagemDetailComponent } from 'src/app/components/viagem/viagem-detail/viagem-detail.component';
// import { ViagensComponent } from 'src/app/components/viagem/viagem/viagens.component';
// import { BlocosTrabalhoComponent } from 'src/app/components/blocoTrabalho/blocosTrabalho/blocosTrabalho.component';
// import { BlocoTrabalhoDetailComponent } from 'src/app/components/blocoTrabalho/blocoTrabalho-detail/blocoTrabalho-detail.component';
// import { ServicosViaturaComponent } from 'src/app/components/servicoViatura/servicosViatura/servicosViatura.component';
// import { ServicosTripulanteComponent } from 'src/app/components/servicoTripulante/servicosTripulante/servicosTripulante.component';
// import { ServicoTripulanteDetailComponent } from 'src/app/components/servicoTripulante/servicoTripulante-detail/servicoTripulante-detail.component';
// import { ServicoViaturaDetailComponent } from 'src/app/components/servicoViatura/servicoViatura-detail/servicoViatura-detail.component';
// import { SequenciamentoMotoristasComponent } from 'src/app/components/sequenciamentoMotoristas/sequenciamentoMotoristas.component';


// describe('ViagensServiceComponent', () => {

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         BrowserModule,
//         AppRoutingModule,
//         FormsModule,
//         HttpClientModule,
//         AngularDualListBoxModule
//       ],
//       declarations: [
//         AppComponent,
//         TiposTripulanteComponent,
//         TipoTripulanteDetailComponent,
//         ViagensComponent,
//         ViagemDetailComponent,
//         PercursosComponent,
//         PercursoDetailComponent,
//         NosComponent,
//         NoDetailComponent,
//         LinhasComponent,
//         LinhaDetailComponent,
//         MessagesComponent,
//         DashboardComponent,
//         ViagemSearchComponent,
//         LoginComponent,
//         MapComponent,
//         PageNotFoundComponent,
//         DragDirective,
//         ImportFileComponent,
//         HomeComponent,
//         PerfilComponent,
//         SignupComponent,
//         GestaoCaminhosComponent,
//         UserDirective,
//         UserRoleDirective,
            // TripulantesComponent,
            // ViaturasComponent,
            // TripulanteDetailComponent,
            // ViaturaDetailComponent,
            // ViagemDetailComponent,
            // ViagensComponent,
            // BlocosTrabalhoComponent,
            // BlocoTrabalhoDetailComponent,
            // ServicosViaturaComponent,
            // ServicosTripulanteComponent,
            // ServicoViaturaDetailComponent,
            // ServicoTripulanteDetailComponent,
            // SequenciamentoMotoristasComponent
//       ],
//     })
//     .compileComponents();
//   }));

//   it('should be created', () => {
//     const service: ViagemService = TestBed.get(ViagemService);
//     expect(service).toBeTruthy();
//   });

//   it('should add tipo', () => {
//     const service: ViagemService = TestBed.get(ViagemService);
//     const domainId = "testeUnit";
//     const nome = "testeUnit";
//     const autonomia = 12;
//     const custo = 13;
//     const velocidadeMedia = 13;
//     const combustivel = 13;
//     const consumo = 13;
//     const emissoes=34;

//     const ti = {domainId, nome, autonomia, custo, velocidadeMedia, combustivel, consumo, emissoes} as Viagem;
//     service.addViagem(ti).subscribe(abc => expect(abc).toEqual(ti));
//   });

//   it('should update tipo', async(() => {
//     const service: ViagemService = TestBed.get(ViagemService);
//     const domainId = "testeUnit";
//     const nome = "newTesteUnit";
//     const autonomia = 2;
//     const custo = 133;
//     const velocidadeMedia = 123;
//     const combustivel = 13;
//     const consumo = 13;
//     const emissoes=34;

//     const ti = {domainId, nome, autonomia, custo, velocidadeMedia, combustivel, consumo, emissoes} as Viagem;
//     service.updateViagem(ti).subscribe(def => {expect(def).toEqual(ti), console.log(def)});
//   }));

//   it('should delete tipo', async(() => {
//     const service: ViagemService = TestBed.get(ViagemService);
//     const domainId = "testeUnit";
//     const nome = "testeUnit";
//     const autonomia = 2;
//     const custo = 133;
//     const velocidadeMedia = 123;
//     const combustivel = 13;
//     const consumo = 13;
//     const emissoes=34;

//     const ti = {domainId, nome, autonomia, custo, velocidadeMedia, combustivel, consumo, emissoes} as Viagem;
//     service.deleteViagem(ti).subscribe(def =>{expect(def.domainId).toEqual("testeUnit"), console.log(def)});
//   }));
// });
