import { Injectable } from '@angular/core';
import { User } from '../../domain/login/user';
import { Role } from '../../domain/login/role';
import { HttpClient, HttpHeaderResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(
        private http: HttpClient) { }

        private nosUrl = 'http://localhost:3000/api/auth';
        private sUrl = 'http://localhost:3000/api/auth/signup';
        private soUrl = 'http://localhost:3000/api/auth/logout';
        private lUrl = 'http://localhost:3000/api/auth/signin';
        private cuUrl = 'http://localhost:3000/api/auth/currentUser';
        private dUrl = 'http://localhost:3000/api/auth/delete';
        private lUrl2 = 'http://localhost:7001/api/auth/login';
        private soUrl2 = 'http://localhost:7001/api/auth/logout';

        user: {role: string, token: string};

    isAuthorized() {
        return !!this.user;
    }

    hasRole(role: Role) {
        return this.isAuthorized() && this.user.role === role;
    }

    set(){
        this.user={role:localStorage.getItem('user_role'), token:localStorage.getItem('user_token')}
    }

    login(email: string, password: string): Observable<{role: string, token: string}>{
        this.http.post(this.lUrl2, {
            "email": email,
            "password": password,
        }, this.httpOptions);
        return this.http.post<{role: string, token: string}>(this.lUrl, {
            "email": email,
            "password": password,
        }, this.httpOptions);

    }

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      };

    signup(email: string, password: string, role: string){

        
        return this.http.post(this.sUrl, {
            "email": email,
            "password": password,
            "role": role
        }, this.httpOptions).subscribe();
    }

    logout() {
        // this.http.post(this.soUrl2, {}).subscribe();
        this.http.get(this.soUrl).subscribe();
        this.user = null;
        localStorage.removeItem('user_role');
        localStorage.removeItem('user_token');

    }

    delete() {
        this.http.delete(this.dUrl).subscribe();
        this.user = null;
        localStorage.removeItem('user_role');
        localStorage.removeItem('user_token');
        
    }

    // getUsers(): Observable<User[]> {
    //     return this.http.get<User[]>(this.nosUrl);
    // }

    // getUser(email: string): Observable<User> {
    //     return this.getUsers().pipe(
    //         map((tipos: User[]) => tipos.find(tipo => tipo.email === email)));
    // }

}
