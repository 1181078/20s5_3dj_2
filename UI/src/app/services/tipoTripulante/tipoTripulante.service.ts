import { Injectable } from '@angular/core';
import { TipoTripulante } from '../../domain/tipoTripulante/tipoTripulante';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { domain } from 'process';

@Injectable({
  providedIn: 'root'
})
export class TipoTripulanteService {

  constructor(
    private http: HttpClient, private messageService: MessageService) { }

  private log(message: string) {
    this.messageService.add(`TipoTripulanteService: ${message}`);
  }

  private tiposTripulanteUrl = 'http://localhost:3000/api/tipoTripulantes';
  private tiposTripulanteUrlAdd = 'http://localhost:3000/api/tipoTripulantes/add';

  getTiposTripulante(): Observable<TipoTripulante[]> {
    return this.http.get<TipoTripulante[]>(this.tiposTripulanteUrl)
      .pipe(
        tap(_ => this.log('fetched tiposTripulante')),
        catchError(this.handleError<TipoTripulante[]>('getTiposTripulante', []))
      );
  } 

  getTipoTripulante(domainId: string): Observable<TipoTripulante> {
    return this.getTiposTripulante().pipe(
      map((tipos: TipoTripulante[]) => tipos.find(tipo => tipo.domainId === domainId)));
    // const url = `${this.tiposTripulanteUrl}/${domainId}`;
    // return this.http.get<TipoTripulante>(url).pipe(
    //   tap(_ => this.log(`fetched tipoTripulante domainId=${domainId}`)),
    //   catchError(this.handleError<TipoTripulante>(`getTipoTripulante domainId=${domainId}`))
    // );
  }



  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updateTipoTripulante(tipoTripulante: TipoTripulante): Observable<any> {
    return this.http.put(this.tiposTripulanteUrl, tipoTripulante, this.httpOptions).pipe(
      tap(_ => this.log(`updated tipoTripulante domainId=${tipoTripulante.domainId}`)),
      catchError(this.handleError<any>('updateTipoTripulante'))
    );
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  addTipoTripulante(tipoTripulante: TipoTripulante): Observable<TipoTripulante> {
    return this.http.post<TipoTripulante>(this.tiposTripulanteUrlAdd, tipoTripulante, this.httpOptions).pipe(
      tap((newTipoTripulante: TipoTripulante) => this.log(`added tipoTripulante w/ domainId=${newTipoTripulante.domainId}`)),
      catchError(this.handleError<TipoTripulante>('addTipoTripulante'))
    );
  }

  deleteTipoTripulante(tipoTripulante: TipoTripulante): Observable<any> {
    const domainId = tipoTripulante.domainId;
    const url = `${this.tiposTripulanteUrl}/${domainId}`;
  
    return this.http.delete<TipoTripulante>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted tipoTripulante domainId=${domainId}`)),
      catchError(this.handleError<TipoTripulante>('deleteTipoTripulante'))
    );
  }

  searchTiposTripulante(term: string): Observable<TipoTripulante[]> {
    if (!term.trim()) {
      // if not search term, return empty tipoTripulante array.
      return of([]);
    }
    return this.http.get<TipoTripulante[]>(`${this.tiposTripulanteUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found tiposTripulante matching "${term}"`) :
         this.log(`no tiposTripulante matching "${term}"`)),
      catchError(this.handleError<TipoTripulante[]>('searchsTipoTripulante', []))
    );
  }

}
