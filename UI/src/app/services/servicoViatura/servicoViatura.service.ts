import { Injectable } from '@angular/core';
import { ServicoViatura } from '../../domain/servicoViatura/servicoViatura';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { domain } from 'process';


@Injectable({
  providedIn: 'root'
})
export class ServicoViaturaService {

  constructor(
    private http: HttpClient, private messageService: MessageService) { }

  public log(message: string) {
    this.messageService.add(`ServicoViaturaService: ${message}`);
  }

  private servicosViaturaUrl = 'https://localhost:7001/api/servicosviatura';
  
  

  getServicosViatura(): Observable<ServicoViatura[]>  {
    return this.http.get<ServicoViatura[]>(this.servicosViaturaUrl)
      .pipe(
        tap(_ => this.log('fetched servicosViatura')),
        catchError(this.handleError<ServicoViatura[]>('getServicosViatura', []))
      );
  } 

  getServicoViatura(domainId: string): Observable<ServicoViatura> 
  {
    return this.getServicosViatura().pipe(
      map((tipos: ServicoViatura[]) => tipos.find(tipo => tipo.domainId === domainId)));
  }
 

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updateServicoViatura(servicoViatura: ServicoViatura): Observable<any> {
    return this.http.put(this.servicosViaturaUrl, servicoViatura, this.httpOptions).pipe(
      tap(_ => this.log(`updated servicoViatura domainId=${servicoViatura.domainId}`)),
      catchError(this.handleError<any>('updateServicoViatura'))
    );
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  addServicoViatura(servicoViatura: ServicoViatura): Observable<ServicoViatura> {
    return this.http.post<ServicoViatura>(this.servicosViaturaUrl, servicoViatura, this.httpOptions).pipe(
      tap((newServicoViatura: ServicoViatura) => this.log(`added servicoViatura w/ domainId=${newServicoViatura.domainId}`)),
      catchError(this.handleError<ServicoViatura>('addServicoViatura'))
    );
  }

  deleteServicoViatura(servicoViatura: ServicoViatura | number): Observable<ServicoViatura> {
    const domainId = typeof servicoViatura === 'number' ? servicoViatura : servicoViatura.domainId;
    const url = `${this.servicosViaturaUrl}/${domainId}/hard`;
  
    return this.http.delete<ServicoViatura>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted servicoViatura domainId=${domainId}`)),
      catchError(this.handleError<ServicoViatura>('deleteServicoViatura'))
    );
  }

  searchServicosViatura(term: string): Observable<ServicoViatura[]> {
    if (!term.trim()) {
      // if servicoViaturat search term, return empty servicoViatura array.
      return of([]);
    }
    return this.http.get<ServicoViatura[]>(`${this.servicosViaturaUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found servicosViatura matching "${term}"`) :
         this.log(`servicoViatura servicosViatura matching "${term}"`)),
      catchError(this.handleError<ServicoViatura[]>('searchsServicoViatura', []))
    );
  }

}
