// import { HttpClientModule } from '@angular/common/http';
// import { TestBed, async, ComponentFixture } from '@angular/core/testing';
// import { FormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
// import { RouterTestingModule } from '@angular/router/testing';
// import { AngularDualListBoxModule } from 'angular-dual-listbox';
// import { AppRoutingModule } from '../app-routing.module';
// import { AppComponent } from '../app.component';

// import { NgModule } from '@angular/core';


// import { TiposTripulanteComponent } from '../tipoTripulante/tiposTripulante/tiposTripulante.component';
// import { TipoTripulanteDetailComponent } from '../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
// import { TiposViaturaComponent } from '../tipoViatura/tiposViatura/tiposViatura.component';
// import { TipoViaturaDetailComponent } from '../tipoViatura/tipoViatura-detail/tipoViatura-detail.component';
// import { PercursosComponent } from '../percurso/percursos/percursos.component';
// import { PercursoDetailComponent } from '../percurso/percurso-detail/percurso-detail.component';
// import { ServicosViaturaComponent } from '../servicoViatura/servicosViatura/servicosViatura.component';
// import { ServicoViaturaDetailComponent } from '../servicoViatura/servicoViatura-detail/servicoViatura-detail.component';
// import { LinhasComponent } from '../linha/linhas/linhas.component';
// import { LinhaDetailComponent } from '../linha/linha-detail/linha-detail.component';
// import { MessagesComponent } from '../messages/messages.component';
// import { DashboardComponent } from '../dashboard/dashboard.component';
// import { TipoViaturaSearchComponent } from '../tipoViatura/tipoViatura-search/tipoViatura-search.component';
// import { LoginComponent } from '../login/login.component';
// import { MapComponent } from '../map/map.component';
// import { DragDirective } from '../importFile/dragDrop.directive';
// import { PageServicoViaturatFoundComponent } from '../pageServicoViaturatFound/page-servicoViaturat-found.component';
// import { ImportFileComponent } from '../importFile/importFile.component';
// import { UserDirective } from '../login/directives/user.directive';
// import { UserRoleDirective } from '../login/directives/user-role.directive';
// import { AuthService } from '../login/services/auth.service';
// import { HomeComponent } from '../home/home.component';
// import { PerfilComponent } from '../login/perfil/perfil.component';
// import { SignupComponent } from '../login/signup/signup.component';
// import { GestaoCaminhosComponent } from '../gestaoCaminhos/gestaoCaminhos.component';
// import { ServicoViaturaService } from './servicoViatura.service';
// import { ServicoViatura } from './servicoViatura';
// import { TripulantesComponent } from 'src/app/components/tripulante/tripulantes/tripulantes.component';
// import { ViaturasComponent } from 'src/app/components/viatura/viatura/viaturas.component';
// import { TripulanteDetailComponent } from 'src/app/components/tripulante/tripulante-detail/tripulante-detail.component';
// import { ViaturaDetailComponent } from 'src/app/components/viatura/viatura-detail/viatura-detail.component';
// import { ViagemDetailComponent } from 'src/app/components/viagem/viagem-detail/viagem-detail.component';
// import { ViagensComponent } from 'src/app/components/viagem/viagem/viagens.component';
// import { BlocosTrabalhoComponent } from 'src/app/components/blocoTrabalho/blocosTrabalho/blocosTrabalho.component';
// import { BlocoTrabalhoDetailComponent } from 'src/app/components/blocoTrabalho/blocoTrabalho-detail/blocoTrabalho-detail.component';
// import { ServicosViaturaComponent } from 'src/app/components/servicoViatura/servicosViatura/servicosViatura.component';
// import { ServicosTripulanteComponent } from 'src/app/components/servicoTripulante/servicosTripulante/servicosTripulante.component';
// import { ServicoTripulanteDetailComponent } from 'src/app/components/servicoTripulante/servicoTripulante-detail/servicoTripulante-detail.component';
// import { ServicoViaturaDetailComponent } from 'src/app/components/servicoViatura/servicoViatura-detail/servicoViatura-detail.component';
// import { SequenciamentoMotoristasComponent } from 'src/app/components/sequenciamentoMotoristas/sequenciamentoMotoristas.component';


// describe('ServicosViaturaServiceComponent', () => {

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         BrowserModule,
//         AppRoutingModule,
//         FormsModule,
//         HttpClientModule,
//         AngularDualListBoxModule
//       ],
//       declarations: [
//         AppComponent,
//         TiposTripulanteComponent,
//         TipoTripulanteDetailComponent,
//         TiposViaturaComponent,
//         TipoViaturaDetailComponent,
//         PercursosComponent,
//         PercursoDetailComponent,
//         ServicosViaturaComponent,
//         ServicoViaturaDetailComponent,
//         LinhasComponent,
//         LinhaDetailComponent,
//         MessagesComponent,
//         DashboardComponent,
//         TipoViaturaSearchComponent,
//         LoginComponent,
//         MapComponent,
//         PageServicoViaturatFoundComponent,
//         DragDirective,
//         ImportFileComponent,
//         HomeComponent,
//         PerfilComponent,
//         SignupComponent,
//         GestaoCaminhosComponent,
//         UserDirective,
//         UserRoleDirective,
            // TripulantesComponent,
            // ViaturasComponent,
            // TripulanteDetailComponent,
            // ViaturaDetailComponent,
            // ViagemDetailComponent,
            // ViagensComponent,
            // BlocosTrabalhoComponent,
            // BlocoTrabalhoDetailComponent,
            // ServicosViaturaComponent,
            // ServicosTripulanteComponent,
            // ServicoViaturaDetailComponent,
            // ServicoTripulanteDetailComponent,
            // SequenciamentoMotoristasComponent
//       ],
//     })
//     .compileComponents();
//   }));

//   it('should be created', () => {
//     const service: ServicoViaturaService = TestBed.get(ServicoViaturaService);
//     expect(service).toBeTruthy();
//   });

//   it('should add servicoViatura', () => {
//     const service: ServicoViaturaService = TestBed.get(ServicoViaturaService);
//     const domainId = "testeUnit";
//     const servicoViaturame = "testeUnit";
//     const latitude = 12;
//     const longitude = 15;
//     const servicoViaturameCurto ="teste";
//     const isEstacaoRecolha = false;
//     const isPontoRendicao = true;
//     const tempoViagem = [{ servicoViaturaId: "idTeste", duracao: 25}];
//     const servicoViatura = {domainId,servicoViaturame,latitude,longitude,servicoViaturameCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem} as ServicoViatura;
//     service.addServicoViatura(servicoViatura).subscribe(abc => expect(abc).toEqual(servicoViatura));
//   });

//   it('should update servicoViatura', async(() => {
//     const service: ServicoViaturaService = TestBed.get(ServicoViaturaService);
//     const domainId = "testeUnit";
//     const servicoViaturame = "testeUnit";
//     const latitude = 12;
//     const longitude = 15;
//     const servicoViaturameCurto ="teste";
//     const isEstacaoRecolha = false;
//     const isPontoRendicao = true;
//     const tempoViagem = [{ servicoViaturaId: "idTeste", duracao: 25}];
//     const servicoViatura = {domainId,servicoViaturame,latitude,longitude,servicoViaturameCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem} as ServicoViatura;
//     service.updateServicoViatura(servicoViatura).subscribe(def => {expect(def).toEqual(servicoViatura), console.log(def)});
//   }));

//   it('should delete servicoViatura', async(() => {
//     const service: ServicoViaturaService = TestBed.get(ServicoViaturaService);
//     const domainId = "testeUnit";
//     const servicoViaturame = "testeUnit";
//     const latitude = 12;
//     const longitude = 15;
//     const servicoViaturameCurto ="teste";
//     const isEstacaoRecolha = false;
//     const isPontoRendicao = true;
//     const tempoViagem = [{ servicoViaturaId: "idTeste", duracao: 25}];
//     const servicoViatura = {domainId,servicoViaturame,latitude,longitude,servicoViaturameCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem} as ServicoViatura;
//     service.deleteServicoViatura(servicoViatura).subscribe(def =>{expect(def.domainId).toEqual("testeUnit"), console.log(def)});
//   }));


// });

