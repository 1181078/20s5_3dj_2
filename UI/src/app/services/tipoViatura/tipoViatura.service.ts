import { Injectable } from '@angular/core';
import { TipoViatura } from '../../domain/tipoViatura/tipoViatura';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { domain } from 'process';

@Injectable({
  providedIn: 'root'
})
export class TipoViaturaService {

  constructor(
    private http: HttpClient, private messageService: MessageService) { }

  private log(message: string) {
    this.messageService.add(`TipoViaturaService: ${message}`);
  }

  private tiposViaturaUrl = 'http://localhost:3000/api/tipoViaturas';

  getTiposViatura(): Observable<TipoViatura[]> {
    return this.http.get<TipoViatura[]>(this.tiposViaturaUrl)
      .pipe(
        tap(_ => this.log('fetched tiposViatura')),
        catchError(this.handleError<TipoViatura[]>('getTiposViatura', []))
      );
  } 

  getTipoViatura(domainId: string): Observable<TipoViatura> 
  {
    return this.getTiposViatura().pipe(
      map((tipos: TipoViatura[]) => tipos.find(tipo => tipo.domainId === domainId)));
  }
 

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updateTipoViatura(tipoViatura: TipoViatura): Observable<any> {
    return this.http.put(this.tiposViaturaUrl, tipoViatura, this.httpOptions).pipe(
      tap(_ => this.log(`updated tipoViatura domainId=${tipoViatura.domainId}`)),
      catchError(this.handleError<any>('updateTipoViatura'))
    );
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  addTipoViatura(tipoViatura: TipoViatura): Observable<TipoViatura> {
    return this.http.post<TipoViatura>(this.tiposViaturaUrl, tipoViatura, this.httpOptions).pipe(
      tap((newTipoViatura: TipoViatura) => this.log(`added tipoViatura w/ domainId=${newTipoViatura.domainId}`)),
      catchError(this.handleError<TipoViatura>('addTipoViatura'))
    );
  }


  deleteTipoViatura(tipoViatura: TipoViatura): Observable<any> {
    const domainId = tipoViatura.domainId;
    const url = `${this.tiposViaturaUrl}/${domainId}`;
  
    return this.http.delete<TipoViatura>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted tipoViatura domainId=${domainId}`)),
      catchError(this.handleError<TipoViatura>('deleteTipoViatura'))
    );
  }

  searchTiposViatura(term: string): Observable<TipoViatura[]> {
    if (!term.trim()) {
      // if not search term, return empty tipoViatura array.
      return of([]);
    }
    return this.http.get<TipoViatura[]>(`${this.tiposViaturaUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found tiposViatura matching "${term}"`) :
         this.log(`no tiposViatura matching "${term}"`)),
      catchError(this.handleError<TipoViatura[]>('searchsTipoViatura', []))
    );
  }

}
