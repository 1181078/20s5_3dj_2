import { Injectable } from '@angular/core';
import { Percurso } from '../../domain/percurso/percurso';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { domain } from 'process';
import { NosPercurso } from "../../../../../MDR/src/domain/percurso/nosPercurso";


@Injectable({
  providedIn: 'root'
})
export class PercursoService {

  constructor(
    private http: HttpClient, private messageService: MessageService) { }

  public log(message: string) {
    this.messageService.add(`PercursoService: ${message}`);
  }

  private percursosUrl = 'http://localhost:3000/api/percursos';

  getPercursos(): Observable<Percurso[]> {
    return this.http.get<Percurso[]>(this.percursosUrl).pipe(
      tap(_ => this.log('fetched percursos')),
      catchError(this.handleError<Percurso[]>('getPercursos', []))
    );
  } 

  getPercurso(domainId: string): Observable<Percurso> 
  {
    return this.getPercursos().pipe(
      map((tipos: Percurso[]) => tipos.find(tipo => tipo.domainId === domainId)));
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updatePercurso(percurso: Percurso): Observable<any> {
    return this.http.put(this.percursosUrl, percurso, this.httpOptions).pipe(
      tap(_ => this.log(`updated percurso domainId=${percurso.domainId}`)),
      catchError(this.handleError<any>('updatePercurso'))
    );
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  addPercurso(percurso: Percurso): Observable<Percurso> {
    return this.http.post<Percurso>(this.percursosUrl, percurso, this.httpOptions).pipe(
      tap((newPercurso: Percurso) => this.log(`added percurso w/ domainId=${newPercurso.domainId}`)),
      catchError(this.handleError<Percurso>('addPercurso'))
    );
  }

  deletePercurso(percurso: Percurso | number): Observable<Percurso> {
    const domainId = typeof percurso === 'number' ? percurso : percurso.domainId;
    const url = `${this.percursosUrl}/${domainId}`;
  
    return this.http.delete<Percurso>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted percurso domainId=${domainId}`)),
      catchError(this.handleError<Percurso>('deletePercurso'))
    );
  }

  searchPercursos(term: string): Observable<Percurso[]> {
    if (!term.trim()) {
      // if not search term, return empty percurso array.
      return of([]);
    }
    return this.http.get<Percurso[]>(`${this.percursosUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found percursos matching "${term}"`) :
         this.log(`no percursos matching "${term}"`)),
      catchError(this.handleError<Percurso[]>('searchsPercurso', []))
    );
  }

  // addLinhaProlog(nome: string, cont: number, nomePercurso: String){
  //   // i1= i1  + ',' + linha.percursoLinha[linha.percursoLinha.length-1].percurso + ']';

  //   const prologUrl: string = 'http://localhost:5000/add_linha?name='+ nome + '&num='+ cont + '&per='+ i1 + '&tempo='+ linha. + '&lon='+ no.longitude + '&lat=' + no.latitude;

    
  //   http://localhost:5000/add_linha?name=Paredes_Aguiar&num=1&per=[AGUIA,RECAR,PARAD,CETE,PARED]&tempo=31&dist=15700
  //   window.open(prologUrl);
  // }

}
