import { Injectable } from '@angular/core';
import { Linha } from '../../domain/linha/linha';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { domain } from 'process';
import { PercursoLinha } from "../../../../../MDR/src/domain/linha/percursoLinha";
import { PercursoService } from '../../services/percurso/percurso.service';


@Injectable({
  providedIn: 'root'
})
export class LinhaService {

  constructor(
    private http: HttpClient, private messageService: MessageService, private percursoService: PercursoService) { 
    }

    private cont = 0;

  public log(message: string) {
    this.messageService.add(`LinhaService: ${message}`);
  }

  private linhasUrl = 'http://localhost:3000/api/linhas';

  getLinhas(): Observable<Linha[]> {
    return this.http.get<Linha[]>(this.linhasUrl)
      .pipe(
        tap(_ => this.log('fetched linhas')),
        catchError(this.handleError<Linha[]>('getLinhas', []))
      );
  } 

  getLinha(domainId: string): Observable<Linha> 
  {
    return this.getLinhas().pipe(
      map((tipos: Linha[]) => tipos.find(tipo => tipo.domainId === domainId)));
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updateLinha(linha: Linha): Observable<any> {
    return this.http.put(this.linhasUrl, linha, this.httpOptions).pipe(
      tap(_ => this.log(`updated linha domainId=${linha.domainId}`)),
      catchError(this.handleError<any>('updateLinha'))
    );
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  addLinha(linha: Linha): Observable<Linha> {

    // for(var i = 0; i<linha.percursoLinha.length; i++){
    //   this.percursoService.addLinhaProlog(linha.nome, this.cont, linha.percursoLinha[i].percurso);
    // }

    // this.cont ++;

    return this.http.post<Linha>(this.linhasUrl, linha, this.httpOptions).pipe(
      tap((newLinha: Linha) => this.log(`added linha w/ domainId=${newLinha.domainId}`)),
      catchError(this.handleError<Linha>('addLinha'))
    );
  }

  deleteLinha(linha: Linha | number): Observable<Linha> {
    const domainId = typeof linha === 'number' ? linha : linha.domainId;
    const url = `${this.linhasUrl}/${domainId}`;
  
    return this.http.delete<Linha>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted linha domainId=${domainId}`)),
      catchError(this.handleError<Linha>('deleteLinha'))
    );
  }

  searchLinhas(term: string): Observable<Linha[]> {
    if (!term.trim()) {
      // if not search term, return empty linha array.
      return of([]);
    }
    return this.http.get<Linha[]>(`${this.linhasUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found linhas matching "${term}"`) :
         this.log(`no linhas matching "${term}"`)),
      catchError(this.handleError<Linha[]>('searchsLinha', []))
    );
  }

}
