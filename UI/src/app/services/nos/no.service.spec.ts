import { HttpClientModule } from '@angular/common/http';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularDualListBoxModule } from 'angular-dual-listbox';
import { AppRoutingModule } from '../../app-routing.module';
import { AppComponent } from '../../app.component';

import { NgModule } from '@angular/core';


import { TiposTripulanteComponent } from '../../components/tipoTripulante/tiposTripulante/tiposTripulante.component';
import { TipoTripulanteDetailComponent } from '../../components/tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
import { TiposViaturaComponent } from '../../components/tipoViatura/tiposViatura/tiposViatura.component';
import { TipoViaturaDetailComponent } from '../../components/tipoViatura/tipoViatura-detail/tipoViatura-detail.component';
import { PercursosComponent } from '../../components/percurso/percursos/percursos.component';
import { PercursoDetailComponent } from '../../components/percurso/percurso-detail/percurso-detail.component';
import { NosComponent } from '../../components/no/nos/nos.component';
import { NoDetailComponent } from '../../components/no/no-detail/no-detail.component';
import { LinhasComponent } from '../../components/linha/linhas/linhas.component';
import { LinhaDetailComponent } from '../../components/linha/linha-detail/linha-detail.component';
import { MessagesComponent } from '../../components/messages/messages.component';
import { DashboardComponent } from '../../components/dashboard/dashboard.component';
import { TipoViaturaSearchComponent } from '../../components/tipoViatura/tipoViatura-search/tipoViatura-search.component';
import { LoginComponent } from '../../components/login/login.component';
import { MapComponent } from '../../components/map/map.component';
import { DragDirective } from '../../components/importFile/dragDrop.directive';
import { PageNotFoundComponent } from '../../components/pageNotFound/page-not-found.component';
import { ImportFileComponent } from '../../components/importFile/importFile.component';
import { UserDirective } from '../../components/login/directives/user.directive';
import { UserRoleDirective } from '../../components/login/directives/user-role.directive';
import { AuthService } from '../../services/login/auth.service';
import { HomeComponent } from '../../components/home/home.component';
import { PerfilComponent } from '../../components/login/perfil/perfil.component';
import { SignupComponent } from '../../components/login/signup/signup.component';
import { GestaoCaminhosComponent } from '../../components/gestaoCaminhos/gestaoCaminhos.component';
import { NoService } from './no.service';
import { No } from '../../domain/nos/no';
import { TripulantesComponent } from 'src/app/components/tripulante/tripulantes/tripulantes.component';
import { ViaturasComponent } from 'src/app/components/viatura/viatura/viaturas.component';
import { TripulanteDetailComponent } from 'src/app/components/tripulante/tripulante-detail/tripulante-detail.component';
import { ViaturaDetailComponent } from 'src/app/components/viatura/viatura-detail/viatura-detail.component';
import { ViagemDetailComponent } from 'src/app/components/viagem/viagem-detail/viagem-detail.component';
import { ViagensComponent } from 'src/app/components/viagem/viagem/viagens.component';
import { BlocosTrabalhoComponent } from 'src/app/components/blocoTrabalho/blocosTrabalho/blocosTrabalho.component';
import { BlocoTrabalhoDetailComponent } from 'src/app/components/blocoTrabalho/blocoTrabalho-detail/blocoTrabalho-detail.component';
import { ServicosViaturaComponent } from 'src/app/components/servicoViatura/servicosViatura/servicosViatura.component';
import { ServicosTripulanteComponent } from 'src/app/components/servicoTripulante/servicosTripulante/servicosTripulante.component';
import { ServicoTripulanteDetailComponent } from 'src/app/components/servicoTripulante/servicoTripulante-detail/servicoTripulante-detail.component';
import { ServicoViaturaDetailComponent } from 'src/app/components/servicoViatura/servicoViatura-detail/servicoViatura-detail.component';
import { SequenciamentoMotoristasComponent } from 'src/app/components/sequenciamentoMotoristas/sequenciamentoMotoristas.component';


describe('NosServiceComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        AngularDualListBoxModule
      ],
      declarations: [
        AppComponent,
        TiposTripulanteComponent,
        TipoTripulanteDetailComponent,
        TiposViaturaComponent,
        TipoViaturaDetailComponent,
        PercursosComponent,
        PercursoDetailComponent,
        NosComponent,
        NoDetailComponent,
        LinhasComponent,
        LinhaDetailComponent,
        MessagesComponent,
        DashboardComponent,
        TipoViaturaSearchComponent,
        LoginComponent,
        MapComponent,
        PageNotFoundComponent,
        DragDirective,
        ImportFileComponent,
        HomeComponent,
        PerfilComponent,
        SignupComponent,
        GestaoCaminhosComponent,
        UserDirective,
        UserRoleDirective,
        TripulantesComponent,
        ViaturasComponent,
        TripulanteDetailComponent,
        ViaturaDetailComponent,
        ViagemDetailComponent,
        ViagensComponent,
        BlocosTrabalhoComponent,
        BlocoTrabalhoDetailComponent,
        ServicosViaturaComponent,
        ServicosTripulanteComponent,
        ServicoViaturaDetailComponent,
        ServicoTripulanteDetailComponent,
        SequenciamentoMotoristasComponent
      ],
    })
    .compileComponents();
  }));

  it('should be created', () => {
    const service: NoService = TestBed.get(NoService);
    expect(service).toBeTruthy();
  });

  it('should add no', () => {
    const service: NoService = TestBed.get(NoService);
    const domainId = "testeUnit";
    const nome = "testeUnit";
    const latitude = 12;
    const longitude = 15;
    const nomeCurto ="teste";
    const isEstacaoRecolha = false;
    const isPontoRendicao = true;
    const tempoViagem = [{ noId: "idTeste", duracao: 25}];
    const no = {domainId,nome,latitude,longitude,nomeCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem} as No;
    service.addNo(no).subscribe(abc => expect(abc).toEqual(no));
  });

  it('should update no', async(() => {
    const service: NoService = TestBed.get(NoService);
    const domainId = "testeUnit";
    const nome = "testeUnit";
    const latitude = 12;
    const longitude = 15;
    const nomeCurto ="teste";
    const isEstacaoRecolha = false;
    const isPontoRendicao = true;
    const tempoViagem = [{ noId: "idTeste", duracao: 25}];
    const no = {domainId,nome,latitude,longitude,nomeCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem} as No;
    service.updateNo(no).subscribe(def => {expect(def).toEqual(no), console.log(def)});
  }));

  it('should delete no', async(() => {
    const service: NoService = TestBed.get(NoService);
    const domainId = "testeUnit";
    const nome = "testeUnit";
    const latitude = 12;
    const longitude = 15;
    const nomeCurto ="teste";
    const isEstacaoRecolha = false;
    const isPontoRendicao = true;
    const tempoViagem = [{ noId: "idTeste", duracao: 25}];
    const no = {domainId,nome,latitude,longitude,nomeCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem} as No;
    service.deleteNo(no).subscribe(def =>{expect(def.domainId).toEqual("testeUnit"), console.log(def)});
  }));


});

