import { Injectable } from '@angular/core';
import { No } from '../../domain/nos/no';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { domain } from 'process';


@Injectable({
  providedIn: 'root'
})
export class NoService {

  constructor(
    private http: HttpClient, private messageService: MessageService) { }

  public log(message: string) {
    this.messageService.add(`NoService: ${message}`);
  }

  private nosUrl = 'http://localhost:3000/api/nos';
  
  

  getNos(): Observable<No[]>  {
    return this.http.get<No[]>(this.nosUrl)
      .pipe(
        tap(_ => this.log('fetched nos')),
        catchError(this.handleError<No[]>('getNos', []))
      );
  } 

  getNo(domainId: string): Observable<No> 
  {
    return this.getNos().pipe(
      map((tipos: No[]) => tipos.find(tipo => tipo.domainId === domainId)));
  }
 

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updateNo(no: No): Observable<any> {
    return this.http.put(this.nosUrl, no, this.httpOptions).pipe(
      tap(_ => this.log(`updated no domainId=${no.domainId}`)),
      catchError(this.handleError<any>('updateNo'))
    );
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  addNo(no: No): Observable<No> {

    var i1;
    var i2;
    if(no.isPontoRendicao === false){
      i1 = 'f';
    }else{
      i1 = 't';
    }
    if(no.isEstacaoRecolha === false){
      i2 = 'f';
    }else{
      i2 = 't';
    }
    // const prologUrl: string = 'http://localhost:5000/add_no?name='+ no.nome + '&abrev='+ no.nomeCurto + '&is1='+ i1 + '&is2='+ i2 + '&lon='+ no.longitude + '&lat=' + no.latitude;

    // window.open(prologUrl);

    return this.http.post<No>(this.nosUrl, no, this.httpOptions).pipe(
      tap((newNo: No) => this.log(`added no w/ domainId=${newNo.domainId}`)),
      catchError(this.handleError<No>('addNo'))
    );
  }

  deleteNo(no: No | number): Observable<No> {
    const domainId = typeof no === 'number' ? no : no.domainId;
    const url = `${this.nosUrl}/${domainId}`;
  
    return this.http.delete<No>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted no domainId=${domainId}`)),
      catchError(this.handleError<No>('deleteNo'))
    );
  }

  searchNos(term: string): Observable<No[]> {
    if (!term.trim()) {
      // if not search term, return empty no array.
      return of([]);
    }
    return this.http.get<No[]>(`${this.nosUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found nos matching "${term}"`) :
         this.log(`no nos matching "${term}"`)),
      catchError(this.handleError<No[]>('searchsNo', []))
    );
  }

}
