// import { HttpClientModule } from '@angular/common/http';
// import { TestBed, async, ComponentFixture } from '@angular/core/testing';
// import { FormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
// import { RouterTestingModule } from '@angular/router/testing';
// import { AngularDualListBoxModule } from 'angular-dual-listbox';
// import { AppRoutingModule } from '../app-routing.module';
// import { AppComponent } from '../app.component';

// import { NgModule } from '@angular/core';


// import { TiposTripulanteComponent } from '../tipoTripulante/tiposTripulante/tiposTripulante.component';
// import { TipoTripulanteDetailComponent } from '../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
// import { TiposTripulanteComponent } from '../tipoTripulante/tiposTripulante/tiposTripulante.component';
// import { TipoTripulanteDetailComponent } from '../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
// import { PercursosComponent } from '../percurso/percursos/percursos.component';
// import { PercursoDetailComponent } from '../percurso/percurso-detail/percurso-detail.component';
// import { ServicosTripulanteComponent } from '../servicoTripulante/servicosTripulante/servicosTripulante.component';
// import { ServicoTripulanteDetailComponent } from '../servicoTripulante/servicoTripulante-detail/servicoTripulante-detail.component';
// import { LinhasComponent } from '../linha/linhas/linhas.component';
// import { LinhaDetailComponent } from '../linha/linha-detail/linha-detail.component';
// import { MessagesComponent } from '../messages/messages.component';
// import { DashboardComponent } from '../dashboard/dashboard.component';
// import { TipoTripulanteSearchComponent } from '../tipoTripulante/tipoTripulante-search/tipoTripulante-search.component';
// import { LoginComponent } from '../login/login.component';
// import { MapComponent } from '../map/map.component';
// import { DragDirective } from '../importFile/dragDrop.directive';
// import { PageServicoTripulantetFoundComponent } from '../pageServicoTripulantetFound/page-servicoTripulantet-found.component';
// import { ImportFileComponent } from '../importFile/importFile.component';
// import { UserDirective } from '../login/directives/user.directive';
// import { UserRoleDirective } from '../login/directives/user-role.directive';
// import { AuthService } from '../login/services/auth.service';
// import { HomeComponent } from '../home/home.component';
// import { PerfilComponent } from '../login/perfil/perfil.component';
// import { SignupComponent } from '../login/signup/signup.component';
// import { GestaoCaminhosComponent } from '../gestaoCaminhos/gestaoCaminhos.component';
// import { ServicoTripulanteService } from './servicoTripulante.service';
// import { ServicoTripulante } from './servicoTripulante';
// import { TripulantesComponent } from 'src/app/components/tripulante/tripulantes/tripulantes.component';
// import { ViaturasComponent } from 'src/app/components/viatura/viatura/viaturas.component';
// import { TripulanteDetailComponent } from 'src/app/components/tripulante/tripulante-detail/tripulante-detail.component';
// import { ViaturaDetailComponent } from 'src/app/components/viatura/viatura-detail/viatura-detail.component';
// import { ViagemDetailComponent } from 'src/app/components/viagem/viagem-detail/viagem-detail.component';
// import { ViagensComponent } from 'src/app/components/viagem/viagem/viagens.component';
// import { BlocosTrabalhoComponent } from 'src/app/components/blocoTrabalho/blocosTrabalho/blocosTrabalho.component';
// import { BlocoTrabalhoDetailComponent } from 'src/app/components/blocoTrabalho/blocoTrabalho-detail/blocoTrabalho-detail.component';
// import { ServicosViaturaComponent } from 'src/app/components/servicoViatura/servicosViatura/servicosViatura.component';
// import { ServicosTripulanteComponent } from 'src/app/components/servicoTripulante/servicosTripulante/servicosTripulante.component';
// import { ServicoTripulanteDetailComponent } from 'src/app/components/servicoTripulante/servicoTripulante-detail/servicoTripulante-detail.component';
// import { ServicoViaturaDetailComponent } from 'src/app/components/servicoViatura/servicoViatura-detail/servicoViatura-detail.component';
// import { SequenciamentoMotoristasComponent } from 'src/app/components/sequenciamentoMotoristas/sequenciamentoMotoristas.component';


// describe('ServicosTripulanteServiceComponent', () => {

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         BrowserModule,
//         AppRoutingModule,
//         FormsModule,
//         HttpClientModule,
//         AngularDualListBoxModule
//       ],
//       declarations: [
//         AppComponent,
//         TiposTripulanteComponent,
//         TipoTripulanteDetailComponent,
//         TiposTripulanteComponent,
//         TipoTripulanteDetailComponent,
//         PercursosComponent,
//         PercursoDetailComponent,
//         ServicosTripulanteComponent,
//         ServicoTripulanteDetailComponent,
//         LinhasComponent,
//         LinhaDetailComponent,
//         MessagesComponent,
//         DashboardComponent,
//         TipoTripulanteSearchComponent,
//         LoginComponent,
//         MapComponent,
//         PageServicoTripulantetFoundComponent,
//         DragDirective,
//         ImportFileComponent,
//         HomeComponent,
//         PerfilComponent,
//         SignupComponent,
//         GestaoCaminhosComponent,
//         UserDirective,
//         UserRoleDirective,
            // TripulantesComponent,
            // ViaturasComponent,
            // TripulanteDetailComponent,
            // ViaturaDetailComponent,
            // ViagemDetailComponent,
            // ViagensComponent,
            // BlocosTrabalhoComponent,
            // BlocoTrabalhoDetailComponent,
            // ServicosViaturaComponent,
            // ServicosTripulanteComponent,
            // ServicoViaturaDetailComponent,
            // ServicoTripulanteDetailComponent,
            // SequenciamentoMotoristasComponent
//       ],
//     })
//     .compileComponents();
//   }));

//   it('should be created', () => {
//     const service: ServicoTripulanteService = TestBed.get(ServicoTripulanteService);
//     expect(service).toBeTruthy();
//   });

//   it('should add servicoTripulante', () => {
//     const service: ServicoTripulanteService = TestBed.get(ServicoTripulanteService);
//     const domainId = "testeUnit";
//     const servicoTripulanteme = "testeUnit";
//     const latitude = 12;
//     const longitude = 15;
//     const servicoTripulantemeCurto ="teste";
//     const isEstacaoRecolha = false;
//     const isPontoRendicao = true;
//     const tempoViagem = [{ servicoTripulanteId: "idTeste", duracao: 25}];
//     const servicoTripulante = {domainId,servicoTripulanteme,latitude,longitude,servicoTripulantemeCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem} as ServicoTripulante;
//     service.addServicoTripulante(servicoTripulante).subscribe(abc => expect(abc).toEqual(servicoTripulante));
//   });

//   it('should update servicoTripulante', async(() => {
//     const service: ServicoTripulanteService = TestBed.get(ServicoTripulanteService);
//     const domainId = "testeUnit";
//     const servicoTripulanteme = "testeUnit";
//     const latitude = 12;
//     const longitude = 15;
//     const servicoTripulantemeCurto ="teste";
//     const isEstacaoRecolha = false;
//     const isPontoRendicao = true;
//     const tempoViagem = [{ servicoTripulanteId: "idTeste", duracao: 25}];
//     const servicoTripulante = {domainId,servicoTripulanteme,latitude,longitude,servicoTripulantemeCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem} as ServicoTripulante;
//     service.updateServicoTripulante(servicoTripulante).subscribe(def => {expect(def).toEqual(servicoTripulante), console.log(def)});
//   }));

//   it('should delete servicoTripulante', async(() => {
//     const service: ServicoTripulanteService = TestBed.get(ServicoTripulanteService);
//     const domainId = "testeUnit";
//     const servicoTripulanteme = "testeUnit";
//     const latitude = 12;
//     const longitude = 15;
//     const servicoTripulantemeCurto ="teste";
//     const isEstacaoRecolha = false;
//     const isPontoRendicao = true;
//     const tempoViagem = [{ servicoTripulanteId: "idTeste", duracao: 25}];
//     const servicoTripulante = {domainId,servicoTripulanteme,latitude,longitude,servicoTripulantemeCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem} as ServicoTripulante;
//     service.deleteServicoTripulante(servicoTripulante).subscribe(def =>{expect(def.domainId).toEqual("testeUnit"), console.log(def)});
//   }));


// });

