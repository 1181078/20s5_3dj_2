import { Injectable } from '@angular/core';
import { ServicoTripulante } from '../../domain/servicoTripulante/servicoTripulante';
import { Observable, of } from 'rxjs';
import { MessageService } from '../messages/message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { domain } from 'process';


@Injectable({
  providedIn: 'root'
})
export class ServicoTripulanteService {

  constructor(
    private http: HttpClient, private messageService: MessageService) { }

  public log(message: string) {
    this.messageService.add(`ServicoTripulanteService: ${message}`);
  }

  private servicosTripulanteUrl = 'https://localhost:7001/api/servicostripulante';
  
  

  getServicosTripulante(): Observable<ServicoTripulante[]>  {
    return this.http.get<ServicoTripulante[]>(this.servicosTripulanteUrl)
      .pipe(
        tap(_ => this.log('fetched servicosTripulante')),
        catchError(this.handleError<ServicoTripulante[]>('getServicosTripulante', []))
      );
  } 

  getServicoTripulante(domainId: string): Observable<ServicoTripulante> 
  {
    return this.getServicosTripulante().pipe(
      map((tipos: ServicoTripulante[]) => tipos.find(tipo => tipo.domainId === domainId)));
  }
 

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  updateServicoTripulante(servicoTripulante: ServicoTripulante): Observable<any> {
    return this.http.put(this.servicosTripulanteUrl, servicoTripulante, this.httpOptions).pipe(
      tap(_ => this.log(`updated servicoTripulante domainId=${servicoTripulante.domainId}`)),
      catchError(this.handleError<any>('updateServicoTripulante'))
    );
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  addServicoTripulante(servicoTripulante: ServicoTripulante): Observable<ServicoTripulante> {
    return this.http.post<ServicoTripulante>(this.servicosTripulanteUrl, servicoTripulante, this.httpOptions).pipe(
      tap((newServicoTripulante: ServicoTripulante) => this.log(`added servicoTripulante w/ domainId=${newServicoTripulante.domainId}`)),
      catchError(this.handleError<ServicoTripulante>('addServicoTripulante'))
    );
  }

  deleteServicoTripulante(servicoTripulante: ServicoTripulante | number): Observable<ServicoTripulante> {
    const domainId = typeof servicoTripulante === 'number' ? servicoTripulante : servicoTripulante.domainId;
    const url = `${this.servicosTripulanteUrl}/${domainId}/hard`;
  
    return this.http.delete<ServicoTripulante>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted servicoTripulante domainId=${domainId}`)),
      catchError(this.handleError<ServicoTripulante>('deleteServicoTripulante'))
    );
  }

  searchServicosTripulante(term: string): Observable<ServicoTripulante[]> {
    if (!term.trim()) {
      // if servicoTripulantet search term, return empty servicoTripulante array.
      return of([]);
    }
    return this.http.get<ServicoTripulante[]>(`${this.servicosTripulanteUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
         this.log(`found servicosTripulante matching "${term}"`) :
         this.log(`servicoTripulante servicosTripulante matching "${term}"`)),
      catchError(this.handleError<ServicoTripulante[]>('searchsServicoTripulante', []))
    );
  }

}
