import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Role } from './domain/login/role';
import { AuthService } from './services/login/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'OPT';
 
  constructor(private router: Router, private authService: AuthService) { }

  get isAuthorized() {
    return this.authService.isAuthorized();
  }

  get isAdmin() {
    return this.authService.hasRole(Role.Admin);
  }

  get isCliente() {
    return this.authService.hasRole(Role.Cliente);
  }

  get isGestor() {
    return this.authService.hasRole(Role.Gestor);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }

  
}
