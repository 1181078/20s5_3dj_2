import { Component, OnInit, Input } from '@angular/core';
import { Percurso } from '../../../domain/percurso/percurso';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { PercursoService } from '../../../services/percurso/percurso.service';
import { MessageService } from '../../../services/messages/message.service';

@Component({
  selector: 'app-percurso-detail',
  templateUrl: './percurso-detail.component.html',
  styleUrls: ['./percurso-detail.component.css']
})
export class PercursoDetailComponent implements OnInit {
  @Input() percurso: Percurso;

  constructor(
    private route: ActivatedRoute,
    private percursoService: PercursoService,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.getPercurso();
  }
  
  getPercurso(): void {
    const domainId = this.route.snapshot.paramMap.get("domainId");
    this.percursoService.getPercurso(domainId)
      .subscribe(percurso => this.percurso = percurso);
  }

  getPercurso2(domainId:string){
    this.percursoService.getPercurso(domainId).subscribe(percurso=>this.percurso = percurso);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.percursoService.updatePercurso(this.percurso)
      .subscribe(() => this.goBack());
  }

}
