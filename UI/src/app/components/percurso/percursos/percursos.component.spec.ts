import { HttpClientModule } from '@angular/common/http';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularDualListBoxModule } from 'angular-dual-listbox';
import { AppRoutingModule } from '../../../app-routing.module';
import { AppComponent } from '../../../app.component';
import * as sinon from 'sinon';

import { ChangeDetectionStrategy, ChangeDetectorRef, NgModule } from '@angular/core';


import { TiposTripulanteComponent } from '../../tipoTripulante/tiposTripulante/tiposTripulante.component';
import { TipoTripulanteDetailComponent } from '../../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
import { TiposViaturaComponent } from '../../tipoViatura/tiposViatura/tiposViatura.component';
import { TipoViaturaDetailComponent } from '../../tipoViatura/tipoViatura-detail/tipoViatura-detail.component';
import { PercursosComponent } from '../../percurso/percursos/percursos.component';
import { PercursoDetailComponent } from '../../percurso/percurso-detail/percurso-detail.component';
import { NosComponent } from '../../no/nos/nos.component';
import { NoDetailComponent } from '../../no/no-detail/no-detail.component';
import { LinhasComponent } from '../../linha/linhas/linhas.component';
import { LinhaDetailComponent } from '../../linha/linha-detail/linha-detail.component';
import { MessagesComponent } from '../../messages/messages.component';
import { DashboardComponent } from '../../../components/dashboard/dashboard.component';
import { TipoViaturaSearchComponent } from '../../tipoViatura/tipoViatura-search/tipoViatura-search.component';
import { LoginComponent } from '../../login/login.component';
import { MapComponent } from '../../../components/map/map.component';
import { DragDirective } from '../../../components/importFile/dragDrop.directive';
import { PageNotFoundComponent } from '../../pageNotFound/page-not-found.component';
import { ImportFileComponent } from '../../../components/importFile/importFile.component';
import { UserDirective } from '../../login/directives/user.directive';
import { UserRoleDirective } from '../../login/directives/user-role.directive';
import { AuthService } from '../../../services/login/auth.service';
import { HomeComponent } from '../../../components/home/home.component';
import { PerfilComponent } from '../../login/perfil/perfil.component';
import { SignupComponent } from '../../login/signup/signup.component';
import { GestaoCaminhosComponent } from '../../../components/gestaoCaminhos/gestaoCaminhos.component';
import { Observable } from 'rxjs';
import { NoService } from '../../../services/nos/no.service';
import { Percurso } from '../../../domain/percurso/percurso';
import { No } from '../../../domain/nos/no';
import { PercursoService } from '../../../services/percurso/percurso.service';
import { TipoTripulanteService } from '../../../services/tipoTripulante/tipoTripulante.service';
import { TripulantesComponent } from 'src/app/components/tripulante/tripulantes/tripulantes.component';
import { ViaturasComponent } from 'src/app/components/viatura/viatura/viaturas.component';
import { TripulanteDetailComponent } from 'src/app/components/tripulante/tripulante-detail/tripulante-detail.component';
import { ViaturaDetailComponent } from 'src/app/components/viatura/viatura-detail/viatura-detail.component';
import { ViagemDetailComponent } from 'src/app/components/viagem/viagem-detail/viagem-detail.component';
import { ViagensComponent } from 'src/app/components/viagem/viagem/viagens.component';
import { BlocosTrabalhoComponent } from 'src/app/components/blocoTrabalho/blocosTrabalho/blocosTrabalho.component';
import { BlocoTrabalhoDetailComponent } from 'src/app/components/blocoTrabalho/blocoTrabalho-detail/blocoTrabalho-detail.component';
import { ServicosViaturaComponent } from 'src/app/components/servicoViatura/servicosViatura/servicosViatura.component';
import { ServicosTripulanteComponent } from 'src/app/components/servicoTripulante/servicosTripulante/servicosTripulante.component';
import { ServicoTripulanteDetailComponent } from 'src/app/components/servicoTripulante/servicoTripulante-detail/servicoTripulante-detail.component';
import { ServicoViaturaDetailComponent } from 'src/app/components/servicoViatura/servicoViatura-detail/servicoViatura-detail.component';
import { SequenciamentoMotoristasComponent } from 'src/app/components/sequenciamentoMotoristas/sequenciamentoMotoristas.component';


describe('PercursosComponent', () => {
    let component: PercursosComponent;
    let fixture: ComponentFixture<PercursosComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                BrowserModule,
                AppRoutingModule,
                FormsModule,
                HttpClientModule,
                AngularDualListBoxModule
            ],
            declarations: [
                AppComponent,
                TiposTripulanteComponent,
                TipoTripulanteDetailComponent,
                TiposViaturaComponent,
                TipoViaturaDetailComponent,
                PercursosComponent,
                PercursoDetailComponent,
                NosComponent,
                NoDetailComponent,
                LinhasComponent,
                LinhaDetailComponent,
                MessagesComponent,
                DashboardComponent,
                TipoViaturaSearchComponent,
                LoginComponent,
                MapComponent,
                PageNotFoundComponent,
                DragDirective,
                ImportFileComponent,
                HomeComponent,
                PerfilComponent,
                SignupComponent,
                GestaoCaminhosComponent,
                UserDirective,
                UserRoleDirective,
                TripulantesComponent,
                ViaturasComponent,
                TripulanteDetailComponent,
                ViaturaDetailComponent,
                ViagemDetailComponent,
                ViagensComponent,
                BlocosTrabalhoComponent,
                BlocoTrabalhoDetailComponent,
                ServicosViaturaComponent,
                ServicosTripulanteComponent,
                ServicoViaturaDetailComponent,
                ServicoTripulanteDetailComponent,
                SequenciamentoMotoristasComponent
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PercursosComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it(`should start array as 0`, () => {
        const fixture = TestBed.createComponent(PercursosComponent);
        const app = fixture.componentInstance;
        expect(app.percursos.length).toBe(0);
    });

    it(`should array should not be afftected because of subscribe`, () => {
        const service: PercursoService = TestBed.get(PercursoService);
        const domainId = "testServicePer";
        const noId = "noTestService";
        const duracao = 999;
        const distancia = 999;
        const nosPercurso = [{ noId, duracao, distancia }];
        const perTeste = { domainId, nosPercurso } as Percurso;


        sinon.stub(service, "addPercurso").returns(Observable.create(observer => { observer.next(perTeste); observer.complete() }));

        sinon.stub(service, "deletePercurso").returns(Observable.create(observer => { observer.next(perTeste); observer.complete() }));

        sinon.stub(service, "getPercursos").returns(Observable.create(observer => { observer.next([]); observer.complete() }));

        var a = new PercursosComponent(service, TestBed.get(NoService));


        const noTeste = {
            domainId: "noTestId", nome: "noTestNome", latitude: 90,
            longitude: 90, nomeCurto: "NOTEST", isEstacaoRecolha: false, isPontoRendicao: false, tempoViagem: []
        } as No;
        var len = a.percursos.length;
        console.log(len);
        a.add("testeNozao", [noTeste]);
        const percursoDelete = { "domainId": "testeNozao", "nosPercurso": [{ "noId": "noTestId", "duracao": 0, "distancia": 0 }] } as Percurso;
        var len2 = len + 1;
        expect(a.percursos.length).toEqual(len2);
        a.delete(percursoDelete);
        a.getPercursos();
        expect(a.percursos.length).toEqual(len);
    });

});