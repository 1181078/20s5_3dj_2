import {
	Component, OnInit, DoCheck, EventEmitter, Input, IterableDiffers, OnChanges,
	Output, SimpleChange, ChangeDetectorRef, ChangeDetectionStrategy
} from '@angular/core';

import { Percurso } from '../../../domain/percurso/percurso';
import { PercursoService } from '../../../services/percurso/percurso.service';
import { NoService } from '../../../services/nos/no.service';
import { No } from '../../../domain/nos/no'
import { MessageService } from '../../../services/messages/message.service';
import { NosPercurso } from "../../../../../../MDR/src/domain/percurso/nosPercurso";
import { DualListComponent } from 'angular-dual-listbox';
import { catchError, map, tap } from 'rxjs/operators';
import { TempoViagemGrupo, TempoViagemGrupoProps } from '../../../../../../MDR/src/domain/no/tempoViagemGrupo';


@Component({
	selector: 'app-percursos',
	templateUrl: './percursos.component.html',
	styleUrls: ['./percursos.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class PercursosComponent implements OnInit {
	tab = 1;
	keepSorted = true;
	key: string;
	display: string;
	filter = false;
	source: No[] = [];
	confirmed: No[] = [];
	userAdd = '';
	disabled = false;
	sourceLeft = true;
	format: any = DualListComponent.DEFAULT_FORMAT;

	percursos: Percurso[] = new Array();
	nos: No[] = new Array();
	confirmedNos: No[] = new Array();


	constructor(
		private percursoService: PercursoService,
		private noService: NoService,
	) {
	}

	ngOnInit() {
		this.getPercursos();
		this.doReset();
	}

	getPercursos(): void {
		this.percursoService.getPercursos()
			.subscribe(percursos => {this.percursos = percursos, console.log(percursos), document.getElementById("refr").click()});
	}

	getNos(): any{
		this.noService.getNos().subscribe((nos) => {
			this.nos = nos;
		});

	}


	add(domainId: string, nP:No[]): void {
		
		const nosPercurso: {noId: String, duracao: Number, distancia: Number}[] = new Array();

		for(var i=0; i<nP.length; i++){
			const noId = nP[i].domainId;
			const duracao = new Number(0);
			const distancia = new Number(0);
			const ne4 = {noId, duracao, distancia};
			nosPercurso[i] = ne4;
		}
		
		this.percursoService.addPercurso({ domainId, nosPercurso } as Percurso)
			.subscribe(percurso => {
				this.percursos.push(percurso);
			});
	}

	delete(percurso: Percurso): void {
		this.percursos = this.percursos.filter(h => h !== percurso);
		this.percursoService.deletePercurso(percurso).subscribe();
	}

	private useNos() {
		this.key = 'domainId';
		this.display = 'nome';
		this.keepSorted = true;
		this.source = this.nos;
		this.confirmed = this.confirmedNos;
	}

	doReset() {
		this.getNos();
		this.confirmedNos = [];
		this.useNos();
	}


	filterBtn() {
		return (this.filter ? 'Hide Filter' : 'Show Filter');
	}

	doDisable() {
		this.disabled = !this.disabled;
	}

	disableBtn() {
		return (this.disabled ? 'Enable' : 'Disabled');
	}

	swapDirection() {
		this.sourceLeft = !this.sourceLeft;
		this.format.direction = this.sourceLeft ? DualListComponent.LTR : DualListComponent.RTL;
	}

	refresh() {
	}


}