import { Component } from '@angular/core';
import { FileHandle } from './dragDrop.directive';
import { ImportFileService } from '../../services/importFile/importFile.service';

@Component({
  selector: 'my-app',
  templateUrl: './importFile.component.html',
  styleUrls: [ './importFile.component.css' ]
})
export class ImportFileComponent  {

    constructor(
		private importFileService: ImportFileService
	) {
	}
  files: FileHandle[] = [];

  filesDropped(files: FileHandle[]): void {
    this.files = files;
  }

  upload(): void {
    this.importFileService.importFile(this.files[0].file);
  }
}
