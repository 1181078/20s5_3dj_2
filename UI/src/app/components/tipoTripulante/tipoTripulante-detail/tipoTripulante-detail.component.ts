import { Component, OnInit, Input } from '@angular/core';
import { TipoTripulante } from '../../../domain/tipoTripulante/tipoTripulante';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { TipoTripulanteService } from '../../../services/tipoTripulante/tipoTripulante.service';
import { MessageService } from '../../../services/messages/message.service';

@Component({
  selector: 'app-tipoTripulante-detail',
  templateUrl: './tipoTripulante-detail.component.html',
  styleUrls: ['./tipoTripulante-detail.component.css']
})
export class TipoTripulanteDetailComponent implements OnInit {
  @Input() tipoTripulante: TipoTripulante;

  constructor(
    private route: ActivatedRoute,
    private tipoTripulanteService: TipoTripulanteService,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.getTipoTripulante();
  }
  
  getTipoTripulante(): void {
    const domainId = this.route.snapshot.paramMap.get("domainId");
    this.tipoTripulanteService.getTipoTripulante(domainId).subscribe(tipoTripulante => this.tipoTripulante = tipoTripulante);
  }

  getTipoTripulante2(domainId: string): void {
    this.tipoTripulanteService.getTipoTripulante(domainId).subscribe(tipoTripulante => this.tipoTripulante = tipoTripulante);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.tipoTripulanteService.updateTipoTripulante(this.tipoTripulante)
      .subscribe(() => this.goBack());
  }

}
