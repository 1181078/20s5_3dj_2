import { Component, OnInit } from '@angular/core';

import { TipoTripulante } from '../../../domain/tipoTripulante/tipoTripulante';
import { TipoTripulanteService } from '../../../services/tipoTripulante/tipoTripulante.service';
import { MessageService } from '../../../services/messages/message.service';

@Component({
  selector: 'app-tiposTripulante',
  templateUrl: './tiposTripulante.component.html',
  styleUrls: ['./tiposTripulante.component.css']
})
export class TiposTripulanteComponent implements OnInit {
  tiposTripulante: TipoTripulante[]= new Array();

  constructor(private tipoTripulanteService: TipoTripulanteService) { }

  ngOnInit() {
    this.getTiposTripulante();
  }

  getTiposTripulante(): void {
    this.tipoTripulanteService.getTiposTripulante()
    .subscribe(tiposTripulante => this.tiposTripulante = tiposTripulante);
  }

  add(domainId: string, desc: string): void {
    domainId = domainId.trim();
    desc = desc.trim();
    if (!desc || !domainId) { return; }
    this.tipoTripulanteService.addTipoTripulante({ domainId, desc } as TipoTripulante)
      .subscribe(tipoTripulante => {
        this.tiposTripulante.push(tipoTripulante);
      });
  }

  delete(tipoTripulante: TipoTripulante): void {
    this.tiposTripulante = this.tiposTripulante.filter(h => h !== tipoTripulante);
    this.tipoTripulanteService.deleteTipoTripulante(tipoTripulante).subscribe();
  }
}