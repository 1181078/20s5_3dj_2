import { environment } from '../../../environments/environment';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { No } from '../../domain/nos/no';
import { NoService } from '../../services/nos/no.service';
import * as mapboxgl from 'mapbox-gl';
import { TempoViagemGrupo, TempoViagemGrupoProps } from '../../../../../MDR/src/domain/no/tempoViagemGrupo';
import { Linha } from '../../domain/linha/linha';
import { Percurso } from '../../domain/percurso/percurso';
import { PercursoService } from '../../services/percurso/percurso.service';
import { LinhaService } from '../../services/linha/linha.service';
import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit {

  i: number = 0;

  nosR: No[] = new Array();
  nosCopy: No[] = new Array();
  percursosR: Percurso[] = new Array();
  percursosCopy: Percurso[] = new Array();
  linhasR: Linha[] = new Array();
  linhasCopy: Linha[] = new Array();
  perIds: string[] = new Array();
  linhasIds: string[] = new Array();
  nodesIds: string[] = new Array();
  markers = new Array();
  mat: number[][] = [];
  ddd: number;

  map: mapboxgl.Map;
  style = 'mapbox://styles/1181076/ckihpg0cy1y6719s1fb4a0ts3';
  //style = 'mapbox://styles/mapbox/streets-v11';
  //style = 'mapbox://styles/mapbox/satellite-v9';  
  style3D = 'mapbox://styles/1181076/ckihoeksb2ju919o2273mbkss';
  lat = 41.1983610215263;
  lng = -8.36577272258403;


  constructor(private noService: NoService, private percursoService: PercursoService, private linhaService: LinhaService) { }
  ngOnInit() {
    Object.getOwnPropertyDescriptor(mapboxgl, "accessToken").set(environment.mapbox.accessToken);
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: 11,
      center: [this.lng, this.lat],
    });


    // Add map controls
    this.map.addControl(new mapboxgl.NavigationControl());


    this.map.addControl(
      new mapboxgl.GeolocateControl({
        positionOptions: {
          enableHighAccuracy: true
        },
        trackUserLocation: true
      })
    );


    this.map.dragRotate.disable();

    this.map.touchZoomRotate.disableRotation();

    this.markers = [];
    this.setMarkers();
    this.setPercursos();
    this.setLinhas();

    this.ddd = 0;

    // this.set3D();

  };

  shiftMaps() {
    console.log(this.map)
    if (this.ddd === 0) {
      document.getElementById('dragRotate').click();
      this.map.setStyle(this.style3D);
      this.map.setPitch(80);
      this.map.setBearing(-17.6);
      this.ddd = 1;
    } else {
      document.getElementById('dragRotate').click();
      this.map.setStyle(this.style);
      this.map.setPitch(0);
      this.map.setBearing(0);
      this.ddd = 0;
    }
  }


  toogleOrbit() {
    if (this.map.dragRotate.isEnabled()) {
      this.map.dragRotate.disable();

      this.map.touchZoomRotate.disableRotation();

    } else {

      this.map.dragRotate.enable();

      this.map.touchZoomRotate.enableRotation();

    }
  }

  tooglePan() {
    if (this.map.dragPan.isEnabled()) {
      this.map.dragPan.disable();

    } else {

      this.map.dragPan.enable();


    }
  }

  setMarkers() {

    const marker = new mapboxgl.Marker({ "color": "#b40219", "scale": 0.7 })
      .setLngLat([-8.6087417, 41.1786221])
      .setPopup(new mapboxgl.Popup({ offset: 25 }).setText(
        "Instituto Superior de Engenharia do Porto"
      ))
      .addTo(this.map);

    this.markers.push(marker);

    //new mapboxgl.GeoJSONSource().setData()

    this.noService.getNos().
      subscribe(nos =>
        this.nosR = nos);

  }

  addMarkers(nodes: No[]) {
    this.nosCopy = nodes;

    for (var i = 0; i < nodes.length; i++) {
      this.mat[i] = [];

      for (var j = 0; j < nodes.length; j++) {
        this.mat[i][j] = 0;
      }

      if (this.ddd === 0) {

        var mk = new mapboxgl.Marker({ "color": "orange", "scale": 0.7 })
          .setLngLat([nodes[i].longitude, nodes[i].latitude])
          .setPopup(new mapboxgl.Popup({ offset: 25 }).setText(
            nodes[i].nome
          ))
          .addTo(this.map);

        this.markers.push(mk);

      } else {

        document.getElementById('try').click();

        this.nodesIds[i] = nodes[i].domainId;

      }

    }

    this.i=0;

  }

  removeMarkers() {

    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].remove();
    }

    this.markers = [];

    for (var i = 0; i < this.nodesIds.length; i++) {
      this.map.removeLayer(this.nodesIds[i]);
    }



  }

  setPercursos() {
    this.percursoService.getPercursos().
      subscribe(percursos =>
        this.percursosR = percursos);

  }

  addPercursos(percursos: Percurso[], nodes: No[]) {

    var coord = new Array();

    var nodess: { noId: String; duracao: Number; distancia: Number; }[] = new Array();

    var currentNode: No;

    for (var i = 0; i < percursos.length; i++) {

      this.perIds[i] = percursos[i].domainId;

      nodess = percursos[i].nosPercurso;

      for (var j = 0; j < nodess.length; j++) {
        currentNode = this.findNo(nodes, nodess[j].noId);
        const lng = currentNode.longitude;
        const lat = currentNode.latitude;

        coord[j] = [lng, lat]
      }


      this.addPer(coord, percursos[i].domainId);

      coord = [];
    }

  }

  addPer(coord: Array<any>, perID: string) {
    this.map.addSource(perID, {
      'type': 'geojson',
      'data': {
        'type': 'Feature',
        'properties': {},
        'geometry': {
          'type': 'LineString',
          'coordinates': coord
        }
      }
    });
    this.map.addLayer({
      'id': perID,
      'type': 'line',
      'source': perID,
      'layout': {
        'line-join': 'round',
        'line-cap': 'round'
      },
      'paint': {
        'line-color': '#546ee3',
        'line-width': 3
      }
    });

  }

  hidePercursos() {
    for (var i = 0; i < this.perIds.length; i++) {
      this.map.removeLayer(this.perIds[i]);
      this.map.removeSource(this.perIds[i]);
    }
  }

  setLinhas() {
    this.linhaService.getLinhas().
      subscribe(linhas =>
        this.linhasR = linhas);

  }

  addLinhas(linhas: Linha[], percursos: Percurso[], nodes: No[]) {

    var coord = new Array();

    var nodess: { noId: String; duracao: Number; distancia: Number; }[] = new Array();

    var currentNode: No;
    var nextNode: No;

    var currentPer: Percurso;

    var a = 0;

    for (var p = 0; p < linhas.length; p++) {

      for (var i = 0; i < linhas[p].percursoLinha.length; i++) {

        currentPer = this.findPercurso(percursos, linhas[p].percursoLinha[i].percurso)

        var temp = linhas[p].domainId + currentPer.domainId;

        nodess = currentPer.nosPercurso;

        for (var j = 0; j < nodess.length - 1; j++) {
          currentNode = this.findNo(nodes, nodess[j].noId);
          var nI = this.findNoPosition(nodes, nodess[j].noId);
          const lng = currentNode.longitude;
          const lat = currentNode.latitude;

          var jT = j + 1;

          nextNode = this.findNo(nodes, nodess[jT].noId);
          var nJ = this.findNoPosition(nodes, nodess[jT].noId);
          const nextlng = nextNode.longitude;
          const nextlat = nextNode.latitude;

          temp = temp + currentNode.nome + nextNode.nome;

          this.linhasIds[a] = temp;

          a++;

          if (nI > nJ) {
            var t = nI;
            nI = nJ;
            nJ = t;
          }



          coord = this.drawSegmento(lng, lat, nextlng, nextlat, nI, nJ);

          this.addLinPer(coord, temp, linhas[p].cor);


          console.log(currentNode.nome, nextNode.nome);
          console.log(this.mat[nI][nJ]);

          if (this.mat[nI][nJ] <= 0) {
            this.mat[nI][nJ] = 1 - this.mat[nI][nJ];
          } else {
            this.mat[nI][nJ] = -this.mat[nI][nJ];
          }

          coord = [];
        }
      }
    }
  }


  drawSegmento(lng1: number, lat1: number, lng2: number, lat2: number, nI: number, nJ: number) {

    var coord = new Array();

    var m1 = (lng2 - lng1);

    m1 = m1 * m1;

    var m2 = (lat2 - lat1);

    m2 = m2 * m2;

    var r = Math.sqrt(m1 + m2);

    var cosBeta = -(lat2 - lat1) / r;

    var sinBeta = (lng2 - lng1) / r;

    var lng3 = lng1 + this.mat[nI][nJ] * 0.000002 * cosBeta

    var lat3 = lat1 + this.mat[nI][nJ] * 0.000002 * sinBeta

    var lng4 = lng2 + this.mat[nI][nJ] * 0.000002 * cosBeta

    var lat4 = lat2 + this.mat[nI][nJ] * 0.000002 * sinBeta

    coord[0] = [lng3, lat3];
    coord[1] = [lng4, lat4];

    return coord;

  }

  addLinPer(coord: Array<any>, perID: string, cor: string) {
    this.map.addSource(perID, {
      'type': 'geojson',
      'data': {
        'type': 'Feature',
        'properties': {},
        'geometry': {
          'type': 'LineString',
          'coordinates': coord
        }
      }
    });
    this.map.addLayer({
      'id': perID,
      'type': 'line',
      'source': perID,
      'layout': {
        'line-join': 'round',
        'line-cap': 'round'
      },
      'paint': {
        'line-color': cor,
        'line-width': 4
      }
    });

  }

  hideLinhas() {
    for (var i = 0; i < this.linhasIds.length; i++) {
      this.map.removeLayer(this.linhasIds[i]);
      this.map.removeSource(this.linhasIds[i]);
    }
  }

  findNo(nodes: No[], id: String) {

    for (var j = 0; j < nodes.length; j++) {
      if (nodes[j].domainId === id) {
        return nodes[j];
      }
    }

  }

  findNoPosition(nodes: No[], id: String) {

    for (var j = 0; j < nodes.length; j++) {
      if (nodes[j].domainId === id) {
        return j;
      }
    }

  }

  findPercurso(percurso: Percurso[], id: String) {

    for (var j = 0; j < percurso.length; j++) {
      if (percurso[j].domainId === id) {
        return percurso[j];
      }
    }

  }

  rgbToHex(color: string) {
    color = "" + color;
    if (!color || color.indexOf("RGB") < 0) {
      return;
    }

    if (color.charAt(0) == "#") {
      return color;
    }

    var nums = /(.*?)RGB\((\d+),\s*(\d+),\s*(\d+)\)/i.exec(color),
      r = parseInt(nums[2], 10).toString(16),
      g = parseInt(nums[3], 10).toString(16),
      b = parseInt(nums[4], 10).toString(16);

    return "#" + (
      (r.length == 1 ? "0" + r : r) +
      (g.length == 1 ? "0" + g : g) +
      (b.length == 1 ? "0" + b : b)
    );
  }

  try(nodes: No[]) {

    var modelAltitude = 0;
    var modelRotate = [Math.PI / 2, 0, 0];

    var modelAsMercatorCoordinate = mapboxgl.MercatorCoordinate.fromLngLat(
      [nodes[this.i].longitude, nodes[this.i].latitude],
      modelAltitude
    );


    var modelTransform = {
      translateX: modelAsMercatorCoordinate.x,
      translateY: modelAsMercatorCoordinate.y,
      translateZ: modelAsMercatorCoordinate.z,
      rotateX: modelRotate[0],
      rotateY: modelRotate[1],
      rotateZ: modelRotate[2],
      scale: modelAsMercatorCoordinate.meterInMercatorCoordinateUnits()
    };



    this.map.addLayer({
      id: nodes[this.i].domainId,
      type: 'custom',
      renderingMode: '3d',
      onAdd: function (map, gl) {
        this.camera = new THREE.Camera();
        this.scene = new THREE.Scene();

        var directionalLight = new THREE.DirectionalLight(0xffffff);
        directionalLight.position.set(0, -70, 100).normalize();
        this.scene.add(directionalLight);

        var directionalLight2 = new THREE.DirectionalLight(0xffffff);
        directionalLight2.position.set(0, 70, 100).normalize();
        this.scene.add(directionalLight2);

        // use the three.js GLTF loader to add the 3D model to the three.js scene
        var loader = new GLTFLoader();
        loader.load(
          'https://docs.mapbox.com/mapbox-gl-js/assets/34M_17/34M_17.gltf',
          function (gltf) {
            this.scene.add(gltf.scene);
          }.bind(this)
        );
        this.map = map;

        // use the Mapbox GL JS map canvas for three.js
        this.renderer = new THREE.WebGLRenderer({
          canvas: map.getCanvas(),
          context: gl,
          antialias: true
        });

        this.renderer.autoClear = false;
      },
      render: function (gl, matrix) {
        var rotationX = new THREE.Matrix4().makeRotationAxis(
          new THREE.Vector3(1, 0, 0),
          modelTransform.rotateX
        );
        var rotationY = new THREE.Matrix4().makeRotationAxis(
          new THREE.Vector3(0, 1, 0),
          modelTransform.rotateY
        );
        var rotationZ = new THREE.Matrix4().makeRotationAxis(
          new THREE.Vector3(0, 0, 1),
          modelTransform.rotateZ
        );

        var m = new THREE.Matrix4().fromArray(matrix);
        var l = new THREE.Matrix4()
          .makeTranslation(
            modelTransform.translateX,
            modelTransform.translateY,
            modelTransform.translateZ
          )
          .scale(
            new THREE.Vector3(
              modelTransform.scale,
              -modelTransform.scale,
              modelTransform.scale
            )
          )
          .multiply(rotationX)
          .multiply(rotationY)
          .multiply(rotationZ);

        this.camera.projectionMatrix = m.multiply(l);
        this.renderer.state.reset();
        this.renderer.render(this.scene, this.camera);
        this.map.triggerRepaint();
      }
    });


    this.i++;

  }

  modoPP(){
    
    var newWin = window.open("assets/newFile.html", "newWindow", "width=500,height=700");
    //newWin.document.body.innerHTML = "<!DOCTYPE html><html><head><meta charset=\"utf-8\" /><title>Set pitch and bearing</title><meta name=\"viewport\" content=\"initial-scale=1,maximum-scale=1,user-scalable=no\" /><script src=\"https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.js\"></script><link href=\"https://api.mapbox.com/mapbox-gl-js/v2.0.0/mapbox-gl.css\" rel=\"stylesheet\" /><style>	body { margin: 0; padding: 0; }	#map { position: absolute; top: 0; bottom: 0; width: 100%; }</style></head><body><div id=\"map\"></div><script>	mapboxgl.accessToken = 'pk.eyJ1IjoiMTE4MTA3NiIsImEiOiJja2h4cjE3cmEwMzI5MnVvYTg4OTU0bGR6In0.cYUrvgkjzcgl-0MiUo0N8A';var map = new mapboxgl.Map({container: 'map',style: 'mapbox://styles/1181076/ckihoeksb2ju919o2273mbkss', center: [-73.5804, 45.53483],pitch: 60, bearing: -60, zoom: 10});</script></body></html>";
  }


}
















