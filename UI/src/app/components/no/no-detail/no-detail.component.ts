import { Component, OnInit, Input } from '@angular/core';
import { No } from '../../../domain/nos/no';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { NoService } from '../../../services/nos/no.service';
import { MessageService } from '../../../services/messages/message.service';

@Component({
  selector: 'app-no-detail',
  templateUrl: './no-detail.component.html',
  styleUrls: ['./no-detail.component.css']
})
export class NoDetailComponent implements OnInit {
  @Input() no: No;

  constructor(
    private route: ActivatedRoute,
    private noService: NoService,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.getNo();
  }
  
  getNo(): void {
    const domainId = this.route.snapshot.paramMap.get("domainId");
    this.noService.getNo(domainId)
      .subscribe(no => this.no = no);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.noService.updateNo(this.no)
      .subscribe(() => this.goBack());
  }

}
