import { Component, OnInit } from '@angular/core';

import { No } from '../../../domain/nos/no';
import { NoService } from '../../../services/nos/no.service';
import { MessageService } from '../../../services/messages/message.service';
import { TempoViagemGrupo } from "../../../../../../MDR/src/domain/no/tempoViagemGrupo";


@Component({
  selector: 'app-nos',
  templateUrl: './nos.component.html',
  styleUrls: ['./nos.component.css']
})
export class NosComponent implements OnInit {
  nos: No[] = new Array();

  constructor(private noService: NoService) { }

  ngOnInit() {
    this.getNos();
  }

  getNos(): void {
    this.noService.getNos()
    .subscribe(nos => this.nos = nos);
  }

  add(domainId:string, nome: string,latitude: number,longitude: number,nomeCurto: string,isEstacaoRecolha: boolean,isPontoRendicao: boolean,tempoViagem: { noId: String, duracao: Number}[]): void {
    nome = nome.trim();
    if (!nome) { return; }
    this.noService.addNo({ domainId, nome,latitude,longitude,nomeCurto, isEstacaoRecolha,isPontoRendicao,tempoViagem } as No)
      .subscribe(no => {
        this.nos.push(no);
      });

  }

  delete(no: No): void {
    this.nos = this.nos.filter(h => h !== no);
    this.noService.deleteNo(no).subscribe();
  }
}