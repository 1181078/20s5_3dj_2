import {
	Component, OnInit, DoCheck, EventEmitter, Input, IterableDiffers, OnChanges,
	Output, SimpleChange, ChangeDetectorRef, ChangeDetectionStrategy
} from '@angular/core';

import { Linha } from '../../../domain/linha/linha';
import { LinhaService } from '../../../services/linha/linha.service';
import { MessageService } from '../../../services/messages/message.service';
import { PercursoLinha } from "../../../../../../MDR/src/domain/linha/percursoLinha";
import { DualListComponent } from 'angular-dual-listbox';
import { Percurso } from '../../../domain/percurso/percurso';
import { PercursoService } from '../../../services/percurso/percurso.service';

@Component({
  selector: 'app-linhas',
  templateUrl: './linhas.component.html',
  styleUrls: ['./linhas.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LinhasComponent implements OnInit {
  linhas: Linha[] = new Array();
  tab = 1;
  keepSorted = true;
  key: string;
  display: string;
  filter = false;
  source: Percurso[] = [];
  confirmed: Percurso[] = [];
  userAdd = '';
  disabled = false;
  sourceLeft = true;
  format: any = DualListComponent.DEFAULT_FORMAT;

  percursos: Percurso[] = new Array();
  confirmedPercursos: Percurso[] = new Array();

  constructor(private linhaService: LinhaService, private percursoService: PercursoService,
		private cd: ChangeDetectorRef) { }

  ngOnInit() {
    this.getLinhas();
    this.doReset();
  }

  getLinhas(): void {
    this.linhaService.getLinhas()
      .subscribe(linhas => this.linhas = linhas);
  }

  getPercursos(): any{
		this.percursoService.getPercursos().subscribe((percursos) => {
      this.percursos = percursos;
			this.cd.markForCheck();
		});

	}

  add(domainId: string, nome: string, cor: string, pL: Percurso[]): void {

    const percursoLinha: { percursoLinhaId: String, percurso: String, orientation: String }[] = new Array();

		for(var i=0; i<pL.length; i++){
			const percursoLinhaId = domainId + pL[i].domainId;
			const percurso = pL[i].domainId;
			const orientation = "not set";
			const ne4 = {percursoLinhaId, percurso, orientation};
			percursoLinha[i] = ne4;
    }
    
    this.linhaService.addLinha({ domainId, nome, cor, percursoLinha } as Linha)
      .subscribe(linha => {
        this.linhas.push(linha);
      });
  }

  delete(linha: Linha): void {
    this.linhas = this.linhas.filter(h => h !== linha);
    this.linhaService.deleteLinha(linha).subscribe();
  }

  private useNos() {
    this.key = 'domainId';
    this.display = 'domainId';
    this.keepSorted = true;
    this.source = this.percursos;
    this.confirmed = this.confirmedPercursos;
  }

  doReset() {
    this.getPercursos();
		console.log(this.percursos);
    this.confirmedPercursos = [];
    this.useNos();
  }


  filterBtn() {
    return (this.filter ? 'Hide Filter' : 'Show Filter');
  }

  doDisable() {
    this.disabled = !this.disabled;
  }

  disableBtn() {
    return (this.disabled ? 'Enable' : 'Disabled');
  }

  swapDirection() {
    this.sourceLeft = !this.sourceLeft;
    this.format.direction = this.sourceLeft ? DualListComponent.LTR : DualListComponent.RTL;
  }

}