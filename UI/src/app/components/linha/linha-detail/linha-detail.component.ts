import { Component, OnInit, Input } from '@angular/core';
import { Linha } from '../../../domain/linha/linha';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { LinhaService } from '../../../services/linha/linha.service';
import { MessageService } from '../../../services/messages/message.service';

@Component({
  selector: 'app-linha-detail',
  templateUrl: './linha-detail.component.html',
  styleUrls: ['./linha-detail.component.css']
})
export class LinhaDetailComponent implements OnInit {
  @Input() linha: Linha;

  constructor(
    private route: ActivatedRoute,
    private linhaService: LinhaService,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.getLinha();
  }
  
  getLinha(): void {
    const domainId = this.route.snapshot.paramMap.get("domainId");
    this.linhaService.getLinha(domainId)
      .subscribe(linha => this.linha = linha);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.linhaService.updateLinha(this.linha)
      .subscribe(() => this.goBack());
  }

}
