import { HttpClientModule } from '@angular/common/http';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularDualListBoxModule } from 'angular-dual-listbox';
import { AppRoutingModule } from '../../../app-routing.module';
import { AppComponent } from '../../../app.component';
import * as sinon from 'sinon';

import { NgModule } from '@angular/core';


import { TiposTripulanteComponent } from '../../tipoTripulante/tiposTripulante/tiposTripulante.component';
import { TipoTripulanteDetailComponent } from '../../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
import { TiposViaturaComponent } from '../../tipoViatura/tiposViatura/tiposViatura.component';
import { TipoViaturaDetailComponent } from '../../tipoViatura/tipoViatura-detail/tipoViatura-detail.component';
import { PercursosComponent } from '../../percurso/percursos/percursos.component';
import { PercursoDetailComponent } from '../../percurso/percurso-detail/percurso-detail.component';
import { NosComponent } from '../../no/nos/nos.component';
import { NoDetailComponent } from '../../no/no-detail/no-detail.component';
import { LinhasComponent } from '../../linha/linhas/linhas.component';
import { LinhaDetailComponent } from '../../linha/linha-detail/linha-detail.component';
import { MessagesComponent } from '../../messages/messages.component';
import { DashboardComponent } from '../../../components/dashboard/dashboard.component';
import { TipoViaturaSearchComponent } from '../../tipoViatura/tipoViatura-search/tipoViatura-search.component';
import { LoginComponent } from '../../login/login.component';
import { MapComponent } from '../../map/map.component';
import { DragDirective } from '../../../components/importFile/dragDrop.directive';
import { PageNotFoundComponent } from '../../pageNotFound/page-not-found.component';
import { ImportFileComponent } from '../../../components/importFile/importFile.component';
import { UserDirective } from '../../login/directives/user.directive';
import { UserRoleDirective } from '../../login/directives/user-role.directive';
import { AuthService } from '../../../services/login/auth.service';
import { HomeComponent } from '../../../components/home/home.component';
import { PerfilComponent } from '../../login/perfil/perfil.component';
import { SignupComponent } from '../../login/signup/signup.component';
import { GestaoCaminhosComponent } from '../../../components/gestaoCaminhos/gestaoCaminhos.component';
import { TipoViatura } from '../../../domain/tipoViatura/tipoViatura';
import { TipoViaturaService } from '../../../services/tipoViatura/tipoViatura.service';
import { Observable } from 'rxjs';
import { TripulantesComponent } from 'src/app/components/tripulante/tripulantes/tripulantes.component';
import { ViaturasComponent } from 'src/app/components/viatura/viatura/viaturas.component';
import { TripulanteDetailComponent } from 'src/app/components/tripulante/tripulante-detail/tripulante-detail.component';
import { ViaturaDetailComponent } from 'src/app/components/viatura/viatura-detail/viatura-detail.component';
import { ViagemDetailComponent } from 'src/app/components/viagem/viagem-detail/viagem-detail.component';
import { ViagensComponent } from 'src/app/components/viagem/viagem/viagens.component';
import { BlocosTrabalhoComponent } from 'src/app/components/blocoTrabalho/blocosTrabalho/blocosTrabalho.component';
import { BlocoTrabalhoDetailComponent } from 'src/app/components/blocoTrabalho/blocoTrabalho-detail/blocoTrabalho-detail.component';
import { ServicosViaturaComponent } from 'src/app/components/servicoViatura/servicosViatura/servicosViatura.component';
import { ServicosTripulanteComponent } from 'src/app/components/servicoTripulante/servicosTripulante/servicosTripulante.component';
import { ServicoTripulanteDetailComponent } from 'src/app/components/servicoTripulante/servicoTripulante-detail/servicoTripulante-detail.component';
import { ServicoViaturaDetailComponent } from 'src/app/components/servicoViatura/servicoViatura-detail/servicoViatura-detail.component';
import { SequenciamentoMotoristasComponent } from 'src/app/components/sequenciamentoMotoristas/sequenciamentoMotoristas.component';


describe('TiposViaturaComponent', () => {
  let component: TiposViaturaComponent;
  let fixture: ComponentFixture<TiposViaturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        AngularDualListBoxModule
      ],
      declarations: [
        AppComponent,
        TiposTripulanteComponent,
        TipoTripulanteDetailComponent,
        TiposViaturaComponent,
        TipoViaturaDetailComponent,
        PercursosComponent,
        PercursoDetailComponent,
        NosComponent,
        NoDetailComponent,
        LinhasComponent,
        LinhaDetailComponent,
        MessagesComponent,
        DashboardComponent,
        TipoViaturaSearchComponent,
        LoginComponent,
        MapComponent,
        PageNotFoundComponent,
        DragDirective,
        ImportFileComponent,
        HomeComponent,
        PerfilComponent,
        SignupComponent,
        GestaoCaminhosComponent,
        UserDirective,
        UserRoleDirective,
        TripulantesComponent,
        ViaturasComponent,
        TripulanteDetailComponent,
        ViaturaDetailComponent,
        ViagemDetailComponent,
        ViagensComponent,
        BlocosTrabalhoComponent,
        BlocoTrabalhoDetailComponent,
        ServicosViaturaComponent,
        ServicosTripulanteComponent,
        ServicoViaturaDetailComponent,
        ServicoTripulanteDetailComponent,
        SequenciamentoMotoristasComponent
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiposViaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should start array as 0`, () => {
    const fixture = TestBed.createComponent(TiposViaturaComponent);
    const app = fixture.componentInstance;
    expect(app.tiposViatura.length).toBe(0);
  });

  it(`should array should not be afftected because of subscribe`, () => {
    const service: TipoViaturaService = TestBed.get(TipoViaturaService);

    var tt: TipoViatura = {domainId: "teste2", nome: "teste2", autonomia: 10, custo: 12, velocidadeMedia: 21,
    combustivel: 14, consumo: 13, emissoes:31} as TipoViatura;

	sinon.stub(service, "addTipoViatura").returns( Observable.create(observer => {observer.next(tt);observer.complete()}));

    sinon.stub(service, "deleteTipoViatura").returns( Observable.create(observer => {observer.next(tt);observer.complete()}));

    sinon.stub(service, "getTiposViatura").returns( Observable.create(observer => {observer.next([]);observer.complete()}));

    var a = new TiposViaturaComponent(service);
    
    var len = a.tiposViatura.length;
    console.log(len);
    a.add("teste2", "teste2", 10, 12, 21, 14, 13, 31);    
    var len2 = len+1;
    expect(a.tiposViatura.length).toEqual(len2); 
    a.delete({ "domainId": "teste2", "nome": "teste2", "autonomia": 10, "custo": 13, "velocidadeMedia": 13, 
    "combustivel": 14, "consumo": 16, "emissoes": 13 } as TipoViatura);
    a.getTiposViatura();
    expect(a.tiposViatura.length).toEqual(len);
  });

  



});
