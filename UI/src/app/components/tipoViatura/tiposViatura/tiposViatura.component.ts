import { Component, OnInit } from '@angular/core';

import { TipoViatura } from '../../../domain/tipoViatura/tipoViatura';
import { TipoViaturaService } from '../../../services/tipoViatura/tipoViatura.service';
import { MessageService } from '../../../services/messages/message.service';

@Component({
  selector: 'app-tiposViatura',
  templateUrl: './tiposViatura.component.html',
  styleUrls: ['./tiposViatura.component.css']
})
export class TiposViaturaComponent implements OnInit {
  tiposViatura: TipoViatura[]= new Array();

  constructor(private tipoViaturaService: TipoViaturaService) { }

  ngOnInit() {
    this.getTiposViatura();
  }

  getTiposViatura(): void {
    this.tipoViaturaService.getTiposViatura()
    .subscribe(tiposViatura => this.tiposViatura = tiposViatura);
  }

  add(domainId:string, nome: string,autonomia: number,custo: number,velocidadeMedia: number,combustivel: number,consumo: number,emissoes: number): void {
    nome = nome.trim();
    if (!nome) { return; }
    this.tipoViaturaService.addTipoViatura({ domainId, nome,autonomia,custo,velocidadeMedia, combustivel,consumo,emissoes } as TipoViatura)
      .subscribe(tipoViatura => {
        this.tiposViatura.push(tipoViatura);
      });
  }

  delete(tipoViatura: TipoViatura): void {
    this.tiposViatura = this.tiposViatura.filter(h => h !== tipoViatura);
    this.tipoViaturaService.deleteTipoViatura(tipoViatura).subscribe();
  }
}