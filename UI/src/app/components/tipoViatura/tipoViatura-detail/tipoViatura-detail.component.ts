import { Component, OnInit, Input } from '@angular/core';
import { TipoViatura } from '../../../domain/tipoViatura/tipoViatura';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { TipoViaturaService } from '../../../services/tipoViatura/tipoViatura.service';
import { MessageService } from '../../../services/messages/message.service';

@Component({
  selector: 'app-tipoViatura-detail',
  templateUrl: './tipoViatura-detail.component.html',
  styleUrls: ['./tipoViatura-detail.component.css']
})
export class TipoViaturaDetailComponent implements OnInit {
  @Input() tipoViatura: TipoViatura;

  constructor(
    private route: ActivatedRoute,
    private tipoViaturaService: TipoViaturaService,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.getTipoViatura();
  }
  
  getTipoViatura(): void {
    const domainId = this.route.snapshot.paramMap.get("domainId");
    this.tipoViaturaService.getTipoViatura(domainId)
      .subscribe(tipoViatura => this.tipoViatura = tipoViatura);
  }

  getTipoViatura2(domainId: string): void {
    this.tipoViaturaService.getTipoViatura(domainId).subscribe(tipoViatura => this.tipoViatura = tipoViatura);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.tipoViaturaService.updateTipoViatura(this.tipoViatura)
      .subscribe(() => this.goBack());
  }

}
