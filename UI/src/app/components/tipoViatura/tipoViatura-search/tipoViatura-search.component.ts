import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { TipoViatura } from '../../../domain/tipoViatura/tipoViatura';
import { TipoViaturaService } from '../../../services/tipoViatura/tipoViatura.service';

@Component({
  selector: 'app-tipoViatura-search',
  templateUrl: './tipoViatura-search.component.html',
  styleUrls: [ './tipoViatura-search.component.css' ]
})
export class TipoViaturaSearchComponent implements OnInit {
  tiposViaturas: Observable<TipoViatura[]>;
  private searchTerms = new Subject<string>();

  constructor(private tipoViaturaService: TipoViaturaService) {}

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.tiposViaturas = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.tipoViaturaService.searchTiposViatura(term)),
    );
  }
}