import {
	Component, OnInit, DoCheck, EventEmitter, Input, IterableDiffers, OnChanges,
	Output, SimpleChange, ChangeDetectorRef, ChangeDetectionStrategy
} from '@angular/core';

import { BlocoTrabalho } from '../../../domain/blocoTrabalho/blocoTrabalho';
import { BlocoTrabalhoService } from '../../../services/blocoTrabalho/blocoTrabalho.service';
import { MessageService } from '../../../services/messages/message.service';
import { DualListComponent } from 'angular-dual-listbox';
import { catchError, map, tap } from 'rxjs/operators';
import { TempoViagemGrupo, TempoViagemGrupoProps } from '../../../../../../MDR/src/domain/no/tempoViagemGrupo';
import { Viagem } from '../../../domain/viagem/viagem';
import { ViagemService } from '../../../services/viagem/viagem.service';
import { PercursoService } from '../../../services/percurso/percurso.service';
import { Percurso } from '../../../domain/percurso/percurso';


@Component({
	selector: 'app-blocosTrabalho',
	templateUrl: './blocosTrabalho.component.html',
	styleUrls: ['./blocosTrabalho.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class BlocosTrabalhoComponent implements OnInit {
	tab = 1;
	keepSorted = true;
	key: string; 
	display: string;
	filter = false;
	source: Viagem[] = [];
	confirmed: Viagem[] = [];
	userAdd = '';
	disabled = false;
	sourceLeft = true;
	format: any = DualListComponent.DEFAULT_FORMAT;

	blocosTrabalho: BlocoTrabalho[] = new Array();
	viagens: Viagem[] = new Array();
	confirmedViagens: Viagem[] = new Array();
	pers: Percurso[] = new Array();


	constructor(
		private blocoTrabalhoService: BlocoTrabalhoService,
		private viagemService: ViagemService,
		private percursoService: PercursoService,
	) {
	}

	ngOnInit() {
		this.getBlocosTrabalhos();
		this.doReset();
	}

	getBlocosTrabalhos(): void {
		this.blocoTrabalhoService.getBlocosTrabalho()
			.subscribe(blocosTrabalho => {this.blocosTrabalho = blocosTrabalho, console.log(blocosTrabalho), document.getElementById("refr").click()});
	}

	getViagens(): any{
		this.viagemService.getViagens().subscribe((via) => {
			this.viagens = via; console.log(this.viagens), document.getElementById("refr").click()
		});

	}

  getPercursos(): any{
		this.percursoService.getPercursos().subscribe((via) => {
			this.pers = via, document.getElementById("refr").click()
		});

	}

	show( nP:Viagem[], per: Percurso[]){
		console.log(nP);
		console.log(per);
	}


	add(domainId: string, nP:Viagem[], per: Percurso[]): void {
		
    var tempoInicio, tempoFim, noInicio, noFim;

    var len1 = nP.length;
    var len2 = nP[len1-1].tempos.length;
    
    tempoInicio = nP[0].tempos[0];
    tempoFim = nP[len1 -1].tempos[len2 -1];

		for(var i=0; i<per.length; i++){

			console.log(nP[0]);

			if(per[i].domainId === nP[0].pathId){
        		noInicio = per[i].nosPercurso[0].noId;
      		}
		
			if(per[i].domainId === nP[len1-1].pathId){
				noFim = per[i].nosPercurso[per[i].nosPercurso.length-1].noId;
			}
    	} 

    var viagens = new Array();
    
    for(var j = 0; j<nP.length; j++){
      viagens[j] = nP[j].domainId;
    }
		
		this.blocoTrabalhoService.addBlocoTrabalho({ domainId, tempoInicio, tempoFim, noInicio, noFim, viagens } as BlocoTrabalho)
			.subscribe(blocoTrabalho => {
				this.blocosTrabalho.push(blocoTrabalho);
			});

		document.getElementById("refr").click();
	}

	delete(blocoTrabalho: BlocoTrabalho): void {
		this.blocosTrabalho = this.blocosTrabalho.filter(h => h !== blocoTrabalho);
		this.blocoTrabalhoService.deleteBlocoTrabalho(blocoTrabalho).subscribe();
	}

	private useViagens() {
		this.key = 'domainId';
		this.display = 'domainId';
		this.keepSorted = true;
		this.source = this.viagens;
		this.confirmed = this.confirmedViagens;
	}

	doReset() {
		this.getViagens();
		this.getPercursos();
		this.confirmedViagens = [];
		this.useViagens();
	}


	filterBtn() {
		return (this.filter ? 'Hide Filter' : 'Show Filter');
	}

	doDisable() {
		this.disabled = !this.disabled;
	}

	disableBtn() {
		return (this.disabled ? 'Enable' : 'Disabled');
	}

	swapDirection() {
		this.sourceLeft = !this.sourceLeft;
		this.format.direction = this.sourceLeft ? DualListComponent.LTR : DualListComponent.RTL;
	}

	refresh() {
	}


}