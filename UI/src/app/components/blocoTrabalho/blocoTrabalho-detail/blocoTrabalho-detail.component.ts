import { Component, OnInit, Input } from '@angular/core';
import { BlocoTrabalho } from '../../../domain/blocoTrabalho/blocoTrabalho';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { BlocoTrabalhoService } from '../../../services/blocoTrabalho/blocoTrabalho.service';
import { MessageService } from '../../../services/messages/message.service';

@Component({
  selector: 'app-blocoTrabalho-detail',
  templateUrl: './blocoTrabalho-detail.component.html',
  styleUrls: ['./blocoTrabalho-detail.component.css']
})
export class BlocoTrabalhoDetailComponent implements OnInit {
  @Input() blocoTrabalho: BlocoTrabalho;

  constructor(
    private route: ActivatedRoute,
    private blocoTrabalhoService: BlocoTrabalhoService,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.getBlocoTrabalho();
  }
  
  getBlocoTrabalho(): void {
    const domainId = this.route.snapshot.paramMap.get("domainId");
    this.blocoTrabalhoService.getBlocoTrabalho(domainId)
      .subscribe(blocoTrabalho => {this.blocoTrabalho = blocoTrabalho, console.log(this.blocoTrabalho), document.getElementById("refr").click()});
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.blocoTrabalhoService.updateBlocoTrabalho(this.blocoTrabalho)
      .subscribe(() => this.goBack());
  }

  refresh(){

  }

}
