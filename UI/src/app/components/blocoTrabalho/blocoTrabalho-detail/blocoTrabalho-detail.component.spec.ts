// import { HttpClientModule } from '@angular/common/http';
// import { TestBed, async, ComponentFixture } from '@angular/core/testing';
// import { FormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
// import { RouterTestingModule } from '@angular/router/testing';
// import { AngularDualListBoxModule } from 'angular-dual-listbox';
// import { AppRoutingModule } from '../../app-routing.module';
// import { AppComponent } from '../../app.component';

// import { NgModule } from '@angular/core';


// import { TiposTripulanteComponent } from '../../tipoTripulante/tiposTripulante/tiposTripulante.component';
// import { TipoTripulanteDetailComponent } from '../../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
// import { TiposViaturaComponent } from '../../tipoViatura/tiposViatura/tiposViatura.component';
// import { TipoViaturaDetailComponent } from '../../tipoViatura/tipoViatura-detail/tipoViatura-detail.component';
// import { PercursosComponent } from '../../percurso/percursos/percursos.component';
// import { PercursoDetailComponent } from '../../percurso/percurso-detail/percurso-detail.component';
// import { BlocosTrabalhoComponent } from '../../blocoTrabalho/blocosTrabalho/blocosTrabalho.component';
// import { BlocoTrabalhoDetailComponent } from '../../blocoTrabalho/blocoTrabalho-detail/blocoTrabalho-detail.component';
// import { LinhasComponent } from '../../linha/linhas/linhas.component';
// import { LinhaDetailComponent } from '../../linha/linha-detail/linha-detail.component';
// import { MessagesComponent } from '../../messages/messages.component';
// import { DashboardComponent } from '../../dashboard/dashboard.component';
// import { TipoViaturaSearchComponent } from '../../tipoViatura/tipoViatura-search/tipoViatura-search.component';
// import { LoginComponent } from '../../login/login.component';
// import { MapComponent } from '../../map/map.component';
// import { DragDirective } from '../../importFile/dragDrop.directive';
// import { PageBlocoTrabalhotFoundComponent } from '../../pageBlocoTrabalhotFound/page-blocoTrabalhot-found.component';
// import { ImportFileComponent } from '../../importFile/importFile.component';
// import { UserDirective } from '../../login/directives/user.directive';
// import { UserRoleDirective } from '../../login/directives/user-role.directive';
// import { AuthService } from '../../login/services/auth.service';
// import { HomeComponent } from '../../home/home.component';
// import { PerfilComponent } from '../../login/perfil/perfil.component';
// import { SignupComponent } from '../../login/signup/signup.component';
// import { GestaoCaminhosComponent } from '../../gestaoCaminhos/gestaoCaminhos.component';
// import * as siblocoTrabalhon from 'siblocoTrabalhon';
// import { Observable } from 'rxjs';
// import { Location } from '@angular/common';
// import { ActivatedRoute } from '@angular/router';
// import { BlocoTrabalhoService } from '../blocoTrabalho.service';
// import { BlocoTrabalho } from '../blocoTrabalho';

// describe('TiposTripulanteDetailComponent', () => {
//   let component: BlocoTrabalhoDetailComponent;
//   let fixture: ComponentFixture<BlocoTrabalhoDetailComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         BrowserModule,
//         AppRoutingModule,
//         FormsModule,
//         HttpClientModule,
//         AngularDualListBoxModule
//       ],
//       declarations: [
//         AppComponent,
//         TiposTripulanteComponent,
//         TipoTripulanteDetailComponent,
//         TiposViaturaComponent,
//         TipoViaturaDetailComponent,
//         PercursosComponent,
//         PercursoDetailComponent,
//         BlocosTrabalhoComponent,
//         BlocoTrabalhoDetailComponent,
//         LinhasComponent,
//         LinhaDetailComponent,
//         MessagesComponent,
//         DashboardComponent,
//         TipoViaturaSearchComponent,
//         LoginComponent,
//         MapComponent,
//         PageBlocoTrabalhotFoundComponent,
//         DragDirective,
//         ImportFileComponent,
//         HomeComponent,
//         PerfilComponent,
//         SignupComponent,
//         GestaoCaminhosComponent,
//         UserDirective,
//         UserRoleDirective
//       ],
//     })
//     .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(BlocoTrabalhoDetailComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it(`should array should blocoTrabalhot be afftected because of subscribe`, () => {
//     const service: BlocoTrabalhoService = TestBed.get(BlocoTrabalhoService);
//     const domainId = "testeUnit";
//     const blocoTrabalhome = "testeUnit";
//     const latitude = 12;
//     const longitude = 15;
//     const blocoTrabalhomeCurto ="teste";
//     const isEstacaoRecolha = false;
//     const isPontoRendicao = true;
//     const tempoViagem = [{ blocoTrabalhoId: "idTeste", duracao: 25}];
//     var blocoTrabalho : BlocoTrabalho = {domainId,blocoTrabalhome,latitude,longitude,blocoTrabalhomeCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem} as BlocoTrabalho;


// 	siblocoTrabalhon.stub(service, "getBlocoTrabalho").returns( Observable.create(observer => {observer.next(blocoTrabalho);observer.complete()}));

//     siblocoTrabalhon.stub(service, "updateBlocoTrabalho").returns( Observable.create(observer => {observer.next(blocoTrabalho);observer.complete()}));

//     var a = new BlocoTrabalhoDetailComponent(TestBed.get(ActivatedRoute), service, TestBed.get(Location));
    
//     a.getBlocoTrabalho(); 

//     expect(a.blocoTrabalho).toEqual(blocoTrabalho); 

//     a.blocoTrabalho.blocoTrabalhome = "testeUnit";
//     blocoTrabalho.blocoTrabalhome="testeUnit";
//     a.save();

//     expect(a.blocoTrabalho).toEqual(blocoTrabalho); 
   
//   });
// });