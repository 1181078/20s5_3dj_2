import { Component, OnInit, Input } from '@angular/core';
import { ServicoViatura } from '../../../domain/servicoViatura/servicoViatura';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ServicoViaturaService } from '../../../services/servicoViatura/servicoViatura.service';
import { MessageService } from '../../../services/messages/message.service';

@Component({
  selector: 'app-servicoViatura-detail',
  templateUrl: './servicoViatura-detail.component.html',
  styleUrls: ['./servicoViatura-detail.component.css']
})
export class ServicoViaturaDetailComponent implements OnInit {
  @Input() servicoViatura: ServicoViatura;

  constructor(
    private route: ActivatedRoute,
    private servicoViaturaService: ServicoViaturaService,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.getServicoViatura();
  }
  
  getServicoViatura(): void {
    const domainId = this.route.snapshot.paramMap.get("domainId");
    this.servicoViaturaService.getServicoViatura(domainId)
      .subscribe(servicoViatura => this.servicoViatura = servicoViatura);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.servicoViaturaService.updateServicoViatura(this.servicoViatura)
      .subscribe(() => this.goBack());
  }

}
