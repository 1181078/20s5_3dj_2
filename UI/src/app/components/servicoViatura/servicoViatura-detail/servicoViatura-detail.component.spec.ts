// import { HttpClientModule } from '@angular/common/http';
// import { TestBed, async, ComponentFixture } from '@angular/core/testing';
// import { FormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
// import { RouterTestingModule } from '@angular/router/testing';
// import { AngularDualListBoxModule } from 'angular-dual-listbox';
// import { AppRoutingModule } from '../../app-routing.module';
// import { AppComponent } from '../../app.component';

// import { NgModule } from '@angular/core';


// import { TiposTripulanteComponent } from '../../tipoTripulante/tiposTripulante/tiposTripulante.component';
// import { TipoTripulanteDetailComponent } from '../../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
// import { TiposViaturaComponent } from '../../tipoViatura/tiposViatura/tiposViatura.component';
// import { TipoViaturaDetailComponent } from '../../tipoViatura/tipoViatura-detail/tipoViatura-detail.component';
// import { PercursosComponent } from '../../percurso/percursos/percursos.component';
// import { PercursoDetailComponent } from '../../percurso/percurso-detail/percurso-detail.component';
// import { ServicosViaturaComponent } from '../../servicoViatura/servicosViatura/servicosViatura.component';
// import { ServicoViaturaDetailComponent } from '../../servicoViatura/servicoViatura-detail/servicoViatura-detail.component';
// import { LinhasComponent } from '../../linha/linhas/linhas.component';
// import { LinhaDetailComponent } from '../../linha/linha-detail/linha-detail.component';
// import { MessagesComponent } from '../../messages/messages.component';
// import { DashboardComponent } from '../../dashboard/dashboard.component';
// import { TipoViaturaSearchComponent } from '../../tipoViatura/tipoViatura-search/tipoViatura-search.component';
// import { LoginComponent } from '../../login/login.component';
// import { MapComponent } from '../../map/map.component';
// import { DragDirective } from '../../importFile/dragDrop.directive';
// import { PageServicoViaturatFoundComponent } from '../../pageServicoViaturatFound/page-servicoViaturat-found.component';
// import { ImportFileComponent } from '../../importFile/importFile.component';
// import { UserDirective } from '../../login/directives/user.directive';
// import { UserRoleDirective } from '../../login/directives/user-role.directive';
// import { AuthService } from '../../login/services/auth.service';
// import { HomeComponent } from '../../home/home.component';
// import { PerfilComponent } from '../../login/perfil/perfil.component';
// import { SignupComponent } from '../../login/signup/signup.component';
// import { GestaoCaminhosComponent } from '../../gestaoCaminhos/gestaoCaminhos.component';
// import * as siservicoViaturan from 'siservicoViaturan';
// import { Observable } from 'rxjs';
// import { Location } from '@angular/common';
// import { ActivatedRoute } from '@angular/router';
// import { ServicoViaturaService } from '../servicoViatura.service';
// import { ServicoViatura } from '../servicoViatura';
// import { TripulantesComponent } from 'src/app/components/tripulante/tripulantes/tripulantes.component';
// import { ViaturasComponent } from 'src/app/components/viatura/viatura/viaturas.component';
// import { TripulanteDetailComponent } from 'src/app/components/tripulante/tripulante-detail/tripulante-detail.component';
// import { ViaturaDetailComponent } from 'src/app/components/viatura/viatura-detail/viatura-detail.component';
// import { ViagemDetailComponent } from 'src/app/components/viagem/viagem-detail/viagem-detail.component';
// import { ViagensComponent } from 'src/app/components/viagem/viagem/viagens.component';
// import { BlocosTrabalhoComponent } from 'src/app/components/blocoTrabalho/blocosTrabalho/blocosTrabalho.component';
// import { BlocoTrabalhoDetailComponent } from 'src/app/components/blocoTrabalho/blocoTrabalho-detail/blocoTrabalho-detail.component';
// import { ServicosViaturaComponent } from 'src/app/components/servicoViatura/servicosViatura/servicosViatura.component';
// import { ServicosTripulanteComponent } from 'src/app/components/servicoTripulante/servicosTripulante/servicosTripulante.component';
// import { ServicoTripulanteDetailComponent } from 'src/app/components/servicoTripulante/servicoTripulante-detail/servicoTripulante-detail.component';
// import { ServicoViaturaDetailComponent } from 'src/app/components/servicoViatura/servicoViatura-detail/servicoViatura-detail.component';
// import { SequenciamentoMotoristasComponent } from 'src/app/components/sequenciamentoMotoristas/sequenciamentoMotoristas.component';


// describe('TiposTripulanteDetailComponent', () => {
//   let component: ServicoViaturaDetailComponent;
//   let fixture: ComponentFixture<ServicoViaturaDetailComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         BrowserModule,
//         AppRoutingModule,
//         FormsModule,
//         HttpClientModule,
//         AngularDualListBoxModule
//       ],
//       declarations: [
//         AppComponent,
//         TiposTripulanteComponent,
//         TipoTripulanteDetailComponent,
//         TiposViaturaComponent,
//         TipoViaturaDetailComponent,
//         PercursosComponent,
//         PercursoDetailComponent,
//         ServicosViaturaComponent,
//         ServicoViaturaDetailComponent,
//         LinhasComponent,
//         LinhaDetailComponent,
//         MessagesComponent,
//         DashboardComponent,
//         TipoViaturaSearchComponent,
//         LoginComponent,
//         MapComponent,
//         PageServicoViaturatFoundComponent,
//         DragDirective,
//         ImportFileComponent,
//         HomeComponent,
//         PerfilComponent,
//         SignupComponent,
//         GestaoCaminhosComponent,
//         UserDirective,
//         UserRoleDirective
//       ],
//     })
//     .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(ServicoViaturaDetailComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it(`should array should servicoViaturat be afftected because of subscribe`, () => {
//     const service: ServicoViaturaService = TestBed.get(ServicoViaturaService);
//     const domainId = "testeUnit";
//     const servicoViaturame = "testeUnit";
//     const latitude = 12;
//     const longitude = 15;
//     const servicoViaturameCurto ="teste";
//     const isEstacaoRecolha = false;
//     const isPontoRendicao = true;
//     const tempoViagem = [{ servicoViaturaId: "idTeste", duracao: 25}];
//     var servicoViatura : ServicoViatura = {domainId,servicoViaturame,latitude,longitude,servicoViaturameCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem} as ServicoViatura;


// 	siservicoViaturan.stub(service, "getServicoViatura").returns( Observable.create(observer => {observer.next(servicoViatura);observer.complete()}));

//     siservicoViaturan.stub(service, "updateServicoViatura").returns( Observable.create(observer => {observer.next(servicoViatura);observer.complete()}));

//     var a = new ServicoViaturaDetailComponent(TestBed.get(ActivatedRoute), service, TestBed.get(Location));
    
//     a.getServicoViatura(); 

//     expect(a.servicoViatura).toEqual(servicoViatura); 

//     a.servicoViatura.servicoViaturame = "testeUnit";
//     servicoViatura.servicoViaturame="testeUnit";
//     a.save();

//     expect(a.servicoViatura).toEqual(servicoViatura); 
   
//   });
// });