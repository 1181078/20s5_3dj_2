import { Component } from '@angular/core';
import { SequenciamentoMotoristasService } from 'src/app/services/sequenciamentoMotoristas/sequenciamentoMotoristas.service';

@Component({
  selector: 'my-app',
  templateUrl: './sequenciamentoMotoristas.component.html',
  styleUrls: ['./sequenciamentoMotoristas.component.css']
})
export class SequenciamentoMotoristasComponent {

  constructor(
    private sequenciamentoMotoristasService: SequenciamentoMotoristasService
  ) {
  }
  response: string;


  ngOnInit() {
  }

  redirect(nvd:number, ng: number, np: number, pc: number, pm: number, pp: number, tl: number, mv: number, sv: number) {
    this.sequenciamentoMotoristasService.ag(nvd,ng,np,pc,pm,pp,tl,mv,sv)
      .subscribe(response => { this.response = response});;
  }

  print(cam: string) {
    var sub = cam.substring(1,cam.length-1);
    var a = sub.split(',1,');
    var p = "";
    for(var k = 0; k<a.length; k++){
      p = p + "\n\nGeração " + k + ": " + a[k];
    }
    document.getElementById("cam").textContent = p;
  }

}
 