// import { HttpClientModule } from '@angular/common/http';
// import { TestBed, async, ComponentFixture } from '@angular/core/testing';
// import { FormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
// import { RouterTestingModule } from '@angular/router/testing';
// import { AngularDualListBoxModule } from 'angular-dual-listbox';
// import { AppRoutingModule } from '../../app-routing.module';
// import { AppComponent } from '../../app.component';

// import { NgModule } from '@angular/core';


// import { TiposTripulanteComponent } from '../../tipoTripulante/tiposTripulante/tiposTripulante.component';
// import { TipoTripulanteDetailComponent } from '../../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
// import { ViaturasComponent } from '../../viatura/viaturas/viaturas.component';
// import { ViaturaDetailComponent } from '../../viatura/viatura-detail/viatura-detail.component';
// import { PercursosComponent } from '../../percurso/percursos/percursos.component';
// import { PercursoDetailComponent } from '../../percurso/percurso-detail/percurso-detail.component';
// import { NosComponent } from '../../no/nos/nos.component';
// import { NoDetailComponent } from '../../no/no-detail/no-detail.component';
// import { LinhasComponent } from '../../linha/linhas/linhas.component';
// import { LinhaDetailComponent } from '../../linha/linha-detail/linha-detail.component';
// import { MessagesComponent } from '../../messages/messages.component';
// import { DashboardComponent } from '../../dashboard/dashboard.component';
// import { ViaturaSearchComponent } from '../../viatura/viatura-search/viatura-search.component';
// import { LoginComponent } from '../../login/login.component';
// import { MapComponent } from '../../map/map.component';
// import { DragDirective } from '../../importFile/dragDrop.directive';
// import { PageNotFoundComponent } from '../../pageNotFound/page-not-found.component';
// import { ImportFileComponent } from '../../importFile/importFile.component';
// import { UserDirective } from '../../login/directives/user.directive';
// import { UserRoleDirective } from '../../login/directives/user-role.directive';
// import { AuthService } from '../../login/services/auth.service';
// import { HomeComponent } from '../../home/home.component';
// import { PerfilComponent } from '../../login/perfil/perfil.component';
// import { SignupComponent } from '../../login/signup/signup.component';
// import { GestaoCaminhosComponent } from '../../gestaoCaminhos/gestaoCaminhos.component';
// import { Viatura } from '../viatura';
// import { ViaturaService } from '../viatura.service';
// import * as sinon from 'sinon';
// import { Observable } from 'rxjs';
// import { Location } from '@angular/common';
// import { ActivatedRoute } from '@angular/router';

// describe('ViaturasDetailComponent', () => {
//   let component: ViaturaDetailComponent;
//   let fixture: ComponentFixture<ViaturaDetailComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         BrowserModule,
//         AppRoutingModule,
//         FormsModule,
//         HttpClientModule,
//         AngularDualListBoxModule
//       ],
//       declarations: [
//         AppComponent,
//         TiposTripulanteComponent,
//         TipoTripulanteDetailComponent,
//         ViaturasComponent,
//         ViaturaDetailComponent,
//         PercursosComponent,
//         PercursoDetailComponent,
//         NosComponent,
//         NoDetailComponent,
//         LinhasComponent,
//         LinhaDetailComponent,
//         MessagesComponent,
//         DashboardComponent,
//         ViaturaSearchComponent,
//         LoginComponent,
//         MapComponent,
//         PageNotFoundComponent,
//         DragDirective,
//         ImportFileComponent,
//         HomeComponent,
//         PerfilComponent,
//         SignupComponent,
//         GestaoCaminhosComponent,
//         UserDirective,
//         UserRoleDirective
//       ],
//     })
//     .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(ViaturaDetailComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it(`should array should not be afftected because of subscribe`, () => {
//     const service: ViaturaService = TestBed.get(ViaturaService);

//     var tt: Viatura =  {domainId: "teste2", nome: "teste2", autonomia: 10, custo: 12, velocidadeMedia: 21,
//     combustivel: 14, consumo: 13, emissoes:31} as Viatura;

// 		sinon.stub(service, "getViatura").returns( Observable.create(observer => {observer.next(tt);observer.complete()}));

//     sinon.stub(service, "updateViatura").returns( Observable.create(observer => {observer.next(tt);observer.complete()}));

//     var a = new ViaturaDetailComponent(TestBed.get(ActivatedRoute), service, TestBed.get(Location));
    
//     a.getViatura2("teste2"); 

//     expect(a.viatura).toEqual(tt); 

//     a.viatura.nome = "newTeste2";
//     tt.nome="newTeste2";
//     a.save();

//     expect(a.viatura).toEqual(tt); 
   
//   });
// });
