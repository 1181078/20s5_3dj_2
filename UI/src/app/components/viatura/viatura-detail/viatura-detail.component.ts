import { Component, OnInit, Input } from '@angular/core';
import { Viatura } from '../../../domain/viatura/viatura';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ViaturaService } from '../../../services/viatura/viatura.service';
import { MessageService } from '../../../services/messages/message.service';
import { TipoViaturaService } from '../../../services/tipoViatura/tipoViatura.service';
import { TipoViatura } from '../../../domain/tipoViatura/tipoViatura';

@Component({
  selector: 'app-viatura-detail',
  templateUrl: './viatura-detail.component.html',
  styleUrls: ['./viatura-detail.component.css']
})
export class ViaturaDetailComponent implements OnInit {
  @Input() viatura: Viatura;
  tiposViatura: TipoViatura[]= new Array();

  constructor(
    private route: ActivatedRoute,
    private viaturaService: ViaturaService,
    private location: Location,
    private tipoViaturaService: TipoViaturaService
  ) {}

  ngOnInit(): void {
    this.getViatura();
    this.getTiposViatura();
  }  
  
  getViatura(): void {
    const domainId = this.route.snapshot.paramMap.get("matricula");
    this.viaturaService.getViatura(domainId)
      .subscribe(viatura => this.viatura = viatura);
  }

  getViatura2(domainId: string): void {
    this.viaturaService.getViatura(domainId).subscribe(viatura => this.viatura = viatura);
  }

  getTiposViatura(): void {
    this.tipoViaturaService.getTiposViatura()
    .subscribe(tiposViatura => this.tiposViatura = tiposViatura);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.viaturaService.updateViatura(this.viatura)
      .subscribe(() => this.goBack());
  }

}
