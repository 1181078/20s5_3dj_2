import { Component, OnInit } from '@angular/core';

import { Viatura } from '../../../domain/viatura/viatura';
import { ViaturaService } from '../../../services/viatura/viatura.service';
import { MessageService } from '../../../services/messages/message.service';
import { TipoViatura } from '../../../domain/tipoViatura/tipoViatura';
import { TipoViaturaService } from '../../../services/tipoViatura/tipoViatura.service';

@Component({
  selector: 'app-viaturas',
  templateUrl: './viaturas.component.html',
  styleUrls: ['./viaturas.component.css']
})
export class ViaturasComponent implements OnInit {
  viaturas: Viatura[]= new Array();
  tiposViatura: TipoViatura[]= new Array();

  constructor(private viaturaService: ViaturaService, private tipoViaturaService: TipoViaturaService) { }

  ngOnInit() {
    this.getViaturas();
    this.getTiposViatura();
  }

  getViaturas(): void {
    this.viaturaService.getViaturas()
    .subscribe(viaturas => this.viaturas = viaturas);
  }

  getTiposViatura(): void {
    this.tipoViaturaService.getTiposViatura()
    .subscribe(tiposViatura => this.tiposViatura = tiposViatura);
  }

  add(matricula: string, VIN: string, tipoViatura: string, dataEntradaDia: number, dataEntradaMes: number, dataEntradaAno: number): void {
    if (!matricula) { return; }
    var dataEntrada = new Date(dataEntradaAno, dataEntradaMes, dataEntradaDia);
    this.viaturaService.addViatura({ matricula, VIN,tipoViatura,dataEntrada} as Viatura)
      .subscribe(viatura => {
        this.viaturas.push(viatura);
      });
  }

  delete(viatura: Viatura): void {
    this.viaturas = this.viaturas.filter(h => h !== viatura);
    this.viaturaService.deleteViatura(viatura).subscribe();
  }
}