import { Component, OnInit } from '@angular/core';
import { TipoTripulante } from '../../domain/tipoTripulante/tipoTripulante';
import { TipoTripulanteService } from '../../services/tipoTripulante/tipoTripulante.service';
import { TipoViatura } from '../../domain/tipoViatura/tipoViatura';
import { TipoViaturaService } from '../../services/tipoViatura/tipoViatura.service';
import { Percurso } from '../../domain/percurso/percurso';
import { PercursoService } from '../../services/percurso/percurso.service';
import { Linha } from '../../domain/linha/linha';
import { LinhaService } from '../../services/linha/linha.service';
import { No } from '../../domain/nos/no';
import { NoService } from '../../services/nos/no.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  tiposTripulante: TipoTripulante[] = [];
  tiposViatura: TipoViatura[] = [];
  percursos: Percurso[] = [];
  nos: No[] = [];
  linhas: Linha[] = [];

  constructor(private tipoTripulanteService: TipoTripulanteService, private tipoViaturaService: TipoViaturaService,
    private percursoService: PercursoService, private noService: NoService, private linhaService: LinhaService) { }

  ngOnInit() {
    this.getTiposTripulante();
    this.getTiposViatura();
    this.getPercursos();
    this.getNos();
    this.getLinhas();
  }

  getTiposTripulante(): void {
    this.tipoTripulanteService.getTiposTripulante()
      .subscribe(tiposTripulante => this.tiposTripulante = tiposTripulante.slice(1, 5));
  }

  getTiposViatura(): void {
    this.tipoViaturaService.getTiposViatura()
      .subscribe(tiposViatura => this.tiposViatura = tiposViatura.slice(1, 5));
  }

  getPercursos(): void {
    this.percursoService.getPercursos()
      .subscribe(percursos => this.percursos = percursos.slice(1, 5));
  }

  getNos(): void {
    this.noService.getNos()
      .subscribe(nos => this.nos = nos.slice(1, 5));
  }

  getLinhas(): void {
    this.linhaService.getLinhas()
      .subscribe(linhas => this.linhas = linhas.slice(1, 5));
  }
}