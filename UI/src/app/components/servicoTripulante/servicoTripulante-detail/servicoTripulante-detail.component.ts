import { Component, OnInit, Input } from '@angular/core';
import { ServicoTripulante } from '../../../domain/servicoTripulante/servicoTripulante';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ServicoTripulanteService } from '../../../services/servicoTripulante/servicoTripulante.service';
import { MessageService } from '../../../services/messages/message.service';

@Component({
  selector: 'app-servicoTripulante-detail',
  templateUrl: './servicoTripulante-detail.component.html',
  styleUrls: ['./servicoTripulante-detail.component.css']
})
export class ServicoTripulanteDetailComponent implements OnInit {
  @Input() servicoTripulante: ServicoTripulante;

  constructor(
    private route: ActivatedRoute,
    private servicoTripulanteService: ServicoTripulanteService,
    private location: Location,
  ) {}

  ngOnInit(): void {
    this.getServicoTripulante();
  }
  
  getServicoTripulante(): void {
    const domainId = this.route.snapshot.paramMap.get("domainId");
    this.servicoTripulanteService.getServicoTripulante(domainId)
      .subscribe(servicoTripulante => this.servicoTripulante = servicoTripulante);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.servicoTripulanteService.updateServicoTripulante(this.servicoTripulante)
      .subscribe(() => this.goBack());
  }

}
