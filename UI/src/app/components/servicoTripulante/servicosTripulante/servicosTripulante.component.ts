import {
	Component, OnInit, DoCheck, EventEmitter, Input, IterableDiffers, OnChanges,
	Output, SimpleChange, ChangeDetectorRef, ChangeDetectionStrategy
} from '@angular/core';

import { ServicoTripulante } from '../../../domain/servicoTripulante/servicoTripulante';
import { ServicoTripulanteService } from '../../../services/servicoTripulante/servicoTripulante.service';
import { MessageService } from '../../../services/messages/message.service';
import { DualListComponent } from 'angular-dual-listbox';
import { catchError, map, tap } from 'rxjs/operators';
import { TempoViagemGrupo, TempoViagemGrupoProps } from '../../../../../../MDR/src/domain/no/tempoViagemGrupo';
import { Viagem } from '../../../domain/viagem/viagem';
import { ViagemService } from '../../../services/viagem/viagem.service';
import { PercursoService } from '../../../services/percurso/percurso.service';
import { Percurso } from '../../../domain/percurso/percurso';
import { BlocoTrabalho } from 'src/app/domain/blocoTrabalho/blocoTrabalho';
import { BlocoTrabalhoService } from 'src/app/services/blocoTrabalho/blocoTrabalho.service';


@Component({
	selector: 'app-servicosTripulante',
	templateUrl: './servicosTripulante.component.html',
	styleUrls: ['./servicosTripulante.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush
})

export class ServicosTripulanteComponent implements OnInit {
	tab = 1;
	keepSorted = true;
	key: string;
	display: string;
	filter = false;
	source: BlocoTrabalho[] = [];
	confirmed: BlocoTrabalho[] = [];
	userAdd = '';
	disabled = false;
	sourceLeft = true;
	format: any = DualListComponent.DEFAULT_FORMAT;

	servicosTripulante: ServicoTripulante[] = new Array();
	blocos: BlocoTrabalho[] = new Array();
	confirmedBlocos: BlocoTrabalho[] = new Array();


	constructor(
		private servicoTripulanteService: ServicoTripulanteService,
		private blocoService: BlocoTrabalhoService,
	) {
	}

	ngOnInit() {
		this.getServicosTripulantes();
		this.doReset();
	}

	getServicosTripulantes(): void {
		this.servicoTripulanteService.getServicosTripulante()
			.subscribe(servicosTripulante => { this.servicosTripulante = servicosTripulante, console.log(servicosTripulante), document.getElementById("refr").click() });
	}

	getBlocos(): any {
		this.blocoService.getBlocosTrabalho().subscribe((via) => {
			this.blocos = via, console.log(this.blocos), document.getElementById("refr").click()
		});

	}


	add(domainId: string, nome: string, cor: string, nP: BlocoTrabalho[]): void {

		var blocks = new Array();

		for (var j = 0; j < nP.length; j++) {
			blocks[j] = nP[j].domainId;
		}

		this.servicoTripulanteService.addServicoTripulante({ domainId, nome, cor, blocks } as ServicoTripulante)
			.subscribe(servicoTripulante => {
				this.servicosTripulante.push(servicoTripulante);
			});
	}

	delete(servicoTripulante: ServicoTripulante): void {
		this.servicosTripulante = this.servicosTripulante.filter(h => h !== servicoTripulante);
		this.servicoTripulanteService.deleteServicoTripulante(servicoTripulante).subscribe();
	}

	private useBlocos() {
		this.key = 'domainId';
		this.display = 'domainId';
		this.keepSorted = true;
		this.source = this.blocos;
		this.confirmed = this.confirmedBlocos;
	}

	doReset() {
		this.getBlocos();
		this.confirmedBlocos = [];
		this.useBlocos();
	}


	filterBtn() {
		return (this.filter ? 'Hide Filter' : 'Show Filter');
	}

	doDisable() {
		this.disabled = !this.disabled;
	}

	disableBtn() {
		return (this.disabled ? 'Enable' : 'Disabled');
	}

	swapDirection() {
		this.sourceLeft = !this.sourceLeft;
		this.format.direction = this.sourceLeft ? DualListComponent.LTR : DualListComponent.RTL;
	}

	refresh() {
	}


}