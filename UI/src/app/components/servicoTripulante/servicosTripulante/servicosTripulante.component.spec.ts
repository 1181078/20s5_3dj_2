// import { HttpClientModule } from '@angular/common/http';
// import { TestBed, async, ComponentFixture } from '@angular/core/testing';
// import { FormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
// import { RouterTestingModule } from '@angular/router/testing';
// import { AngularDualListBoxModule } from 'angular-dual-listbox';
// import { AppRoutingModule } from '../../app-routing.module';
// import { AppComponent } from '../../app.component';
// import * as siservicoTripulanten from 'siservicoTripulanten';

// import { NgModule } from '@angular/core';


// import { TiposTripulanteComponent } from '../../tipoTripulante/tiposTripulante/tiposTripulante.component';
// import { TipoTripulanteDetailComponent } from '../../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
// import { TiposTripulanteComponent } from '../../tipoTripulante/tiposTripulante/tiposTripulante.component';
// import { TipoTripulanteDetailComponent } from '../../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
// import { PercursosComponent } from '../../percurso/percursos/percursos.component';
// import { PercursoDetailComponent } from '../../percurso/percurso-detail/percurso-detail.component';
// import { ServicosTripulanteComponent } from '../../servicoTripulante/servicosTripulante/servicosTripulante.component';
// import { ServicoTripulanteDetailComponent } from '../../servicoTripulante/servicoTripulante-detail/servicoTripulante-detail.component';
// import { LinhasComponent } from '../../linha/linhas/linhas.component';
// import { LinhaDetailComponent } from '../../linha/linha-detail/linha-detail.component';
// import { MessagesComponent } from '../../messages/messages.component';
// import { DashboardComponent } from '../../dashboard/dashboard.component';
// import { TipoTripulanteSearchComponent } from '../../tipoTripulante/tipoTripulante-search/tipoTripulante-search.component';
// import { LoginComponent } from '../../login/login.component';
// import { MapComponent } from '../../map/map.component';
// import { DragDirective } from '../../importFile/dragDrop.directive';
// import { PageServicoTripulantetFoundComponent } from '../../pageServicoTripulantetFound/page-servicoTripulantet-found.component';
// import { ImportFileComponent } from '../../importFile/importFile.component';
// import { UserDirective } from '../../login/directives/user.directive';
// import { UserRoleDirective } from '../../login/directives/user-role.directive';
// import { AuthService } from '../../login/services/auth.service';
// import { HomeComponent } from '../../home/home.component';
// import { PerfilComponent } from '../../login/perfil/perfil.component';
// import { SignupComponent } from '../../login/signup/signup.component';
// import { GestaoCaminhosComponent } from '../../gestaoCaminhos/gestaoCaminhos.component';
// import { Observable } from 'rxjs';
// import { ServicoTripulanteService } from '../servicoTripulante.service';
// import { ServicoTripulante } from '../servicoTripulante';
// import { TripulantesComponent } from 'src/app/components/tripulante/tripulantes/tripulantes.component';
// import { ViaturasComponent } from 'src/app/components/viatura/viatura/viaturas.component';
// import { TripulanteDetailComponent } from 'src/app/components/tripulante/tripulante-detail/tripulante-detail.component';
// import { ViaturaDetailComponent } from 'src/app/components/viatura/viatura-detail/viatura-detail.component';
// import { ViagemDetailComponent } from 'src/app/components/viagem/viagem-detail/viagem-detail.component';
// import { ViagensComponent } from 'src/app/components/viagem/viagem/viagens.component';
// import { BlocosTrabalhoComponent } from 'src/app/components/blocoTrabalho/blocosTrabalho/blocosTrabalho.component';
// import { BlocoTrabalhoDetailComponent } from 'src/app/components/blocoTrabalho/blocoTrabalho-detail/blocoTrabalho-detail.component';
// import { ServicosViaturaComponent } from 'src/app/components/servicoViatura/servicosViatura/servicosViatura.component';
// import { ServicosTripulanteComponent } from 'src/app/components/servicoTripulante/servicosTripulante/servicosTripulante.component';
// import { ServicoTripulanteDetailComponent } from 'src/app/components/servicoTripulante/servicoTripulante-detail/servicoTripulante-detail.component';
// import { ServicoViaturaDetailComponent } from 'src/app/components/servicoViatura/servicoViatura-detail/servicoViatura-detail.component';
// import { SequenciamentoMotoristasComponent } from 'src/app/components/sequenciamentoMotoristas/sequenciamentoMotoristas.component';


// describe('ServicosTripulanteComponent', () => {
//   let component: ServicosTripulanteComponent;
//   let fixture: ComponentFixture<ServicosTripulanteComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         BrowserModule,
//         AppRoutingModule,
//         FormsModule,
//         HttpClientModule,
//         AngularDualListBoxModule
//       ],
//       declarations: [
//         AppComponent,
//         TiposTripulanteComponent,
//         TipoTripulanteDetailComponent,
//         TiposTripulanteComponent,
//         TipoTripulanteDetailComponent,
//         PercursosComponent,
//         PercursoDetailComponent,
//         ServicosTripulanteComponent,
//         ServicoTripulanteDetailComponent,
//         LinhasComponent,
//         LinhaDetailComponent,
//         MessagesComponent,
//         DashboardComponent,
//         TipoTripulanteSearchComponent,
//         LoginComponent,
//         MapComponent,
//         PageServicoTripulantetFoundComponent,
//         DragDirective,
//         ImportFileComponent,
//         HomeComponent,
//         PerfilComponent,
//         SignupComponent,
//         GestaoCaminhosComponent,
//         UserDirective,
//         UserRoleDirective
//       ],
//     })
//     .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(ServicosTripulanteComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it(`should start array as 0`, () => {
//     const fixture = TestBed.createComponent(ServicosTripulanteComponent);
//     const app = fixture.componentInstance;
//     expect(app.servicosTripulante.length).toBe(0);
//   });

//   it(`should array should servicoTripulantet be afftected because of subscribe`, () => {
//     const service: ServicoTripulanteService = TestBed.get(ServicoTripulanteService);
//     const domainId = "testeUnit";
//     const servicoTripulanteme = "testeUnit";
//     const latitude = 12;
//     const longitude = 15;
//     const servicoTripulantemeCurto ="teste";
//     const isEstacaoRecolha = false;
//     const isPontoRendicao = true;
//     const tempoViagem = [{ servicoTripulanteId: "idTeste", duracao: 25}];
//     var servicoTripulante : ServicoTripulante = {domainId,servicoTripulanteme,latitude,longitude,servicoTripulantemeCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem} as ServicoTripulante;

// 	  siservicoTripulanten.stub(service, "addServicoTripulante").returns( Observable.create(observer => {observer.next(servicoTripulante);observer.complete()}));

//     siservicoTripulanten.stub(service, "deleteServicoTripulante").returns( Observable.create(observer => {observer.next(servicoTripulante);observer.complete()}));

//     siservicoTripulanten.stub(service, "getServicoTripulante").returns( Observable.create(observer => {observer.next([]);observer.complete()}));

//     var a = new ServicosTripulanteComponent(service);
    
//     var len = a.servicosTripulante.length;
//     console.log(len);
//     a.add(domainId,servicoTripulanteme,latitude,longitude,servicoTripulantemeCurto,isEstacaoRecolha,isPontoRendicao,tempoViagem);    
//     var len2 = len+1;
//     expect(a.servicosTripulante.length).toEqual(len2); 
//     a.delete(servicoTripulante);
//     expect(a.servicosTripulante.length).toEqual(len);
//   });

  

// });
