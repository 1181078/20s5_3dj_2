import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { empty } from 'rxjs';
import { Role } from '../../../domain/login/role';
import { User } from '../../../domain/login/user';
import { AuthService } from '../../../services/login/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  Role = Role;
  users: User[];
  submitted = false;
  model = new User();
  checkTerms = true;

  constructor(private router: Router, private authService: AuthService) { }

  onSubmit() {this.submitted = true;}
  ngOnInit() {document.getElementById("termsAndConditions").style.display = "none" ;
  }

  signup(email: string, password: string, role: string) {
    this.authService.signup(email, password, role);
  }


  print(email: string, password: string){
    if(Object.keys(email).length ===0 ||Object.keys(password).length ===0)
    {document.getElementById("suc").textContent = 'Verifique campos errados ou vazios';}
    else document.getElementById("suc").textContent = 'Registado';
    this.checkTerms = false;
  }

  setTermsChecked(){
    if (this.checkTerms) {
      this.checkTerms = false;
      document.getElementById('login').style.color = "green";

    } else {

      this.checkTerms = true;
      document.getElementById('login').style.color = "black";


    }
  }

  buttonState(){
    console.log(this.checkTerms);
    return this.checkTerms;
  }

  showTermsAndConditions() {
    var x = document.getElementById("termsAndConditions");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }


}

