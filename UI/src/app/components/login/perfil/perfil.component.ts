import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Role } from '../../../domain/login/role';
import { User } from '../../../domain/login/user';
import { AuthService } from '../../../services/login/auth.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})

export class PerfilComponent {
  
  user: {role: string, token: string};
  role = Role;

  constructor(private router: Router, private authService: AuthService) { 

  }

  getCurrentUser(){
    this.user = this.authService.user;
  }

  eliminarConta(){
    this.authService.delete();
    this.router.navigate(['login']);
  }

}


