import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Role } from '../../domain/login/role';
import { User } from '../../domain/login/user';
import { AuthService } from '../../services/login/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  user: {role: string, token: string};
  Role: string;

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

  login(email: string, password: string) {
    this.authService.login(email, password).subscribe(a => {
      localStorage.setItem('user_role', a.role),
      localStorage.setItem('user_token', a.token)
      
    });

    console.log(localStorage.getItem('user_role'));

    
    if(localStorage.getItem('user_role')!=null){
    this.authService.set();
    }
    var a =  localStorage.getItem('user_role');
    var q: string;
    switch (a) {
      case "Admin":
        q= '/dashboard';
        break;
      case "Gestor": 
        q = '/me';
        break;
      case "Cliente":
        q ='/map';
        break;
      default:
        q = '/login';
        break;
    }
    if(q!=null){
    this.router.navigate([q]);
  }

  }

  refresh() {
	}



}

