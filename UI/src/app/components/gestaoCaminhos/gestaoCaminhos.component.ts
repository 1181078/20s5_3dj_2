import { Component } from '@angular/core';
import { No } from '../../domain/nos/no';
import { GestaoCaminhosService } from '../../services/gestaoCaminhos/gestaoCaminhos.service';

@Component({
  selector: 'my-app',
  templateUrl: './gestaoCaminhos.component.html',
  styleUrls: ['./gestaoCaminhos.component.css']
})
export class GestaoCaminhosComponent {

  constructor(
    private gestaoCaminhosService: GestaoCaminhosService
  ) {
  }
  caminho: string;
  custo: string;
  response: string;
  nos: No[] = new Array();


  ngOnInit() {
    this.getNos();
  }

  caminhoCusto(caminho: string, custo: string): void {
    this.caminho = caminho;
    this.custo = custo;
  }

  redirect(algor: string, time: string, orig: string, dest: string) {
    const timeInt = parseInt(time);
    switch (algor) {
      case "A*":
        this.aStar(timeInt, orig, dest);
        break;

        case "Gerador de todas as Soluções":
          this.todas(timeInt, orig, dest);
          break;

        case "Best First":
        this.bestfs(timeInt, orig, dest);
        break;
    }
    
  }

  aStar(time: number, orig: string, dest: string): void {
    this.gestaoCaminhosService.aStar(time, orig, dest)
      .subscribe(response => { this.response = response, this.caminho = this.response.split(';')[0], this.custo = this.response.split(';')[1]});;
  }

  todas(time: number, orig: string, dest: string): void {
    this.gestaoCaminhosService.todas(time, orig, dest)
      .subscribe(response => { this.response = response, this.caminho = this.response.split(';')[1], this.custo = this.response.split(';')[0]});;
  }

  bestfs(time: number, orig: string, dest: string): void {
    this.gestaoCaminhosService.bestF(time, orig, dest)
      .subscribe(response => { this.response = response, this.caminho = this.response.split(';')[0], this.custo = this.response.split(';')[1]});;
  }

  print(cam: string, cust: string) {
    document.getElementById("cam").textContent = cam;
    document.getElementById("cust").textContent = cust;
  }

  getNos(): void {
    this.gestaoCaminhosService.getNos()
      .subscribe(nos => this.nos = nos);
  }
}
