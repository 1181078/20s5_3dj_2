// import { HttpClientModule } from '@angular/common/http';
// import { TestBed, async, ComponentFixture } from '@angular/core/testing';
// import { FormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
// import { RouterTestingModule } from '@angular/router/testing';
// import { AngularDualListBoxModule } from 'angular-dual-listbox';
// import { AppRoutingModule } from '../../app-routing.module';
// import { AppComponent } from '../../app.component';

// import { NgModule } from '@angular/core';


// import { TiposTripulanteComponent } from '../../tipoTripulante/tiposTripulante/tiposTripulante.component';
// import { TipoTripulanteDetailComponent } from '../../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
// import { ViagensComponent } from '../../viagem/viagens/viagens.component';
// import { ViagemDetailComponent } from '../../viagem/viagem-detail/viagem-detail.component';
// import { PercursosComponent } from '../../percurso/percursos/percursos.component';
// import { PercursoDetailComponent } from '../../percurso/percurso-detail/percurso-detail.component';
// import { NosComponent } from '../../no/nos/nos.component';
// import { NoDetailComponent } from '../../no/no-detail/no-detail.component';
// import { LinhasComponent } from '../../linha/linhas/linhas.component';
// import { LinhaDetailComponent } from '../../linha/linha-detail/linha-detail.component';
// import { MessagesComponent } from '../../messages/messages.component';
// import { DashboardComponent } from '../../dashboard/dashboard.component';
// import { ViagemSearchComponent } from '../../viagem/viagem-search/viagem-search.component';
// import { LoginComponent } from '../../login/login.component';
// import { MapComponent } from '../../map/map.component';
// import { DragDirective } from '../../importFile/dragDrop.directive';
// import { PageNotFoundComponent } from '../../pageNotFound/page-not-found.component';
// import { ImportFileComponent } from '../../importFile/importFile.component';
// import { UserDirective } from '../../login/directives/user.directive';
// import { UserRoleDirective } from '../../login/directives/user-role.directive';
// import { AuthService } from '../../login/services/auth.service';
// import { HomeComponent } from '../../home/home.component';
// import { PerfilComponent } from '../../login/perfil/perfil.component';
// import { SignupComponent } from '../../login/signup/signup.component';
// import { GestaoCaminhosComponent } from '../../gestaoCaminhos/gestaoCaminhos.component';
// import { Viagem } from '../viagem';
// import { ViagemService } from '../viagem.service';
// import * as sinon from 'sinon';
// import { Observable } from 'rxjs';
// import { Location } from '@angular/common';
// import { ActivatedRoute } from '@angular/router';

// describe('ViagensDetailComponent', () => {
//   let component: ViagemDetailComponent;
//   let fixture: ComponentFixture<ViagemDetailComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         BrowserModule,
//         AppRoutingModule,
//         FormsModule,
//         HttpClientModule,
//         AngularDualListBoxModule
//       ],
//       declarations: [
//         AppComponent,
//         TiposTripulanteComponent,
//         TipoTripulanteDetailComponent,
//         ViagensComponent,
//         ViagemDetailComponent,
//         PercursosComponent,
//         PercursoDetailComponent,
//         NosComponent,
//         NoDetailComponent,
//         LinhasComponent,
//         LinhaDetailComponent,
//         MessagesComponent,
//         DashboardComponent,
//         ViagemSearchComponent,
//         LoginComponent,
//         MapComponent,
//         PageNotFoundComponent,
//         DragDirective,
//         ImportFileComponent,
//         HomeComponent,
//         PerfilComponent,
//         SignupComponent,
//         GestaoCaminhosComponent,
//         UserDirective,
//         UserRoleDirective
//       ],
//     })
//     .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(ViagemDetailComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it(`should array should not be afftected because of subscribe`, () => {
//     const service: ViagemService = TestBed.get(ViagemService);

//     var tt: Viagem =  {domainId: "teste2", nome: "teste2", autonomia: 10, custo: 12, velocidadeMedia: 21,
//     combustivel: 14, consumo: 13, emissoes:31} as Viagem;

// 		sinon.stub(service, "getViagem").returns( Observable.create(observer => {observer.next(tt);observer.complete()}));

//     sinon.stub(service, "updateViagem").returns( Observable.create(observer => {observer.next(tt);observer.complete()}));

//     var a = new ViagemDetailComponent(TestBed.get(ActivatedRoute), service, TestBed.get(Location));
    
//     a.getViagem2("teste2"); 

//     expect(a.viagem).toEqual(tt); 

//     a.viagem.nome = "newTeste2";
//     tt.nome="newTeste2";
//     a.save();

//     expect(a.viagem).toEqual(tt); 
   
//   });
// });
