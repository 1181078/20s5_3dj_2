import { Component, OnInit, Input } from '@angular/core';
import { Viagem } from '../../../domain/viagem/viagem';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ViagemService } from '../../../services/viagem/viagem.service';
import { MessageService } from '../../../services/messages/message.service';
import { LinhaService } from 'src/app/services/linha/linha.service';
import { Linha } from 'src/app/domain/linha/linha';

@Component({
  selector: 'app-viagem-detail',
  templateUrl: './viagem-detail.component.html',
  styleUrls: ['./viagem-detail.component.css']
})
export class ViagemDetailComponent implements OnInit {
  @Input() viagem: Viagem;
  linhas: Linha[]= new Array();

  constructor(
    private route: ActivatedRoute,
    private viagemService: ViagemService,
    private location: Location,
    private linhaService: LinhaService
  ) {}

  ngOnInit(): void {
    this.getViagem();
    this.getLinhas();
  }  
  
  getViagem(): void {
    const domainId = this.route.snapshot.paramMap.get("domainId");
    this.viagemService.getViagem(domainId)
      .subscribe(viagem => {this.viagem = viagem, 
    console.log(this.viagem);});
  }

  getViagem2(domainId: string): void {
    this.viagemService.getViagem(domainId).subscribe(viagem => this.viagem = viagem);
  }

  getLinhas(): void {
    this.linhaService.getLinhas()
    .subscribe(linhas => this.linhas = linhas);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.viagemService.updateViagem(this.viagem)
      .subscribe(() => this.goBack());
  }

}
