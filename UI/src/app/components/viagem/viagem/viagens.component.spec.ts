// import { HttpClientModule } from '@angular/common/http';
// import { TestBed, async, ComponentFixture } from '@angular/core/testing';
// import { FormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
// import { RouterTestingModule } from '@angular/router/testing';
// import { AngularDualListBoxModule } from 'angular-dual-listbox';
// import { AppRoutingModule } from '../../app-routing.module';
// import { AppComponent } from '../../app.component';
// import * as sinon from 'sinon';

// import { NgModule } from '@angular/core';


// import { TiposTripulanteComponent } from '../../tipoTripulante/tiposTripulante/tiposTripulante.component';
// import { TipoTripulanteDetailComponent } from '../../tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
// import { ViagensComponent } from '../../viagem/viagens/viagens.component';
// import { ViagemDetailComponent } from '../viagem-detail/viagem-detail.component';
// import { PercursosComponent } from '../../percurso/percursos/percursos.component';
// import { PercursoDetailComponent } from '../../percurso/percurso-detail/percurso-detail.component';
// import { NosComponent } from '../../no/nos/nos.component';
// import { NoDetailComponent } from '../../no/no-detail/no-detail.component';
// import { LinhasComponent } from '../../linha/linhas/linhas.component';
// import { LinhaDetailComponent } from '../../linha/linha-detail/linha-detail.component';
// import { MessagesComponent } from '../../messages/messages.component';
// import { DashboardComponent } from '../../dashboard/dashboard.component';
// import { ViagemSearchComponent } from '../../viagem/viagem-search/viagem-search.component';
// import { LoginComponent } from '../../login/login.component';
// import { MapComponent } from '../../map/map.component';
// import { DragDirective } from '../../importFile/dragDrop.directive';
// import { PageNotFoundComponent } from '../../pageNotFound/page-not-found.component';
// import { ImportFileComponent } from '../../importFile/importFile.component';
// import { UserDirective } from '../../login/directives/user.directive';
// import { UserRoleDirective } from '../../login/directives/user-role.directive';
// import { AuthService } from '../../login/services/auth.service';
// import { HomeComponent } from '../../home/home.component';
// import { PerfilComponent } from '../../login/perfil/perfil.component';
// import { SignupComponent } from '../../login/signup/signup.component';
// import { GestaoCaminhosComponent } from '../../gestaoCaminhos/gestaoCaminhos.component';
// import { Viagem } from '../viagem';
// import { ViagemService } from '../viagem.service';
// import { Observable } from 'rxjs';

// describe('ViagensComponent', () => {
//   let component: ViagensComponent;
//   let fixture: ComponentFixture<ViagensComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         BrowserModule,
//         AppRoutingModule,
//         FormsModule,
//         HttpClientModule,
//         AngularDualListBoxModule
//       ],
//       declarations: [
//         AppComponent,
//         TiposTripulanteComponent,
//         TipoTripulanteDetailComponent,
//         ViagensComponent,
//         ViagemDetailComponent,
//         PercursosComponent,
//         PercursoDetailComponent,
//         NosComponent,
//         NoDetailComponent,
//         LinhasComponent,
//         LinhaDetailComponent,
//         MessagesComponent,
//         DashboardComponent,
//         ViagemSearchComponent,
//         LoginComponent,
//         MapComponent,
//         PageNotFoundComponent,
//         DragDirective,
//         ImportFileComponent,
//         HomeComponent,
//         PerfilComponent,
//         SignupComponent,
//         GestaoCaminhosComponent,
//         UserDirective,
//         UserRoleDirective
//       ],
//     })
//     .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(ViagensComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it(`should start array as 0`, () => {
//     const fixture = TestBed.createComponent(ViagensComponent);
//     const app = fixture.componentInstance;
//     expect(app.viagens.length).toBe(0);
//   });

//   it(`should array should not be afftected because of subscribe`, () => {
//     const service: ViagemService = TestBed.get(ViagemService);

//     var tt: Viagem = {domainId: "teste2", nome: "teste2", autonomia: 10, custo: 12, velocidadeMedia: 21,
//     combustivel: 14, consumo: 13, emissoes:31} as Viagem;

// 	sinon.stub(service, "addViagem").returns( Observable.create(observer => {observer.next(tt);observer.complete()}));

//     sinon.stub(service, "deleteViagem").returns( Observable.create(observer => {observer.next(tt);observer.complete()}));

//     sinon.stub(service, "getViagens").returns( Observable.create(observer => {observer.next([]);observer.complete()}));

//     var a = new ViagensComponent(service);
    
//     var len = a.viagens.length;
//     console.log(len);
//     a.add("teste2", "teste2", 10, 12, 21, 14, 13, 31);    
//     var len2 = len+1;
//     expect(a.viagens.length).toEqual(len2); 
//     a.delete({ "domainId": "teste2", "nome": "teste2", "autonomia": 10, "custo": 13, "velocidadeMedia": 13, 
//     "combustivel": 14, "consumo": 16, "emissoes": 13 } as Viagem);
//     a.getViagens();
//     expect(a.viagens.length).toEqual(len);
//   });

  



// });
