import { Component, OnInit } from '@angular/core';

import { Viagem } from '../../../domain/viagem/viagem';
import { ViagemService } from '../../../services/viagem/viagem.service';
import { MessageService } from '../../../services/messages/message.service';
import { LinhaService } from 'src/app/services/linha/linha.service';
import { Linha } from 'src/app/domain/linha/linha';

@Component({
  selector: 'app-viagens',
  templateUrl: './viagens.component.html',
  styleUrls: ['./viagens.component.css']
})
export class ViagensComponent implements OnInit {
  viagens: Viagem[] = new Array();
  linhas: Linha[] = new Array();
  tempos: number[] = new Array();

  constructor(private viagemService: ViagemService, private linhaService: LinhaService) { }

  ngOnInit() {
    this.getViagens();
    this.getLinhas();    
  }

  getViagens(): void {
    this.viagemService.getViagens()
      .subscribe(viagens => this.viagens = viagens);
  }

  getLinhas(): void {
    this.linhaService.getLinhas()
      .subscribe(tiposViagem => {this.linhas = tiposViagem, 
        document.getElementById('try').click();});

  }

  add(domainId: string, vazio: boolean, lineId: string, pathId: string, gerado: boolean,
     tempos: number[]): void {
    if (!domainId) { return; }
    this.viagemService.addViagem({ domainId,vazio, lineId, pathId,gerado,tempos } as Viagem)
      .subscribe(viagem => {
        this.viagens.push(viagem);
      });
    this.tempos = [];
      
  }

  delete(viagem: Viagem): void {
    this.viagens = this.viagens.filter(h => h !== viagem);
    this.viagemService.deleteViagem(viagem).subscribe();
  }

  prepare(linhas: Linha[]) {
    const paths = document.querySelector('[id=pathID]');

    document.querySelector('[id=lineID]').addEventListener('change', function (e) {

      let options = "";
      var abc = this.options[this.selectedIndex].text;

      for (var i = 0; i < linhas.length; i++) {
        if (linhas[i].nome === abc) {
          for (var obj of linhas[i].percursoLinha) {
            options += '<option value="' + obj.percurso + '">' + obj.percurso + '</option>';
          }
          paths.innerHTML = options;
        }
      }
    

    });
  }

  addTempo(tempo: number){
    this.tempos.push(tempo);
  }
  
  deleteTempo(){
    this.tempos.pop();
  }
}