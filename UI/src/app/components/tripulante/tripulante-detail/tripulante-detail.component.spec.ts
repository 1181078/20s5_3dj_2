// import { HttpClientModule } from '@angular/common/http';
// import { TestBed, async, ComponentFixture } from '@angular/core/testing';
// import { FormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
// import { RouterTestingModule } from '@angular/router/testing';
// import { AngularDualListBoxModule } from 'angular-dual-listbox';
// import { AppRoutingModule } from '../../app-routing.module';
// import { AppComponent } from '../../app.component';

// import { NgModule } from '@angular/core';


// import { TripulantesComponent } from '../../tripulante/tripulantes/tripulantes.component';
// import { TripulanteDetailComponent } from './tripulante-detail.component';
// import { TiposViaturaComponent } from '../../tipoViatura/tiposViatura/tiposViatura.component';
// import { TipoViaturaDetailComponent } from '../../tipoViatura/tipoViatura-detail/tipoViatura-detail.component';
// import { PercursosComponent } from '../../percurso/percursos/percursos.component';
// import { PercursoDetailComponent } from '../../percurso/percurso-detail/percurso-detail.component';
// import { NosComponent } from '../../no/nos/nos.component';
// import { NoDetailComponent } from '../../no/no-detail/no-detail.component';
// import { LinhasComponent } from '../../linha/linhas/linhas.component';
// import { LinhaDetailComponent } from '../../linha/linha-detail/linha-detail.component';
// import { MessagesComponent } from '../../messages/messages.component';
// import { DashboardComponent } from '../../dashboard/dashboard.component';
// import { TipoViaturaSearchComponent } from '../../tipoViatura/tipoViatura-search/tipoViatura-search.component';
// import { LoginComponent } from '../../login/login.component';
// import { MapComponent } from '../../map/map.component';
// import { DragDirective } from '../../importFile/dragDrop.directive';
// import { PageNotFoundComponent } from '../../pageNotFound/page-not-found.component';
// import { ImportFileComponent } from '../../importFile/importFile.component';
// import { UserDirective } from '../../login/directives/user.directive';
// import { UserRoleDirective } from '../../login/directives/user-role.directive';
// import { AuthService } from '../../login/services/auth.service';
// import { HomeComponent } from '../../home/home.component';
// import { PerfilComponent } from '../../login/perfil/perfil.component';
// import { SignupComponent } from '../../login/signup/signup.component';
// import { GestaoCaminhosComponent } from '../../gestaoCaminhos/gestaoCaminhos.component';
// import { Tripulante } from '../tripulante';
// import { TripulanteService } from '../tripulante.service';
// import * as sinon from 'sinon';
// import { Observable } from 'rxjs';
// import { Location } from '@angular/common';
// import { ActivatedRoute } from '@angular/router';

// describe('TripulantesDetailComponent', () => {
//   let component: TripulanteDetailComponent;
//   let fixture: ComponentFixture<TripulanteDetailComponent>;

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         BrowserModule,
//         AppRoutingModule,
//         FormsModule,
//         HttpClientModule,
//         AngularDualListBoxModule
//       ],
//       declarations: [
//         AppComponent,
//         TripulantesComponent,
//         TripulanteDetailComponent,
//         TiposViaturaComponent,
//         TipoViaturaDetailComponent,
//         PercursosComponent,
//         PercursoDetailComponent,
//         NosComponent,
//         NoDetailComponent,
//         LinhasComponent,
//         LinhaDetailComponent,
//         MessagesComponent,
//         DashboardComponent,
//         TipoViaturaSearchComponent,
//         LoginComponent,
//         MapComponent,
//         PageNotFoundComponent,
//         DragDirective,
//         ImportFileComponent,
//         HomeComponent,
//         PerfilComponent,
//         SignupComponent,
//         GestaoCaminhosComponent,
//         UserDirective,
//         UserRoleDirective
//       ],
//     })
//     .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(TripulanteDetailComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   it(`should array should not be afftected because of subscribe`, () => {
//     const service: TripulanteService = TestBed.get(TripulanteService);

//     var tt: Tripulante = {domainId: "teste2", desc: "teste2"} as Tripulante;

// 		sinon.stub(service, "getTripulante").returns( Observable.create(observer => {observer.next(tt);observer.complete()}));

//     sinon.stub(service, "updateTripulante").returns( Observable.create(observer => {observer.next(tt);observer.complete()}));

//     var a = new TripulanteDetailComponent(TestBed.get(ActivatedRoute), service, TestBed.get(Location));
    
//     a.getTripulante2("teste2"); 

//     expect(a.tripulante).toEqual(tt); 

//     a.tripulante.desc = "newTeste2";
//     tt.desc="newTeste2";
//     a.save();

//     expect(a.tripulante).toEqual(tt); 
   
//   });
// });