import { Component, OnInit, Input } from '@angular/core';
import { Tripulante } from '../../../domain/tripulante/tripulante';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { TripulanteService } from '../../../services/tripulante/tripulante.service';
import { MessageService } from '../../../services/messages/message.service';
import { TipoTripulanteService } from 'src/app/services/tipoTripulante/tipoTripulante.service';
import { TipoTripulante } from 'src/app/domain/tipoTripulante/tipoTripulante';

@Component({
  selector: 'app-tripulante-detail',
  templateUrl: './tripulante-detail.component.html',
  styleUrls: ['./tripulante-detail.component.css']
})
export class TripulanteDetailComponent implements OnInit {
  @Input() tripulante: Tripulante;
  tiposTripulante: TipoTripulante[]= new Array();

  constructor(
    private route: ActivatedRoute,
    private tripulanteService: TripulanteService,
    private location: Location,private tipoTripulanteService: TipoTripulanteService
  ) {}

  ngOnInit(): void {
    this.getTripulante();
    this.getTiposTripulante();
  }
  
  getTripulante(): void {
    const domainId = this.route.snapshot.paramMap.get("numMecanografico");
    this.tripulanteService.getTripulante(Number.parseInt(domainId)).subscribe(tripulante => {this.tripulante = tripulante, console.log(this.tripulante)});
  }

  getTripulante2(domainId: number): void {
    this.tripulanteService.getTripulante(domainId).subscribe(tripulante => this.tripulante = tripulante);
  }

  getTiposTripulante(): void {
    this.tipoTripulanteService.getTiposTripulante()
    .subscribe(tiposTripulante => this.tiposTripulante = tiposTripulante);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.tripulanteService.updateTripulante(this.tripulante)
      .subscribe(() => this.goBack());
  }

}
