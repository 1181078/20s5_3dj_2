import { Component, OnInit } from '@angular/core';

import { Tripulante } from '../../../domain/tripulante/tripulante';
import { TripulanteService } from '../../../services/tripulante/tripulante.service';
import { MessageService } from '../../../services/messages/message.service';
import { TipoTripulante } from 'src/app/domain/tipoTripulante/tipoTripulante';
import { TipoTripulanteService } from 'src/app/services/tipoTripulante/tipoTripulante.service';

@Component({
  selector: 'app-tripulantes',
  templateUrl: './tripulantes.component.html',
  styleUrls: ['./tripulantes.component.css']
})
export class TripulantesComponent implements OnInit {
  tripulantes: Tripulante[]= new Array();
  tiposTripulante: TipoTripulante[]= new Array();

  constructor(private tripulanteService: TripulanteService, private tipoTripulanteService: TipoTripulanteService) { }

  ngOnInit() {
    this.getTripulantes();
    this.getTiposTripulante();
  }

  getTripulantes(): void {
    this.tripulanteService.getTripulantes()
    .subscribe(tripulantes => this.tripulantes = tripulantes);
  }

  getTiposTripulante(): void {
    this.tipoTripulanteService.getTiposTripulante()
    .subscribe(tiposTripulante => this.tiposTripulante = tiposTripulante);
  }

  add(numMecanografico: number, nome: string, dataNascimentoDia: number, dataNascimentoMes: number, dataNascimentoAno: number,  
    numCartaoCidadao: number, NIF: number, tipoTripulante: string, dataEntradaDia: number, dataEntradaMes: number, dataEntradaAno: number, 
    dataSaidaDia:number, dataSaidaMes: number , dataSaidaAno: number): void {
      
    if (!numMecanografico || !nome) { return; }
    var dataNascimento = new Date(dataNascimentoAno, dataNascimentoMes, dataNascimentoDia);
    var dataEntrada = new Date(dataEntradaAno, dataEntradaMes, dataEntradaDia);
    var dataSaida = new Date(dataSaidaAno, dataSaidaMes, dataSaidaDia);
    this.tripulanteService.addTripulante({ numMecanografico, nome, dataNascimento , numCartaoCidadao, NIF, tipoTripulante, dataEntrada,dataSaida } as Tripulante)
      .subscribe(tripulante => {
        this.tripulantes.push(tripulante);
      });
  }

  delete(tripulante: Tripulante): void {
    this.tripulantes = this.tripulantes.filter(h => h !== tripulante);
    this.tripulanteService.deleteTripulante(tripulante).subscribe();
  }
}