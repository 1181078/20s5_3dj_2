import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TiposTripulanteComponent } from './components/tipoTripulante/tiposTripulante/tiposTripulante.component';
import { TipoTripulanteDetailComponent } from './components/tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
import { TiposViaturaComponent } from './components/tipoViatura/tiposViatura/tiposViatura.component';
import { TipoViaturaDetailComponent } from './components/tipoViatura/tipoViatura-detail/tipoViatura-detail.component';
import { PercursosComponent } from './components/percurso/percursos/percursos.component';
import { PercursoDetailComponent } from './components/percurso/percurso-detail/percurso-detail.component';
import { NosComponent } from './components/no/nos/nos.component';
import { NoDetailComponent } from './components/no/no-detail/no-detail.component';
import { LinhasComponent } from './components/linha/linhas/linhas.component';
import { LinhaDetailComponent } from './components/linha/linha-detail/linha-detail.component';
import { MessagesComponent } from './components/messages/messages.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { TipoViaturaSearchComponent } from './components/tipoViatura/tipoViatura-search/tipoViatura-search.component';
import { LoginComponent } from './components/login/login.component';
import { MapComponent } from './components/map/map.component';
import {AngularDualListBoxModule} from 'angular-dual-listbox'
import { DragDirective } from './components/importFile/dragDrop.directive';
import { PageNotFoundComponent } from './components/pageNotFound/page-not-found.component';
import { ImportFileComponent } from './components/importFile/importFile.component';
import { UserDirective } from './components/login/directives/user.directive';
import { UserRoleDirective } from './components/login/directives/user-role.directive';
import { AuthService } from './services/login/auth.service';
import { HomeComponent } from './components/home/home.component';
import { PerfilComponent } from './components/login/perfil/perfil.component';
import { SignupComponent } from './components/login/signup/signup.component';
import { GestaoCaminhosComponent } from './components/gestaoCaminhos/gestaoCaminhos.component';
import { TripulanteDetailComponent } from './components/tripulante/tripulante-detail/tripulante-detail.component';
import { TripulantesComponent } from './components/tripulante/tripulantes/tripulantes.component';
import { ViaturasComponent } from './components/viatura/viatura/viaturas.component';
import { ViaturaDetailComponent } from './components/viatura/viatura-detail/viatura-detail.component';
import { ViagensComponent } from './components/viagem/viagem/viagens.component';
import { ViagemDetailComponent } from './components/viagem/viagem-detail/viagem-detail.component';
import { BlocosTrabalhoComponent } from './components/blocoTrabalho/blocosTrabalho/blocosTrabalho.component';
import { BlocoTrabalhoDetailComponent } from './components/blocoTrabalho/blocoTrabalho-detail/blocoTrabalho-detail.component';
import { ServicosViaturaComponent } from './components/servicoViatura/servicosViatura/servicosViatura.component';
import { ServicoViaturaDetailComponent } from './components/servicoViatura/servicoViatura-detail/servicoViatura-detail.component';
import { ServicosTripulanteComponent } from './components/servicoTripulante/servicosTripulante/servicosTripulante.component';
import { ServicoTripulanteDetailComponent } from './components/servicoTripulante/servicoTripulante-detail/servicoTripulante-detail.component';
import { SequenciamentoMotoristasComponent } from './components/sequenciamentoMotoristas/sequenciamentoMotoristas.component';

@NgModule({
  declarations: [
    AppComponent,
    TiposTripulanteComponent,
    TipoTripulanteDetailComponent,
    TiposViaturaComponent,
    TipoViaturaDetailComponent,
    PercursosComponent,
    PercursoDetailComponent,
    NosComponent,
    NoDetailComponent,
    LinhasComponent,
    LinhaDetailComponent,
    MessagesComponent,
    DashboardComponent,
    TipoViaturaSearchComponent,
    LoginComponent,
    MapComponent,
    PageNotFoundComponent,
    DragDirective,
    ImportFileComponent,
    HomeComponent,
    PerfilComponent,
    SignupComponent,
    GestaoCaminhosComponent,
    UserDirective,
    UserRoleDirective,
    TripulantesComponent,
    TripulanteDetailComponent,
    ViaturasComponent,
    ViaturaDetailComponent,
    ViagensComponent,
    ViagemDetailComponent,
    BlocosTrabalhoComponent,
    BlocoTrabalhoDetailComponent,
    ServicosViaturaComponent,
    ServicoViaturaDetailComponent,
    ServicosTripulanteComponent,
    ServicoTripulanteDetailComponent,
    SequenciamentoMotoristasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    AngularDualListBoxModule  
  ],
  exports: [
    UserDirective,
    UserRoleDirective
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
