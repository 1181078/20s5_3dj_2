import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TiposTripulanteComponent } from './components/tipoTripulante/tiposTripulante/tiposTripulante.component';
import { TiposViaturaComponent } from './components/tipoViatura/tiposViatura/tiposViatura.component';
import { PercursosComponent } from './components/percurso/percursos/percursos.component';
import { NosComponent } from './components/no/nos/nos.component';
import { LinhasComponent } from './components/linha/linhas/linhas.component';
import { ImportFileComponent } from './components/importFile/importFile.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TipoTripulanteDetailComponent } from './components/tipoTripulante/tipoTripulante-detail/tipoTripulante-detail.component';
import { TipoViaturaDetailComponent } from './components/tipoViatura/tipoViatura-detail/tipoViatura-detail.component';
import { PercursoDetailComponent } from './components/percurso/percurso-detail/percurso-detail.component';
import { NoDetailComponent } from './components/no/no-detail/no-detail.component';
import { LinhaDetailComponent } from './components/linha/linha-detail/linha-detail.component';
import { LoginComponent } from './components/login/login.component';
import { MapComponent } from './components/map/map.component';
import { PageNotFoundComponent } from './components/pageNotFound/page-not-found.component';
import { AuthGuard } from './app-routing-guard';
import { Role } from './domain/login/role';
import { HomeComponent } from './components/home/home.component';
import { PerfilComponent } from './components/login/perfil/perfil.component';
import { AuthService } from './services/login/auth.service';
import { SignupComponent } from './components/login/signup/signup.component';
import { GestaoCaminhosComponent } from './components/gestaoCaminhos/gestaoCaminhos.component';
import { TripulantesComponent } from './components/tripulante/tripulantes/tripulantes.component';
import { TripulanteDetailComponent } from './components/tripulante/tripulante-detail/tripulante-detail.component';
import { ViaturaDetailComponent } from './components/viatura/viatura-detail/viatura-detail.component';
import { ViaturasComponent } from './components/viatura/viatura/viaturas.component';
import { ViagensComponent } from './components/viagem/viagem/viagens.component';
import { ViagemDetailComponent } from './components/viagem/viagem-detail/viagem-detail.component';
import { BlocosTrabalhoComponent } from './components/blocoTrabalho/blocosTrabalho/blocosTrabalho.component';
import { BlocoTrabalhoDetailComponent } from './components/blocoTrabalho/blocoTrabalho-detail/blocoTrabalho-detail.component';
import { ServicosViaturaComponent } from './components/servicoViatura/servicosViatura/servicosViatura.component';
import { ServicoViaturaDetailComponent } from './components/servicoViatura/servicoViatura-detail/servicoViatura-detail.component';
import { ServicosTripulanteComponent } from './components/servicoTripulante/servicosTripulante/servicosTripulante.component';
import { ServicoTripulanteDetailComponent } from './components/servicoTripulante/servicoTripulante-detail/servicoTripulante-detail.component';
import { SequenciamentoMotoristasComponent } from './components/sequenciamentoMotoristas/sequenciamentoMotoristas.component';
const routes: Routes = [
  {
    path: 'tiposTripulante', component: TiposTripulanteComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'tripulantes', component: TripulantesComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'tiposViatura', component: TiposViaturaComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'viaturas', component: ViaturasComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'viagens', component: ViagensComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  
  {
    path: 'blocosTrabalho', component: BlocosTrabalhoComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'percursos', component: PercursosComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'nos', component: NosComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'linhas', component: LinhasComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'servicosViatura', component: ServicosViaturaComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'servicosTripulante', component: ServicosTripulanteComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'map', component: MapComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
        Role.Cliente,
      ]
    }
  },
  {
    path: 'dashboard', component: DashboardComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'importFile', component: ImportFileComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'tiposTripulante/:domainId', component: TipoTripulanteDetailComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'tripulantes/:numMecanografico', component: TripulanteDetailComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'tiposViatura/:domainId', component: TipoViaturaDetailComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'servicosViatura/:domainId', component: ServicoViaturaDetailComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'servicosTripulante/:domainId', component: ServicoTripulanteDetailComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'viaturas/:matricula', component: ViaturaDetailComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'viagens/:domainId', component: ViagemDetailComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'blocosTrabalho/:domainId', component: BlocoTrabalhoDetailComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'percursos/:domainId', component: PercursoDetailComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'nos/:domainId', component: NoDetailComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },
  {
    path: 'linhas/:domainId', component: LinhaDetailComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    data: {
      roles: [
        Role.Admin,
      ]
    }
  },

  {
    path: 'me',
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    component: PerfilComponent
  },

  {
    path: 'gestaoCaminhos',
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    component: GestaoCaminhosComponent
  },
  {
    path: 'sequenciamentoMotoristas',
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    component: SequenciamentoMotoristasComponent
  },

  {
    path: 'home',
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    component: HomeComponent
  },

  { path: '', redirectTo: '/login', pathMatch: 'full' },
  
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },

  { path: '**', component: PageNotFoundComponent }, 
  
  {
    path: '',
    component: HomeComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    AuthGuard,
    AuthService
  ]
})

export class AppRoutingModule { }