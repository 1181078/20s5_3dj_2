:-dynamic driverduty/3.
:-dynamic agenda/2.
:-dynamic disponibilidade/3.
:-dynamic balanceador/3.

balanceador(276, [6,2], []).
balanceador(5188, [4,4], []).
balanceador(16690, [6,2], []).
balanceador(18107, [8], []).

%-------------------------

afet(NN):-
	write('Vehicle Duty: '),read(VD), 			
	(retract(duty(_));true), asserta(duty(VD)),
	write('Numero de novas Geracoes: '),read(NG), 			
	(retract(geracoes(_));true), asserta(geracoes(NG)),
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100, 
	(retract(prob_cruzamento(_));true), 	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100, 
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)),
	write('Percentagem de passagem para a próxima geração (20% ou 30%):'), read(P3),
	PP is P3/100, 
	(retract(perc_passagem(_));true), asserta(perc_passagem(PP)),
	write('Tempo limite de execução:'), read(TT),	
	(retract(tLim(_));true), asserta(tLim(TT)),
	get_time(TI), 	
	(retract(tStart(_));true), asserta(tStart(TI)),	
	write('Avaliação mínima para conclusão:'), read(AMIN),	
	(retract(minVal(_));true), asserta(minVal(AMIN)),
	write('Número de gerações idênticas para estabilização:'), read(STABVAL),	
	(retract(stabil(_));true), asserta(stabil(STABVAL)),
	geraDirect(NN).

afetAll:-
	write('Numero de novas Geracoes: '),read(NG), 			
	(retract(geracoes(_));true), asserta(geracoes(NG)),
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100, 
	(retract(prob_cruzamento(_));true), 	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100, 
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)),
	write('Percentagem de passagem para a próxima geração (20% ou 30%):'), read(P3),
	PP is P3/100, 
	(retract(perc_passagem(_));true), asserta(perc_passagem(PP)),
	write('Tempo limite de execução:'), read(TT),	
	(retract(tLim(_));true), asserta(tLim(TT)),
	get_time(TI), 	
	(retract(tStart(_));true), asserta(tStart(TI)),	
	write('Avaliação mínima para conclusão:'), read(AMIN),	
	(retract(minVal(_));true), asserta(minVal(AMIN)),
	write('Número de gerações idênticas para estabilização:'), read(STABVAL),	
	(retract(stabil(_));true), asserta(stabil(STABVAL)).
	
	
start:- 
	(retractall(driverduty(_,_,_));true),
	afet(NN),
	write("Lista de Gerações:"),write(NN),nl,
	reverse(NN,NN1),
	duty(VD),
	vehicleduty(VD, LL),
	getFirstValid(NN1, LL, N),
	write("Sequenciamento Usado:"),write(N),nl,
	remove_duplicates(N, LM),
	updateDriverDuties(LM,N,LL),
	findall((Mot, LB), driverduty(Mot, LB, _), MLB),
	updateDisponibilidades(MLB).

startAll:- 
	(retractall(driverduty(_,_,_));true),
	afetAll,
	findall(V,vehicleduty(V,_), VV),
	startAll(VV).
	
startAll([]):-!.

startAll([V|V1]):-
	(retract(duty(_));true), asserta(duty(V)),
	geraDirect(NN),
	nl,write("Vehicle Duty "),write(V),nl,
	write("Lista de Gerações:"),write(NN),nl,
	reverse(NN,NN1),
	vehicleduty(V, LL),
	getFirstValid(NN1, LL, N),
	write("Sequenciamento Usado:"),write(N),nl,
	remove_duplicates(N, LM),
	updateDriverDuties(LM,N,LL),
	startAll(V1).
	
	
getFirstValid([], _, []). 

getFirstValid([X|[]], LL, N):- getFirstValidGen(X, LL, N).

getFirstValid([X,1|Y], LL, N):- getFirstValidGen(X, LL, N).

getFirstValid([X,1|Y], LL, N):- getFirstValid(Y, LL, N).


getFirstValidGen([], _, N):- false.

getFirstValidGen([X*V|Y], LL, X):- 
		validateSeq(X, LL), 
		duty(VD), 
		remove_duplicates(X, LM),
		validateSeq2(LM, X, VD),
		updateBalanceamento(LM, X, VD),!. 

getFirstValidGen([X*V|Y], LL, N):- getFirstValidGen(Y,LL,N). 


validateSeq([], []):- !, true.

validateSeq([A|B], [C|D]):- 
		workblock(C,_,BS,BE),
		findall((A,S,E),(disponibilidade(A,S,E), S=<BS, BE=<E),L),
		length(L, Len),
		Len>0,
		validateSeq(B,D).

validateSeq2([], _, _):- !, true.

validateSeq2([A|B], X, VD):- 
		balanceador(A, S, T),
		length(S,NS),
		length(T,NT),
		NS>=NT,
		getBalanceamentoVals(VD,S,T,RS,RT),
		count(X, A, Count),
		RF = RT + Count,
		RS >= RF,
		validateSeq2(B,X,VD).

validateSeq2([A|B], X, VD):- 
		balanceador(A, S, T),
		length(S,NS),
		length(T,NT),
		NS>NT,
		getNextBalanceamento(S,T,RS),
		count(X, A, Count),
		RS >= Count,
		validateSeq2(B,X,VD).

getBalanceamentoVals(VD,[],[],_,_):- !, false.

getBalanceamentoVals(VD,[RS|S2],[(VD,RT)|T2],RS,RT):- !.

getBalanceamentoVals(VD,[S1|S2],[T1|T2],RS,RT):- getBalanceamentoVals(VD,S2,T2,RS,RT).

getNextBalanceamento([R|T],[],R):- !.

getNextBalanceamento([S1|S2],[T1|T2],RS):- getNextBalanceamento(S2,T2,RS).
		
updateBalanceamento([], _, _):- !.

updateBalanceamento([A|B], X, VD):-
		balanceador(A, S, T),
		count(X, A, Count),
		scu(T, Count, VD, T1),
		retract(balanceador(A, S, T)),
		assert(balanceador(A, S, T1)),
		updateBalanceamento(B,X,VD).	
		
scu([], Count, VD, [(VD,Count)]).
	
scu([(VD,TA)|TB], Count, VD, [(VD,TC)|TB]):- TC is TA + Count.

scu([TA|TB], Count, VD, [TA|TB1]):- scu(TB,Count,VD,TB1).
		
count([],X,0).
count([X|T],X,Y):- count(T,X,Z), Y is 1+Z.
count([X1|T],X,Z):- X1\=X,count(T,X,Z).





updateDriverDuties([],N,LL).

updateDriverDuties([X|Y],N,LL):-
	\+ driverduty(X,_,_), 
	setupDriverDuty(X,N,LL,DD),
	findall(W,(order_by([asc(S)],workblock(W,_,S,E)), member(W,DD)),DD1),
	getAgendas2(DD1),
	getDDPenalty(V),
	correctDD(DD1, V, N, DD2, N1, NY),
	append(Y, NY, NNY),
	remove_duplicates(NNY, Y1),
	getAgendas2(DD2),
	getDDPenalty(V2),
	sumV(V,TV),
	sumV(V2,TV2),
	((TV2<TV, (retract(driverduty(X,_,_));true), assert(driverduty(X,DD2,V2)), YFinal = Y1, NFinal = N1)
	;
	((retract(driverduty(X,_,_));true), assert(driverduty(X,DD1,V)), YFinal = Y, NFinal = N)),
	updateDriverDuties(YFinal,NFinal,LL).


updateDriverDuties([X|Y],N,LL):-
	driverduty(X,DDA,_),
	setupDriverDuty(X,N,LL,DD),
	append(DDA,DD,DDF),
	findall(W,(order_by([asc(S)],workblock(W,_,S,E)), member(W,DDF)),DDFF),
	remove_duplicates(DDFF, DD1),
	getAgendas2(DD1),
	getDDPenalty(V),
	correctDD(DD1, V, N, DD2, N1, NY),
	append(Y, NY, NNY),
	remove_duplicates(NNY, Y1),
	getAgendas2(DD2),
	getDDPenalty(V2),
	sumV(V,TV),
	sumV(V2,TV2)	,
	((TV2<TV, (retract(driverduty(X,_,_));true), assert(driverduty(X,DD2,V2)), YFinal = Y1, NFinal = N1)
	;
	((retract(driverduty(X,_,_));true), assert(driverduty(X,DD1,V)), YFinal = Y, NFinal = N)),
	updateDriverDuties(YFinal,NFinal,LL).

	
	
remove_duplicates([],[]).

remove_duplicates([H | T], List) :-    
     member(H, T),
     remove_duplicates( T, List).

remove_duplicates([H | T], [H|T1]) :- 
      \+member(H, T),
      remove_duplicates( T, T1).
      
sumV([],V):- V is 0.

sumV([X|Y],V):- sumV(Y,V1), V is V1 + X.
            

setupDriverDuty(X,[],[],[]):-!. 

setupDriverDuty(M,[M|N],[O|P],[O|ResDD]):- setupDriverDuty(M,N,P,ResDD).

setupDriverDuty(X,[M|N],[O|P],DD):- setupDriverDuty(X,N,P,DD).




getDDPenalty(V):- 
	findall((S,E),agenda(S,E),Ag1),
	bsortA(Ag1,Ag),
	newavalia1(Ag, 0, V1), 
	newavalia2(Ag, 0, V2), 
	newavalia3(Ag, V3), 
	newavalia4(Ag, 0, V4),
	V = [V1,V2,V3,V4].
	
	
newavalia1([], 1 , V):- V is  8*3600.	

newavalia1([], T , 0).

newavalia1([(B,C)|D], T, V):-	
	B < 39600, C > 54000, 
	newavalia1([], 1, V).


newavalia1([(B,C)|D], T, V):-	
	C < 39600, 
	newavalia1(D, T, V).

newavalia1([(B,C)|D], T, V):-
	C > 54000, 
	newavalia1(D, T, V).

newavalia1([(B,C)|D], T, V):-
	C > 39600, C < 54000,
	newavalia11(D, C, T, V).

newavalia11([], U, T, V):- newavalia1([], T, V).

newavalia11([(B,C)|[]], U, T, V):- newavalia1([], T, V).

newavalia11([(B,C)|D], U, T, V):-
	U1 is B-U,
	U1 < 3600,
	((T==0, T1=1);(T1=T)),
	newavalia1([(B,C)|D], T1, V).

newavalia11([(B,C)|D], U, T, V):-
	T1 is T + 1,
	newavalia1([(B,C)|D], T1, V).





newavalia2([], 1 , V):- V is  8*3600.	

newavalia2([], T , 0).

newavalia2([(B,C)|D], T, V):-	
	B < 64800, C > 79200, 
	newavalia2([], 1, V).

newavalia2([(B,C)|D], T, V):-	
	C < 64800, 
	newavalia2(D, T, V).

newavalia2([(B,C)|D], T, V):-
	C > 79200, 
	newavalia2(D, T, V).

newavalia2([(B,C)|D], T, V):-
	C > 64800, C < 79200,
	newavalia22(D, C, T, V).
	
newavalia22([], U, T, V):- newavalia2([], T, V).

newavalia22([(B,C)|[]], U, T, V):- newavalia2([], T, V).

newavalia22([(B,C)|D], U, T, V):-
	U1 is B-U,
	U1 < 3600,
	newavalia2([(B,C)|D], T, V).

newavalia22([(B,C)|D], U, T, V):-
	T1 is T + 1,
	newavalia2([(B,C)|D], T1, V).





newavalia3([], 0):- !.

newavalia3([(B,C)|D], V):-
	Temp is C-B, 
	Temp > 14400, !, 
	NewTemp is Temp - 14400,
	V1 is NewTemp * 10,
	newavalia35(D, C, V2),
	newavalia3(D, V3),
	V is V1 + V2 + V3.
	
newavalia3([(B,C)|D], V):-	
	newavalia3(D,V).
	
newavalia35([], Z, 0):- !.

newavalia35([(B,C)|D], Z, V):- 	
	Temp is Z-C,
	Temp <3600,
	V is 3600*10.

newavalia35([(B,C)|D], Z, 0). 	
	
	

newavalia4([], T, V):- T<28800, V is 0.

newavalia4([], T, V):- !,Temp is T-28800, V is Temp*10.

newavalia4([(B,C)|D], T, V):-
	Temp is C-B + T, 
	newavalia4(D, Temp, V).
	
	
setAgendas2([]):- !.

setAgendas2([Block, Block2|ResB]):- 
	workblock(Block,_,_,A),
	workblock(Block2,_,A,_),
	setSequencial2(Block, Block2, ResB).
	
setSequencial2(Block, Block2, []):- 
	workblock(Block,_,Start,_),
	workblock(Block2,_,_,End),
	assert(agenda(Start,End)).

setSequencial2(Block, Block2, [Block3|ResB]):-
	workblock(Block2,_,_,A),
	workblock(Block3,_,A,_),
	setSequencial2(Block, Block3, ResB).
	
setSequencial2(Block, Block2, [Block3|ResB]):-
	workblock(Block,_,Start,_),
	workblock(Block2,_,_,End),
	assert(agenda(Start,End)),
	setAgendas2([Block3|ResB]).

setAgendas2([Block|ResB]):- 
	workblock(Block,_,Start,End),
	assert(agenda(Start,End)),
	setAgendas2(ResB).


getAgendas2(L):- 
	retractall(agenda(_,_)), 
	setAgendas2(L).	
	

correctDD(DD1, [A,B,C,D], N, DD2, N1, NY):-
	findall((M,S,E),(workblock(M,_,S,E), member(M,DD1), 39600=<S, E=<54000),Alm),
	correct1(A, Alm, DD1, N, DD11, N11, NY1),
	findall((M,S,E),(workblock(M,_,S,E), member(M,DD11), 64800=<S, E=<79200),Jan),
	correct1(B, Jan, DD11, N11, DD12, N12, NY2),
	append(NY1, NY2, NY12),
	findall((S,E),(workblock(M,_,S,E), member(M,DD12)),[(X,Y)|Z]),
	Temp is Y-X,
	correct3(C, [(X,Y)|Z], DD12, N12, DD13, N13, NY3, Temp),
	append(NY12,NY3, NY123),
	findall((S,E),(workblock(M,_,S,E), member(M,DD13)),KN),
	correct4(D, KN, DD13, N13, DD2, N1, NY4, 0),
	append(NY123,NY4, NY).	

correct1(0, _, DD1, N, DD1, N, []).

correct1(_, [], DD1, N, DD1, N, []).

correct1(AN, Alm, DD1, N, DD11, N1, NY1):- !,
	escolherBlocos(Alm, Bls, 0),
	replaceCor1(Bls,DD1,DD11,N,N1,NY1).
	
correct1(_, Alm, DD1, N, DD1, N, []).


escolherBlocos([], X, T):- false.

escolherBlocos([(X,Y,Z)|W], [X], T):-
	Temp is T + Z-Y,
	Temp >= 3600.

escolherBlocos([(X,Y,Z)|W], [X|Res], T):-
	Temp is T + Z-Y,
	escolherBlocos(W, Res, Temp).
	

replaceCor1([],DD1,DD1,N,N,[]):- !.

replaceCor1([X|Y],DD1,DD11,N,N1,[NY11|NY12]):- 
	findReplacement1(X,DD1,DD111,N,N11,NY11),
	replaceCor1(Y,DD111,DD11,N11,N1,NY12).
	
	
correct3(0, Ag1, DD1, N, DD1, N, [], _).

correct3(_, [], DD1, N, DD1, N, [], _):- !.

correct3(_, [X|[]], DD1, N, DD1, N, [], _):- !.

correct3(CN, [(A,B),(B,C)|D], [M,R|L], N, [M|L1], N1, [Rep|NY], T):-
	Temp is T + C-B, 
	Temp > 14400, !, 	
	findReplacement(B,C,Rep,N,N11, R),	
	correct3(CN, D, L, N11, L1, N1, NY, 0).
	
correct3(CN, [(A,B),(B,C)|D], [M,R|L], N, [M|L1], N1, NY, T):-
	Temp is T + C-B, 
	correct3(CN, [(B,C)|D], [R|L], N, L1, N1, NY, Temp).

correct3(CN, [(A,B),(P,C)|D], [M,R|L], N, [M|L1], N1, NY, T):-
	Temp is C-P,
	correct3(CN, [(P,C)|D], [R|L], N, L1, N1, NY, Temp).
	


correct4(0, Ag1, DD1, N, DD1, N, [], _).

correct4(_, [], [], N, [], N1, [], _).

correct4(D4, [(A,B)|C], [M|R], N, [M|R1], N1, NY, T):- 
	Temp is T+ B-A,
	Temp<28800, 
	correct4(D4, C,R,N,R1,N1,NY,Temp).

correct4(D4, [(A,B)|C], [M|R], N, [], N1, NY, T):-
	redistribute([(A,B)|C], [M|R], N, N1, NY).


redistribute([], [], N, N, []).

redistribute([(A,B)|C], [M|R], N, N1, [Rep|NY]):- 
	findReplacement(A,B,Rep,N,N11, M),
	redistribute(C,R,N11,N1, NY).

redistribute([(A,B)|C], [M|R], N, N1, NY):- 
	redistribute(C,R,N,N1, NY).
	


findReplacement(B,C,Rep,N,N11,R):- 
	duty(NVD),
	vehicleduty(NVD, LB),
	nth1(SRN, LB, R),
	nth1(SRN, N, No),
	findall(M,(disponibilidade(M,S,E), S=<B, C=<E, M\==No),LL),
	length(LL, Len),
	Len>0,
	Len1 is Len +1,
	random(1, Len1, Ch),
	nth1(Ch, LL, Rep),
	sAndR(N,N11,1,SRN,Rep).
	
findReplacement(_,_,_,_,_,_):- false.

findReplacement1(X,DD1,DD111,N,N11,Rep):- 
	duty(NVD),
	vehicleduty(NVD, LB),
	nth1(SRN, LB, X),
	nth1(SRN, N, No),
	workblock(X,_,B,C),
	findall(M,(disponibilidade(M,S,E), S=<B, C=<E, M\==No),LL),
	length(LL, Len),
	Len1 is Len +1,
	random(1, Len1, Ch),
	nth1(Ch, LL, Rep),
	sAndR(N,N11,1,SRN,Rep),
	removeFromDuty(X,DD1,DD111).
	
	
sAndR([A|B],[Rep|B],NF,NF,Rep):-!.

sAndR([A|B],[A|C],N,NF,Rep):- N1 is N+1, sAndR(B,C,N1,NF,Rep).

removeFromDuty(_,[],[]).

removeFromDuty(X,[X|Y],Y):-!.

removeFromDuty(X,[Y|Z],[Y|Z1]):-removeFromDuty(X,Z,Z1).





updateDisponibilidades([]):-!, retractall(disponibilidade(_,A,A)).

updateDisponibilidades([(X,Y)|Z]):- 
	updateDisp(X,Y),
	updateDisponibilidades(Z). 


updateDisp(X,[]):-!. 

updateDisp(X,[R|T]):- 
	workblock(R,_,S,E),
	findall((A,B), (disponibilidade(X,A,B), S>=A, E=<B), [LB|LBR]),
	retractCutReplace(X,LB,S,E),
	updateDisp(X,T).
	
	
retractCutReplace(X,(A,B),S,E):- 
	retract(disponibilidade(X,A,B)),
	assert(disponibilidade(X,A,S)),	
	assert(disponibilidade(X,E,B)).

