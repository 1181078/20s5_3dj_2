:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/json_convert)).
:- use_module(library(http/http_json)).
:- use_module(library(http/json)).
:- use_module(library(http/http_cors)).

:- http_handler('/ag', setup, []).	

setup(Request) :-
   	cors_enable,
   	http_read_json(Request, DictIn,[json_object(term)]),
   	DictOut=DictIn,
   	extractAG(DictOut, VD, NG, DP, P1, P2, P3, TT, AMIN, STABVAL),
   	(retract(duty(_));true), asserta(duty(VD)),
   	(retract(geracoes(_));true), asserta(geracoes(NG)),
	(retract(populacao(_));true), asserta(populacao(DP)),
	PC is P1/100, 
	(retract(prob_cruzamento(_));true), 	asserta(prob_cruzamento(PC)),
	PM is P2/100, 
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)),
	PP is P3/100, 
	(retract(perc_passagem(_));true), asserta(perc_passagem(PP)),
	(retract(tLim(_));true), asserta(tLim(TT)),
	get_time(TI), 	
	(retract(tStart(_));true), asserta(tStart(TI)),	
	(retract(minVal(_));true), asserta(minVal(AMIN)),
	(retract(stabil(_));true), asserta(stabil(STABVAL)),
	geraDirect(RR),
	term_to_atom(RR, JSONObject),
	reply_json(JSONObject).	
   
extractAG(json([nvd=VD,ngeracoes=NG,dpopulacao=DP,pcruzamento=P1,pmutacao=P2,ppassagem=P3, tlim=TT, amin=AM, stabval=SV]), VD1, NG1, DP1, P11, P21, P31, TT1, AM1, SV1):- 
	atom_number(VD, VD1),
	atom_number(NG, NG1),
	atom_number(DP, DP1),
	atom_number(P1, P11),
	atom_number(P2, P21),
	atom_number(P3, P31),
	atom_number(TT, TT1),
	atom_number(AM, AM1),
	atom_number(SV, SV1).

:-dynamic geracoes/1.
:-dynamic populacao/1.
:-dynamic prob_cruzamento/1.
:-dynamic prob_mutacao/1.
:-dynamic perc_passagem/1.
:-dynamic tLim/1.
:-dynamic tStart/1.
:-dynamic minVal/1.
:-dynamic stabil/1.
:-dynamic stabilValue/1.
:-dynamic stabilCount/1.
:-dynamic workblocks/1.
:-dynamic agenda/4.
:-dynamic lista_motoristas_nworkblocks/2.
:-dynamic counter/2.
:-dynamic duty/1.

% parameterização
inicializa:-write('Vehicle Duty: '),read(VD), 			
	(retract(duty(_));true), asserta(duty(VD)),
	write('Numero de novas Geracoes: '),read(NG), 			
	(retract(geracoes(_));true), asserta(geracoes(NG)),
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100, 
	(retract(prob_cruzamento(_));true), 	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100, 
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)),
	write('Percentagem de passagem para a próxima geração (20% ou 30%):'), read(P3),
	PP is P3/100, 
	(retract(perc_passagem(_));true), asserta(perc_passagem(PP)),
	write('Tempo limite de execução:'), read(TT),	
	(retract(tLim(_));true), asserta(tLim(TT)),
	get_time(TI), 	
	(retract(tStart(_));true), asserta(tStart(TI)),	
	write('Avaliação mínima para conclusão:'), read(AMIN),	
	(retract(minVal(_));true), asserta(minVal(AMIN)),
	write('Número de gerações idênticas para estabilização:'), read(STABVAL),	
	(retract(stabil(_));true), asserta(stabil(STABVAL)).


%----------------------------------------

gera:-
	inicializa,
	gera_populacao(Pop),
	write('Pop='),write(Pop),nl,nl,
	duty(NVD),
	lista_motoristas_nworkblocks(NVD, NM),
	avalia_populacao(1,NM,Pop,PopAv),
	write('PopAv='),write(PopAv),nl,nl,
	ordena_populacao(PopAv,PopOrd,NM,NM1),	
	(retract(lista_motoristas_nworkblocks(_));true), asserta(lista_motoristas_nworkblocks(NM1)),	
	write('PopOrd='),write(PopOrd),nl,nl,nl,
	geracoes(NG),
	(retract(stabilValue(_));true), asserta(stabilValue(PopOrd)),	
	(retract(stabilCount(_));true), asserta(stabilCount(1)),
	gera_geracao(0,5,PopOrd,RR),
	presentGens(RR,0).

geraDirect(RR):-
	gera_populacao(Pop),
	%write('Pop='),write(Pop),nl,nl,
	duty(NVD),
	lista_motoristas_nworkblocks(NVD, NM),
	avalia_populacao(1,NM,Pop,PopAv),
	%write('PopAv='),write(PopAv),nl,nl,
	ordena_populacao(PopAv,PopOrd,NM,NM1),	
	(retract(lista_motoristas_nworkblocks(_));true), asserta(lista_motoristas_nworkblocks(NM1)),	
	%write('PopOrd='),write(PopOrd),nl,nl,nl,
	geracoes(NG),
	(retract(stabilValue(_));true), asserta(stabilValue(PopOrd)),	
	(retract(stabilCount(_));true), asserta(stabilCount(1)),
	gera_geracao(0,5,PopOrd,RR).
	

presentGens([X],N):- write('Geração '), write(N), write(':'), nl, write(X), nl,nl.

presentGens([X,1|Y],N):- 	write('Geração '), write(N), write(':'), nl, write(X), nl,nl, N1 is N+1, presentGens(Y,N1).

	
%----------------------------------------
disponibilidade(16690, 52020,76020).
disponibilidade(18107, 59220,77340).
disponibilidade(276, 27300,52020).
disponibilidade(5188, 34080,65220).

gera_populacao(Pop):-		
	populacao(TamPop),	
	duty(NVD),
	vehicleduty(NVD, LB),
	gera_populacao2(TamPop,LB,Pop,BB),
	duty(NVD),
	(retract(lista_motoristas_nworkblocks(_));true), asserta(lista_motoristas_nworkblocks(NVD, BB)).
	
	
gera_populacao(0,_,[],[]):-!.

gera_populacao2(0,_,[],[]):-!.

gera_populacao2(TamPop,ListaBlocos,[Ind,Ind2|Resto],[LM,LM2|R]):-
	TamPop1 is TamPop-2,
	gera_populacao(TamPop1,ListaBlocos,Resto,R),
	gera_individuo2(ListaBlocos,Ind,LM,0),
	gera_individuo2(ListaBlocos,Ind2, LM2,0).
	
gera_populacao(TamPop,ListaBlocos,[Ind|Resto],[LM|R]):-
	TamPop1 is TamPop-1,
	gera_populacao(TamPop1,ListaBlocos,Resto,R),
	gera_individuo(ListaBlocos,Ind, LM).
		
gera_populacao(TamPop,ListaBlocos,L,B):-
	gera_populacao(TamPop,ListaBlocos,L,B).
	

%----------------------------------------

gera_individuo([],[],[]):-!.

gera_individuo([B1|B2],[G|Resto],TT):-
	workblock(B1,_,BS,BE),
	findall(M,(disponibilidade(M,S,E), S=<BS, BE=<E),LL),
	length(LL, Tam),
	NumTemp is Tam + 1, % To use with random
	random(1,NumTemp,N),
	nth1(N, LL, G),
	gera_individuo(B2,Resto,TT1),
	searchAndModify(G,TT1,TT).

gera_individuo2([],[],[],_):-!.

gera_individuo2([B1|B2],[G|Resto],TT,N0):-
	workblock(B1,_,BS,BE),
	findall(M,(order_by([asc(S)],disponibilidade(M,S,E)), S=<BS, BE=<E),LL),
	nth1(1, LL, G), 
	N1 is N0 + BE - BS,
	N1=<14400,
	gera_individuo2(B2,Resto,TT1,N1),
	searchAndModify(G,TT1,TT).

gera_individuo2([B1|B2],[G|Resto],TT,N0):- !,
	workblock(B1,_,BS,BE),
	findall(M,(order_by([asc(S)],disponibilidade(M,S,E)), S=<BS, BE=<E),LL),
	nth1(2, LL, G),
	gera_individuo2(B2,Resto,TT1,0),
	searchAndModify(G,TT1,TT).


gera_individuo3([],[],[],_):-!.

gera_individuo3([B1|B2],[G|Resto],TT,N0):-
	workblock(B1,_,BS,BE),
	findall(M,(order_by([asc(E)],disponibilidade(M,S,E)), S=<BS, BE=<E),LL),
	nth1(1, LL, G), 
	N1 is N0 + BE - BS,
	N1=<14400,
	gera_individuo3(B2,Resto,TT1,N1),
	searchAndModify(G,TT1,TT).

gera_individuo3([B1|B2],[G|Resto],TT,N0):- !,
	workblock(B1,_,BS,BE),
	findall(M,(order_by([asc(E)],disponibilidade(M,S,E)), S=<BS, BE=<E),LL),
	nth1(2, LL, G),
	gera_individuo3(B2,Resto,TT1,0),
	searchAndModify(G,TT1,TT).

    
searchAndModify(G,[],[(G,1)]).

searchAndModify(G,[(G,M)|O],[(G,M1)|O]):- M1 is M+1.

searchAndModify(G,[(N,M)|O],[(N,M)|S]):- searchAndModify(G,O,S).


%----------------------------------------


avalia_populacao(_,[],[],[]).
avalia_populacao(N,[First|Res],[Ind|Resto],[Ind*V|Resto1]):-
	avalia(N,First,Ind,V),
	N1 is N + 1,
	avalia_populacao(N1,Res,Resto,Resto1).

avalia(N,First, Seq,V):-
	duty(NVD),
	vehicleduty(NVD,L),
	getAgendas(N,Seq,L),
	getMotoristas(First, LL),
	avalias(N,LL, V).

getMotoristas([], []).

getMotoristas([(M,X)|N], [M|R]):-
	getMotoristas(N,R).


avalias(_,[], 0).

avalias(N,[X|Y], V):- avalias(N,Y, V2), avaliaAgenda(N,X,V1), V is V1 + V2.

avaliaAgenda(N,X, V):-
	findall((S,E),agenda(N,X,S,E),Ag1),
	bsortA(Ag1,Ag),
	avalia1(X, Ag, 0, V1), 
	avalia2(X, Ag, 0, V2), 
	avalia3(X, Ag, V3), 
	avalia4(X, Ag, 0, V4),
	V is V1 + V2 + V3 + V4.	
	
	

avalia1(X, [], 1 , V):- V is  8*3600.	

avalia1(X, [], T , 0).

avalia1([(B,C)|D], T, V):-	
	B < 39600, C > 54000, 
	avalia1([], 1, V).


avalia1(X, [(B,C)|D], T, V):-	
	C < 39600, 
	avalia1(X, D, T, V).

avalia1(X, [(B,C)|D], T, V):-
	C > 54000, 
	avalia1(X, D, T, V).

avalia1(X, [(B,C)|D], T, V):-
	C > 39600, C < 54000,
	avalia11(X, D, C, T, V).

avalia11(X, [], U, T, V):- avalia1(X, [], T, V).

avalia11(X, [(B,C)|[]], U, T, V):- avalia1(X, [], T, V).

avalia11(X, [(B,C)|D], U, T, V):-
	U1 is B-U,
	U1 < 3600,
	((T==0, T1=1);(T1=T)),
	avalia1(X, [(B,C)|D], T1, V).

avalia11(X, [(B,C)|D], U, T, V):-
	T1 is T + 1,
	avalia1(X, [(B,C)|D], T1, V).





avalia2(X, [], 1 , V):- V is  8*3600.	

avalia2(X, [], T , 0).

avalia2([(B,C)|D], T, V):-	
	B < 64800, C > 79200, 
	avalia2([], 1, V).


avalia2(X, [(B,C)|D], T, V):-	
	C < 64800, 
	avalia2(X, D, T, V).

avalia2(X, [(B,C)|D], T, V):-
	C > 79200, 
	avalia2(X, D, T, V).

avalia2(X, [(B,C)|D], T, V):-
	C > 64800, C < 79200,
	avalia22(X, D, C, T, V).
	
avalia22(X, [], U, T, V):- avalia2(X, [], T, V).

avalia22(X, [(B,C)|[]], U, T, V):- avalia2(X, [], T, V).

avalia22(X, [(B,C)|D], U, T, V):-
	U1 is B-U,
	U1 < 3600,
	avalia2(X, [(B,C)|D], T, V).

avalia22(X, [(B,C)|D], U, T, V):-
	T1 is T + 1,
	avalia2(X, [(B,C)|D], T1, V).





avalia3(X, [], 0):- !.

avalia3(X, [(B,C)|D], V):-
	Temp is C-B, 
	Temp > 14400, !, 
	NewTemp is Temp - 14400,
	V1 is NewTemp * 10,
	avalia35(X, D, C, V2),
	avalia3(X, D, V3),
	V is V1 + V2 + V3.

avalia3(X, [(B,C)|D], V):-	
	avalia3(X,D,V).
	
avalia35(X, [], Z, 0):- !.

avalia35(X, [(B,C)|D], Z, V):- 	
	Temp is Z-C,
	Temp <3600,
	V is 3600*10.

avalia35(X, [(B,C)|D], Z, 0). 	
	
	

avalia4(X, [], T, V):- T<28800, V is 0.

avalia4(X, [], T, V):- !,Temp is T-28800, V is Temp*10.

avalia4(X, [(B,C)|D], T, V):-
	Temp is C-B + T, 
	avalia4(X, D, Temp, V).






	
setAgendas(_,[],[]):- !.

setAgendas(N,[M, M|Res],[Block, Block2|ResB]):- 
	setSequencial(N,Res, M, Block, Block2, ResB).
	
setSequencial(N,[], M, Block, Block2, _):- !,
	workblock(Block,_,Start,_),
	workblock(Block2,_,_,End),
	assert(agenda(N,M,Start,End)).


setSequencial(N,[M|Res], M, Block, Block2, [Block3|ResB]):-
	setSequencial(N,Res, M, Block, Block3, ResB).
	
setSequencial(N, [P|Res], M, Block, Block2, [Block3|ResB]):-
	workblock(Block,_,Start,_),
	workblock(Block2,_,_,End),
	assert(agenda(N,M,Start,End)),
	setAgendas(N,[P|Res],[Block3|ResB]).

setAgendas(N,[M|Res],[Block|ResB]):- 
	workblock(Block,_,Start,End),
	assert(agenda(N,M,Start,End)),
	setAgendas(N,Res,ResB).


getAgendas(N,Seq, L):- 
	retractall(agenda(N,_,_,_)), 
	setAgendas(N,Seq,L).	

%----------------------------------------


ordena_populacao(PopAv,PopAvOrd,NM,NM1):-
	bsort(PopAv,PopAvOrd,NM,NM1).

bsort([X],[X],[K],[K]):-!.
bsort([X|Xs],Ys,[K|Ks],Ts):-
	bsort(Xs,Zs,Ks,Ws),
	btroca([X|Zs],Ys, [K|Ws],Ts).


btroca([X],[X],[K],[K]):-!.

btroca([X*VX,Y*VY|L1],[Y*VY|L2],[K*VK,T*VT|W1],[T*VT|W2]):-
	VX>VY,!,
	btroca([X*VX|L1],L2,[K*VK|W1],W2).

btroca([X|L1],[X|L2],[K|W1],[K|W2]):-btroca(L1,L2,W1,W2).


ordena_populacao(PopAv,PopAvOrd):-
	bsortA(PopAv,PopAvOrd).


bsortA([X],[X]):-!.
bsortA([X|Xs],Ys):-
	bsortA(Xs,Zs),
	btrocaA([X|Zs],Ys).


btrocaA([X],[X]):-!.

btrocaA([(X,VX),(Y,VY)|L1],[(Y,VY)|L2]):-
	VX>VY,!,
	btrocaA([(X,VX)|L1],L2).

btrocaA([X|L1],[X|L2]):-btrocaA(L1,L2).


%----------------------------------------



gera_geracao(G,G,Pop,X):-!,
	X = [Pop].

gera_geracao(N,G,Pop, X):- 
	tLim(TT), tStart(TS), get_time(TN), 
	Dif is TN - TS,
	(Dif>TT), !,
	X = [Pop].

gera_geracao(N,G,Pop, X):- 	
	minVal(MV), getTotalAval(Pop, Av), 
	(MV>Av), !,
	X = [Pop].

gera_geracao(N,G,Pop, X):- 
	stabil(SS), stabilValue(SV), stabilCount(CC),
	(Pop==SV), 
	SN = CC + 1,
	(retract(stabilCount(_));true), asserta(stabilCount(SN)),
	(SN>=SS), !,
	X = [Pop].

gera_geracao(N,G,Pop, RR):- 
	stabil(SS), stabilValue(SV), stabilCount(CC),
	(Pop\==SV), !,
	(retract(stabilCount(_));true), asserta(stabilCount(1)),	
	(retract(stabilValue(_));true), asserta(stabilValue(Pop)),
	gera_nova_geracao(N,G,Pop,RR).
	
gera_geracao(N,G,Pop, RR):- gera_nova_geracao(N,G,Pop, RR).

gera_nova_geracao(N,G,Pop, [Pop,1|RR]):-
	duty(NVD),
	random_permutation(Pop,PopR),	
	resetLNB(PopR, RanLNB),			
	(retractall(lista_motoristas_nworkblocks(_));true), asserta(lista_motoristas_nworkblocks(NVD,RanLNB)),
	lista_motoristas_nworkblocks(NVD, LNB),
	cruzamento(PopR,NPop1, LNB),
	mutacao(NPop1,NPop, LNB),
	lista_motoristas_nworkblocks(NVD, NM),
	avalia_populacao(1,NM,NPop1,NPop1Av),	
	avalia_populacao(1,NM,NPop,NPopAv),	
	joinPops(Pop, NPop1Av, Mix1),
	joinPops(Mix1, NPopAv, Mix),
	ordena_populacao(Mix,NPopOrd),
	populacao(Tam), perc_passagem(Per),
	S is round(Tam * Per),
	NResto is round(Tam - S),	
	nMelhores(NPopOrd, NewNPopOrd, Bestm, S),
	nRestantes(NewNPopOrd, Restos),
	ordenaNRestantes(Restos, RestosSorted, NewNPopOrd, NewNPopOrdSorted),
	nMelhores(NewNPopOrdSorted, Void, RestM, NResto),
	append(Bestm, RestM, Final),
	ordena_populacao(Final,FinalOrd),
	resetLNB(FinalOrd, RLNB),			
	(retractall(lista_motoristas_nworkblocks(_));true), asserta(lista_motoristas_nworkblocks(NVD,RLNB)),
	N1 is N+1,
	gera_geracao(N1,G,FinalOrd,RR)	.
	
resetLNB([], []):-!. 

resetLNB([X*_|Y], [A|B]):- 
	resetLNB(Y,B),
	resetLNBInd(X,A).

resetLNBInd([],[]):-!.

resetLNBInd([G|Resto],TT):-
	resetLNBInd(Resto,TT1),
	searchAndModify(G,TT1,TT).
	
	
	
searchAndModify(G,[],[(G,1)]).

searchAndModify(G,[(G,M)|O],[(G,M1)|O]):- M1 is M+1.

searchAndModify(G,[(N,M)|O],[(N,M)|S]):- searchAndModify(G,O,S).

	
appendPops([], R, R) .
appendPops([H|T], R1, [H|R2]) :-
    appendPops(T, R1, R2).


memberPops(H, [H|_]).
memberPops(H, [_|T]) :-
    memberPops(H, T).

joinPops(L, [], L).

joinPops(L, [H| T], R) :- member(H, L), joinPops(L, T, R).

joinPops(L, [H| T], R) :- appendPops(L, [H], L1), joinPops(L1, T, R).
        
nMelhores(NPopOrd, NPopOrd, [], 0):- !.

nMelhores(NPopOrd, NewNPopOrd, [G|Z], Tam):- Tam1 is Tam-1, retira(1,NPopOrd,G,NN), nMelhores(NN, NewNPopOrd,Z,Tam1).

retira(1,[G|Resto],G,Resto).
retira(N,[G1|Resto],G,[G1|Resto1]):-
	N1 is N-1,
	retira(N1,Resto,G,Resto1).


nRestantes([],[]).
nRestantes([Ind*_|Resto],[Ind*V1|Resto1]):-
	random(0.0,1.0,V1),
	nRestantes(Resto,Resto1).

ordenaNRestantes([X],[X],[Y],[Y]):-!.

ordenaNRestantes([X|Xs],Ys, [T|Ts],Us):-
		ordenaNRestantes(Xs,Zs, Ts,Ns),
		ordenaNRestantesTroca([X|Zs],Ys, [T|Ns], Us).


ordenaNRestantesTroca([X],[X],[Y],[Y]):-!.

ordenaNRestantesTroca([X*VX,Y*VY|L1],[Y*VY|L2], [T*V1T,U*V1U|M1],[U*V1U|M2]):-
	VX>VY,!,
	ordenaNRestantesTroca([X*VX|L1],L2,[T*V1T|M1],M2).

ordenaNRestantesTroca([X|L1],[X|L2], [T|M1],[T|M2]):-ordenaNRestantesTroca(L1,L2, M1,M2).

getTotalAval([], 0):-!.

getTotalAval([Ind*V|Resto], Av):- getTotalAval(Resto, Av1), Av is Av1 + V.


gerar_pontos_cruzamento(P1,P2, N):-
	gerar_pontos_cruzamento1(P1,P2,N).

gerar_pontos_cruzamento1(P1,P2,N):-
	NTemp is N+1,
	random(1,NTemp,P11),
	random(1,NTemp,P21),
	P11\==P21,!,
	((P11<P21,!,P1=P11,P2=P21);(P1=P21,P2=P11)).
gerar_pontos_cruzamento1(P1,P2,N):-
	gerar_pontos_cruzamento1(P1,P2,N).


cruzamento([],[],[]).
cruzamento([Ind*_],[Ind],_).
cruzamento([Ind1*_,Ind2*_|Resto],[NInd1,NInd2|Resto1], [LNB1,LNB2|LNBR]):-
	length(Ind1,N),
	gerar_pontos_cruzamento(P1,P2, N),
	prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(N,LNB1,Ind1,Ind2,P1,P2,NInd1),
	  cruzar(N,LNB2,Ind2,Ind1,P1,P2,NInd2))	;
	(NInd1=Ind1,NInd2=Ind2)),
	cruzamento(Resto,Resto1,LNBR).
	
preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).


sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2).

rotate_right(N,L,K,L1):-
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


elimina([],_,[]):-!.

elimina([X|R1],LNB,[X|R2]):-
	member((X,M),LNB), 	
	counter(X,Val),
	M>Val,!,
	N1 is Val+1,
	retract(counter(X,_)),
	assert(counter(X,N1)),
	elimina(R1,LNB,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).

insere(_,[],L,_,L):-!.
insere(T,[X|R],L,N,L2):-
	((N>T,!,N1 is N mod T);N1 = N),
	insere1(X,N1,L,L1),
	N2 is N + 1,
	insere(T,R,L1,N2,L2).


insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

cruzar(N,LNB,Ind1,Ind2,P1,P2,NInd11):-
	sublista(Ind1,P1,P2,Sub1),	
	R is N-P2,	
	rotate_right(N,Ind2,R,Ind21),
	(retractall(counter(_,_));true),
	setLNB(Ind1),
	updateLNB(Sub1),
	elimina(Ind21,LNB,Sub2),
	fillIfIncomplete(LNB, NSub2),
	append(NSub2, Sub2, FSub2),
	P3 is P2 + 1,
	insere(N,FSub2,Sub1,P3,NInd1),
	eliminah(NInd1,NInd11).

setLNB([]):-!.
	
setLNB([X|Y]):- 
	\+ counter(X,_), 
	assert(counter(X,0)),
	setLNB(Y).

setLNB([X|Y]):-
	setLNB(Y).


updateLNB([]):-!.

updateLNB([h|Y]):-
	updateLNB(Y).
	
updateLNB([X|Y]):- 
	updateLNB(Y),
	(\+ counter(X,_), assert(counter(X,1));
	counter(X,N),
	N1 is N+1,
	retract(counter(X,_)), assert(counter(X,N1))).
	
	
fillIfIncomplete([],[]):-!.

fillIfIncomplete([(X,Y)|Z], NSub2):- 
	fillIfIncomplete(Z,NN),
	counter(X,V),
	Temp is Y-V,
	insert(Temp, X, NN1),
	append(NN,NN1,NSub2).
	
	
insert(0,_,[]):- !.

insert(T,X,[X|Res]):- T1 is T-1, insert(T1, X, Res).
	
	


eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).

mutacao([],[],_).
mutacao([Ind|Rest],[NInd|Rest1], LNB):-
	prob_mutacao(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut,!,mutacao1(Ind,NInd, LNB));NInd = Ind),
	mutacao(Rest,Rest1, LNB).

mutacao1(Ind,NInd, LNB):-
	length(LNB, N),
	gerar_pontos_cruzamento(P1,P2, N),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd).

