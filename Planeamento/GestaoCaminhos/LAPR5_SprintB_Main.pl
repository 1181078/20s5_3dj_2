% Bibliotecas 
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/json_convert)).
:- use_module(library(http/http_json)).
:- use_module(library(http/json)).
:- use_module(library(http/http_cors)).

% Relação entre pedidos HTTP e predicados que os processam
:- http_handler('/add_no', add_no, []).			
:- http_handler('/add_linha', add_linha, []).
:- http_handler('/aStar', aStarSetup, []).
:- http_handler('/todosMelhor', todosSetup,[]).
:- http_handler('/todosMelhor1', todosSetup1,[]).
:- http_handler('/bestfs', bestfsSetup,[]).
:- http_server(http_dispatch, [port(5000)]).

:- set_setting(http:cors, [*]).


% MÉTODO GET: Tratamento de 'http://localhost:5000/add_no?name=Aguiar de Sousa&abrev=AGUIA&is1=f&is2=f&lon=-8.4464785432391&lat=41.1293363229325'	
% ou http_client:http_get('http://localhost:5000/add_no?name=Aguiar de Sousa&abrev=AGUIA&is1=f&is2=f&lon=-8.4464785432391&lat=41.1293363229325',X,[]).


add_no(Request) :-
    http_parameters(Request,
                    [ name(Name, []),
                    	abrev(Abrev, []),
                      is1(I1, [oneof([t,f])]),
                      is2(I2, [oneof([t,f])]),                     
                      lon(LON, [between(-90.0,90.0)]),
                      lat(LAT, [between(-90.0,90.0)])
                     ]),
    format('Content-type: text/plain~n~n'),
    format('Nó adicionado!~n'),
	format('Nome: ~w~nAbreviatura: ~w~nIs?: ~w~nIs?: ~w~nLatitude: ~w~nLongitude: ~w~n',[Name,Abrev,I1,I2,LON,LAT]),
	assertz(no(Name,Abrev,I1,I2,LON,LAT)).


% MÉTODO GET: Tratamento de 'http://localhost:5000/add_linha?name=Paredes_Aguiar&num=1&per=[AGUIA,RECAR,PARAD,CETE,PARED]&tempo=31&dist=15700'	

add_linha(Request) :-
    http_parameters(Request,
                    [ name(Name, []),
                    	num(Num, []),
                      per(Per, []),
                      tempo(Tempo, []),                     
                      dist(Dist, [])
                    ]),
    format('Content-type: text/plain~n~n'),
    format('Linha adicionada!~n'),
	format('Nome: ~w~nNúmero de linha: ~w~nPercurso: ~w~nTempo: ~w~nDistância: ~w~n',[Name,Num,Per,Tempo,Dist]),
	assertz(linha(Name,Num,Per,Tempo,Dist)).


% MÉTODO GET: Tratamento de 'http://localhost:5000/aStar?time=0&orig=PARED&dest=ESTPA'
% ou http_client:http_get('http://localhost:5000/aStar?time=0&orig=PARED&dest=ESTPA',X,[]).
% http_client:http_post('http://localhost:5000/aStar', form_data([time=0, orig='PARED', dest='ESTPA']), Reply, []).

aStarSetup(Request) :-
   cors_enable,
   http_read_json(Request, DictIn,[json_object(term)]),
   DictOut=DictIn,
   extractIt(DictOut, T1, O1, D1),    
   aStar(T1,O1,D1, Cam, Custo),
        term_to_atom((Cam; Custo), JSONObject),
  	reply_json(JSONObject).	

bestfsSetup(Request) :-
	cors_enable,	
	http_read_json(Request, DictIn,[json_object(term)]),
	DictOut=DictIn,
	extractIt(DictOut, T1, O1, D1),
	bestfs(T1,O1,D1, Cam, Custo),
	term_to_atom((Cam; Custo), JSONObject),
	reply_json(JSONObject).

	
extractIt(json([time=T,orig=O,dest=D]), T1, O1, D1):- T1 = T, O1 = O, D1 = D.
	
	


% Método GET : Tratamento de 'http://localhost:5000/todosMelhor?time=0&orig=ESTPA&dest=CRIST'
% http_client:http_get('http://localhost:5000/todosMelhor?time=0&orig=ESTPA&dest=CRIST,X,[]').
% http_client:http_post('http://localhost:5000/todosMelhor', form_data([time=0, orig=ESTPA, dest=CRIST]), Reply, []).


todosSetup(Request):-
    cors_enable,
	http_read_json(Request, DictIn,[json_object(term)]),
   	DictOut=DictIn,
   	extractIt(DictOut, T1, O1, D1),    
	caminhosTodos(T1,O1,D1,LCTodos,Melhor),
	format(user_output, 'Melhor: ~w~n',[Melhor]),
	term_to_atom(Melhor, JSONObject),
  	reply_json(JSONObject).

/*Todos Setup 1 sem findall*/
todosSetup1(Request):-
	http_read_json(Request, DictIn,[json_object(term)]),
   	DictOut=DictIn,
   	extractIt(DictOut, T1, O1, D1),    
	caminhosTodos1(T1,O1,D1,Melhor),
	format(user_output, 'Melhor: ~w~n',[Melhor]),
	term_to_atom(Melhor, JSONObject),
  	reply_json(JSONObject).

gera_ligacoes:- retractall(liga(_,_,_)),
			findall(_,((no(_,No1,t,f,_,_);no(_,No1,f,t,_,_)),(no(_,No2,t,f,_,_);no(_,No2,f,t,_,_)),
			No1\==No2, linha(_,N,LNos,_,_),
			ordem_membros(No1,No2,LNos),
			assertz(liga(No1,No2,N))),_).
			
ordem_membros(No1,No2,[No1|L]):- member(No2,L),!.
ordem_membros(No1,No2,[_|L]):- ordem_membros(No1,No2,L).


/*
implementação de gerador de todas as soluções para mudança de motorista/tripulação
 entre estações de rendição e escolha da solução que minimiza tempo total de mudança.
*/
caminho(Noi,Nof,LCaminho):-
	caminho(Noi,Nof,[],LCaminho).
	caminho(No,No,Lusadas,Lfinal):-reverse(Lusadas,Lfinal).
	caminho(No1,Nof,Lusadas,Lfinal):-
	liga(No1,No2,N),
	\+member((_,_,N),Lusadas),
	\+member((No2,_,_),Lusadas),
	\+member((_,No2,_),Lusadas),
	caminho(No2,Nof,[(No1,No2,N)|Lusadas],Lfinal).
/*
	Sem findall
*/
	:-dynamic melhor_sol_menorTempo/2.
	caminhosTodos1(TempoPartida,Orig,Dest,Melhor):-	
															gera_ligacoes(),
															get_time(Ti),
															(caminhoMaisCurto1(TempoPartida,Orig,Dest);true),
															retract(melhor_sol_menorTempo(MelhorCam,MelhorCusto)),
															Melhor = (MelhorCusto;MelhorCam),
															get_time(Tf),
															TSol is Tf-Ti,
															write('Tempo de geracao da solucao:'),write(TSol),nl.
	caminhoMaisCurto1(T,O,D):-
								asserta(melhor_sol_menorTempo(_,1000000)),
								caminho(O,D,LCa),
								atualiza_melhor(T,LCa),
								fail.

	atualiza_melhor(T,LCa):-
							melhor_sol_menorTempo(_,N),
							caminhoPorSegmento(T,LCa,Custo),
							Custo<N,
							retract(melhor_sol_menorTempo(_,_)),
							asserta(melhor_sol_menorTempo(LCa,Custo)).
															
/*
		Com findall
*/




caminhosTodos(TempoPartida,Orig,Dest,LCTodos,Melhor):-
													gera_ligacoes(),
													findall(L,caminho(Orig,Dest,L),LTodos),
													sort(LTodos,LTodos2),
													caminhoPorOpcao(TempoPartida,LC,LTodos2),													
													LCTodos = LC,
													%showList(LCTodos),
													melhor(LCTodos,Melhor),!.
   
melhor(L,Cam):-
				sort(L,S),
				nth1(1,S,Cam),!.

/*--------Ver cada membro individualmente----------------                                         
showList([X|[]]):- write('Alternativa de custo = '), write(X),nl,nl,nl.
showList([X|Y]):- write('Alternativa de custo = '), write(X),nl, showList(Y).
*/  
											
caminhoPorOpcao(Tempo , R, [H|[]]):- !,caminhoPorSegmento(Tempo,H, Custo), R = [(Custo;H)].

caminhoPorOpcao(Tempo,LC,[H|T]):-
									caminhoPorOpcao(Tempo,LC1,T),
									caminhoPorSegmento(Tempo,H, Custo),
									LC = [(Custo;H)|LC1].

caminhoPorSegmento(Tempo,[(O,D,NLinha)|[]],Custo):- !,
	 												tempoNaLinha(Tempo,O,D,NLinha,Custo,NewTime).

 
caminhoPorSegmento(Tempo,[(O,D,NLinha)|T], Custo):-
	 												tempoNaLinha(Tempo,O,D,NLinha,CustoL,NewTime),
	 												caminhoPorSegmento(NewTime,T,CustoN),
	  												Custo is CustoL + CustoN.














/*
Implementação do algoritmo A* para mudança de motorista/tripulação entre estações de rendição
*/


aStar(Time,Orig,Dest,Cam,Custo):-	gera_ligacoes(), aStar2(1, Orig, Dest,[(_,0,Time,[])],Cam,Custo),!.

aStar2(Velo, Orig, Dest,[(_,Ca,Time,LA)|Outros],Cam,Custo):-Act = Orig, 
								findall((CEX,CaX,NewTime,Final),(Dest\==Act,liga(Act,X,NLinha), \+ member((_,X,_),LA), \+ member((X,_,_),LA), 
								add([(Act,X,NLinha)|LA], Final), 
								velocidades(Velo, NLinha, NewVelocidade),
								tempoNaLinha(Time,Act,X,NLinha,CustoX, NewTime), CaX is CustoX + Ca, estimativa(X,Dest, NewVelocidade, EstX), CEX is CaX +EstX),	Novos),
								append(Outros,Novos,Todos), 
								sort(Todos,TodosOrd),
								sort(TodosOrd,TodosOrd),
								aStar3(Velo, Dest,TodosOrd,Cam,Custo).

aStar3(Velo, Dest,[(_,Custo,Time,[(A,Dest,Z)|T])|_],Cam,Custo):-	reverse([(A,Dest,Z)|T],Cam).

aStar3(Velo, Dest,[(_,Ca,Time,LA)|Outros],Cam,Custo):-LA=[(_,Act,_)|_], 
								findall((CEX,CaX,NewTime,Final),(Dest\==Act,liga(Act,X,NLinha), \+ member((_,X,_),LA), \+ member((X,_,_),LA),
								add([(Act,X,NLinha)|LA], Final), 								
								velocidades(Velo, NLinha, NewVelocidade),
								tempoNaLinha(Time,Act,X,NLinha,CustoX, NewTime), CaX is CustoX + Ca, estimativa(X,Dest, NewVelocidade, EstX), CEX is CaX +EstX),	Novos),
								append(Outros,Novos,Todos), 
								sort(Todos,TodosOrd),
								sort(TodosOrd,TodosOrd),
								aStar3(Velo, Dest,TodosOrd,Cam,Custo).
								
estimativa(Nodo1,Nodo2,NewVelocidade,Estimativa):- distanciaNos(Nodo1,Nodo2,Dist), Estimativa is Dist/NewVelocidade.

velocidades(Velo,NLinha, NewVelocidade):- 	linha(_,NLinha,_,Tempo,Dist), Tempo1 is Tempo*60, NewVel is Dist/Tempo1, NewVel>Velo, NewVelocidade is NewVel.

velocidades(Velo, NLinha, Velo).

tempoNaLinha(Time,Act,Next,NLinha,CustoX, NewTime):- findall(List, horario(NLinha, List), NewListTemp),sort(NewListTemp, NewList),
							  linha(_,NLinha,LL,_,_),  nth1(Pos,LL,Act), nth1(PosFinal,LL,Next), 							
							  getExactCusto(NewList, Time, Pos, PosFinal, CustoX, NewTime).
							  
getExactCusto([X|[]], Time, Pos, PosFinal, CustoX, NewTime):- nth1(Pos,X,Elemento), nth1(PosFinal,X,ElementoFinal), 
												CustoX is ElementoFinal - Time, NewTime is ElementoFinal. 

getExactCusto([X|Y], Time, Pos, PosFinal, CustoX, NewTime):- nth1(Pos,X,Elemento), Elemento<Time,!,getExactCusto(Y, Time, Pos, PosFinal, CustoX, NewTime).

getExactCusto([X|Y], Time, Pos, PosFinal, CustoX, NewTime):- !,getExactCusto(Y, Time, Pos, PosFinal, CustoX1, NewTime1), nth1(Pos,X,Elemento), nth1(PosFinal,X,ElementoFinal), 
												Contador is ElementoFinal - Time, Contador<CustoX1, CustoX is Contador, NewTime is ElementoFinal.

getExactCusto([X|Y], Time, Pos, PosFinal, CustoX, NewTime):- getExactCusto(Y, Time, Pos, PosFinal, CustoX, NewTime).

add(L, L).


/*
	Best First Algorithm com consideracao de horarios e tempos de espera
	*/

bestfs(Time,Orig,Dest,Cam,Custo):- gera_ligacoes(),bestfs2(1, Orig, Dest,( 0,Time,[]),Cam,Custo),!.

bestfs2(Velo, Orig, Dest,(Ca,Time,LA),Cam,Custo):- Act = Orig ,
													findall((EstX,CaX,NewTime,[(Act,X,NLinha)|LA]),(liga(Act,X,NLinha), \+ member((_,X,_),LA), \+ member((X,_,_),LA),
													velocidades(Velo, NLinha, NewVelocidade),
													tempoNaLinha(Time,Act,X,NLinha,CustoX, NewTime), CaX is CustoX + Ca, estimativa(X,Dest, NewVelocidade, EstX)), Novos),
													sort(Novos,NovosOrd),
													proximo(NovosOrd,CM,TM,Melhor),
													bestfs3(Velo,Dest,(CM,TM, Melhor),Cam,Custo).

bestfs3(Velo, Dest,(Custo,Time,[(A,Dest,Z)|T]),Cam,Custo):- reverse([(A,Dest,Z)|T],Cam).

bestfs3(Velo, Dest,(Ca,Time,LA),Cam,Custo):- LA=[(_,Act,_)|_],
											findall((EstX,CaX,NewTime,[(Act,X,NLinha)|LA]),(liga(Act,X,NLinha), \+ member((_,X,_),LA), \+ member((X,_,_),LA),
											velocidades(Velo, NLinha, NewVelocidade),
											tempoNaLinha(Time,Act,X,NLinha,CustoX, NewTime), CaX is CustoX + Ca, estimativa(X,Dest, NewVelocidade, EstX)), Novos),
											sort(Novos,NovosOrd),
											proximo(NovosOrd,CM,TM,Melhor),
											aStar3(Velo,Dest,(CM,TM, Melhor),Cam,Custo).
											
											
											
											
proximo([(_,CM,TM,Melhor)|_],CM,TM,Melhor).
proximo([_|L],CM,TM,Melhor):-proximo(L,CM,TM,Melhor).




/*
	Distancia entre nós calculo de 2 pontos por latitude e longitude
*/


distanciaNos(No1,No2,Dist):- 
	no(_,No1,_,_,Lat1,Long1), no(_,No2,_,_,Lat2,Long2),
	distancia(Lat1,Long1,Lat2,Long2,Dist).

distancia(Lat1,Long1,Lat2,Long2,Dist):- 
				LatX1 is Lat1*pi/180,
				LongX1 is Long1*pi/180,
				LatX2 is Lat2*pi/180,
				LongX2 is Long2*pi/180,
				Dist is 6378.137*acos((cos(LatX1) * cos(LatX2) *cos(LongX2-LongX1)) + (sin(LatX1)
				 * sin(LatX2)))*1000.