menor_ntrocas(Noi,Nof,LCaminho_menostrocas):- findall(LCaminho,caminho(Noi,Nof,LCaminho),LLCaminho), menor(LLCaminho,LCaminho_menostrocas).
											  menor([H],H):-!.

menor([H|T],Hmenor):-menor(T,L1),length(H,C),length(L1,C1),
						((C<C1,!,Hmenor=H);Hmenor=L1).

plan_mud_mot(Noi,Nof,LCaminho_menostrocas):-
                 get_time(Ti),
                 findall(LCaminho,caminho(Noi,Nof,LCaminho),LLCaminho),
                 menor(LLCaminho,LCaminho_menostrocas),
                 get_time(Tf),
                 length(LLCaminho,NSol),
                 TSol is Tf-Ti,
                 write('Numero de Solucoes:'),write(NSol),nl,
                 write('Tempo de geracao da solucao:'),write(TSol),nl.

caminho(Noi,Nof,LCaminho):-
	caminho(Noi,Nof,[],LCaminho).
	caminho(No,No,Lusadas,Lfinal):-reverse(Lusadas,Lfinal).
	caminho(No1,Nof,Lusadas,Lfinal):-
	liga(No1,No2,N),
	\+member((_,_,N),Lusadas),
	\+member((No2,_,_),Lusadas),
	\+member((_,No2,_),Lusadas),
	caminho(No2,Nof,[(No1,No2,N)|Lusadas],Lfinal).


gera_ligacoes:- retractall(liga(_,_,_)),
			findall(_,((no(_,No1,t,f,_,_);no(_,No1,f,t,_,_)),(no(_,No2,t,f,_,_);no(_,No2,f,t,_,_)),
			No1\==No2, linha(_,N,LNos,_,_),
			ordem_membros(No1,No2,LNos),
			assertz(liga(No1,No2,N))),_).


ordem_membros(No1,No2,[No1|L]):- member(No2,L),!.
ordem_membros(No1,No2,[_|L]):- ordem_membros(No1,No2,L).

:- dynamic melhor_sol_ntrocas/2.
plan_mud_mot1(Noi,Nof,LCaminho_menostrocas):-
                 get_time(Ti),
                 (melhor_caminho(Noi,Nof);true),
                 retract(melhor_sol_ntrocas(LCaminho_menostrocas,_)),
                 get_time(Tf),
                 TSol is Tf-Ti,
                 write('Tempo de geracao da solucao:'),write(TSol),nl.
melhor_caminho(Noi,Nof):-
                 asserta(melhor_sol_ntrocas(_,10000)),
                 caminho(Noi,Nof,LCaminho),
                 atualiza_melhor(LCaminho),
                 fail.
atualiza_melhor(LCaminho):-
                 melhor_sol_ntrocas(_,N),
                 length(LCaminho,C),
                 C<N,retract(melhor_sol_ntrocas(_,_)),
                 asserta(melhor_sol_ntrocas(LCaminho,C)).

			