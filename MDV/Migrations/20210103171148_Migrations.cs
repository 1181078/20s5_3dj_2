﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MDV.Migrations
{
    public partial class Migrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BlocosTrabalho",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    domainId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TempoInicio = table.Column<int>(type: "int", nullable: false),
                    TempoFim = table.Column<int>(type: "int", nullable: false),
                    NoInicio = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NoFim = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Viagens = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlocosTrabalho", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServicosTripulante",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    domainId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    nome = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    cor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    blocks = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServicosTripulante", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServicosViatura",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    domainId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    nome = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    cor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    blocks = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServicosViatura", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tripulantes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    numMecanografico = table.Column<int>(type: "int", nullable: false),
                    nome = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    dataDeNascimento = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    numCartaoCidadao = table.Column<int>(type: "int", nullable: false),
                    NIF = table.Column<int>(type: "int", nullable: false),
                    tipoTripulante = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    dataDeEntrada = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    dataDeSaida = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tripulantes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Viagens",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    domainId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    vazio = table.Column<bool>(type: "bit", nullable: false),
                    lineId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    pathId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    gerado = table.Column<bool>(type: "bit", nullable: false),
                    tempos = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Viagens", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Viaturas",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    matricula = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VIN = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    tipoViatura = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    dataEntrada = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Viaturas", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BlocosTrabalho");

            migrationBuilder.DropTable(
                name: "ServicosTripulante");

            migrationBuilder.DropTable(
                name: "ServicosViatura");

            migrationBuilder.DropTable(
                name: "Tripulantes");

            migrationBuilder.DropTable(
                name: "Viagens");

            migrationBuilder.DropTable(
                name: "Viaturas");
        }
    }
}
