using System;

namespace MDV1.Exceptions
{
    public class DuplicatedObjectException : Exception
    {
        public DuplicatedObjectException()
        {
        }

        public DuplicatedObjectException(string message) : base(message)
        {
        }
    }
}