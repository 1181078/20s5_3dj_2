﻿using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using MDV1.Infrastructure;
using MDV1.Infrastructure.Viaturas;
using MDV1.Infrastructure.BlocosTrabalho;
using MDV1.Infrastructure.Shared;
using MDV1.Domain.Shared;
using MDV1.Domain.Viaturas;
using MDV1.Domain.BlocosTrabalho;
using MDV1.Domain.ServicosViatura;
using MDV1.Domain.ServicosTripulante;
using MDV1.Domain.ReadFile;
using MDV1.Infrastructure.Viagens;
using MDV1.Domain.Viagens;
using MDV1.Infrastructure.Tripulantes;
using MDV1.Infrastructure.ServicosViatura;
using MDV1.Infrastructure.ServicosTripulante;
using MDV1.Domain.Tripulantes;
using MDV1.Infrastructure.AuthJwt;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace MDV1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {   
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
// This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            String dbConn = Configuration.GetSection("Persistence").GetValue<String>("ConnectionString");
            
            services.AddDbContext<MDV1DbContext>(opt =>
                opt.UseSqlServer(dbConn).ReplaceService<IValueConverterSelector, StronglyEntityIdValueConverterSelector>());
    

    

            ConfigureMyServices(services);

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });


            services.AddControllers().AddNewtonsoftJson();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }

        public void ConfigureMyServices(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IViaturaRepository, ViaturaRepository>();
            services.AddTransient<ViaturaService>();
            services.AddTransient<IBlocoTrabalhoRepository, BlocoTrabalhoRepository>();
            services.AddTransient<BlocoTrabalhoService>();
            services.AddTransient<IViagemRepository, ViagemRepository>();
            services.AddTransient<ViagemService>();
            services.AddTransient<ITripulanteRepository, TripulanteRepository>();
            services.AddTransient<TripulanteService>();
            services.AddTransient<IServicoViaturaRepository, ServicoViaturaRepository>();
            services.AddTransient<ServicoViaturaService>();
            services.AddTransient<IServicoTripulanteRepository, ServicoTripulanteRepository>();
            services.AddTransient<ServicoTripulanteService>();
            services.AddTransient<ReadFileService>();

            var jwtTokenConfig = Configuration.GetSection("jwtTokenConfig").Get<JwtTokenConfig>();
            services.AddSingleton(jwtTokenConfig);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = true;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = jwtTokenConfig.Issuer,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtTokenConfig.Secret)),
                    ValidAudience = jwtTokenConfig.Audience,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMinutes(1)
                };
            });
            
            services.AddSingleton<IJwtAuthManager, JwtAuthManager>();
            services.AddHostedService<JwtRefreshTokenCache>();
            services.AddScoped<IUserService, UserService>();
        }

    }
}
