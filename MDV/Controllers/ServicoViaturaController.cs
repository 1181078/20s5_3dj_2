
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV1.Domain.Shared;
using MDV1.Domain.ServicosViatura;
using MDV1.Infrastructure.AuthJwt;

namespace MDV1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServicosViaturaController : ControllerBase
    {
        private readonly ServicoViaturaService _service;

        public ServicosViaturaController(ServicoViaturaService service)
        {
            _service = service;
        }

        // GET: api/ServicosViatura
        [HttpGet]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<IEnumerable<ServicoViaturaDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/ServicosViatura/5
        [HttpGet("{id}")]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<ServicoViaturaDto>> GetGetById(String id)
        {
            var via = await _service.GetByIdAsync(new ServicoViaturaId(id));

            if (via == null)
            {
                return NotFound();
            }

            return via;
        }

        // POST: api/ServicosViatura
        [HttpPost]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<ServicoViaturaDto>> Create(ServicoViaturaDto dto)
        {
            var via = await _service.AddAsync(dto);

            return CreatedAtAction(nameof(GetGetById), new { id = via.Id }, via);
        }

        
        // PUT: api/ServicosViatura/5
        [HttpPut("{id}")]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<ServicoViaturaDto>> Update(String id, ServicoViaturaDto dto)
        {
            if (id != dto.Id)
            {
                return BadRequest();
            }

            try
            {
                var via = await _service.UpdateAsync(dto);
                
                if (via == null)
                {
                    return NotFound();
                }
                return Ok(via);
            }
            catch(BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        // // Inactivate: api/ServicosViatura/5
        // [HttpDelete("{id}")]
        // // [Authorize(Roles = UserRoles.Admin)]
        // public async Task<ActionResult<ViaturaDto>> SoftDelete(String id)
        // {
        //     var via = await _service.InactivateAsync(new ServicoViaturaId(id));

        //     if (via == null)
        //     {
        //         return NotFound();
        //     }

        //     return Ok(via);
        // }
        
        // DELETE: api/ServicosViatura/5
        [HttpDelete("{id}/hard")]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<ServicoViaturaDto>> HardDelete(String id)
        {
            try
            {
                var via = await _service.DeleteAsync(new ServicoViaturaId(id));

                if (via == null)
                {
                    return NotFound();
                }

                return Ok(via);
            }
            catch(BusinessRuleValidationException ex)
            {
               return BadRequest(new {Message = ex.Message});
            }
        }
    }
}