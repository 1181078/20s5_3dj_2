using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV1.Domain.Shared;
using MDV1.Domain.BlocosTrabalho;
using MDV1.Domain.ReadFile;
namespace MDV1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReadFileController : ControllerBase
    {
        private readonly ReadFileService _service;

        public ReadFileController(ReadFileService service)
        {
            _service = service;
        }

        // POST: api/BlocosTrabalho
        [HttpPost]
        public async Task<ActionResult<bool>> Create()
        {
            IFormFile xmlFile = Request.Form.Files[0];
            var via = await _service.AddAsync(xmlFile);
            return true;
        }

        
    }
}