
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV1.Domain.Shared;
using MDV1.Domain.ServicosTripulante;
using MDV1.Infrastructure.AuthJwt;

namespace MDV1.Controllers
{
    [Route("api/servicostripulante")]
    [ApiController]
    public class ServicosTripulanteController : ControllerBase
    {
        private readonly ServicoTripulanteService _service;

        public ServicosTripulanteController(ServicoTripulanteService service)
        {
            _service = service;
        }

        // GET: api/ServicosTripulante
        [HttpGet]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<IEnumerable<ServicoTripulanteDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/ServicosTripulante/5
        [HttpGet("{id}")]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<ServicoTripulanteDto>> GetGetById(String id)
        {
            var via = await _service.GetByIdAsync(new ServicoTripulanteId(id));

            if (via == null)
            {
                return NotFound();
            }

            return via;
        }

        // POST: api/ServicosTripulante
        [HttpPost]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<ServicoTripulanteDto>> Create(ServicoTripulanteDto dto)
        {
            var via = await _service.AddAsync(dto);

            return CreatedAtAction(nameof(GetGetById), new { id = via.Id }, via);
        }

        
        // PUT: api/ServicosTripulante/5
        [HttpPut("{id}")]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<ServicoTripulanteDto>> Update(String id, ServicoTripulanteDto dto)
        {
            if (id != dto.Id)
            {
                return BadRequest();
            }

            try
            {
                var via = await _service.UpdateAsync(dto);
                
                if (via == null)
                {
                    return NotFound();
                }
                return Ok(via);
            }
            catch(BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        // // Inactivate: api/ServicosTripulante/5
        // [HttpDelete("{id}")]
        // // [Authorize(Roles = UserRoles.Admin)]
        // public async Task<ActionResult<TripulanteDto>> SoftDelete(String id)
        // {
        //     var via = await _service.InactivateAsync(new ServicoTripulanteId(id));

        //     if (via == null)
        //     {
        //         return NotFound();
        //     }

        //     return Ok(via);
        // }
        
        // DELETE: api/ServicosTripulante/5
        [HttpDelete("{id}/hard")]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<ServicoTripulanteDto>> HardDelete(String id)
        {
            try
            {
                var via = await _service.DeleteAsync(new ServicoTripulanteId(id));

                if (via == null)
                {
                    return NotFound();
                }

                return Ok(via);
            }
            catch(BusinessRuleValidationException ex)
            {
               return BadRequest(new {Message = ex.Message});
            }
        }
    }
}