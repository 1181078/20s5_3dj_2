
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV1.Domain.Shared;
using MDV1.Domain.Viaturas;
using MDV1.Infrastructure.AuthJwt;

namespace MDV1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViaturasController : ControllerBase
    {
        private readonly ViaturaService _service;

        public ViaturasController(ViaturaService service)
        {
            _service = service;
        }

        // GET: api/Viaturas
        [HttpGet]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<IEnumerable<ViaturaDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/Viaturas/5
        [HttpGet("{id}")]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<ViaturaDto>> GetGetById(String id)
        {
            var via = await _service.GetByIdAsync(new ViaturaId(id));

            if (via == null)
            {
                return NotFound();
            }

            return via;
        }

        // POST: api/Viaturas
        [HttpPost]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<ViaturaDto>> Create(ViaturaDto dto)
        {
            var via = await _service.AddAsync(dto);

            return CreatedAtAction(nameof(GetGetById), new { id = via.Id }, via);
        }

        
        // PUT: api/Viaturas/5
        [HttpPut("{id}")]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<ViaturaDto>> Update(String id, ViaturaDto dto)
        {
            if (id != dto.Id)
            {
                return BadRequest();
            }

            try
            {
                var via = await _service.UpdateAsync(dto);
                
                if (via == null)
                {
                    return NotFound();
                }
                return Ok(via);
            }
            catch(BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        // // Inactivate: api/Viaturas/5
        // [HttpDelete("{id}")]
        // // [Authorize(Roles = UserRoles.Admin)]
        // public async Task<ActionResult<ViaturaDto>> SoftDelete(String id)
        // {
        //     var via = await _service.InactivateAsync(new ViaturaId(id));

        //     if (via == null)
        //     {
        //         return NotFound();
        //     }

        //     return Ok(via);
        // }
        
        // DELETE: api/Viaturas/5
        [HttpDelete("{id}/hard")]
        // [Authorize(Roles = UserRoles.Admin)]
        public async Task<ActionResult<ViaturaDto>> HardDelete(String id)
        {
            try
            {
                var via = await _service.DeleteAsync(new ViaturaId(id));

                if (via == null)
                {
                    return NotFound();
                }

                return Ok(via);
            }
            catch(BusinessRuleValidationException ex)
            {
               return BadRequest(new {Message = ex.Message});
            }
        }
    }
}