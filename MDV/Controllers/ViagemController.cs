using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV1.Domain.Shared;
//using MDV1.Domain.Viaturas;
using MDV1.Domain.Viagens;

namespace MDV1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViagensController : ControllerBase
    {
        private readonly ViagemService _service;

        public ViagensController(ViagemService service)
        {
            _service = service;
        }

        // GET: api/viagem
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViagemDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/viagem/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ViagemDto>> GetGetById(String id)
        {
            var viagem = await _service.GetByIdAsync(new ViagemId(id));

            if (viagem == null)
            {
                return NotFound();
            }

            return viagem;
        }

        // POST: api/viagem
        [HttpPost]
        public async Task<ActionResult<ViagemDto>> Create(ViagemDto dto)
        {
            var viagem = await _service.AddAsync(dto);

            return CreatedAtAction(nameof(GetGetById), new { id = viagem.Id }, viagem);
        }

        
        // PUT: api/viagem/5
        [HttpPut("{id}")]
        public async Task<ActionResult<ViagemDto>> Update(String id, ViagemDto dto)
        {
            if (id != dto.Id)
            {
                return BadRequest();
            }

            try
            {
                var viagem = await _service.UpdateAsync(dto);
                
                if (viagem == null)
                {
                    return NotFound();
                }
                return Ok(viagem);
            }
            catch(BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        // Inactivate: api/viagem/5
        
        
        // DELETE: api/viagem/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<ViagemDto>> HardDelete(String id)
        {
            try
            {
                var viagem = await _service.DeleteAsync(new ViagemId(id));

                if (viagem == null)
                {
                    return NotFound();
                }

                return Ok(viagem);
            }
            catch(BusinessRuleValidationException ex)
            {
               return BadRequest(new {Message = ex.Message});
            }
        }
    }
}