using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV1.Domain.Shared;
using MDV1.Domain.Tripulantes;

namespace MDV1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TripulantesController : ControllerBase
    {
        private readonly TripulanteService _service;

        public TripulantesController(TripulanteService service)
        {
            _service = service;
        }

        // GET: api/Tripulantes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TripulanteDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/Tripulantes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TripulanteDto>> GetGetById(int id)
        {
            var via = await _service.GetByIdAsync(new TripulanteId(id.ToString()));

            if (via == null)
            {
                return NotFound();
            }

            return via;
        }

        // POST: api/Tripulantes
        [HttpPost]
        public async Task<ActionResult<TripulanteDto>> Create(TripulanteDto dto)
        {
            var via = await _service.AddAsync(dto);

            return CreatedAtAction(nameof(GetGetById), new { id = via.Id }, via);
        }

        
        // PUT: api/Tripulantes/5
        [HttpPut("{id}")]
        public async Task<ActionResult<TripulanteDto>> Update(int id, TripulanteDto dto)
        {
            if (id.ToString() != dto.Id)
            {
                return BadRequest();
            }

            try
            {
                var via = await _service.UpdateAsync(dto);
                
                if (via == null)
                {
                    return NotFound();
                }
                return Ok(via);
            }
            catch(BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        // DELETE: api/Tripulantes/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<TripulanteDto>> HardDelete(int id)
        {
            try
            {
                var via = await _service.DeleteAsync(new TripulanteId(id.ToString()));

                if (via == null)
                {
                    return NotFound();
                }

                return Ok(via);
            }
            catch(BusinessRuleValidationException ex)
            {
               return BadRequest(new {Message = ex.Message});
            }
        }
    }
}