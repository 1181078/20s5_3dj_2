using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV1.Domain.Shared;
using MDV1.Domain.BlocosTrabalho;

namespace MDV1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlocosTrabalhoController : ControllerBase
    {
        private readonly BlocoTrabalhoService _service;

        public BlocosTrabalhoController(BlocoTrabalhoService service)
        {
            _service = service;
        }

        // GET: api/BlocosTrabalho
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BlocoTrabalhoDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/BlocosTrabalho/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BlocoTrabalhoDto>> GetGetById(String id)
        {
            var via = await _service.GetByIdAsync(new BlocoTrabalhoId(id));

            if (via == null)
            {
                return NotFound();
            }

            
            Console.WriteLine(via);

            return via;

        }

        // POST: api/BlocosTrabalho
        [HttpPost]
        public async Task<ActionResult<BlocoTrabalhoDto>> Create(BlocoTrabalhoDto dto)
        {
            var via = await _service.AddAsync(dto);
            
            Console.WriteLine(via);
            Console.WriteLine(dto);

            return CreatedAtAction(nameof(GetGetById), new { id = via.Id }, via);
        }

        
        // PUT: api/BlocosTrabalho/5
        [HttpPut("{id}")]
        public async Task<ActionResult<BlocoTrabalhoDto>> Update(String id, BlocoTrabalhoDto dto)
        {
            if (id != dto.Id)
            {
                return BadRequest();
            }

            try
            {
                var via = await _service.UpdateAsync(dto);
                
                if (via == null)
                {
                    return NotFound();
                }
                return Ok(via);
            }
            catch(BusinessRuleValidationException ex)
            {
                return BadRequest(new {Message = ex.Message});
            }
        }

        // Inactivate: api/BlocosTrabalho/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<BlocoTrabalhoDto>> SoftDelete(String id)
        {
            var via = await _service.InactivateAsync(new BlocoTrabalhoId(id));

            if (via == null)
            {
                return NotFound();
            }

            return Ok(via);
        }
        
        // DELETE: api/BlocosTrabalho/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<BlocoTrabalhoDto>> HardDelete(String id)
        {
            try
            {
                var via = await _service.DeleteAsync(new BlocoTrabalhoId(id));

                if (via == null)
                {
                    return NotFound();
                }

                return Ok(via);
            }
            catch(BusinessRuleValidationException ex)
            {
               return BadRequest(new {Message = ex.Message});
            }
        }
    }
}