
using MDV1.Domain.Shared;

namespace MDV1.Domain.Tripulantes
{
    public interface ITripulanteRepository: IRepository<Tripulante, TripulanteId>
    {
    }
}