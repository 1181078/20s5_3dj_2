using System;

namespace MDV1.Domain.Tripulantes
{
    public class TripulanteDto
    {
        public String Id { get; set; }

        public int numMecanografico { get;  set; }

        public string nome { get;   set; }


        public string dataDeNascimento { get;   set; }

        public int numCartaoCidadao { get;   set; }

        public int NIF { get;   set; }
        
        public string tipoTripulante { get;   set; }
        public string dataDeEntrada { get;   set; }
        public string dataDeSaida { get;   set; }

        public override bool Equals(object obj)
        {
            return (this.Id == ((TripulanteDto)obj).Id || this.numMecanografico == ((TripulanteDto)obj).numMecanografico
            || this.nome == ((TripulanteDto)obj).nome || this.dataDeNascimento == ((TripulanteDto)obj).dataDeNascimento
            || this.numCartaoCidadao == ((TripulanteDto)obj).numCartaoCidadao || this.NIF == ((TripulanteDto)obj).NIF 
            || this.tipoTripulante == ((TripulanteDto)obj).tipoTripulante || this.dataDeEntrada == ((TripulanteDto)obj).dataDeEntrada
            || this.dataDeSaida == ((TripulanteDto)obj).dataDeSaida);
        }

    }
}