using System.Threading.Tasks;
using System.Collections.Generic;
using MDV1.Domain.Shared;
using System;


namespace MDV1.Domain.Tripulantes
{
    public class TripulanteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITripulanteRepository _repo;

        public TripulanteService(IUnitOfWork unitOfWork, ITripulanteRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<TripulanteDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

           List<TripulanteDto> listDto = list.ConvertAll<TripulanteDto>(Tripulante => new TripulanteDto { Id = Tripulante.Id.Value ,numMecanografico = Tripulante.numMecanografico,nome = Tripulante.nome,dataDeNascimento = Tripulante.dataDeNascimento,numCartaoCidadao = Tripulante.numCartaoCidadao,NIF = Tripulante.NIF,tipoTripulante=Tripulante.tipoTripulante,dataDeEntrada= Tripulante.dataDeEntrada,dataDeSaida = Tripulante.dataDeSaida });

            return listDto;
        }

        public async Task<TripulanteDto> GetByIdAsync(TripulanteId id)
        {
            var Tripulante = await this._repo.GetByIdAsync(id);

            if (Tripulante == null)
                return null;

            return new TripulanteDto { Id = Tripulante.Id.Value,numMecanografico = Tripulante.numMecanografico,nome = Tripulante.nome,dataDeNascimento = Tripulante.dataDeNascimento,numCartaoCidadao = Tripulante.numCartaoCidadao,NIF = Tripulante.NIF,tipoTripulante=Tripulante.tipoTripulante,dataDeEntrada= Tripulante.dataDeEntrada,dataDeSaida = Tripulante.dataDeSaida };
        }

        public async Task<TripulanteDto> AddAsync(TripulanteDto dto)
        {
            var Tripulante = new Tripulante(dto.numMecanografico, dto.nome, dto.dataDeNascimento, dto.numCartaoCidadao, dto.NIF, dto.tipoTripulante, dto.dataDeEntrada, dto.dataDeSaida);

            await this._repo.AddAsync(Tripulante);

            await this._unitOfWork.CommitAsync();

           return new TripulanteDto { Id = Tripulante.Id.Value,numMecanografico = Tripulante.numMecanografico,nome = Tripulante.nome,dataDeNascimento = Tripulante.dataDeNascimento,numCartaoCidadao = Tripulante.numCartaoCidadao,NIF = Tripulante.NIF,tipoTripulante=Tripulante.tipoTripulante,dataDeEntrada= Tripulante.dataDeEntrada,dataDeSaida = Tripulante.dataDeSaida };
        }

        public async Task<TripulanteDto> UpdateAsync(TripulanteDto dto)
        {
            var Tripulante = await this._repo.GetByIdAsync(new TripulanteId(dto.Id));

            if (Tripulante == null)
                return null;

            // change all field
            Tripulante.mudaNome(dto.nome);
            Tripulante.mudaNascimento(dto.dataDeNascimento);
            Tripulante.mudaCartaoCidadao(dto.numCartaoCidadao);
            Tripulante.mudaNIF(dto.NIF);
            Tripulante.mudarTipo(dto.tipoTripulante);
            Tripulante.mudarDataDeEntrada(dto.dataDeEntrada);
            Tripulante.mudarDataDeSaida(dto.dataDeSaida);

            await this._unitOfWork.CommitAsync();

            return new TripulanteDto { Id = Tripulante.Id.Value,
             numMecanografico = Tripulante.numMecanografico,nome = Tripulante.nome,
             dataDeNascimento = Tripulante.dataDeNascimento,
             numCartaoCidadao = Tripulante.numCartaoCidadao,NIF = Tripulante.NIF,
             tipoTripulante=Tripulante.tipoTripulante,dataDeEntrada= Tripulante.dataDeEntrada,dataDeSaida =Tripulante.dataDeSaida };
        }
        public async Task<TripulanteDto> DeleteAsync(TripulanteId id)
        {
            var Tripulante = await this._repo.GetByIdAsync(id);

            if (Tripulante == null)
                return null;

            this._repo.Remove(Tripulante);
            await this._unitOfWork.CommitAsync();

            return new TripulanteDto { Id = Tripulante.Id.Value,numMecanografico = Tripulante.numMecanografico,nome = Tripulante.nome,dataDeNascimento = Tripulante.dataDeNascimento,numCartaoCidadao = Tripulante.numCartaoCidadao,NIF = Tripulante.NIF,tipoTripulante=Tripulante.tipoTripulante,dataDeEntrada= Tripulante.dataDeEntrada,dataDeSaida = Tripulante.dataDeSaida };
        }
    }
}