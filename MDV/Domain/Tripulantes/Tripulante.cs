using System;
using MDV1.Domain.Shared;


namespace MDV1.Domain.Tripulantes
{
    public class Tripulante : Entity<TripulanteId>, IAggregateRoot
    {

        public int numMecanografico { get; private set; }
        public string nome { get; private set; }
        public string dataDeNascimento { get; private set; }
        public int numCartaoCidadao { get; private set; }
        public int NIF { get; private set; } 
        public string tipoTripulante { get; private set; }
        public string dataDeEntrada { get; private set; }
        public string dataDeSaida { get; private set; }



        private Tripulante()
        {
            
        }


        public Tripulante(int numMecanografico, string nome, string dataDeNascimento, int numCartaoCidadao, int NIF,string tipoTripulante ,string dataDeEntrada, string dataDeSaida)
        {
            this.Id = new TripulanteId(numMecanografico.ToString());
            this.numMecanografico = numMecanografico;
            this.nome = nome;
            this.dataDeNascimento = dataDeNascimento;
            this.numCartaoCidadao = numCartaoCidadao;
            this.NIF=NIF;
            this.tipoTripulante = tipoTripulante;
            this.dataDeEntrada = dataDeEntrada;
            this.dataDeSaida = dataDeSaida;
        }

        public string mudaNome(string nome){
            this.nome = nome;
            return nome;
        }
        public string mudaNascimento(string nascimento){
            this.dataDeNascimento = nascimento;
            return nascimento;
        }

        public int mudaCartaoCidadao(int cartao){
            this.numCartaoCidadao = cartao;
            return cartao;
        }

        public int mudaNIF(int NIF){
            this.NIF = NIF;
            return NIF;
        }
        public string mudarTipo(string tipo)
        {
            this.tipoTripulante = tipo;
            return tipo;
        }

        public string mudarDataDeEntrada(string date)
        {
            this.dataDeEntrada = date;
            return date;
        }

        public string mudarDataDeSaida(string date)
        {
            this.dataDeSaida= date;
            return date;
        }

        public override bool Equals(object obj)
        {
            return (this.numMecanografico == ((Tripulante)obj).numMecanografico
            || this.nome == ((Tripulante)obj).nome || this.dataDeNascimento == ((Tripulante)obj).dataDeNascimento
            || this.numCartaoCidadao == ((Tripulante)obj).numCartaoCidadao || this.NIF == ((Tripulante)obj).NIF 
            || this.tipoTripulante == ((Tripulante)obj).tipoTripulante || this.dataDeEntrada == ((Tripulante)obj).dataDeEntrada
            || this.dataDeSaida == ((Tripulante)obj).dataDeSaida);
        }

    }
}


