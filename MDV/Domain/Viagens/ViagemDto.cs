using System;
using System.Collections.Generic;

namespace MDV1.Domain.Viagens{

    public class ViagemDto{
        public String Id { get; set;}
        public string domainId { get; set; }
        public bool vazio { get; set; }
        public string lineId { get; set; }
        public string pathId { get; set; }
        public bool gerado {get; set;}
        public int[]  tempos { get; set; }

        public bool Equal(object obj)
        {
            return (this.Id == ((ViagemDto)obj).Id || this.domainId == ((ViagemDto)obj).domainId
            || this.vazio == ((ViagemDto)obj).vazio || this.lineId == ((ViagemDto)obj).lineId
            || this.pathId == ((ViagemDto)obj).pathId || this.gerado == ((ViagemDto)obj).gerado 
            || this.tempos == ((ViagemDto)obj).tempos);
        }
    }
}