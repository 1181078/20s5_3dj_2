using MDV1.Domain.Shared;

namespace MDV1.Domain.Viagens
{
    public interface IViagemRepository: IRepository<Viagem, ViagemId>
    {
    }
}