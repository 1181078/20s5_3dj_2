using System;
using MDV1.Domain.Shared;
using System.Collections.Generic;

namespace MDV1.Domain.Viagens
{
    public class Viagem : Entity<ViagemId>, IAggregateRoot
    {

        public string domainId { get; set; }
        public bool vazio { get; set; }
        public string lineId { get; set; }
        public string pathId { get; set; }
        public bool gerado {get; set;}
        public int[] tempos { get; set; }

        public Viagem(){

        }

        public Viagem(string domainId, bool vazio,string lineId, string pathId,
         bool gerado, int[]  tempos)
        {
            this.Id = new ViagemId(domainId);
            this.domainId = domainId;
            this.vazio = vazio;
            this.lineId = lineId;
            this.pathId = pathId;
            this.gerado = gerado;
            this.tempos = tempos;
        }

        public bool mudaVazio(bool vazio){
            this.vazio = vazio;
            return vazio;
        }
        public string mudaLine(string line){
            this.lineId = line;
            return line;
        }
        public string mudaPath(string path){
            this.pathId = path;
            return path;
        }
        public bool mudaGerado(bool gerado){
            this.gerado = gerado;
            return gerado;
        }

        public int[] mudarTempos(int[] tempos){
            this.tempos = tempos;
            return tempos;
        }

        public override bool Equals(object obj)
        {
            return (this.domainId == ((Viagem)obj).domainId
            || this.vazio == ((Viagem)obj).vazio || this.lineId == ((Viagem)obj).lineId
            || this.pathId == ((Viagem)obj).pathId || this.gerado == ((Viagem)obj).gerado 
            || this.tempos == ((Viagem)obj).tempos);
        }

    }
}



