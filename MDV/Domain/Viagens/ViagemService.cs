using System.Threading.Tasks;
using System.Collections.Generic;
using MDV1.Domain.Shared;

namespace MDV1.Domain.Viagens
{
    public class ViagemService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IViagemRepository _repo;

        public ViagemService(IUnitOfWork unitOfWork, IViagemRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<ViagemDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<ViagemDto> listDto = list.ConvertAll<ViagemDto>(via =>
             new ViagemDto { Id = via.Id.AsString(), domainId = via.domainId, vazio = via.vazio,
              lineId = via.lineId, pathId = via.pathId,
              gerado = via.gerado, tempos = via.tempos});

            return listDto;
        }

        public async Task<ViagemDto> GetByIdAsync(ViagemId id)
        {
            var via = await this._repo.GetByIdAsync(id);

            if (via == null)
                return null;

            return new ViagemDto { Id = via.Id.AsString(), domainId = via.domainId, vazio = via.vazio,
              lineId = via.lineId, pathId = via.pathId,
              gerado = via.gerado, tempos = via.tempos};
        }

        public async Task<ViagemDto> AddAsync(ViagemDto dto)
        {
            var via = new Viagem(dto.domainId, dto.vazio,
            dto.lineId,dto.pathId,dto.gerado, dto.tempos);

            await this._repo.AddAsync(via);

            await this._unitOfWork.CommitAsync();

            return new ViagemDto  { Id = via.Id.AsString(), domainId = via.domainId, vazio = via.vazio,
             lineId = via.lineId, pathId = via.pathId,
              gerado = via.gerado, tempos = via.tempos};
        }
        public async Task<ViagemDto> UpdateAsync(ViagemDto dto)
        {
            var via = await this._repo.GetByIdAsync(new ViagemId(dto.Id));

            if (via == null)
                return null;


            via.mudaVazio(dto.vazio);
            via.mudaLine(dto.lineId);
            via.mudaPath(dto.pathId);
            via.mudaGerado(dto.gerado);
            via.mudarTempos(dto.tempos);

            await this._unitOfWork.CommitAsync();

            return new ViagemDto {  Id = via.Id.AsString(), domainId = via.domainId, vazio = via.vazio,
              lineId = via.lineId, pathId = via.pathId,
              gerado = via.gerado, tempos = via.tempos};
        }

        public async Task<ViagemDto> DeleteAsync(ViagemId id)
        {
            var via = await this._repo.GetByIdAsync(id);

            if (via == null)
                return null;

            this._repo.Remove(via);
            await this._unitOfWork.CommitAsync();

             return new ViagemDto {  Id = via.Id.AsString(), domainId = via.domainId, vazio = via.vazio,
              lineId = via.lineId, pathId = via.pathId,
              gerado = via.gerado, tempos = via.tempos};
        }
    }
}