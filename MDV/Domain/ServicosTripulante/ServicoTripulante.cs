using System;
using MDV1.Domain.Shared;
using System.Collections.Generic;

namespace MDV1.Domain.ServicosTripulante
{
    public class ServicoTripulante : Entity<ServicoTripulanteId>, IAggregateRoot
    {
        public string domainId { get; set; }
        public string nome { get; set; }
        public string cor { get; set; }
        public string[] blocks {get; set;}

        public ServicoTripulante(){

        }

        public ServicoTripulante(string domainId,string nome, string cor, string[] blocks)
        {
            this.Id = new ServicoTripulanteId(domainId);
            this.domainId = domainId;
            this.nome = nome;
            this.cor = cor;
            this.blocks = blocks;
        }

        public string mudaNome(string nome){
            this.nome = nome;
            return nome;
        }
        public string mudaCor(string cor){
            this.cor = cor;
            return cor;
        }

        public string[] mudaBlocks(string[] blocks){
            this.blocks = blocks;
            return blocks;
        }

        public override bool Equals(object obj)
        {
            return (this.domainId == ((ServicoTripulante)obj).domainId
            || this.nome == ((ServicoTripulante)obj).nome || this.cor == ((ServicoTripulante)obj).cor
            || this.blocks == ((ServicoTripulante)obj).blocks);
        }


    }
}



