using System;
using System.Collections.Generic;

namespace MDV1.Domain.ServicosTripulante{

    public class ServicoTripulanteDto{
        public String Id { get; set;}
        public string domainId { get; set; }
        public string nome { get; set; }
        public string cor { get; set; }
        public string[] blocks {get; set;}

        public bool Equal(object obj)
        {
            return (this.Id == ((ServicoTripulanteDto)obj).Id || this.domainId == ((ServicoTripulanteDto)obj).domainId
            || this.nome == ((ServicoTripulanteDto)obj).nome || this.cor == ((ServicoTripulanteDto)obj).cor
            || this.blocks == ((ServicoTripulanteDto)obj).blocks);
        }
    }
}