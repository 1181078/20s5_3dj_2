using System.Threading.Tasks;
using System.Collections.Generic;
using MDV1.Domain.Shared;

namespace MDV1.Domain.ServicosTripulante
{
    public class ServicoTripulanteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServicoTripulanteRepository _repo;

        public ServicoTripulanteService(IUnitOfWork unitOfWork, IServicoTripulanteRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<ServicoTripulanteDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<ServicoTripulanteDto> listDto = list.ConvertAll<ServicoTripulanteDto>(via =>
             new ServicoTripulanteDto { Id = via.Id.AsString(), domainId = via.domainId, nome = via.nome,
              cor = via.cor, blocks = via.blocks});

            return listDto;
        }

        public async Task<ServicoTripulanteDto> GetByIdAsync(ServicoTripulanteId id)
        {
            var via = await this._repo.GetByIdAsync(id);

            if (via == null)
                return null;

            return new ServicoTripulanteDto { Id = via.Id.AsString(), domainId = via.domainId, nome = via.nome,
              cor = via.cor, blocks = via.blocks};
        }

        public async Task<ServicoTripulanteDto> AddAsync(ServicoTripulanteDto dto)
        {
            var via = new ServicoTripulante(dto.domainId, dto.nome,
            dto.cor,dto.blocks);

            await this._repo.AddAsync(via);

            await this._unitOfWork.CommitAsync();

            return new ServicoTripulanteDto  { Id = via.Id.AsString(), domainId = via.domainId, nome = via.nome,
             cor = via.cor, blocks = via.blocks};
        }
        public async Task<ServicoTripulanteDto> UpdateAsync(ServicoTripulanteDto dto)
        {
            var via = await this._repo.GetByIdAsync(new ServicoTripulanteId(dto.Id));

            if (via == null)
                return null;

            via.mudaNome(dto.nome);
            via.mudaCor(dto.cor);
            via.mudaBlocks(dto.blocks);

            await this._unitOfWork.CommitAsync();

            return new ServicoTripulanteDto {  Id = via.Id.AsString(), domainId = via.domainId, nome = via.nome,
              cor = via.cor, blocks = via.blocks};
        }

        public async Task<ServicoTripulanteDto> DeleteAsync(ServicoTripulanteId id)
        {
            var via = await this._repo.GetByIdAsync(id);

            if (via == null)
                return null;

            this._repo.Remove(via);
            await this._unitOfWork.CommitAsync();

             return new ServicoTripulanteDto {  Id = via.Id.AsString(), domainId = via.domainId, nome = via.nome,
              cor = via.cor, blocks = via.blocks};
        }
    }
}