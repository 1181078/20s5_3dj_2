using MDV1.Domain.Shared;

namespace MDV1.Domain.ServicosTripulante
{
    public interface IServicoTripulanteRepository: IRepository<ServicoTripulante, ServicoTripulanteId>
    {
    }
}