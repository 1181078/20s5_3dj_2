using System.Threading.Tasks;

namespace MDV1.Domain.Shared
{
    public interface IUnitOfWork
    {
        Task<int> CommitAsync();
    }
}