using System;
using System.Collections.Generic;
using MDV1.Domain.Shared;

namespace MDV1.Domain.BlocosTrabalho
{
    public class BlocoTrabalho : Entity<BlocoTrabalhoId>, IAggregateRoot
    {

        public string domainId { get; private set; }

        public int TempoInicio { get; private set; }

        public int TempoFim { get; private set; }

        public string NoInicio { get; private set; }

        public string NoFim { get; private set; }

        public string[] Viagens { get; private set; }

        public bool Active { get; private set; }


        private BlocoTrabalho()
        {
            this.Active = true;
        }


        public BlocoTrabalho(string domainId, int TempoInicio, int TempoFim, string NoInicio, string NoFim, string[] Viagens)
        {
            this.Id = new BlocoTrabalhoId(domainId);
            this.domainId = domainId;
            this.TempoInicio = TempoInicio;
            this.TempoFim = TempoFim;
            this.NoInicio = NoInicio;
            this.NoFim = NoFim;
            this.Viagens = Viagens;
            this.Active = true;
        }

        public int mudarTempoInicio(int tempo)
        {
            this.TempoInicio = tempo;
            return tempo;
        }

        public int mudarTempoFim(int tempo)
        {
            this.TempoFim = tempo;
            return tempo;
        }

        public string mudarNoInicio(string no)
        {
            this.NoInicio = no;
            return no;
        }

        public string mudarNoFim(string no)
        {
            this.NoFim = no;
            return no;
        }

        public string[] mudarViagens(string[] via)
        {
            this.Viagens = via;
            return via;
        }

        public void MarkAsInative()
        {
            this.Active = false;
        }


        public override bool Equals(object obj)
        {
            return (this.domainId == ((BlocoTrabalho)obj).domainId
            || this.TempoInicio == ((BlocoTrabalho)obj).TempoInicio || this.TempoFim == ((BlocoTrabalho)obj).TempoFim
            || this.NoInicio == ((BlocoTrabalho)obj).NoInicio || this.NoFim == ((BlocoTrabalho)obj).NoFim 
            || this.Viagens == ((BlocoTrabalho)obj).Viagens );
        }

    }
}


