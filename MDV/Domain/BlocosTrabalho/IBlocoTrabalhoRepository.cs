
using MDV1.Domain.Shared;

namespace MDV1.Domain.BlocosTrabalho
{
    public interface IBlocoTrabalhoRepository: IRepository<BlocoTrabalho, BlocoTrabalhoId>
    {
    }
}