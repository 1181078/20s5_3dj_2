using System.Threading.Tasks;
using System.Collections.Generic;
using MDV1.Domain.Shared;

namespace MDV1.Domain.BlocosTrabalho
{
    public class BlocoTrabalhoService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBlocoTrabalhoRepository _repo;

        public BlocoTrabalhoService(IUnitOfWork unitOfWork, IBlocoTrabalhoRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<BlocoTrabalhoDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<BlocoTrabalhoDto> listDto = list.ConvertAll<BlocoTrabalhoDto>(via => new BlocoTrabalhoDto { Id = via.Id.AsString(), domainId = via.domainId, TempoInicio = via.TempoInicio, TempoFim = via.TempoFim, NoInicio = via.NoInicio, NoFim = via.NoFim, Viagens = via.Viagens });

            return listDto;
        }

        public async Task<BlocoTrabalhoDto> GetByIdAsync(BlocoTrabalhoId id)
        {
            var via = await this._repo.GetByIdAsync(id);

            if (via == null)
                return null;

            return new BlocoTrabalhoDto { Id = via.Id.AsString(),
             domainId= via.domainId, TempoInicio = via.TempoInicio, 
             TempoFim = via.TempoFim, NoInicio = via.NoInicio,
             NoFim = via.NoFim, Viagens = via.Viagens  };
        }

        public async Task<BlocoTrabalhoDto> AddAsync(BlocoTrabalhoDto dto)
        {
            var blocoTrabalho = new BlocoTrabalho(dto.domainId, dto.TempoInicio, dto.TempoFim, dto.NoInicio, dto.NoFim, dto.Viagens);

            await this._repo.AddAsync(blocoTrabalho);

            await this._unitOfWork.CommitAsync();

            return new BlocoTrabalhoDto { Id = blocoTrabalho.Id.AsString(),
             domainId = blocoTrabalho.domainId, TempoInicio = blocoTrabalho.TempoInicio,
              TempoFim = blocoTrabalho.TempoFim, NoInicio = blocoTrabalho.NoInicio,
              NoFim = blocoTrabalho.NoFim, Viagens = blocoTrabalho.Viagens  };
        }

        public async Task<BlocoTrabalhoDto> UpdateAsync(BlocoTrabalhoDto dto)
        {
            var blocoTrabalho = await this._repo.GetByIdAsync(new BlocoTrabalhoId(dto.Id));

            if (blocoTrabalho == null)
                return null;

            // change all field
            blocoTrabalho.mudarNoInicio(dto.NoInicio);
            blocoTrabalho.mudarNoFim(dto.NoFim);
            blocoTrabalho.mudarTempoFim(dto.TempoFim);
            blocoTrabalho.mudarTempoInicio(dto.TempoInicio);
            blocoTrabalho.mudarViagens(dto.Viagens);

            await this._unitOfWork.CommitAsync();

            return new BlocoTrabalhoDto { Id = blocoTrabalho.Id.AsString(),
             domainId = blocoTrabalho.domainId, TempoInicio = blocoTrabalho.TempoInicio,
              TempoFim = blocoTrabalho.TempoFim, NoInicio = blocoTrabalho.NoInicio,
              NoFim = blocoTrabalho.NoFim, Viagens = blocoTrabalho.Viagens  };
        }


        public async Task<BlocoTrabalhoDto> InactivateAsync(BlocoTrabalhoId id)
        {
            var blocoTrabalho = await this._repo.GetByIdAsync(id);

            if (blocoTrabalho == null)
                return null;

            // change all fields
            blocoTrabalho.MarkAsInative();

            await this._unitOfWork.CommitAsync();

            return new BlocoTrabalhoDto { Id = blocoTrabalho.Id.AsString(),
             domainId = blocoTrabalho.domainId, TempoInicio = blocoTrabalho.TempoInicio,
              TempoFim = blocoTrabalho.TempoFim, NoInicio = blocoTrabalho.NoInicio,
              NoFim = blocoTrabalho.NoFim, Viagens = blocoTrabalho.Viagens };
        }

        public async Task<BlocoTrabalhoDto> DeleteAsync(BlocoTrabalhoId id)
        {
            var blocoTrabalho = await this._repo.GetByIdAsync(id);

            if (blocoTrabalho == null)
                return null;

            this._repo.Remove(blocoTrabalho);
            await this._unitOfWork.CommitAsync();

            return new BlocoTrabalhoDto { Id = blocoTrabalho.Id.AsString(),
             domainId = blocoTrabalho.domainId, TempoInicio = blocoTrabalho.TempoInicio,
              TempoFim = blocoTrabalho.TempoFim, NoInicio = blocoTrabalho.NoInicio,
              NoFim = blocoTrabalho.NoFim, Viagens = blocoTrabalho.Viagens };
        }
    }
}