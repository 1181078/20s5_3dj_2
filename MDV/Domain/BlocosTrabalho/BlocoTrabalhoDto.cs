using System;
using System.Collections.Generic;

namespace MDV1.Domain.BlocosTrabalho
{
    public class BlocoTrabalhoDto
    {
        public String Id { get; set; }

        public string domainId { get; set; }

        public int TempoInicio { get; set; }

        public int TempoFim { get; set; }

        public string NoInicio { get; set; }

        public string NoFim { get; set; }

        public string[] Viagens { get; set; }

        public bool Equal(object obj)
        {
            return (this.Id == ((BlocoTrabalhoDto)obj).Id || this.domainId == ((BlocoTrabalhoDto)obj).domainId
            || this.TempoInicio == ((BlocoTrabalhoDto)obj).TempoInicio || this.TempoFim == ((BlocoTrabalhoDto)obj).TempoFim
            || this.NoInicio == ((BlocoTrabalhoDto)obj).NoInicio || this.NoFim == ((BlocoTrabalhoDto)obj).NoFim 
            || this.Viagens == ((BlocoTrabalhoDto)obj).Viagens );
        }

    }
}