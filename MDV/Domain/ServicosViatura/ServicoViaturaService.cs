using System.Threading.Tasks;
using System.Collections.Generic;
using MDV1.Domain.Shared;

namespace MDV1.Domain.ServicosViatura
{
    public class ServicoViaturaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServicoViaturaRepository _repo;

        public ServicoViaturaService(IUnitOfWork unitOfWork, IServicoViaturaRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<ServicoViaturaDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<ServicoViaturaDto> listDto = list.ConvertAll<ServicoViaturaDto>(via =>
             new ServicoViaturaDto { Id = via.Id.AsString(), domainId = via.domainId, nome = via.nome,
              cor = via.cor, blocks = via.blocks});

            return listDto;
        }

        public async Task<ServicoViaturaDto> GetByIdAsync(ServicoViaturaId id)
        {
            var via = await this._repo.GetByIdAsync(id);

            if (via == null)
                return null;

            return new ServicoViaturaDto { Id = via.Id.AsString(), domainId = via.domainId, nome = via.nome,
              cor = via.cor, blocks = via.blocks};
        }

        public async Task<ServicoViaturaDto> AddAsync(ServicoViaturaDto dto)
        {
            var via = new ServicoViatura(dto.domainId, dto.nome,
            dto.cor,dto.blocks);

            await this._repo.AddAsync(via);

            await this._unitOfWork.CommitAsync();

            return new ServicoViaturaDto  { Id = via.Id.AsString(), domainId = via.domainId, nome = via.nome,
             cor = via.cor, blocks = via.blocks};
        }
        public async Task<ServicoViaturaDto> UpdateAsync(ServicoViaturaDto dto)
        {
            var via = await this._repo.GetByIdAsync(new ServicoViaturaId(dto.Id));

            if (via == null)
                return null;

            via.mudaNome(dto.nome);
            via.mudaCor(dto.cor);
            via.mudaBlocks(dto.blocks);

            await this._unitOfWork.CommitAsync();

            return new ServicoViaturaDto {  Id = via.Id.AsString(), domainId = via.domainId, nome = via.nome,
              cor = via.cor, blocks = via.blocks};
        }

        public async Task<ServicoViaturaDto> DeleteAsync(ServicoViaturaId id)
        {
            var via = await this._repo.GetByIdAsync(id);

            if (via == null)
                return null;

            this._repo.Remove(via);
            await this._unitOfWork.CommitAsync();

             return new ServicoViaturaDto {  Id = via.Id.AsString(), domainId = via.domainId, nome = via.nome,
              cor = via.cor, blocks = via.blocks};
        }
    }
}