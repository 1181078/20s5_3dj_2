using System;
using MDV1.Domain.Shared;
using System.Collections.Generic;

namespace MDV1.Domain.ServicosViatura
{
    public class ServicoViatura : Entity<ServicoViaturaId>, IAggregateRoot
    {
        public string domainId { get; set; }
        public string nome { get; set; }
        public string cor { get; set; }
        public string[] blocks {get; set;}

        public ServicoViatura(){

        }

        public ServicoViatura(string domainId,string nome, string cor, string[] blocks)
        {
            this.Id = new ServicoViaturaId(domainId);
            this.domainId = domainId;
            this.nome = nome;
            this.cor = cor;
            this.blocks = blocks;
        }

        
        public string mudaNome(string nome){
            this.nome = nome;
            return nome;
        }
        public string mudaCor(string cor){
            this.cor = cor;
            return cor;
        }
        public string[] mudaBlocks(string[] blocks){
            this.blocks = blocks;
            return blocks;
        }

        public override bool Equals(object obj)
        {
            return (this.domainId == ((ServicoViatura)obj).domainId
            || this.nome == ((ServicoViatura)obj).nome || this.cor == ((ServicoViatura)obj).cor
            || this.blocks == ((ServicoViatura)obj).blocks);
        }


    }
}



