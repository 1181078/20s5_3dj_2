using System;
using System.Collections.Generic;

namespace MDV1.Domain.ServicosViatura{

    public class ServicoViaturaDto{
        public String Id { get; set;}
        public string domainId { get; set; }
        public string nome { get; set; }
        public string cor { get; set; }
        public string[] blocks {get; set;}

        public bool Equal(object obj)
        {
            return (this.Id == ((ServicoViaturaDto)obj).Id || this.domainId == ((ServicoViaturaDto)obj).domainId
            || this.nome == ((ServicoViaturaDto)obj).nome || this.cor == ((ServicoViaturaDto)obj).cor
            || this.blocks == ((ServicoViaturaDto)obj).blocks);
        }
    }
}