using MDV1.Domain.Shared;

namespace MDV1.Domain.ServicosViatura
{
    public interface IServicoViaturaRepository: IRepository<ServicoViatura, ServicoViaturaId>
    {
    }
}