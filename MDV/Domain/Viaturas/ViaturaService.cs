using System.Threading.Tasks;
using System.Collections.Generic;
using MDV1.Domain.Shared;

namespace MDV1.Domain.Viaturas
{
    public class ViaturaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IViaturaRepository _repo;

        public ViaturaService(IUnitOfWork unitOfWork, IViaturaRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<ViaturaDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<ViaturaDto> listDto = list.ConvertAll<ViaturaDto>(via => new ViaturaDto { Id = via.Id.AsString(), matricula = via.matricula, VIN = via.VIN, tipoViatura = via.tipoViatura, dataEntrada = via.dataEntrada });

            return listDto;
        }

        public async Task<ViaturaDto> GetByIdAsync(ViaturaId id)
        {
            var via = await this._repo.GetByIdAsync(id);

            if (via == null)
                return null;

            return new ViaturaDto { Id = via.Id.AsString(), matricula = via.matricula, VIN = via.VIN, tipoViatura = via.tipoViatura, dataEntrada = via.dataEntrada };
        }

        public async Task<ViaturaDto> AddAsync(ViaturaDto dto)
        {
            var viatura = new Viatura(dto.matricula, dto.VIN, dto.tipoViatura, dto.dataEntrada);

            await this._repo.AddAsync(viatura);

            await this._unitOfWork.CommitAsync();

            return new ViaturaDto { Id = viatura.Id.AsString(), matricula = viatura.matricula, VIN = viatura.VIN, tipoViatura = viatura.tipoViatura, dataEntrada = viatura.dataEntrada };
        }

        public async Task<ViaturaDto> UpdateAsync(ViaturaDto dto)
        {
            var viatura = await this._repo.GetByIdAsync(new ViaturaId(dto.Id));

            if (viatura == null)
                return null;

            // change all field
            viatura.mudaVIN(dto.VIN);
            viatura.mudarTipo(dto.tipoViatura);
            viatura.mudarData(dto.dataEntrada);

            await this._unitOfWork.CommitAsync();

            return new ViaturaDto { Id = viatura.Id.AsString(), matricula = viatura.matricula, VIN = viatura.VIN, tipoViatura = viatura.tipoViatura, dataEntrada = viatura.dataEntrada };
        }


        // public async Task<ViaturaDto> InactivateAsync(ViaturaId id)
        // {
        //     var viatura = await this._repo.GetByIdAsync(id);

        //     if (viatura == null)
        //         return null;

        //     // change all fields
        //     viatura.MarkAsInative();

        //     await this._unitOfWork.CommitAsync();

        //     return new ViaturaDto { Id = viatura.Id.AsString(), matricula = viatura.matricula, VIN = viatura.VIN, tipoViatura = viatura.tipoViatura, dataEntrada = viatura.dataEntrada };
        // }

        public async Task<ViaturaDto> DeleteAsync(ViaturaId id)
        {
            var viatura = await this._repo.GetByIdAsync(id);

            if (viatura == null)
                return null;

            // if (viatura.Active)
            //     throw new BusinessRuleValidationException("Impossível apagar viatura ativa.");


            this._repo.Remove(viatura);
            await this._unitOfWork.CommitAsync();

            return new ViaturaDto { Id = viatura.Id.AsString(), matricula = viatura.matricula, VIN = viatura.VIN, tipoViatura = viatura.tipoViatura, dataEntrada = viatura.dataEntrada };
        }
    }
}