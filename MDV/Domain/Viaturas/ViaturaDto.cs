using System;

namespace MDV1.Domain.Viaturas
{
    public class ViaturaDto
    {
        public String Id { get; set; }

        public string matricula { get; set; }


        public string VIN { get; set; }


        public string tipoViatura { get; set; }

        public string dataEntrada { get; set; }

        public override bool Equals(object obj)
        {
            return (this.Id == ((ViaturaDto)obj).Id || this.matricula == ((ViaturaDto)obj).matricula
            || this.VIN == ((ViaturaDto)obj).VIN || this.tipoViatura == ((ViaturaDto)obj).tipoViatura
            || this.dataEntrada == ((ViaturaDto)obj).dataEntrada);
        }

    }
}