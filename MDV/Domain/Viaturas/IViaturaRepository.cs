
using MDV1.Domain.Shared;

namespace MDV1.Domain.Viaturas
{
    public interface IViaturaRepository: IRepository<Viatura, ViaturaId>
    {
    }
}