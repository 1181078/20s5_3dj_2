using System;
using MDV1.Domain.Shared;
using Newtonsoft.Json;

namespace MDV1.Domain.Viaturas
{
    public class ViaturaId : EntityId
    {

        public ViaturaId(String value):base(value)
        {

        }

        override
        protected  Object createFromString(String text){
            return text;
        }
        override
        public String AsString(){
            return (String) base.Value;
        }
    }
}