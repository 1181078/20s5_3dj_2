using System;
using MDV1.Domain.Shared;

namespace MDV1.Domain.Viaturas
{
    public class Viatura : Entity<ViaturaId>, IAggregateRoot
    {

        public string matricula { get; private set; }


        public string VIN { get; private set; }


        public string tipoViatura { get; private set; }

        public string dataEntrada { get; private set; }


        private Viatura()
        {
        }


        public Viatura(string matricula, string VIN, string tipoViatura, string date)
        {
            this.Id = new ViaturaId(matricula);
            this.matricula = matricula;
            this.VIN = VIN;
            this.tipoViatura = tipoViatura;
            this.dataEntrada = date;
        }

        public string mudaVIN(string VIN){
            this.VIN = VIN;
            return VIN;
        }
        public string mudarTipo(string tipo)
        {
            this.tipoViatura = tipo;
            return tipo;
        }

        public string mudarData(string date)
        {
            this.dataEntrada = date;
            return date;
        }

        public override bool Equals(object obj)
        {
            return (this.matricula == ((Viatura)obj).matricula
            || this.VIN == ((Viatura)obj).VIN || this.tipoViatura == ((Viatura)obj).tipoViatura
            || this.dataEntrada == ((Viatura)obj).dataEntrada);
        }

    }
}


