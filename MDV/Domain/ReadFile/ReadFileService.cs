using System.Threading.Tasks;
using System.Collections.Generic;
using MDV1.Domain.Shared;
using Microsoft.AspNetCore.Http;
using System;
using System.Xml;
using MDV1.Domain.BlocosTrabalho;
using MDV1.Domain.Tripulantes;
using MDV1.Domain.Viagens;
using MDV1.Domain.ServicosTripulante;
using MDV1.Domain.ServicosViatura;

namespace MDV1.Domain.ReadFile
{
    public class ReadFileService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBlocoTrabalhoRepository blocoTrabalhoRepo;
        private readonly IViagemRepository viagemrepo;
        private readonly IServicoViaturaRepository servicoViaturaRepo;
        private readonly IServicoTripulanteRepository servicoTripulanteRepo;

        public ReadFileService(IUnitOfWork unitOfWork, IBlocoTrabalhoRepository blocoTrabalhoRepo, IViagemRepository viagemrepo, ITripulanteRepository tripulanteRepo, IServicoViaturaRepository servicoViaturaRepo, IServicoTripulanteRepository servicoTripulanteRepo)
        {
            this._unitOfWork = unitOfWork;
            this.blocoTrabalhoRepo = blocoTrabalhoRepo;
            this.viagemrepo = viagemrepo;
            this.servicoViaturaRepo = servicoViaturaRepo;
            this.servicoTripulanteRepo = servicoTripulanteRepo;
        }


        public async Task<bool> AddAsync(IFormFile file)
        {
           
            XmlDocument glx = new XmlDocument();
            glx.Load(file.OpenReadStream());

            //Blocos de Trabalho
            XmlNodeList wbs = glx.GetElementsByTagName("WorkBlock");  
            foreach (XmlElement workblock in wbs){
                string domainId = workblock.Attributes.Item(0).InnerText;
                int TempoInicio = Int32.Parse(workblock.Attributes.Item(1).InnerText);
                int TempoFim = Int32.Parse(workblock.Attributes.Item(2).InnerText);
                string NoInicio = workblock.Attributes.Item(3).InnerText;
                string NoFim = workblock.Attributes.Item(4).InnerText;
                var Viagens = new List<string>();
                XmlNodeList childs = workblock.ChildNodes;
                foreach (XmlElement tripss in childs ){
                    XmlNodeList grandchilds = tripss.ChildNodes;
                    foreach (XmlElement tripsref in grandchilds ){
                        //var via = await this.viagemrepo.GetByIdAsync(new ViagemId(tripsref.Attributes.Item(0).InnerText));
                        Viagens.Add(tripsref.Attributes.Item(0).InnerText);
                    }
                }
                BlocoTrabalho bloco = new BlocoTrabalho(domainId,TempoInicio,TempoFim,NoInicio,NoFim,Viagens.ToArray());
                await this.blocoTrabalhoRepo.AddAsync(bloco);
                await this._unitOfWork.CommitAsync();
            }
            //


            //Servicos Viaturas
            XmlNodeList vduties = glx.GetElementsByTagName("VehicleDuty");  
            foreach (XmlElement vduty in vduties){
                string domainId = vduty.Attributes.Item(0).InnerText;
                string nome = vduty.Attributes.Item(1).InnerText;
                string cor = vduty.Attributes.Item(2).InnerText;
                var blocks = new List<string>();
                XmlNodeList childs = vduty.ChildNodes;
                foreach (XmlElement wbss in childs ){
                    XmlNodeList grandchilds = wbss.ChildNodes;
                    foreach (XmlElement reference in grandchilds){
                        blocks.Add(reference.Attributes.Item(0).InnerText);
                    }
                }
                ServicoViatura servicoViatura = new ServicoViatura(domainId,nome,cor, blocks.ToArray());
                await this.servicoViaturaRepo.AddAsync(servicoViatura);
                await this._unitOfWork.CommitAsync();
            }
            //


            //ServicosTripulante
            XmlNodeList dduties = glx.GetElementsByTagName("DriverDuty");  
            foreach (XmlElement dduty in dduties){
                string domainId = dduty.Attributes.Item(0).InnerText;
                string nome = dduty.Attributes.Item(1).InnerText;
                string cor = dduty.Attributes.Item(2).InnerText;
                var blocks = new List<string>();
                XmlNodeList childs = dduty.ChildNodes;
                foreach (XmlElement wbss in childs ){
                    XmlNodeList grandchilds = wbss.ChildNodes;
                    foreach (XmlElement reference in grandchilds ){
                        blocks.Add(reference.Attributes.Item(0).InnerText);
                    }
                }
                ServicoTripulante servicoTripulante = new ServicoTripulante(domainId,nome,cor, blocks.ToArray());
                await this.servicoTripulanteRepo.AddAsync(servicoTripulante);
                await this._unitOfWork.CommitAsync();
            }


            //Viagens
            XmlNodeList trips = glx.GetElementsByTagName("Trip");
            foreach (XmlElement trip in trips){
                string domainId = trip.Attributes.Item(0).InnerText;
                bool vazio = bool.Parse(trip.Attributes.Item(1).InnerText);
                string lineId = trip.Attributes.Item(3).InnerText;
                string pathId = trip.Attributes.Item(4).InnerText;
                bool gerado = bool.Parse(trip.Attributes.Item(5).InnerText);
                var tempos = new List<int>();
                XmlNodeList childs = trip.ChildNodes;
                foreach (XmlElement times in childs ){
                    XmlNodeList grandchilds = times.ChildNodes;
                    foreach (XmlElement time in grandchilds ){
                        tempos.Add(Int32.Parse(time.Attributes.Item(1).InnerText));
                    }
                }
                Viagem viagem = new Viagem( domainId,vazio,lineId,pathId,gerado,tempos.ToArray());
                await this.viagemrepo.AddAsync(viagem);
                await this._unitOfWork.CommitAsync();
            }
            //
                
                return true;
        }

    }
}