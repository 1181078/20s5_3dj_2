using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV1.Domain.ServicosTripulante;

namespace MDV1.Infrastructure.ServicosTripulante
{
    internal class ServicoTripulanteEntityTypeConfiguration : IEntityTypeConfiguration<ServicoTripulante>
    {
        public void Configure(EntityTypeBuilder<ServicoTripulante> builder)
        {
            //builder.ToTable("Families", SchemaNames.MDV1);
            builder.Property(b => b.Id).IsRequired();
            //builder.Property<bool>("_active").HasColumnName("Active");
        }
    }
}