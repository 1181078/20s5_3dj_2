using MDV1.Domain.ServicosTripulante;
using MDV1.Infrastructure.Shared;

namespace MDV1.Infrastructure.ServicosTripulante 
{
    public class ServicoTripulanteRepository : BaseRepository<ServicoTripulante, ServicoTripulanteId>, IServicoTripulanteRepository
    {
      
        public ServicoTripulanteRepository(MDV1DbContext context):base(context.ServicosTripulante)
        {
            
        }

    }
}