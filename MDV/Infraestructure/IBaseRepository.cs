using System.Collections.Generic;

namespace MDV1.Infraestructure
{
    public interface IBaseRepository<Dto, Entity>
    {
        Dto GetById(long id);
        
        IEnumerable<Dto> GetAll();
        
        Entity Add(Dto writeDto);

        Dto UpdateElement(long id, Dto Dto);
        
        Dto DeleteElement(long id);
        
    }
}