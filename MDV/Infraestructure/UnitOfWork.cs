using System.Threading.Tasks;
using MDV1.Domain.Shared;

namespace MDV1.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MDV1DbContext _context;

        public UnitOfWork(MDV1DbContext context)
        {
            this._context = context;
        }

        public async Task<int> CommitAsync()
        {
            return await this._context.SaveChangesAsync();
        }
    }
}