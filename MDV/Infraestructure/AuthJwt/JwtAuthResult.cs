using System.Text.Json.Serialization;

namespace MDV1.Infrastructure.AuthJwt
{
    public class JwtAuthResult
    {
        [JsonPropertyName("accessToken")]
        public string AccessToken { get; set;}

        [JsonPropertyName("refreshToken")]
        public RefreshToken RefreshToken { get; set;}


    }
}