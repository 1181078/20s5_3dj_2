using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV1.Domain.ServicosViatura;

namespace MDV1.Infrastructure.ServicosViatura
{
    internal class ServicoViaturaEntityTypeConfiguration : IEntityTypeConfiguration<ServicoViatura>
    {
        public void Configure(EntityTypeBuilder<ServicoViatura> builder)
        {
            //builder.ToTable("Families", SchemaNames.MDV1);
            builder.Property(b => b.Id).IsRequired();
            //builder.Property<bool>("_active").HasColumnName("Active");
        }
    }
}