using MDV1.Domain.ServicosViatura;
using MDV1.Infrastructure.Shared;

namespace MDV1.Infrastructure.ServicosViatura 
{
    public class ServicoViaturaRepository : BaseRepository<ServicoViatura, ServicoViaturaId>, IServicoViaturaRepository
    {
      
        public ServicoViaturaRepository(MDV1DbContext context):base(context.ServicosViatura)
        {
            
        }

    }
}