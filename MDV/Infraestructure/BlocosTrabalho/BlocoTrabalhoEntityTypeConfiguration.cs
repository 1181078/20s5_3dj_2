using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV1.Domain.BlocosTrabalho;

namespace MDV1.Infrastructure.BlocosTrabalho
{
    internal class BlocoTrabalhoEntityTypeConfiguration : IEntityTypeConfiguration<BlocoTrabalho>
    {
        public void Configure(EntityTypeBuilder<BlocoTrabalho> builder)
        {
            //builder.ToTable("Families", SchemaNames.MDV1);
            builder.Property(b => b.Id).IsRequired();
            //builder.Property<bool>("_active").HasColumnName("Active");
        }
    }
}