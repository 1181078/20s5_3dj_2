using MDV1.Domain.BlocosTrabalho;
using MDV1.Infrastructure.Shared;

namespace MDV1.Infrastructure.BlocosTrabalho
{
    public class BlocoTrabalhoRepository : BaseRepository<BlocoTrabalho, BlocoTrabalhoId>, IBlocoTrabalhoRepository
    {
      
        public BlocoTrabalhoRepository(MDV1DbContext context):base(context.BlocosTrabalho)
        {
            
        }

    }
}