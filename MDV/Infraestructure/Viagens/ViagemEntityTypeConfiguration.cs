using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV1.Domain.Viagens;

namespace MDV1.Infrastructure.Viagens
{
    internal class ViagemEntityTypeConfiguration : IEntityTypeConfiguration<Viagem>
    {
        public void Configure(EntityTypeBuilder<Viagem> builder)
        {
            //builder.ToTable("Families", SchemaNames.MDV1);
            builder.Property(b => b.Id).IsRequired();
            //builder.Property<bool>("_active").HasColumnName("Active");
        }
    }
}