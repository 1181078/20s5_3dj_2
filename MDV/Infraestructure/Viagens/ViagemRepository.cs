using MDV1.Domain.Viagens;
using MDV1.Infrastructure.Shared;

namespace MDV1.Infrastructure.Viagens 
{
    public class ViagemRepository : BaseRepository<Viagem, ViagemId>, IViagemRepository
    {
      
        public ViagemRepository(MDV1DbContext context):base(context.Viagens)
        {
            
        }

    }
}