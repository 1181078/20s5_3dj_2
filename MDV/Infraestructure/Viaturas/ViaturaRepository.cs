using MDV1.Domain.Viaturas;
using MDV1.Infrastructure.Shared;

namespace MDV1.Infrastructure.Viaturas
{
    public class ViaturaRepository : BaseRepository<Viatura, ViaturaId>, IViaturaRepository
    {
      
        public ViaturaRepository(MDV1DbContext context):base(context.Viaturas)
        {
            
        }

    }
}