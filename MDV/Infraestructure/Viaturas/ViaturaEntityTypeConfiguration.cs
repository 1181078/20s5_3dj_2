using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV1.Domain.Viaturas;

namespace MDV1.Infrastructure.Viaturas
{
    internal class ViaturaEntityTypeConfiguration : IEntityTypeConfiguration<Viatura>
    {
        public void Configure(EntityTypeBuilder<Viatura> builder)
        {
            //builder.ToTable("Families", SchemaNames.MDV1);
            builder.Property(b => b.Id).IsRequired();
            //builder.Property<bool>("_active").HasColumnName("Active");
        }
    }
}