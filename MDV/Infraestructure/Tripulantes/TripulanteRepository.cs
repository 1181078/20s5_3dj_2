using System;
using System.Collections.Generic;
using System.Linq;
using MDV1.Infrastructure;
using MDV1.Domain.Tripulantes;
using MDV1.Exceptions;
using Microsoft.EntityFrameworkCore;
using MDV1.Infrastructure.Shared;

namespace MDV1.Infrastructure.Tripulantes 
{
    public class TripulanteRepository : BaseRepository<Tripulante, TripulanteId>, ITripulanteRepository
    {
              
        public TripulanteRepository(MDV1DbContext context):base(context.Tripulantes)
        {
        }
      
}}

      

       