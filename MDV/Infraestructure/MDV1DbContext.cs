using Microsoft.EntityFrameworkCore;
using MDV1.Domain.Viaturas;
using MDV1.Infrastructure.Viaturas;
using MDV1.Domain.BlocosTrabalho;
using MDV1.Infrastructure.BlocosTrabalho;
using MDV1.Domain.Viagens;
using MDV1.Infrastructure.Viagens;
using MDV1.Domain.Tripulantes;
using MDV1.Infrastructure.Tripulantes;
using MDV1.Domain.ServicosViatura;
using MDV1.Infrastructure.ServicosViatura;
using MDV1.Domain.ServicosTripulante;
using MDV1.Infrastructure.ServicosTripulante;
using System.Collections;
using System;

namespace MDV1.Infrastructure
{
    public class MDV1DbContext : DbContext
    {
        public DbSet<Viatura> Viaturas { get; set; }
        public DbSet<BlocoTrabalho> BlocosTrabalho { get; set; }
        public DbSet<Viagem> Viagens {get; set;}
        public DbSet<Tripulante> Tripulantes {get; set;}
        public DbSet<ServicoViatura> ServicosViatura {get; set;}
        public DbSet<ServicoTripulante> ServicosTripulante{get; set;}

        public MDV1DbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<ServicoViatura>()
            .Property(e => e.blocks)
            .HasConversion(
                v => string.Join(',', v),
                v => v.Split(',', StringSplitOptions.RemoveEmptyEntries));
            
            modelBuilder.Entity<ServicoTripulante>()
            .Property(e => e.blocks)
            .HasConversion(
                v => string.Join(',', v),
                v => v.Split(',', StringSplitOptions.RemoveEmptyEntries));
                
            modelBuilder.Entity<BlocoTrabalho>()
            .Property(e => e.Viagens)
            .HasConversion(
                v => string.Join(',', v),
                v => v.Split(',', StringSplitOptions.RemoveEmptyEntries));

            modelBuilder.Entity<Viagem>()
            .Property(e => e.tempos)
            .HasConversion(
                v => string.Join(',', v),
                v => Array.ConvertAll(v.Split(',', StringSplitOptions.RemoveEmptyEntries), s => int.Parse(s)));

            modelBuilder.ApplyConfiguration(new ViaturaEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new BlocoTrabalhoEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TripulanteEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ViagemEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ServicoViaturaEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ServicoTripulanteEntityTypeConfiguration());
        }

        
    }
}