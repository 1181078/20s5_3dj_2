using System;
using System.Net;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Threading.Tasks;
using Moq;
using MDV1.Domain.Tripulantes;
using MDV1.Controllers;
using MDV1.Domain.Shared;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
namespace Testes
{
    public class TripulanteTest
    {


        public int numMecanografico = 111;
        public string nome = "teste";
        public string dataDeNascimento = "teste";
        public int numCartaoCidadao = 111;
        public int NIF = 111;
        public string tipoTripulante = "teste";
        public string dataDeEntrada = "teste";
        public string dataDeSaida = "teste";

        public int numMecanografico2 = 222;
        public string nome2 = "teste2";
        public string dataDeNascimento2 = "teste2";
        public int numCartaoCidadao2 = 222;
        public int NIF2 = 222;
        public string tipoTripulante2 = "teste2";
        public string dataDeEntrada2 = "teste2";
        public string dataDeSaida2 = "teste2";

        [Fact]
        public async void GetAll()
        {
            //Mock Repo
            var mockRepo = new Mock<ITripulanteRepository>();
            mockRepo.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(GetTripulantes());
            var mockUnitRepo = new Mock<IUnitOfWork>();
            //Create service
            var service = new TripulanteService(mockUnitRepo.Object, mockRepo.Object);
            //Create controller
            var controller = new TripulantesController(service);

            //Resposta
            var response = await controller.GetAll();
            var responseBody = response.Value as List<TripulanteDto>;
            Assert.Equal(2, responseBody.Count());
        }

        [Fact]
        public async void GetGetById()
        {

            //create tripulante
            var id = new TripulanteId(this.numMecanografico.ToString());
            var t1 = new Tripulante(
                this.numMecanografico,
                this.nome,
                this.dataDeNascimento,
                this.numCartaoCidadao,
                this.NIF,
                this.tipoTripulante,
                this.dataDeEntrada,
                this.dataDeSaida
            );
            //Mock Repo
            var mockRepo = new Mock<ITripulanteRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(t1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            //Create service
            var service = new TripulanteService(mockUnitRepo.Object, mockRepo.Object);
            //Create controller
            var controller = new TripulantesController(service);

            //Resposta 
            var response = await controller.GetGetById(this.numMecanografico);
            var responseBody = response.Value as TripulanteDto;

            //Teste - Valida nome
            Assert.Equal("teste", responseBody.tipoTripulante);
        }
        [Fact]
        public async void Create()
        {

            //Create Tripulante
            var id = new TripulanteId(this.numMecanografico.ToString());
            var t1 = new Tripulante(
                this.numMecanografico,
                this.nome,
                this.dataDeNascimento,
                this.numCartaoCidadao,
                this.NIF,
                this.tipoTripulante,
                this.dataDeEntrada,
                this.dataDeSaida
            );

            //Create TripulanteDto
            var t2 = new TripulanteDto
            {
                Id = new TripulanteId(this.numMecanografico.ToString()).AsString(),
                numMecanografico = this.numMecanografico,
                nome = this.nome,
                dataDeNascimento = this.dataDeNascimento,
                numCartaoCidadao = this.numCartaoCidadao,
                NIF = this.NIF,
                tipoTripulante = this.tipoTripulante,
                dataDeEntrada = this.dataDeEntrada,
                dataDeSaida = this.dataDeSaida
            };

            //Mock Repo com tripulante
            var mockRepo = new Mock<ITripulanteRepository>();
            mockRepo.Setup(repo => repo.AddAsync(t1)).ReturnsAsync(t1); ;
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create Service
            var service = new TripulanteService(mockUnitRepo.Object, mockRepo.Object);

            //Create Controller
            var controller = new TripulantesController(service);

            //Resposta
            var response = await controller.Create(t2);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is CreatedAtActionResult);
        }

        [Fact]
        public async void Update()
        {

            //Create Tripulante
            var id = new TripulanteId(this.numMecanografico.ToString());
            var t1 = new Tripulante(
                this.numMecanografico,
                this.nome,
                this.dataDeNascimento,
                this.numCartaoCidadao,
                this.NIF,
                this.tipoTripulante,
                this.dataDeEntrada,
                this.dataDeSaida
            );

            //Novo TripulanteDto para Update
            var t2 = new TripulanteDto
            {
                Id = new TripulanteId(this.numMecanografico.ToString()).AsString(),
                numMecanografico = this.numMecanografico,
                nome = this.nome,
                dataDeNascimento = this.dataDeNascimento,
                numCartaoCidadao = this.numCartaoCidadao,
                NIF = this.NIF,
                tipoTripulante = this.tipoTripulante,
                dataDeEntrada = this.dataDeEntrada,
                dataDeSaida = this.dataDeSaida
            };

            //Mock Repo
            var mockRepo = new Mock<ITripulanteRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(t1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create Service
            var service = new TripulanteService(mockUnitRepo.Object, mockRepo.Object);

            //Create Controller
            var controller = new TripulantesController(service);

            //Resposta
            var response = await controller.Update(this.numMecanografico, t2);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is OkObjectResult);

        }

        [Fact]
        public async void Delete()
        {


            //Create Tripulante
            var id = new TripulanteId(this.numMecanografico.ToString());
            var t1 = new Tripulante(
                this.numMecanografico,
                this.nome,
                this.dataDeNascimento,
                this.numCartaoCidadao,
                this.NIF,
                this.tipoTripulante,
                this.dataDeEntrada,
                this.dataDeSaida
            );

            //Mock Repo
            var mockRepo = new Mock<ITripulanteRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(t1);
            mockRepo.Setup(repo => repo.Remove(t1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create service
            var service = new TripulanteService(mockUnitRepo.Object, mockRepo.Object);

            //Create controller
            var controller = new TripulantesController(service);

            //Resposta
            var response = await controller.HardDelete(this.numMecanografico);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is OkObjectResult);

        }

        private List<Tripulante> GetTripulantes()
        {
            var t = new List<Tripulante>();
            t.Add(new Tripulante(
                this.numMecanografico,
                this.nome,
                this.dataDeNascimento,
                this.numCartaoCidadao,
                this.NIF,
                this.tipoTripulante,
                this.dataDeEntrada,
                this.dataDeSaida
                ));

            t.Add(new Tripulante(
                this.numMecanografico2,
                this.nome2,
                this.dataDeNascimento2,
                this.numCartaoCidadao2,
                this.NIF2,
                this.tipoTripulante2,
                this.dataDeEntrada2,
                this.dataDeSaida2
                ));

            return t;
        }
    }
}