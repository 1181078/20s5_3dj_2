using System;
using System.Net;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Threading.Tasks;
using Moq;
using MDV1.Domain.Viaturas;
using MDV1.Controllers;
using MDV1.Domain.Shared;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
namespace Testes
{
    public class ViaturaTest
    {

          public string matricula= "76-89-ZR";
          public string VIN= "VIN";
          public string tipoViatura= "carro";
          public string dataEntrada= "20/12/2020";

          public string matricula2= "90-12-MS";
          public string VIN2= "NIV";
          public string tipoViatura2= "mota";
          public string dataEntrada2= "23/01/2018";

        [Fact]
        public async void GetAll()
        {
            //Mock Repo
            var mockRepo = new Mock<IViaturaRepository>();
            mockRepo.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(GetViaturas());
            var mockUnitRepo = new Mock<IUnitOfWork>();
            //Create service
            var service = new ViaturaService(mockUnitRepo.Object, mockRepo.Object);
            //Create controller
            var controller = new ViaturasController(service);

            //Resposta
            var response = await controller.GetAll();
            var responseBody = response.Value as List<ViaturaDto>;
            Assert.Equal(2, responseBody.Count());
        }

        [Fact]
        public async void GetGetById()
        {

            //create viatura
            var id = new ViaturaId(this.matricula);
            var v1 = new Viatura(this.matricula,this.VIN,this.tipoViatura, this.dataEntrada);
            //Mock Repo
            var mockRepo = new Mock<IViaturaRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(v1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            //Create service
            var service = new ViaturaService(mockUnitRepo.Object, mockRepo.Object);
            //Create controller
            var controller = new ViaturasController(service);

            //Resposta 
            var response = await controller.GetGetById(this.matricula);
            var responseBody = response.Value as ViaturaDto;

            //Teste - Valida tipoViatura
            Assert.Equal("carro", responseBody.tipoViatura);
        }
        [Fact]
        public async void Create()
        {

            //Create Viatura
            var id = new ViaturaId(this.matricula);
            var v1 = new Viatura(this.matricula,this.VIN,this.tipoViatura, this.dataEntrada);

            //Create ViaturaDto
            var v2 = new ViaturaDto{ Id = id.AsString(), matricula =  this.matricula, VIN = this.VIN,  tipoViatura = this.tipoViatura,  dataEntrada = this.dataEntrada};


            //Mock Repo com viatura
            var mockRepo = new Mock<IViaturaRepository>();
            mockRepo.Setup(repo => repo.AddAsync(v1)).ReturnsAsync(v1); ;
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create Service
            var service = new ViaturaService(mockUnitRepo.Object, mockRepo.Object);

            //Create Controller
            var controller = new ViaturasController(service);

            //Resposta
            var response = await controller.Create(v2);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is CreatedAtActionResult);
        }

        [Fact]
        public async void Update()
        {

            //Create Viatura
            var id = new ViaturaId(this.matricula);
            var v1 = new Viatura(this.matricula,this.VIN,this.tipoViatura, this.dataEntrada);

            //Novo ViaturaDto para Update
             var v2Alt = new ViaturaDto{ Id = id.AsString(), matricula =  this.matricula, VIN = "999",  tipoViatura = "newTipo",  dataEntrada = "00/00/00"};

            //Mock Repo
            var mockRepo = new Mock<IViaturaRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(v1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create Service
            var service = new ViaturaService(mockUnitRepo.Object, mockRepo.Object);

            //Create Controller
            var controller = new ViaturasController(service);

            //Resposta
            var response = await controller.Update(this.matricula, v2Alt);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is OkObjectResult);

        }

        [Fact]
        public async void Delete()
        {


            //Create Viatura
            var id = new ViaturaId(this.matricula);
            var v1 = new Viatura(this.matricula,this.VIN,this.tipoViatura, this.dataEntrada);

            //Mock Repo
            var mockRepo = new Mock<IViaturaRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(v1);
            mockRepo.Setup(repo => repo.Remove(v1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create service
            var service = new ViaturaService(mockUnitRepo.Object, mockRepo.Object);

            //Create controller
            var controller = new ViaturasController(service);

            //Resposta
            var response = await controller.HardDelete(this.matricula);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is OkObjectResult);

        }

       private List<Viatura> GetViaturas()
        {
            var viaturas = new List<Viatura>();
            viaturas.Add(new Viatura(this.matricula,this.VIN,this.tipoViatura, this.dataEntrada));
            viaturas.Add(new Viatura(this.matricula2,this.VIN2,this.tipoViatura2, this.dataEntrada2));
            
            return viaturas;
        }
    }
}