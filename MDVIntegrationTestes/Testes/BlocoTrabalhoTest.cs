using System;
using System.Net;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Threading.Tasks;
using Moq;
using MDV1.Domain.BlocosTrabalho;
using MDV1.Controllers;
using MDV1.Domain.Shared;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
namespace Testes
{
    public class BlocoTrabalhoTest
    {
        public string domainId = "teste";
        public int TempoInicio = 1;
        public int TempoFim = 2;
        public string NoInicio = "teste";
        public string NoFim = "teste";
        public string[] Viagens = { "teste", "teste" };

        public string domainId2 = "teste2";
        public int TempoInicio2 = 2;
        public int TempoFim2 = 3;
        public string NoInicio2 = "teste2";
        public string NoFim2 = "teste2";
        public string[] Viagens2 = { "teste2", "teste2" };

        [Fact]
        public async void GetAll()
        {
            //Mock Repo
            var mockBlocoRepo = new Mock<IBlocoTrabalhoRepository>();
            mockBlocoRepo.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(GetBlocos());
            var mockUnitRepo = new Mock<IUnitOfWork>();
            //Create service
            var service = new BlocoTrabalhoService(mockUnitRepo.Object, mockBlocoRepo.Object);
            //Create controller
            var controller = new BlocosTrabalhoController(service);

            //Resposta
            var response = await controller.GetAll();
            var responseBody = response.Value as List<BlocoTrabalhoDto>;
            Assert.Equal(responseBody.Count(), 2);
        }

        [Fact]
        public async void GetGetById()
        {

            //create blocoTrabalho
            var id = new BlocoTrabalhoId(this.domainId);

            var b1 = new BlocoTrabalho(
               this.domainId,
               this.TempoInicio,
               this.TempoFim,
               this.NoInicio,
               this.NoFim,
               this.Viagens
               );
            //Mock Repo
            var mockBlocoRepo = new Mock<IBlocoTrabalhoRepository>();
            mockBlocoRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(b1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            //Create service
            var service = new BlocoTrabalhoService(mockUnitRepo.Object, mockBlocoRepo.Object);
            //Create controller
            var controller = new BlocosTrabalhoController(service);

            //Resposta 
            var response = await controller.GetGetById(this.domainId);
            var responseBody = response.Value as BlocoTrabalhoDto;

            //Teste - Valida NoInicio
            Assert.Equal(responseBody.NoInicio, "teste");
        }
        [Fact]
        public async void Create()
        {

            //Create BlocoTrabalho
            var id = new BlocoTrabalhoId(this.domainId);
            var b1 = new BlocoTrabalho(
            this.domainId,
            this.TempoInicio,
            this.TempoFim,
            this.NoInicio,
            this.NoFim,
            this.Viagens
            );

            //Create BlocoTrabalhoDto
            var b2 = new BlocoTrabalhoDto
            {
                Id = new BlocoTrabalhoId(this.domainId).AsString(),
                domainId = this.domainId,
                TempoInicio = this.TempoInicio,
                TempoFim = this.TempoFim,
                NoInicio = this.NoInicio,
                NoFim = this.NoFim,
                Viagens = this.Viagens
            };

            //Mock Repo com bloco de trabalho
            var mockRepo = new Mock<IBlocoTrabalhoRepository>();
            mockRepo.Setup(repo => repo.AddAsync(b1)).ReturnsAsync(b1); ;
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create Service
            var service = new BlocoTrabalhoService(mockUnitRepo.Object, mockRepo.Object);

            //Create Controller
            var controller = new BlocosTrabalhoController(service);

            //Resposta
            var response = await controller.Create(b2);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is CreatedAtActionResult);
        }

        [Fact]
        public async void Update()
        {

            //Create BlocoTrabalho
            var id = new BlocoTrabalhoId(this.domainId);
            var b1 = new BlocoTrabalho(
                this.domainId,
                this.TempoInicio,
                this.TempoFim,
                this.NoInicio,
                this.NoFim,
                this.Viagens
                );

            //Novo BlocoTrabalhoDto para Update
            string[] novo = { "novo", "novo" };
            var b2Alt = new BlocoTrabalhoDto
            {
                Id = new BlocoTrabalhoId(this.domainId).AsString(),
                domainId = this.domainId,
                TempoInicio = 5,
                TempoFim = 6,
                NoInicio = "novo",
                NoFim = "novo",
                Viagens = novo
            };

            //Mock Repo
            var mockRepo = new Mock<IBlocoTrabalhoRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(b1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create Service
            var service = new BlocoTrabalhoService(mockUnitRepo.Object, mockRepo.Object);

            //Create Controller
            var controller = new BlocosTrabalhoController(service);

            //Resposta
            var response = await controller.Update(this.domainId, b2Alt);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is OkObjectResult);

        }

        [Fact]
        public async void Delete()
        {


            //Create BlocoTrabalho
            var id = new BlocoTrabalhoId(this.domainId);
            var b1 = new BlocoTrabalho(
                this.domainId,
                this.TempoInicio,
                this.TempoFim,
                this.NoInicio,
                this.NoFim,
                this.Viagens
                );

            //Mock Repo
            var mockRepo = new Mock<IBlocoTrabalhoRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(b1);
            mockRepo.Setup(repo => repo.Remove(b1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create service
            var service = new BlocoTrabalhoService(mockUnitRepo.Object, mockRepo.Object);

            //Create controller
            var controller = new BlocosTrabalhoController(service);

            //Resposta
            var response = await controller.HardDelete(this.domainId);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is OkObjectResult);

        }


        private List<BlocoTrabalho> GetBlocos()
        {
            var blocos = new List<BlocoTrabalho>();
            blocos.Add(new BlocoTrabalho(
                this.domainId,
                this.TempoInicio,
                this.TempoFim,
                this.NoInicio,
                this.NoFim,
                this.Viagens
                ));

            blocos.Add(new BlocoTrabalho(
                this.domainId2,
                this.TempoInicio2,
                this.TempoFim2,
                this.NoInicio2,
                this.NoFim2,
                this.Viagens2
                ));

            return blocos;
        }
    }
}

