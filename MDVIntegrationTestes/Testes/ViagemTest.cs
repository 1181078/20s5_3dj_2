using System;
using System.Net;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Threading.Tasks;
using Moq;
using MDV1.Domain.Viagens;
using MDV1.Controllers;
using MDV1.Domain.Shared;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
namespace Testes
{
    public class ViagemTest
    {


        public string domainId = "teste";
        public bool vazio = false;
        public string lineId = "teste";
        public string pathId = "teste";
        public bool gerado = false;
        public int[] tempos = { 111, 222 };

        public string domainId2 = "teste2";
        public bool vazio2 = false;
        public string lineId2 = "teste2";
        public string pathId2 = "teste2";
        public bool gerado2 = false;
        public int[] tempos2 = { 1, 11, 2, 22 };

        [Fact]
        public async void GetAll()
        {
            //Mock Repo
            var mockRepo = new Mock<IViagemRepository>();
            mockRepo.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(GetViagens());
            var mockUnitRepo = new Mock<IUnitOfWork>();
            //Create service
            var service = new ViagemService(mockUnitRepo.Object, mockRepo.Object);
            //Create controller
            var controller = new ViagensController(service);

            //Resposta
            var response = await controller.GetAll();
            var responseBody = response.Value as List<ViagemDto>;
            Assert.Equal(2, responseBody.Count());
        }

        [Fact]
        public async void GetGetById()
        {

            //create viagem
            var id = new ViagemId(this.domainId);
            var v1 = new Viagem(
                this.domainId,
                this.vazio,
                this.lineId,
                this.pathId,
                this.gerado,
                this.tempos
            );
            //Mock Repo
            var mockRepo = new Mock<IViagemRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(v1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            //Create service
            var service = new ViagemService(mockUnitRepo.Object, mockRepo.Object);
            //Create controller
            var controller = new ViagensController(service);

            //Resposta 
            var response = await controller.GetGetById(this.domainId);
            var responseBody = response.Value as ViagemDto;

            //Teste - Valida lineId
            Assert.Equal("teste", responseBody.lineId);
        }
        [Fact]
        public async void Create()
        {

            //Create Viagem
            var id = new ViagemId(this.domainId);
            var v1 = new Viagem(
                this.domainId,
                this.vazio,
                this.lineId,
                this.pathId,
                this.gerado,
                this.tempos
            );

            //Create ViagemDto
            var v2 = new ViagemDto
            {
                Id = new ViagemId(this.domainId).AsString(),
                domainId = this.domainId,
                vazio = this.vazio,
                lineId = this.lineId,
                pathId = this.pathId,
                gerado = this.gerado,
                tempos = this.tempos
            };


            //Mock Repo com viagem
            var mockRepo = new Mock<IViagemRepository>();
            mockRepo.Setup(repo => repo.AddAsync(v1)).ReturnsAsync(v1); ;
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create Service
            var service = new ViagemService(mockUnitRepo.Object, mockRepo.Object);

            //Create Controller
            var controller = new ViagensController(service);

            //Resposta
            var response = await controller.Create(v2);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is CreatedAtActionResult);
        }

        [Fact]
        public async void Update()
        {

            //Create Viagem
            var id = new ViagemId(this.domainId);
            var v1 = new Viagem(
                 this.domainId,
                this.vazio,
                this.lineId,
                this.pathId,
                this.gerado,
                this.tempos
            );

            //Novo ViagemDto para Update
            var v2 = new ViagemDto
            {
                Id = new ViagemId(this.domainId).AsString(),
                domainId = this.domainId,
                vazio = this.vazio,
                lineId = this.lineId,
                pathId = this.pathId,
                gerado = this.gerado,
                tempos = this.tempos
            };

            //Mock Repo
            var mockRepo = new Mock<IViagemRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(v1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create Service
            var service = new ViagemService(mockUnitRepo.Object, mockRepo.Object);

            //Create Controller
            var controller = new ViagensController(service);

            //Resposta
            var response = await controller.Update(this.domainId, v2);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is OkObjectResult);

        }

        [Fact]
        public async void Delete()
        {


            //Create Viagem
            var id = new ViagemId(this.domainId);
            var v1 = new Viagem(
                this.domainId,
                this.vazio,
                this.lineId,
                this.pathId,
                this.gerado,
                this.tempos
            );

            //Mock Repo
            var mockRepo = new Mock<IViagemRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(v1);
            mockRepo.Setup(repo => repo.Remove(v1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create service
            var service = new ViagemService(mockUnitRepo.Object, mockRepo.Object);

            //Create controller
            var controller = new ViagensController(service);

            //Resposta
            var response = await controller.HardDelete(this.domainId);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is OkObjectResult);

        }

        private List<Viagem> GetViagens()
        {
            var viagens = new List<Viagem>();
            viagens.Add(new Viagem(
                this.domainId,
                this.vazio,
                this.lineId,
                this.pathId,
                this.gerado,
                this.tempos
                ));

            viagens.Add(new Viagem(
                this.domainId2,
                this.vazio2,
                this.lineId2,
                this.pathId2,
                this.gerado2,
                this.tempos2
                ));

            return viagens;
        }
    }
}