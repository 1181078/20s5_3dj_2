using System;
using System.Net;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Threading.Tasks;
using Moq;
using MDV1.Domain.ServicosViatura;
using MDV1.Controllers;
using MDV1.Domain.Shared;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
namespace Testes
{
    public class ServicoViaturaTest
    {


        public string domainId = "teste";
        public string nome = "teste";
        public string cor = "teste";
        public string[] blocks = { "teste", "teste" };

        public string domainId2 = "teste2";
        public string nome2 = "teste2";
        public string cor2 = "teste2";
        public string[] blocks2 = { "teste2", "teste2" };

        [Fact]
        public async void GetAll()
        {
            //Mock Repo
            var mockRepo = new Mock<IServicoViaturaRepository>();
            mockRepo.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(GetSV());
            var mockUnitRepo = new Mock<IUnitOfWork>();
            //Create service
            var service = new ServicoViaturaService(mockUnitRepo.Object, mockRepo.Object);
            //Create controller
            var controller = new ServicosViaturaController(service);

            //Resposta
            var response = await controller.GetAll();
            var responseBody = response.Value as List<ServicoViaturaDto>;
            Assert.Equal(2,responseBody.Count());
        }

        [Fact]
        public async void GetGetById()
        {

            //create servicoTripulante
            var id = new ServicoViaturaId(this.domainId);
            var st1 = new ServicoViatura(
                this.domainId,
                this.nome,
                this.cor,
                this.blocks
            );
            //Mock Repo
            var mockRepo = new Mock<IServicoViaturaRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(st1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            //Create service
            var service = new ServicoViaturaService(mockUnitRepo.Object, mockRepo.Object);
            //Create controller
            var controller = new ServicosViaturaController(service);

            //Resposta 
            var response = await controller.GetGetById(this.domainId);
            var responseBody = response.Value as ServicoViaturaDto;

            //Teste - Valida nome
            Assert.Equal("teste",responseBody.nome);
        }
        [Fact]
        public async void Create()
        {

            //Create ServicoViatura
            var id = new ServicoViaturaId(this.domainId);
            var st1 = new ServicoViatura(
              this.domainId,
              this.nome,
              this.cor,
              this.blocks
          );

            //Create ServicoViaturaDto
            var st2 = new ServicoViaturaDto
            {
                Id = new ServicoViaturaId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            };

            //Mock Repo com servico de viatura
            var mockRepo = new Mock<IServicoViaturaRepository>();
            mockRepo.Setup(repo => repo.AddAsync(st1)).ReturnsAsync(st1); ;
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create Service
            var service = new ServicoViaturaService(mockUnitRepo.Object, mockRepo.Object);

            //Create Controller
            var controller = new ServicosViaturaController(service);

            //Resposta
            var response = await controller.Create(st2);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is CreatedAtActionResult);
        }

        [Fact]
        public async void Update()
        {

            //Create ServicoViatura
            var id = new ServicoViaturaId(this.domainId);
            var st1 = new ServicoViatura(
              this.domainId,
              this.nome,
              this.cor,
              this.blocks
          );

            //Novo ServicoViaturaDto para Update
            string[] novo = { "novo", "novo" };
            var st2Alt = new ServicoViaturaDto
            {
                Id = new ServicoViaturaId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome2,
                cor = this.cor2,
                blocks = this.blocks2
            };

            //Mock Repo
            var mockRepo = new Mock<IServicoViaturaRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(st1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create Service
            var service = new ServicoViaturaService(mockUnitRepo.Object, mockRepo.Object);

            //Create Controller
            var controller = new ServicosViaturaController(service);

            //Resposta
            var response = await controller.Update(this.domainId, st2Alt);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is OkObjectResult);

        }

        [Fact]
        public async void Delete()
        {


            //Create ServicoViatura
            var id = new ServicoViaturaId(this.domainId);
            var st1 = new ServicoViatura(
              this.domainId,
              this.nome,
              this.cor,
              this.blocks
          );

            //Mock Repo
            var mockRepo = new Mock<IServicoViaturaRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(st1);
            mockRepo.Setup(repo => repo.Remove(st1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            //Create service
            var service = new ServicoViaturaService(mockUnitRepo.Object, mockRepo.Object);

            //Create controller
            var controller = new ServicosViaturaController(service);

            //Resposta
            var response = await controller.HardDelete(this.domainId);
            var result = response.Result as ObjectResult;

            //Teste
            Assert.True(result is OkObjectResult);

        }
        private List<ServicoViatura> GetSV()
        {
            var sv = new List<ServicoViatura>();
            sv.Add(new ServicoViatura(
                this.domainId,
                this.nome,
                this.cor,
                this.blocks
                ));

            sv.Add(new ServicoViatura(
                this.domainId2,
                this.nome2,
                this.cor2,
                this.blocks2
                ));

            return sv;
        }




    }



}