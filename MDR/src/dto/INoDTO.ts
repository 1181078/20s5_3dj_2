import { TempoViagemGrupo } from "../domain/no/tempoViagemGrupo";

export default interface INoDTO {
    domainId: string;
    nome: string;
    latitude: number;
    longitude: number;
    nomeCurto: string;
    isEstacaoRecolha: boolean;
    isPontoRendicao: boolean;
    tempoViagem: TempoViagemGrupo[];
}