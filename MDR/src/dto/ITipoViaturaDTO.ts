export default interface ITipoViaturaDTO {
    domainId: string;
    nome: string;
    autonomia: number;
    custo: number;
    velocidadeMedia: number;
    combustivel: number;
    consumo: number;
    emissoes: number;
}