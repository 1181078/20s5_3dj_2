import { NosPercurso } from "../domain/percurso/nosPercurso";

export default interface IPercursoDTO {
  domainId: string;
  nosPercurso: NosPercurso[];
}