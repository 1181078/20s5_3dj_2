import { PercursoLinha } from "../domain/linha/percursoLinha";

export default interface ILinhaDTO {
    domainId: string;
    nome: string;
    cor: string;
    percursoLinha : PercursoLinha[];
  }