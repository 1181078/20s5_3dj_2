
export default interface IUserDTO {
  email: string;
  password: string;
  role: string
}
