import { Document, Model } from "mongoose";
import { INoPersistence } from "../dataschema/INoPersistence";
import INoDTO from "../dto/INoDTO";
import { No } from "../domain/no/no";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Mapper } from "../core/infra/Mapper";

export class NoMap extends Mapper<No> {

    public static toDTO(no: No): INoDTO {
        return{
          domainId: no.domainId,
            nome: no.nome,
            latitude: no.latitude,
            longitude: no.longitude,
            nomeCurto: no.nomeCurto,
            isEstacaoRecolha: no.isEstacaoRecolha,
            isPontoRendicao: no.isPontoRendicao,
            tempoViagem: no.isTempoViagem
        } as INoDTO;
    }

    public static toDomain(no: any | Model<INoPersistence & Document>): No {
        const noOrError = No.create(no, new UniqueEntityID(no.domainId)); 
        
        noOrError.isFailure ? console.log(noOrError.error) : '';

        return noOrError.isSuccess ? noOrError.getValue(): null;
    }

    public static toDomainArray(no: any | Model<INoPersistence & Document>[]): No[] {
      var pers: No[] = new Array(no.length);
  
      for (var i = 0; i < no.length; i++) {
        
        const noOrError = No.create(no[i],  new UniqueEntityID(no[i].domainId));
  
        noOrError.isFailure ? console.log(noOrError.error) : '';
  
        noOrError.isSuccess ? pers[i] = noOrError.getValue() : null;
      }
      return pers;
    }
    
      public static toPersistence (no: No): any {
        return {
          id: no.id.toString(),
          domainId: no.domainId,
          nome: no.nome,
          latitude: no.latitude,
          longitude: no.longitude,
          nomeCurto: no.nomeCurto,
          isEstacaoRecolha: no.isEstacaoRecolha,
          isPontoRendicao: no.isPontoRendicao,
          tempoViagem: no.isTempoViagem
        }
      }

}



