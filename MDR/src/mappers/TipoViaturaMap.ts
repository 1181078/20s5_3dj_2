import { Document, Model } from "mongoose";
import { ITipoViaturaPersistence } from "../dataschema/ITipoViaturaPersistence";
import ITipoViaturaDTO from "../dto/ITipoViaturaDTO";
import { TipoViatura } from "../domain/tipoViatura/tipoViatura";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Mapper } from "../core/infra/Mapper";

export class TipoViaturaMap extends Mapper<TipoViatura> {

    public static toDTO(tipoViatura: TipoViatura): ITipoViaturaDTO {
        return{
            domainId: tipoViatura.props.domainId,
            nome: tipoViatura.nome,
            autonomia: tipoViatura.autonomia,
            custo: tipoViatura.custo,
            velocidadeMedia: tipoViatura.velocidadeMedia,
            combustivel: tipoViatura.combustivel,
            consumo: tipoViatura.consumo,
            emissoes: tipoViatura.emissoes
        } as ITipoViaturaDTO;
    }

    public static toDomain(tipoViatura: any | Model<ITipoViaturaPersistence & Document>): TipoViatura {
        const tipoOrError = TipoViatura.create(tipoViatura, new UniqueEntityID(tipoViatura.domainId)); 
        
        tipoOrError.isFailure ? console.log(tipoOrError.error) : '';

        return tipoOrError.isSuccess ? tipoOrError.getValue(): null;
    }

    public static toDomainArray(tipoViatura: any | Model<ITipoViaturaPersistence & Document>[]): TipoViatura[] {
      var tipos: TipoViatura[] = new Array(tipoViatura.length);
  
      for (var i = 0; i < tipoViatura.length; i++) {
        
        const tipoOrError = TipoViatura.create(tipoViatura[i],  new UniqueEntityID(tipoViatura[i].domainId));
  
        tipoOrError.isFailure ? console.log(tipoOrError.error) : '';
  
        tipoOrError.isSuccess ? tipos[i] = tipoOrError.getValue() : null;
      }
      return tipos;
    }
    
      public static toPersistence (tipoViatura: TipoViatura): any {
        return {
          id: tipoViatura.id.toString(),
          domainId: tipoViatura.props.domainId,
          nome: tipoViatura.nome,
          autonomia: tipoViatura.autonomia,
          custo: tipoViatura.custo,
          velocidadeMedia: tipoViatura.velocidadeMedia,
          combustivel: tipoViatura.combustivel,
          consumo: tipoViatura.consumo,
          emissoes: tipoViatura.emissoes
        }
      }

}



