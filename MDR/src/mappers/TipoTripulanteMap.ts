import { Document, Model } from "mongoose";
import { ITipoTripulantePersistence } from "../dataschema/ITipoTripulantePersistence";
import ITipoTripulanteDTO from "../dto/ITipoTripulanteDTO";
import { TipoTripulante } from "../domain/tipoTripulante/tipoTripulante";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Mapper } from "../core/infra/Mapper";

export class TipoTripulanteMap extends Mapper<TipoTripulante> {

  public static toDTO(tipoTripulante: TipoTripulante): ITipoTripulanteDTO {
    return {
      domainId: tipoTripulante.props.domainId,
      desc: tipoTripulante.desc,
    } as ITipoTripulanteDTO;
  }

  public static toDomain(tipoTripulante: any | Model<ITipoTripulantePersistence & Document>): TipoTripulante {
    const tipoOrError = TipoTripulante.create(tipoTripulante, new UniqueEntityID(tipoTripulante.domainId));

    tipoOrError.isFailure ? console.log(tipoOrError.error) : '';

    return tipoOrError.isSuccess ? tipoOrError.getValue() : null;
  }

  public static toDomainArray(tipoTripulante: any | Model<ITipoTripulantePersistence & Document>[]): TipoTripulante[] {
    var tipos: TipoTripulante[] = new Array(tipoTripulante.length);

    for (var i = 0; i < tipoTripulante.length; i++) {
      
      const tipoOrError = TipoTripulante.create(tipoTripulante[i],  new UniqueEntityID(tipoTripulante[i].domainId));

      tipoOrError.isFailure ? console.log(tipoOrError.error) : '';

      tipoOrError.isSuccess ? tipos[i] = tipoOrError.getValue() : null;
    }
    return tipos;
  }

  public static toPersistence(tipoTripulante: TipoTripulante): any {
    return {
      id: tipoTripulante.id.toString(),
      domainId: tipoTripulante.props.domainId,
      desc: tipoTripulante.desc
    }
  }


}