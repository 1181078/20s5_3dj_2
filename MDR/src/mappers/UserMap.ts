import { Container } from 'typedi';

import { Mapper } from "../core/infra/Mapper";

import IUserDTO from "../dto/IUserDTO";

import { User } from "../domain/user/user";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import { UserEmail } from "../domain/user/userEmail";
import { UserPassword } from "../domain/user/userPassword";

import RoleRepo from "../repos/roleRepo";

export class UserMap extends Mapper<User> {

  public static toDTO( user: User): IUserDTO {
    return {
      id: user.id.toString(),
      email: user.email,
      password: user.password,
      role: user.role
    } as IUserDTO;
  }

  public static async toDomain (raw: any): Promise<User> {
    const userEmailOrError = UserEmail.create(raw.email);
    const userPasswordOrError = UserPassword.create({value: raw.password, hashed: true});
    const repo = Container.get(RoleRepo);
    const role = raw.role;

    const userOrError = User.create({
      email: raw.email,
      password: raw.password,
      role: role,
    }, new UniqueEntityID(raw.base_user_id))

    userOrError.isFailure ? console.log(userOrError.error) : '';
    
    return userOrError.isSuccess ? userOrError.getValue() : null;
  }

  public static toDomainArray(user: any ): User[] {
    var us: User[] = new Array(user.length);

    for (var i = 0; i < user.length; i++) {
      
      const tipoOrError = User.create(user[i],  new UniqueEntityID(user[i].username));

      tipoOrError.isFailure ? console.log(tipoOrError.error) : '';

      tipoOrError.isSuccess ? us[i] = tipoOrError.getValue() : null;
    }
    return us;
  }


  public static toPersistence (user: User): any {
    const a = {
      base_user_id: user.id.toString(),
      email: user.email,
      password: user.password,
      role: user.role,
    }
    return a;
  }
}