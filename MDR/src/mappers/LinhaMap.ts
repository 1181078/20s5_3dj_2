import { Document, Model } from "mongoose";
import { ILinhaPersistence } from "../dataschema/ILinhaPersistence";
import ILinhaDTO from "../dto/ILinhaDTO";
import { Linha } from "../domain/linha/linha";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Mapper } from "../core/infra/Mapper";

export class LinhaMap extends Mapper<Linha> {

  public static toDTO(linha: Linha): ILinhaDTO {
    return {
      domainId: linha.domainId,
      nome: linha.props.nome,
      cor: linha.props.cor,
      percursoLinha: linha.props.percursoLinha
    } as ILinhaDTO;
  }

  public static toDomain(linha: any | Model<ILinhaPersistence & Document>): Linha {
    const linhaOrError = Linha.create(linha, new UniqueEntityID(linha.domainId));

    linhaOrError.isFailure ? console.log(linhaOrError.error) : '';

    return linhaOrError.isSuccess ? linhaOrError.getValue() : null;
  }

  public static toDomainArray(linha: any | Model<ILinhaPersistence & Document>[]): Linha[] {
    var pers: Linha[] = new Array(linha.length);

    for (var i = 0; i < linha.length; i++) {

      const linhaOrError = Linha.create(linha[i], new UniqueEntityID(linha[i].domainId));

      linhaOrError.isFailure ? console.log(linhaOrError.error) : '';

      linhaOrError.isSuccess ? pers[i] = linhaOrError.getValue() : null;
    }
    return pers;
  }

  public static toPersistence(linha: Linha): any {
    return {
      id: linha.id.toString(),
      domainId: linha.props.domainId,
      nome: linha.props.nome,
      cor: linha.props.cor,
      percursoLinha: linha.props.percursoLinha
    }
  }

}