import { Document, Model } from "mongoose";
import { IPercursoPersistence } from "../dataschema/IPercursoPersistence";
import IPercursoDTO from "../dto/IPercursoDTO";
import { Percurso } from "../domain/percurso/percurso";

import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Mapper } from "../core/infra/Mapper";

export class PercursoMap extends Mapper<Percurso> {

    public static toDTO(percurso: Percurso): IPercursoDTO {
        return{
            domainId: percurso.props.domainId,
            nosPercurso: percurso.props.nosPercurso,
        } as IPercursoDTO;
    }

    public static toDomain(percurso: any | Model<IPercursoPersistence & Document>): Percurso {
        const percursoOrError = Percurso.create(percurso, new UniqueEntityID(percurso.domainId)); 
        
        percursoOrError.isFailure ? console.log(percursoOrError.error) : '';

        return percursoOrError.isSuccess ? percursoOrError.getValue(): null;
    }
    
      public static toPersistence (percurso: Percurso): any {
        return {
          id: percurso.id.toString(),
          domainId: percurso.props.domainId,
          nosPercurso: percurso.props.nosPercurso
        }
      }

      public static toDomainArray(percurso: any | Model<IPercursoPersistence & Document>[]): Percurso[] {
        var pers: Percurso[] = new Array(percurso.length);
    
        for (var i = 0; i < percurso.length; i++) {
          
          const percursoOrError = Percurso.create(percurso[i],  new UniqueEntityID(percurso[i].domainId));
    
          percursoOrError.isFailure ? console.log(percursoOrError.error) : '';
    
          percursoOrError.isSuccess ? pers[i] = percursoOrError.getValue() : null;
        }
        return pers;
      }

}
