import {Request, Response, NextFunction } from 'express';
import {Container, Inject, Service } from 'typedi';
import ITipoViaturaService from '../services/IServices/ITipoViaturaService';
import ITipoViaturaController from './IControllers/ITipoViaturaController';
import ITipoViaturaDTO from '../dto/ITipoViaturaDTO';
import {celebrate, Joi } from 'celebrate';
import { nextTick } from 'process';
import { Result } from "../core/logic/Result";
import config from "../../config.js";

export default class TipoViaturaController implements ITipoViaturaController{
    constructor(
        @Inject(config.services.tipoViatura.name)
        private tipoViaturaServiceInstance: ITipoViaturaService
    ){}

    public async createTipoViatura(req: Request, res: Response, next: NextFunction){

        try{
            const tipoOrError = await this.tipoViaturaServiceInstance.createTipoViatura(req.body as ITipoViaturaDTO) as Result<ITipoViaturaDTO>;
        
        if(tipoOrError.isFailure){
            return res.status(400).send();
        }
        
        const tipoDTO = tipoOrError.getValue();
        return res.status(201).json(tipoDTO).send();

        }
        catch(err){
            return next(err);
        }
    }

    public async updateTipoViatura(req: Request, res: Response, next: NextFunction) {
        try {
          const tipoOrError = await this.tipoViaturaServiceInstance.updateTipoViatura(req.body as ITipoViaturaDTO) as Result<ITipoViaturaDTO>;
    
          if (tipoOrError.isFailure) {
            return res.status(404).send();
          }
    
          const tipoDTO = tipoOrError.getValue();
          return res.json(tipoDTO).status(200);
        }
        catch (e) {
          return next(e);
        }
      }

      public async getTipoViaturaById(req: Request, res: Response, next: NextFunction) {
        try {
          const tipoOrError = await this.tipoViaturaServiceInstance.getTipoViaturaById(req.params.id) as Result<ITipoViaturaDTO>;
    
          if (tipoOrError.isFailure) {
            return res.status(404).send();
          }
    
          const tipoDTO = tipoOrError.getValue();
          return res.json(tipoDTO).status(200);
        }
        catch (e) {
          return next(e);
        }
      };
    
      public async listTiposViatura(req: Request, res: Response, next: NextFunction){
        try {
          const tipoList = await this.tipoViaturaServiceInstance.listTiposViatura()as Result<ITipoViaturaDTO[]>;
    
          if (tipoList.isFailure) {
            return res.status(404).send();
          }
    
          const tipoDTOList = tipoList.getValue();
          return res.json(tipoDTOList).status(200);
        }
        catch (e) {
          return next(e);
        }
    }

    public async listTiposViaturaSorted(req: Request, res: Response, next: NextFunction){
      try {
        const tipoList = await this.tipoViaturaServiceInstance.listTiposViaturaSorted(req.params.ord)as Result<ITipoViaturaDTO[]>;
  
        if (tipoList.isFailure) {
          return res.status(404).send();
        }
  
        const tipoDTOList = tipoList.getValue();
        return res.json(tipoDTOList).status(200);
      }
      catch (e) {
        return next(e);
      }
  }

  public async deleteTipoViaturaById(req: Request, res: Response, next: NextFunction) {
    try {
      const tipoIDOrError = await this.tipoViaturaServiceInstance.deleteTipoViaturaById(req.params.id) as Result<string>;

      if (tipoIDOrError.isFailure || tipoIDOrError.getValue()=== null) {
        return res.status(404).send();
      }

      const tipoDTO = tipoIDOrError.getValue();
      return res.json({ domainId: tipoDTO}).status(200);
    }
    catch (e) {
      return next(e);
    }
  };


}