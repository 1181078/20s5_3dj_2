import { Request, Response, NextFunction } from 'express';
import { Container, Inject, Service } from 'typedi';
import ITipoTripulanteService from '../services/IServices/ITipoTripulanteService';
import ITipoTripulanteController from './IControllers/ITipoTripulanteController';
import ITipoTripulanteDTO from '../dto/ITipoTripulanteDTO';
import { celebrate, Joi } from 'celebrate';
import { nextTick } from 'process';
import { Result } from "../core/logic/Result";
import config from "../../config.js";
import { TipoTripulante } from '../domain/tipoTripulante/tipoTripulante';

export default class TipoTripulanteController implements ITipoTripulanteController {
  constructor(
    @Inject(config.services.tipoTripulante.name)
    private tipoTripulanteServiceInstance: ITipoTripulanteService
  ) { }

  public async createTipoTripulante(req: Request, res: Response, next: NextFunction) {

    try {
      
      const tipoOrError = await this.tipoTripulanteServiceInstance.createTipoTripulante(req.body as ITipoTripulanteDTO) as Result<ITipoTripulanteDTO>;

      if (tipoOrError.isFailure) {
        return res.status(400).send();
      }

      const tipoDTO = tipoOrError.getValue();

      return res.status(201).json(tipoDTO).send();


    }
    catch (err) {
      return next(err);
    }
  }


  public async updateTipoTripulante(req: Request, res: Response, next: NextFunction) {
    try {
      const tipoOrError = await this.tipoTripulanteServiceInstance.updateTipoTripulante(req.body as ITipoTripulanteDTO) as Result<ITipoTripulanteDTO>;
     
      if (tipoOrError.isFailure) {
        return res.status(404).send();
      }

      const tipoDTO = tipoOrError.getValue();

      return res.json(tipoDTO).status(200);
    }
    catch (e) {
      return next(e);
    }
  };


  public async getTipoTripulanteById(req: Request, res: Response, next: NextFunction) {
    try {
      const tipoOrError = await this.tipoTripulanteServiceInstance.getTipoTripulanteById(req.params.id) as Result<ITipoTripulanteDTO>;

      if (tipoOrError.isFailure || tipoOrError.getValue()=== null) {
        return res.status(404).send();
      }

      const tipoDTO = tipoOrError.getValue();
      return res.json({ domainId: tipoDTO.domainId, descricao: tipoDTO.desc }).status(200);
    }
    catch (e) {
      return next(e);
    }
  };

  public async listTiposTripulante(req: Request, res: Response, next: NextFunction) {
    try {
      const tipoList = await this.tipoTripulanteServiceInstance.listTiposTripulante() as Result<ITipoTripulanteDTO[]>;

      if (tipoList.isFailure) {
        return res.status(404).send();
      }

      const tipoDTOList = tipoList.getValue();
      return res.json(tipoDTOList).status(200);
    }
    catch (e) {
      return next(e);
    }
  }

  public async listTiposTripulanteSorted(req: Request, res: Response, next: NextFunction) {
    try {
      const tipoList = await this.tipoTripulanteServiceInstance.listTiposTripulanteSorted(req.params.ord) as Result<ITipoTripulanteDTO[]>;

      if (tipoList.isFailure) {
        return res.status(404).send();
      }

      const tipoDTOList = tipoList.getValue();
      return res.json(tipoDTOList).status(200);
    }
    catch (e) {
      return next(e);
    }
  }


  public async deleteTipoTripulanteById(req: Request, res: Response, next: NextFunction) {
    try {
      const tipoIDOrError = await this.tipoTripulanteServiceInstance.deleteTipoTripulanteById(req.params.id) as Result<string>;

      if (tipoIDOrError.isFailure || tipoIDOrError.getValue()=== null) {
        return res.status(404).send();
      }

      const tipoDTO = tipoIDOrError.getValue();
      return res.json({ domainId: tipoDTO}).status(200);
    }
    catch (e) {
      return next(e);
    }
  };

}