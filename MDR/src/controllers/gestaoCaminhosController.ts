import { Request, Response, NextFunction, response } from 'express';
import { Container, Inject, Service } from 'typedi';
import { celebrate, Joi } from 'celebrate';
import { nextTick } from 'process';
import { Result } from "../core/logic/Result";
import config from "../../config.js";
import IGestaoCaminhosController from './IControllers/IGestaoCaminhosController';
import { request } from 'http';

export default class gestaoCaminhosController implements IGestaoCaminhosController {

    constructor(

    ) { };


    public async gerirCaminhoAstar(req: Request, res: Response, next: NextFunction) {
        const req1 = request(
            {
                host: 'localhost',
                port: '5000',
                path: '/aStar',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            },
            response => {

                const chunks = [];
                response.on('data', (chunk) => {
                    chunks.push(chunk);
                });
                response.on('end', () => {
                    const result = Buffer.concat(chunks).toString();
    
 
                    res.json(JSON.parse(result));
                }); 
    
            }   

        );
        
  
        req1.write(JSON.stringify(req.body));

        req1.end();

    }


    public async gerirCaminhoTodosMelhor(req: Request, res: Response, next: NextFunction) {
        const req1 = request(
            {
                host: 'localhost',
                port: '5000',
                path: '/todosMelhor',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            },
            response => {

                const chunks = [];
                response.on('data', (chunk) => {
                    chunks.push(chunk);
                });
                response.on('end', () => {
                    const result = Buffer.concat(chunks).toString();

                    

                    res.json(JSON.parse(result));
                });

            }

        );
        
  
        req1.write(JSON.stringify(req.body));

        req1.end();

    }

    public async gerirCaminhoBestfs(req: Request, res: Response, next: NextFunction) {
        const req1 = request(
            {
                host: 'localhost',
                port: '5000',
                path: '/bestfs',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            },
            response => {

                const chunks = [];
                response.on('data', (chunk) => {
                    chunks.push(chunk);
                });
                response.on('end', () => {
                    const result = Buffer.concat(chunks).toString();
                

                    res.json(JSON.parse(result));
                });

            }

        );

        
  
        req1.write(JSON.stringify(req.body));

        req1.end();
    }


    public async ag(req: Request, res: Response, next: NextFunction) {
        const req1 = request(
            {
                host: 'localhost',
                port: '5000',
                path: '/ag',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            },
            response => {

                const chunks = [];
                response.on('data', (chunk) => {
                    chunks.push(chunk);
                });
                response.on('end', () => {
                    const result = Buffer.concat(chunks).toString();

                    console.log(JSON.parse(result));
                    
                    res.json(JSON.parse(result));
                    
                });

            }

        );

        
        console.log(req.body);
  
        req1.write(JSON.stringify(req.body)); 

        req1.end();
    }
} 