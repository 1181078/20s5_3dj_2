import {Request, Response, NextFunction } from 'express';
import {Container, Inject, Service } from 'typedi';
import IPercursoService from '../services/IServices/IPercursoService';
import IPercursoController from './IControllers/IPercursoController';
import IPercursoDTO from '../dto/IPercursoDTO';
import {celebrate, Joi } from 'celebrate';
import { nextTick } from 'process';
import { Result } from "../core/logic/Result";
import config from "../../config.js";

export default class PercursoController implements IPercursoController{
    constructor(
        @Inject(config.services.percurso.name)
        private percursoServiceInstance: IPercursoService
    ){}

    public async createPercurso(req: Request, res: Response, next: NextFunction){

        try{
            const percursoOrError = await this.percursoServiceInstance.createPercurso(req.body as IPercursoDTO) as Result<IPercursoDTO>;
        
        if(percursoOrError.isFailure){
            return res.status(400).send();
        }
        
        const tipoDTO = percursoOrError.getValue();
        return res.status(201).json(tipoDTO).send();

        }
        catch(err){
            return next(err);
        }
    }

    public async updatePercurso(req: Request, res: Response, next: NextFunction) {
        try {
          const percursoOrError = await this.percursoServiceInstance.updatePercurso(req.body as IPercursoDTO) as Result<IPercursoDTO>;
    
          if (percursoOrError.isFailure) {
            return res.status(404).send();
          }
    
          const tipoDTO = percursoOrError.getValue();
          return res.json(tipoDTO).status(200);
        }
        catch (e) {
          return next(e);
        }
      };

      public async getPercursoById(req: Request, res: Response, next: NextFunction) {
        try {
          const percursoOrError = await this.percursoServiceInstance.getPercursoById(req.params.id) as Result<IPercursoDTO>;
    
          if (percursoOrError.isFailure) {
            return res.status(404).send();
          }
    
          const percursoDTO = percursoOrError.getValue();
          return res.json(percursoDTO).status(200);
        }
        catch (e) {
          return next(e);
        }
      };
    
      public async listPercursos(req: Request, res: Response, next: NextFunction){
        try {
          const percursoList = await this.percursoServiceInstance.listPercursos() as Result<IPercursoDTO[]>;
    
          if (percursoList.isFailure) {
            return res.status(404).send();
          }
    
          const percursoDTOList = percursoList.getValue();
          return res.json(percursoDTOList).status(200);
        }
        catch (e) {
          return next(e);
        }
    }

    public async deletePercursoById(req: Request, res: Response, next: NextFunction) {
      try {
        const tipoIDOrError = await this.percursoServiceInstance.deletePercursoById(req.params.id) as Result<string>;
  
        if (tipoIDOrError.isFailure || tipoIDOrError.getValue()=== null) {
          return res.status(404).send();
        }
  
        const tipoDTO = tipoIDOrError.getValue();
        return res.json({ domainId: tipoDTO}).status(200);
      }
      catch (e) {
        return next(e);
      }
    };

}