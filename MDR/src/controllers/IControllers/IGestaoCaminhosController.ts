import { Request, Response, NextFunction } from 'express';

export default interface IGestaoCaminhosController  {
    gerirCaminhoAstar(req: Request, res: Response, next: NextFunction);
    gerirCaminhoTodosMelhor(req: Request, res: Response, next: NextFunction);
    gerirCaminhoBestfs(req: Request, res: Response, next: NextFunction);
    ag(req: Request, res: Response, next: NextFunction);
}