import { Request, Response, NextFunction } from 'express';

export default interface ITipoViaturaController  {
    createTipoViatura(req: Request, res: Response, next: NextFunction);
    updateTipoViatura(req: Request, res: Response, next: NextFunction);
    listTiposViatura(req: Request, res: Response, next: NextFunction);
    listTiposViaturaSorted(req: Request, res: Response, next: NextFunction);
    getTipoViaturaById(req: Request, res: Response, next: NextFunction);
    deleteTipoViaturaById(req: Request, res: Response, next: NextFunction);
}