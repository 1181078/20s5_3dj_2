import { Request, Response, NextFunction } from 'express';

export default interface ITipoTripulanteController  {
    createTipoTripulante(req: Request, res: Response, next: NextFunction);
    updateTipoTripulante(req: Request, res: Response, next: NextFunction);
    listTiposTripulante(req: Request, res: Response, next: NextFunction);
    listTiposTripulanteSorted(req: Request, res: Response, next: NextFunction);
    getTipoTripulanteById(req: Request, res: Response, next: NextFunction);
    deleteTipoTripulanteById(req: Request, res: Response, next: NextFunction);
}