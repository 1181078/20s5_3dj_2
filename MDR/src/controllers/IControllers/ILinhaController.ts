import { Request, Response, NextFunction } from 'express';

export default interface ILinhaController  {
    createLinha(req: Request, res: Response, next: NextFunction);
    updateLinha(req: Request, res: Response, next: NextFunction);
    listLinhas(req: Request, res: Response, next: NextFunction);
    listLinhasSorted(req: Request, res: Response, next: NextFunction);
    getLinhaById(req: Request, res: Response, next: NextFunction);
    deleteLinhaById(req: Request, res: Response, next: NextFunction);
}