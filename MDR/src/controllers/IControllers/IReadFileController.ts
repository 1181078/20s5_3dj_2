import { Request, Response, NextFunction } from 'express';

export default interface IReadFileController  {
    readFile(req: Request, res: Response, next: NextFunction);
  

}