import { Request, Response, NextFunction } from 'express';

export default interface INoController  {
    createNo(req: Request, res: Response, next: NextFunction);
    updateNo(req: Request, res: Response, next: NextFunction);
    listNos(req: Request, res: Response, next: NextFunction);
    listNosSorted(req: Request, res: Response, next: NextFunction);
    getNoById(req: Request, res: Response, next: NextFunction);
    getNosPontoOuEstacao(req: Request, res: Response, next: NextFunction);    
    deleteNoById(req: Request, res: Response, next: NextFunction);
}