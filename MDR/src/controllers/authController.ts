import { Request, Response, NextFunction } from 'express';
import { Container, Inject, Service } from 'typedi';
import AuthService from '../services/auth';
import IUserDTO from '../dto/IUserDTO';
import { celebrate, Joi } from 'celebrate';
import { nextTick } from 'process';
import { Result } from "../core/logic/Result";
import config from "../../config.js";
import IAuthService from '../services/IServices/IAuthService';

export default class AuthController{
    constructor(
        @Inject(config.services.auth.name)
        private authServiceInstance: IAuthService)
    { }


    public async listUsers(req: Request, res: Response, next: NextFunction) {


        try {
            const userList = await this.authServiceInstance.listUsers() as Result<IUserDTO[]>;

            if (userList.isFailure) {
                return res.status(404).send();
            }

            const userDTOList = userList.getValue();
            return res.json(userDTOList).status(200);
        }
        catch (e) {
            return next(e);
        }
    }


}