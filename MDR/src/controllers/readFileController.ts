import {Request, Response, NextFunction } from 'express';
import {Container, Inject, Service } from 'typedi';
import IReadFileService from '../services/IServices/IReadFileService';
import IReadFileController from './IControllers/IReadFileController';
import {celebrate, Joi } from 'celebrate';
import { nextTick } from 'process';
import { Result } from "../core/logic/Result";
import config from "../../config.js";
import { fstat } from 'fs/promises';
import readFileService from '../services/readFileService';
const fs = require('fs');
const formidable = require('formidable');

export default class readFileController implements IReadFileController{
    constructor(
        @Inject(config.services.readFile.name)
        private readFileServiceInstance: IReadFileService
    ){}

    public async readFile(req: Request, res: Response, next: NextFunction){

 
        try{
            let form = new formidable.IncomingForm();
            let awaitImport = this.awaitImport;
            let data;
            form.parse(req, function(error, fields, files) {
                if (error) {
                    throw error;
                }
                fs.readFile(files.lapr5file.path, function (error, data){
                    if (error) {
                        throw error;
                    } 
                    awaitImport(data); 
                });

            });
            return res.status(201).json("Sucesso!").send();
            
        }catch(err){
            return next(err);
        }
    }

    private async awaitImport(data: any){
        const readFileServiceInstance = Container.get(readFileService);
        const result = await readFileServiceInstance.readFile(data);
        return result;
    }


}