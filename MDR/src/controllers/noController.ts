import {Request, Response, NextFunction } from 'express';
import {Container, Inject, Service } from 'typedi';
import INoService from '../services/IServices/INoService';
import INoController from './IControllers/INoController';
import INoDTO from '../dto/INoDTO';
import {celebrate, Joi } from 'celebrate';
import { nextTick } from 'process';
import { Result } from "../core/logic/Result";
import config from "../../config.js";

export default class NoController implements INoController{
    constructor(
        @Inject(config.services.no.name)
        private noServiceInstance: INoService
    ){}


    public async createNo(req: Request, res: Response, next: NextFunction){

        try{
            const noOrError = await this.noServiceInstance.createNo(req.body as INoDTO) as Result<INoDTO>;
        
        if(noOrError.isFailure){
            return res.status(400).send();
        }
        
        const noDTO = noOrError.getValue();
        return res.status(201).json(noDTO).send();

        }
        catch(err){
            return next(err);
        }
    }

    public async updateNo(req: Request, res: Response, next: NextFunction) {
        try {        
          const noOrError = await this.noServiceInstance.updateNo(req.body as INoDTO) as Result<INoDTO>;
    
          if (noOrError.isFailure) {
            return res.status(404).send();
          }
    
          const tipoDTO = noOrError.getValue();
          return res.json(tipoDTO).status(200);
        }
        catch (e) {
          return next(e);
        }
      }

      public async getNoById(req: Request, res: Response, next: NextFunction) {
        try {
          const noOrError = await this.noServiceInstance.getNoById(req.params.id) as Result<INoDTO>;
    
          if (noOrError.isFailure) {
            return res.status(404).send();
          }
    
          const noDTO = noOrError.getValue();
          return res.json(noDTO).status(200);
        }
        catch (e) {
          return next(e);
        }
      };
      
    
      public async listNos(req: Request, res: Response, next: NextFunction){
        try {
          const noList = await this.noServiceInstance.listNos()as Result<INoDTO[]>;
    
          if (noList.isFailure) {
            return res.status(404).send();
          }
    
          const noDTOList = noList.getValue();
          return res.json(noDTOList).status(200);
        }
        catch (e) {
          return next(e);
        }
    }

    public async listNosSorted(req: Request, res: Response, next: NextFunction){
      try {
        const noList = await this.noServiceInstance.listNosSorted(req.params.ord)as Result<INoDTO[]>;
  
        if (noList.isFailure) {
          return res.status(404).send();
        }
  
        const noDTOList = noList.getValue();
        return res.json(noDTOList).status(200);
      }
      catch (e) {
        return next(e);
      }
  }

  public async getNosPontoOuEstacao(req: Request, res: Response, next: NextFunction){

    try {
      const noList = await this.noServiceInstance.listPOuE()as Result<INoDTO[]>;

      if (noList.isFailure) {
        return res.status(404).send();
      }

      const noDTOList = noList.getValue();
      return res.json(noDTOList).status(200);
    }
    catch (e) {
      return next(e);
    }
}

public async deleteNoById(req: Request, res: Response, next: NextFunction) {
  try {
    const tipoIDOrError = await this.noServiceInstance.deleteNoById(req.params.id) as Result<string>;

    if (tipoIDOrError.isFailure || tipoIDOrError.getValue()=== null) {
      return res.status(404).send();
    }

    const tipoDTO = tipoIDOrError.getValue();
    return res.json({ domainId: tipoDTO}).status(200);
  }
  catch (e) {
    return next(e);
  }
};

}