import {Request, Response, NextFunction } from 'express';
import {Container, Inject, Service } from 'typedi';
import ILinhaService from '../services/IServices/ILinhaService';
import ILinhaController from './IControllers/ILinhaController';
import ILinhaDTO from '../dto/ILinhaDTO';
import {celebrate, Joi } from 'celebrate';
import { nextTick } from 'process';
import { Result } from "../core/logic/Result";
import config from "../../config.js";

export default class LinhaController implements ILinhaController{
    constructor(
        @Inject(config.services.linha.name)
        private linhaServiceInstance: ILinhaService
    ){}

    public async createLinha(req: Request, res: Response, next: NextFunction){

        try{
            const linhaOrError = await this.linhaServiceInstance.createLinha(req.body as ILinhaDTO) as Result<ILinhaDTO>;
        
        if(linhaOrError.isFailure){
            return res.status(400).send();
        }
        
        const tipoDTO = linhaOrError.getValue();
        return res.status(201).json(tipoDTO).send();

        }
        catch(err){
            return next(err);
        }
    }

    public async updateLinha(req: Request, res: Response, next: NextFunction) {
        try {
          const linhaOrError = await this.linhaServiceInstance.updateLinha(req.body as ILinhaDTO) as Result<ILinhaDTO>;
    
          if (linhaOrError.isFailure) {
            return res.status(404).send();
          }
    
          const tipoDTO = linhaOrError.getValue();
          return res.json(tipoDTO).status(200);
        }
        catch (e) {
          return next(e);
        }
      };

      
      public async getLinhaById(req: Request, res: Response, next: NextFunction) {
        try {
          const noOrError = await this.linhaServiceInstance.getLinhaById(req.params.id) as Result<ILinhaDTO>;
    
          if (noOrError.isFailure) {
            return res.status(404).send();
          }
    
          const linhaDTO = noOrError.getValue();

          return res.json(linhaDTO).status(200);
        }
        catch (e) {
          return next(e);
        }
      };
    
      public async listLinhas(req: Request, res: Response, next: NextFunction){
        try {
          const linhaList = await this.linhaServiceInstance.listLinhas()as Result<ILinhaDTO[]>;
    
          if (linhaList.isFailure) {
            return res.status(404).send();
          }
    
          const linhaDTOList = linhaList.getValue();
          return res.json(linhaDTOList).status(200);
        }
        catch (e) {
          return next(e);
        }
    }

    public async listLinhasSorted(req: Request, res: Response, next: NextFunction){
      try {
        const linhaList = await this.linhaServiceInstance.listLinhasSorted(req.params.ord)as Result<ILinhaDTO[]>;
  
        if (linhaList.isFailure) {
          return res.status(404).send();
        }
  
        const linhaDTOList = linhaList.getValue();
        return res.json(linhaDTOList).status(200);
      }
      catch (e) {
        return next(e);
      }
  }

  public async deleteLinhaById(req: Request, res: Response, next: NextFunction) {
    try {
      const tipoIDOrError = await this.linhaServiceInstance.deleteLinhaById(req.params.id) as Result<string>;

      if (tipoIDOrError.isFailure || tipoIDOrError.getValue()=== null) {
        return res.status(404).send();
      }

      const tipoDTO = tipoIDOrError.getValue();
      return res.json({ domainId: tipoDTO}).status(200);
    }
    catch (e) {
      return next(e);
    }
  };

}