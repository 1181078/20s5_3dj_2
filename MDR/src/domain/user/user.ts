import { AggregateRoot } from "../../core/domain/AggregateRoot";
import { UniqueEntityID } from "../../core/domain/UniqueEntityID";
import { Result } from "../../core/logic/Result";
import { UserId } from "./userId";
import { UserEmail } from "./userEmail";
import { Role } from "../role/role";
import { UserPassword } from "./userPassword";
import { Guard } from "../../core/logic/Guard";
import IUserDTO from "../../dto/IUserDTO";


interface UserProps {
  email: string;
  password: string;
  role: string;
}

export class User extends AggregateRoot<UserProps> {
  get id (): UniqueEntityID {
    return this._id;
  }

  get userId (): UserId {
    return UserId.caller(this.id)
  }

  get email (): string {
    return this.props.email;
  }


  get password (): string {
    return this.props.password;
  }

  get role (): string {
    return this.props.role;
  }
  
  set role (value: string) {
    this.props.role = value;
  }

  set password (password: string) {
    this.props.password = password;
  }

  private constructor (props: UserProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create (userDTO: IUserDTO, id?: UniqueEntityID): Result<User> {

    const pa = userDTO.password;
    const em = userDTO.email;
    const ro = userDTO.role;

    if (!!em === false || em.length === 0) {
      return Result.fail<User>('Must provide an email')
    } else {
      const tipo = new User({ email:em, password:pa, role:ro}, id);
      return Result.ok<User>( tipo )
    }
  }
}


