import { AggregateRoot } from "../../core/domain/AggregateRoot";
import { UniqueEntityID } from "../../core/domain/UniqueEntityID";

import { Result } from "../../core/logic/Result";
import { PercursoId } from "./percursoId";

import IPercursoDTO from "../../dto/IPercursoDTO";
import { NosPercurso } from "./nosPercurso";

import { Guard } from "../../core/logic/Guard";

interface PercursoProps {
  domainId: string;
  nosPercurso: NosPercurso[];
}

export class Percurso extends AggregateRoot<PercursoProps> {
  get id (): UniqueEntityID {
    return this._id;
  }

  get percursoId (): PercursoId {
    return PercursoId.create(this.id);
  }

  get nosPercurso (): NosPercurso[] {
    return this.props.nosPercurso;
  }

  set nosPercurso ( value: NosPercurso[]) {
    this.props.nosPercurso = value;
  }

  private constructor (props: PercursoProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create (props: IPercursoDTO, id?: UniqueEntityID): Result<Percurso> {

    const domainId = props.domainId;
    const nosPer = props.nosPercurso;
    const newProps = {
      domainId: domainId,
      nosPercurso: nosPer
    } as PercursoProps;

    if(!!nosPer === false){
      return Result.fail<Percurso>('Must provide information')
    }else{
      const per = new Percurso(newProps, id);
      return Result.ok<Percurso>( per )
    }
    
  }
}
