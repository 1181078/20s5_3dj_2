import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface NosPercursoProps {
    noId: String, duracao: Number, distancia: Number ;
}

export class NosPercurso extends ValueObject<NosPercursoProps> {
    get value(): { noId: String, duracao: Number, distancia: Number } {
        return this.props;
    }

    private constructor(props: NosPercursoProps) {
        super(props);
    }

    public static create(nos:{ noId: String, duracao: Number, distancia: Number }): Result<NosPercurso> {
        const guardResult = Guard.againstNullOrUndefined(nos, 'nos');
        if (!guardResult.succeeded) {
            return Result.fail<NosPercurso>(guardResult.message);
        } else {
            return Result.ok<NosPercurso>(new NosPercurso(nos))
        }
    }
}