import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

interface percursoLinhaProps {
    percursoLinhaId: String, percurso: String, orientation: String ;
}

export class PercursoLinha extends ValueObject<percursoLinhaProps> {
    get value(): { percursoLinhaId: String, percurso: String, orientation: String }{
        return this.props;
    }

    private constructor(props: percursoLinhaProps) {
        super(props);
    }

    public static create(percursoLinhas: { percursoLinhaId: String, percurso: String, orientation: String }): Result<PercursoLinha> {
        const guardResult = Guard.againstNullOrUndefined(percursoLinhas, 'percursoLinhas');
        if (!guardResult.succeeded) {
            return Result.fail<PercursoLinha>(guardResult.message);
        } else {
            return Result.ok<PercursoLinha>(new PercursoLinha(percursoLinhas))
        }
    }
}