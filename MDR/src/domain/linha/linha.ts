import { AggregateRoot } from "../../core/domain/AggregateRoot";
import { UniqueEntityID } from "../../core/domain/UniqueEntityID";

import { Result } from "../../core/logic/Result";
import { LinhaId } from "./linhaId";

import ILinhaDTO from "../../dto/ILinhaDTO";
import { PercursoLinha } from "./percursoLinha";

import { Guard } from "../../core/logic/Guard";

interface LinhaProps {
  domainId: string;
  nome: string;
  cor: string;
  percursoLinha: PercursoLinha[];
}

export class Linha extends AggregateRoot<LinhaProps> {
  get id(): UniqueEntityID {
    return this._id;
  }

  get linhaId(): LinhaId {
    return LinhaId.create(this.id);
  }

  get percursoLinha(): PercursoLinha[] {
    return this.props.percursoLinha;
  }

  set percursoLinha(value: PercursoLinha[]) {
    this.props.percursoLinha = value;
  }

  get domainId(): string {
    return this.props.domainId;
  }

  set domainId(value: string) {
    this.props.domainId = value;
  }

  get nome(): string {
    return this.props.nome;
  }

  set nome(value: string) {
    this.props.nome = value;
  }

  get cor(): string {
    return this.props.cor;
  }

  set cor(value: string) {
    this.props.cor = value;
  }

  private constructor(props: LinhaProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(props: ILinhaDTO, id?: UniqueEntityID): Result<Linha> {

    const domainId = props.domainId;
    const per = props.percursoLinha;
    const nome = props.nome;
    const cor = props.cor;

    const newProps = {
      domainId: domainId,
      nome: nome,
      cor: cor,
      percursoLinha: per
    } as LinhaProps;

    if (!!per === false) {
      return Result.fail<Linha>('Must provide information')
    } else {
      const linha = new Linha(newProps, id);
      return Result.ok<Linha>(linha)
    }
  }
}