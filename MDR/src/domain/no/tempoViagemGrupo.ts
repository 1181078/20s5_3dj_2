import { ValueObject } from "../../core/domain/ValueObject";
import { Result } from "../../core/logic/Result";
import { Guard } from "../../core/logic/Guard";

export interface TempoViagemGrupoProps {
     noId: String, duracao: Number;
}

export class TempoViagemGrupo extends ValueObject<TempoViagemGrupoProps> {
    get value(): { noId: String, duracao: Number } {
        return this.props;
    }

   get id(): String{
       return this.props.noId;
   }
    get duracao(): Number{
        return this.props.duracao;
    }

    public constructor(props: TempoViagemGrupoProps) {
        super(props);
    }

    public static create(tempo: { noId: String, duracao: Number}): Result<TempoViagemGrupo> {
        const guardResult = Guard.againstNullOrUndefined(tempo, 'nos');
        if (!guardResult.succeeded) {
            return Result.fail<TempoViagemGrupo>(guardResult.message);
        } else {
            return Result.ok<TempoViagemGrupo>(new TempoViagemGrupo(tempo))
        }
    }
}