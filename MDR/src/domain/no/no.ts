import { AggregateRoot } from "../../core/domain/AggregateRoot";
import { UniqueEntityID } from "../../core/domain/UniqueEntityID";

import { Result } from "../../core/logic/Result";
import { NoId } from "./noId";

import INoDTO from "../../dto/INoDTO";

import { Guard } from "../../core/logic/Guard";
import { TempoViagemGrupo } from "./tempoViagemGrupo";

interface NoProps {
    domainId: string;
    nome: string;
    latitude: number;
    longitude: number;
    nomeCurto: string;
    isEstacaoRecolha: boolean;
    isPontoRendicao: boolean;
    tempoViagem: TempoViagemGrupo[];
}

export class No extends AggregateRoot<NoProps> {
    get id(): UniqueEntityID {
        return this._id;
    }

    get noId(): NoId {
        return NoId.create(this.id);
    }

    get domainId(): string {
        return this.props.domainId;
    }

    set domainId(value: string) {
        this.props.domainId = value;
    }

    get latitude(): number {
        return this.props.latitude;
    }

    set latitude(value: number) {
        this.props.latitude = value;
    }

    get nome(): string {
        return this.props.nome;
    }

    set nome(value: string) {
        this.props.nome = value;
    }

    get longitude(): number {
        return this.props.longitude;
    }

    set longitude(value: number) {
        this.props.longitude = value;
    }

    get nomeCurto(): string {
        return this.props.nomeCurto;
    }

    set nomeCurto(value: string) {
        this.props.nomeCurto = value;
    }

    get isEstacaoRecolha(): boolean {
        return this.props.isEstacaoRecolha;
    }

    set isEstacaoRecolha(value: boolean) {
        this.props.isEstacaoRecolha = value;
    }

    get isPontoRendicao() {
        return this.props.isPontoRendicao;
    }

    set isPontoRendicao(value: boolean) {
        this.props.isPontoRendicao = value;
    }

    get isTempoViagem() : TempoViagemGrupo[]{
        return this.props.tempoViagem;
    }

    set isTempoViagem(value: TempoViagemGrupo[]) {
        this.props.tempoViagem = value;
    }


    private constructor(props: NoProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(noDTO: INoDTO, id?: UniqueEntityID): Result<No> {

        const domainId = noDTO.domainId;
        const nome = noDTO.nome;
        const latitude = noDTO.latitude;
        const longitude = noDTO.longitude;
        const nomeCurto = noDTO.nomeCurto;
        const isEstacaoRecolha = noDTO.isEstacaoRecolha;
        const isPontoRendicao = noDTO.isPontoRendicao;
        const tempo = noDTO.tempoViagem;
        const newProps = {
            domainId: domainId,
            nome: nome,
            latitude: latitude,
            longitude: longitude,
            nomeCurto: nomeCurto,
            isEstacaoRecolha: isEstacaoRecolha,
            isPontoRendicao: isPontoRendicao,
            tempoViagem: tempo  
        } as NoProps;
        if (isEstacaoRecolha === null) {
            return Result.fail<No>('isEstacaoRecolha cant be null')
        }
        else if (isPontoRendicao === null) {
            return Result.fail<No>('isPontoRendicao cant be null')
        }
        else if (!!nome === false || nome.length == 0) {
            return Result.fail<No>('Must provide nodes')
        } else {
            const per = new No({ domainId: domainId, nome: nome, latitude: latitude, longitude: longitude, nomeCurto: nomeCurto, isEstacaoRecolha: isEstacaoRecolha, isPontoRendicao: isPontoRendicao, tempoViagem: tempo }, id);
            return Result.ok<No>(per)
        }
    }
}
