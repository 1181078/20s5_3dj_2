import { AggregateRoot } from "../../core/domain/AggregateRoot";
import { UniqueEntityID } from "../../core/domain/UniqueEntityID";

import { Result } from "../../core/logic/Result";
import { TipoViaturaId } from "./tipoViaturaId";

import ITipoViaturaDTO from "../../dto/ITipoViaturaDTO";

import { Guard } from "../../core/logic/Guard";

interface TipoViaturaProps {
  domainId: string,
  nome: string;
  autonomia: number;
  custo: number;
  velocidadeMedia: number;
  combustivel: number;
  consumo: number;
  emissoes: number;
}

export class TipoViatura extends AggregateRoot<TipoViaturaProps> {
  get id (): UniqueEntityID {
    return this._id;
  }

  get tipoViaturaId (): TipoViaturaId {
    return TipoViaturaId.create(this.id);
  }

  get autonomia (): number {
    return this.props.autonomia;
  }

  set autonomia ( value: number) {
    this.props.autonomia = value;
  }

  get nome (): string {
    return this.props.nome;
  }

  set nome ( value: string) {
    this.props.nome = value;
  }

  get custo (): number {
    return this.props.custo;
  }

  set custo ( value: number) {
    this.props.custo = value;
  }

  get velocidadeMedia (): number {
    return this.props.velocidadeMedia;
  }

  set velocidadeMedia ( value: number) {
    this.props.velocidadeMedia = value;
  }

  get combustivel (): number {
    return this.props.combustivel;
  }

  set combustivel ( value: number) {
    this.props.combustivel = value;
  }

  get consumo (): number {
    return this.props.consumo;
  }

  set consumo ( value: number) {
    this.props.consumo = value;
  }

  get emissoes (): number {
    return this.props.emissoes;
  }

  set emissoes ( value: number) {
    this.props.emissoes = value;
  }

  private constructor (props: TipoViaturaProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create (ITipoViaturaDTO: ITipoViaturaDTO, id?: UniqueEntityID): Result<TipoViatura> {

    const dId = ITipoViaturaDTO.domainId;
    const nome = ITipoViaturaDTO.nome;
    const autonomia = ITipoViaturaDTO.autonomia;
    const custo = ITipoViaturaDTO.custo;
    const velocidadeMedia = ITipoViaturaDTO.velocidadeMedia;
    const combustivel = ITipoViaturaDTO.combustivel;
    const consumo = ITipoViaturaDTO.consumo;
    const emissoes = ITipoViaturaDTO.emissoes;

    if (!!nome === false || nome.length === 0) {
      return Result.fail<TipoViatura>('Must provide a name')
    } else {
      const tipo = new TipoViatura({ domainId: dId, nome: nome, autonomia:autonomia, custo:custo, velocidadeMedia:velocidadeMedia, combustivel:combustivel, consumo:consumo, emissoes:emissoes }, id);
      return Result.ok<TipoViatura>( tipo )
    }


  }
}