import { AggregateRoot } from "../../core/domain/AggregateRoot";
import { UniqueEntityID } from "../../core/domain/UniqueEntityID";

import { Result } from "../../core/logic/Result";
import { TipoTripulanteId } from "./tipoTripulanteId";

import ITipoTripulanteDTO from "../../dto/ITipoTripulanteDTO";

import { Guard } from "../../core/logic/Guard";

interface TipoTripulanteProps {
  domainId: string,
  desc: string;
}

export class TipoTripulante extends AggregateRoot<TipoTripulanteProps> {
  get id (): UniqueEntityID {
    return this._id;
  }

  get tipoTripulanteId (): TipoTripulanteId {
    return TipoTripulanteId.create(this.id);
  }

  get desc (): string {
    return this.props.desc;
  }

  set desc ( value: string) {
    this.props.desc = value;
  }

  private constructor (props: TipoTripulanteProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create (tipoTripulanteDTO: ITipoTripulanteDTO, id?: UniqueEntityID): Result<TipoTripulante> {

    const desc = tipoTripulanteDTO.desc;
    const domainId = tipoTripulanteDTO.domainId;

    if (!!desc === false || desc.length === 0) {
      return Result.fail<TipoTripulante>('Must provide a description')
    } else {
      const tipo = new TipoTripulante({ domainId:domainId, desc: desc }, id);
      return Result.ok<TipoTripulante>( tipo )
    }
  }
}