
import ITipoTripulanteDTO from '../../dto/ITipoTripulanteDTO';
import { Result } from "../../core/logic/Result";

export default interface ITipoTripulanteService {
  createTipoTripulante(percursoDTO: ITipoTripulanteDTO): Promise<Result<ITipoTripulanteDTO>>;
  updateTipoTripulante(percursoDTO: ITipoTripulanteDTO): Promise<Result<ITipoTripulanteDTO>>;
  listTiposTripulante(): Promise<Result<ITipoTripulanteDTO[]>>;
  listTiposTripulanteSorted(ord: string): Promise<Result<ITipoTripulanteDTO[]>>;
  getTipoTripulanteById(id: string): Promise<Result<ITipoTripulanteDTO>>;
  deleteTipoTripulanteById(id: string): Promise<Result<string>>;
}

