import { Result } from "../../core/logic/Result";

export default interface IReadFileService {
  readFile(data: any): Promise<boolean>;

}
