
import ILinhaDTO from '../../dto/ILinhaDTO';
import { Result } from "../../core/logic/Result";

export default interface ILinhaService {
  createLinha(linhaDTO: ILinhaDTO): Promise<Result<ILinhaDTO>>;
  updateLinha(linhaDTO: ILinhaDTO): Promise<Result<ILinhaDTO>>;
  listLinhas(): Promise<Result<ILinhaDTO[]>>;
  getLinhaById(id: string): Promise<Result<ILinhaDTO>>;
  listLinhasSorted(ord: string): Promise<Result<ILinhaDTO[]>>;
  deleteLinhaById(id: string): Promise<Result<string>>;
}