import IUserDTO from '../../dto/IUserDTO';
import { Result } from "../../core/logic/Result";

export default interface ILinhaService {
    SignUp(userDTO: IUserDTO): Promise<Result<{ userDTO: IUserDTO; token: string }>>
    SignIn(email: string, password: string): Promise<{ role: string; token: string }>
    listUsers(): Promise<Result<IUserDTO[]>>;
    changePassword(id: string, oldPassword: string, newPassword: string);

}