
import IPercursoDTO from '../../dto/IPercursoDTO';
import { Result } from "../../core/logic/Result";

export default interface IPercursoService {
  createPercurso(percursoDTO: IPercursoDTO): Promise<Result<IPercursoDTO>>;
  updatePercurso(percursoDTO: IPercursoDTO): Promise<Result<IPercursoDTO>>;
  listPercursos(): Promise<Result<IPercursoDTO[]>>;
  getPercursoById(id: string): Promise<Result<IPercursoDTO>>;
  deletePercursoById(id: string): Promise<Result<string>>;
}

