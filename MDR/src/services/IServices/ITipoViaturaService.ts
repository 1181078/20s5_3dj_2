
import ITipoViaturaDTO from '../../dto/ITipoViaturaDTO';
import { Result } from "../../core/logic/Result";

export default interface ITipoViaturaService {
  createTipoViatura(percursoDTO: ITipoViaturaDTO): Promise<Result<ITipoViaturaDTO>>;
  updateTipoViatura(percursoDTO: ITipoViaturaDTO): Promise<Result<ITipoViaturaDTO>>;
  listTiposViatura(): Promise<Result<ITipoViaturaDTO[]>>;
  listTiposViaturaSorted(ord: string): Promise<Result<ITipoViaturaDTO[]>>;
  getTipoViaturaById(id: string): Promise<Result<ITipoViaturaDTO>>;
  deleteTipoViaturaById(id: string): Promise<Result<string>>;
}
