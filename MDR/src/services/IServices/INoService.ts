import INoDTO from '../../dto/INoDTO';
import { Result } from "../../core/logic/Result";

export default interface INoService {
  createNo(noDTO: INoDTO): Promise<Result<INoDTO>>;
  updateNo(noDTO: INoDTO): Promise<Result<INoDTO>>;
  listNos(): Promise<Result<INoDTO[]>>;
  getNoById(id: string): Promise<Result<INoDTO>>;
  listNosSorted(ord: string): Promise<Result<INoDTO[]>>;
  listPOuE(): Promise<Result<INoDTO[]>>;
  deleteNoById(id: string): Promise<Result<string>>;
}
