import { Container, Service, Inject } from 'typedi';
import config from '../../config.js';
import INoDTO from '../dto/INoDTO';
import INoRepo from '../repos/IRepos/INoRepo';
import INoService from '../services/IServices/INoService';
import { Result } from "../core/logic/Result";
import { NoMap } from "../mappers/NoMap";
import { No } from "../domain/no/no";

@Service()
export default class NoService implements INoService {
  constructor(
    @Inject(config.repos.no.name) private noRepo: INoRepo
  ) {
  }

  public async createNo(noDTO: INoDTO): Promise<Result<INoDTO>> {
    try {
      const noOrError = await No.create(noDTO);

      if (noOrError.isFailure) {
        return Result.fail<INoDTO>(noOrError.errorValue());
      }

      const noRes = noOrError.getValue();

      await this.noRepo.save(noRes);

      const noDTOResult = NoMap.toDTO(noRes) as INoDTO;
      return Result.ok<INoDTO>(noDTOResult);

    } catch (e) {
      throw e;
    }
  }

  public async updateNo(noDTO: INoDTO): Promise<Result<INoDTO>> {
    try {
      const no = await this.noRepo.findByDomainId(noDTO.domainId);

      if (no === null) {
        return Result.fail<INoDTO>("Nó não encontrado");
      }
      else {
        no.nome = noDTO.nome;
        no.latitude = noDTO.latitude;
        no.longitude = noDTO.longitude;
        no.nomeCurto = noDTO.nomeCurto;
        no.isEstacaoRecolha = noDTO.isEstacaoRecolha;
        no.isPontoRendicao = noDTO.isPontoRendicao;
        no.isTempoViagem = noDTO.tempoViagem;
        await this.noRepo.save(no);

        const noDTOResult = NoMap.toDTO(no) as INoDTO;
        return Result.ok<INoDTO>(noDTOResult)
      }
    } catch (e) {
      throw e;
    }

  }

  public async getNoById(id: string): Promise<Result<INoDTO>> {
    try {

      const no: No = await this.noRepo.findByDomainId(id);
      if (no === null) {
        return Result.fail<INoDTO>("No não existe");
      } else {
        const noDTOResult = NoMap.toDTO(no) as INoDTO;

        return Result.ok<INoDTO>(noDTOResult);
      }
    } catch (e) {
      throw e;
    }
  }

  public async listNos(): Promise<Result<INoDTO[]>> {
    try {
      const no = await this.noRepo.findAll();

      var nos: INoDTO[] = new Array(no.length);

      for (var i = 0; i < no.length; i++) {

        const noOrError = NoMap.toDTO(no[i]) as INoDTO;

        nos[i] = noOrError;
      }

      return Result.ok<INoDTO[]>(nos);

    } catch (e) {
      throw e;
    }
  }

  public async listNosSorted(ord: string): Promise<Result<INoDTO[]>> {
    try {
      const no = await this.noRepo.findAllSorted(ord);

      var nos: INoDTO[] = new Array(no.length);

      for (var i = 0; i < no.length; i++) {

        const noOrError = NoMap.toDTO(no[i]) as INoDTO;

        nos[i] = noOrError;
      }

      return Result.ok<INoDTO[]>(nos);

    } catch (e) {
      throw e;
    }
  }

  public async listPOuE(): Promise<Result<INoDTO[]>> {
    try {
      const no = await this.noRepo.findPOuE();

      var nos: INoDTO[] = new Array(no.length);

      for (var i = 0; i < no.length; i++) {

        const noOrError = NoMap.toDTO(no[i]) as INoDTO;

        nos[i] = noOrError;
      }

      return Result.ok<INoDTO[]>(nos);

    } catch (e) {
      throw e;
    }
  }

  public async deleteNoById(id: string): Promise<Result<string>> {
    try {

      const no = await this.noRepo.deleteByDomainId(id);

      return Result.ok<string>(no);

    } catch (e) {
      throw e;
    }
  }
}