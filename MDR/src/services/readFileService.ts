import { Container, Service, Inject } from 'typedi';
import config from '../../config.js';
import IReadFileService from '../services/IServices/IReadFileService';
import { Result } from "../core/logic/Result";
import { result } from 'lodash';
import ITipoViaturaDTO from '../dto/ITipoViaturaDTO';
import ITipoViaturaRepo from '../repos/IRepos/ITipoViaturaRepo';
import ITipoViaturaService from '../services/IServices/ITipoViaturaService';
import { TipoViaturaMap } from "../mappers/TipoViaturaMap";
import { TipoViatura } from "../domain/tipoViatura/tipoViatura";
import tipoViatura from '../api/routes/tipoViatura';
import TipoViaturaRepo from '../repos/tipoViaturaRepo';
import ILinhaDTO from '../dto/ILinhaDTO';
import { Linha } from '../domain/linha/linha';
import LinhaRepo from '../repos/linhaRepo';
import ILinhaRepo from '../repos/IRepos/ILinhaRepo';
import { PercursoLinha } from "../domain/linha/percursoLinha";
import IPercursoDTO from '../dto/IPercursoDTO';
import { Percurso } from '../domain/percurso/percurso';
import PercursoRepo from '../repos/percursoRepo';
import NoRepo from '../repos/noRepo';
import INoDTO from '../dto/INoDTO';
import { No } from '../domain/no/no';
const xml2js = require('xml2js')

interface percursoLinhaElements {
    percursoLinhaId: String,
    percurso: String,
    orientation: String;
}

interface nosPercursoElements {
    noId: String,
    duracao: Number,
    distancia: Number
}

interface TempoViagemGrupoElements {
    Array()
}

@Service("readFileService")
export default class readFileService implements IReadFileService {
    constructor(
    ) {
    }


    public async readFile(data: any): Promise<boolean> {
        try {
            xml2js.parseString(data, async function (err, result) {
                if (err) {
                    throw err;
                } else {
                    const tipoViaturaRepo = Container.get(TipoViaturaRepo);
                    const linhasRepo = Container.get(LinhaRepo);
                    const percursoRepo = Container.get(PercursoRepo);
                    const nosRepo = Container.get(NoRepo);

                    //Para Tipos de Viatura
                    const vehicleTypes = result.GlDocumentInfo.world[0].GlDocument[0].GlDocumentNetwork[0].Network[0].VehicleTypes[0].VehicleType;
                    for (let i = 0; i < vehicleTypes.length; i++) {
                        const atual = vehicleTypes[i];
                        const newViatura = {
                            domainId: atual.$.key,
                            nome: atual.$.Name,
                            autonomia: atual.$.Autonomy,
                            custo: atual.$.Cost,
                            velocidadeMedia: atual.$.AverageSpeed,
                            combustivel: atual.$.EnergySource,
                            consumo: atual.$.Consumption,
                            emissoes: atual.$.Emissions,
                        } as ITipoViaturaDTO;
                        const tipoViaturaOrError = TipoViatura.create(newViatura);
                        tipoViaturaRepo.save(tipoViaturaOrError.getValue());
                    }

                    //Para Linhas
                    const linhas = result.GlDocumentInfo.world[0].GlDocument[0].GlDocumentNetwork[0].Network[0].Lines[0].Line;
                    for (let i = 0; i < linhas.length; i++) {
                        const atual = linhas[i];
                        const caminhosLinha = atual.LinePaths[0].LinePath;
                        let arrayLista: percursoLinhaElements[] = new Array();
                        for (let j = 0; j < caminhosLinha.length; j++) {
                            const atualCaminho = caminhosLinha[j];
                            const newLinha = {
                                percursoLinhaId: atualCaminho.$.key,
                                percurso: atualCaminho.$.Path,
                                orientation: atualCaminho.$.Orientation
                            } as percursoLinhaElements;
                            arrayLista[j] = newLinha;
                        }
                        const newLinha = {
                            domainId: atual.$.key,
                            nome: atual.$.Name,
                            cor: atual.$.Color,
                            percursoLinha: arrayLista
                        } as unknown as ILinhaDTO;
                        const newLinhaOrError = Linha.create(newLinha);
                        linhasRepo.save(newLinhaOrError.getValue());
                    }

                    //Para percursos
                    const percursos = result.GlDocumentInfo.world[0].GlDocument[0].GlDocumentNetwork[0].Network[0].Paths[0].Path;
                    for (let i = 0; i < percursos.length; i++) {
                        const atual = percursos[i];
                        const percursosNos = atual.PathNodes[0].PathNode;
                        let arrayNos: nosPercursoElements[] = new Array();
                        for (let j = 0; j < percursosNos.length; j++) {
                            const atualNo = percursosNos[j];
                            const newNoPercurso = {
                                noId: atualNo.$.Node,
                                duracao: atualNo.$.Duration,
                                distancia: atualNo.$.Distance
                            } as nosPercursoElements;
                            arrayNos[j] = newNoPercurso;
                        }
                        const newPath = {
                            domainId: atual.$.key,
                            nosPercurso: arrayNos
                        } as unknown as IPercursoDTO;
                        const newPercursoOrError = Percurso.create(newPath);
                        percursoRepo.save(newPercursoOrError.getValue());
                    }


                    //Para Nós
                    const nos = result.GlDocumentInfo.world[0].GlDocument[0].GlDocumentNetwork[0].Network[0].Nodes[0].Node;
                    for (let i = 0; i < nos.length; i++) {
                        const atual = nos[i];
                        const tViagemGrupo = atual.CrewTravelTimes[0].CrewTravelTime;
                        let arrayViagem= new Array();
                        if (tViagemGrupo != undefined) {
                            for (let j = 0; j < tViagemGrupo.length; j++) {
                                const atualCaminho = tViagemGrupo[j];
                                const newTempo = {
                                        noId: atualCaminho.$.Node,
                                        duracao: atualCaminho.$.Duration,
                                    }
                                arrayViagem[j] = newTempo;
                            }
                        }
                        var isDepot: boolean;
                        if (atual.$.IsDepot == "False") {
                            isDepot = false;
                        } else {
                            isDepot = true;
                        }

                        var isRelief: boolean;
                        if (atual.$.IsReliefPoint == "False") {
                            isRelief = false;
                        } else {
                            isRelief = true;
                        }

                        const newNo = {
                            domainId: atual.$.key,
                            nome: atual.$.Name,
                            latitude: atual.$.Latitude,
                            longitude: atual.$.Longitude,
                            nomeCurto: atual.$.ShortName,
                            isEstacaoRecolha: isDepot,
                            isPontoRendicao: isRelief,
                            tempoViagem: arrayViagem
                        } as unknown as INoDTO;
                        const newNoOrError = No.create(newNo);
                        nosRepo.save(newNoOrError.getValue());
                    }
                }
            })
            return true;
        } catch (e) {
            throw e; 

        }
    }


}