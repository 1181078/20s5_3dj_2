import { Container, Service, Inject } from 'typedi';
import config from '../../config.js';
import IPercursoDTO from '../dto/IPercursoDTO';
import IPercursoRepo from '../repos/IRepos/IPercursoRepo';
import IPercursoService from '../services/IServices/IPercursoService';
import { Result } from "../core/logic/Result";
import { PercursoMap } from "../mappers/PercursoMap";
import { Percurso } from "../domain/percurso/percurso";

@Service()
export default class PercursoService implements IPercursoService {
  constructor(
    @Inject(config.repos.percurso.name) private percursoRepo: IPercursoRepo
  ) {
  }

  public async createPercurso(percursoDTO: IPercursoDTO): Promise<Result<IPercursoDTO>> {
    try {
      const percursoOrError = await Percurso.create(percursoDTO);

      if (percursoOrError.isFailure) {
        return Result.fail<IPercursoDTO>(percursoOrError.errorValue());
      }
      const percursoRes = percursoOrError.getValue();

      await this.percursoRepo.save(percursoRes);

      const percursoDTOResult = PercursoMap.toDTO(percursoRes) as IPercursoDTO;
      return Result.ok<IPercursoDTO>(percursoDTOResult)

    } catch (e) {
      throw e;
    }
  }




  public async updatePercurso(percursoDTO: IPercursoDTO): Promise<Result<IPercursoDTO>> {
    try {
      const per = await this.percursoRepo.findByDomainId(percursoDTO.domainId);

      if (per === null) {
        return Result.fail<IPercursoDTO>("Percurso não encontrado");
      }
      else {
        per.nosPercurso = percursoDTO.nosPercurso;
        await this.percursoRepo.save(per);

        const perDTOResult = PercursoMap.toDTO(per) as IPercursoDTO;
        return Result.ok<IPercursoDTO>(perDTOResult)
      }
    } catch (e) {
      throw e;
    }
  }


  public async getPercursoById(id: string): Promise<Result<IPercursoDTO>> {
    try {

      const per: Percurso = await this.percursoRepo.findByDomainId(id);
      if (per === null) {
        return Result.fail<IPercursoDTO>("Percurso não existe");
      } else {
        const perDTOResult = PercursoMap.toDTO(per) as IPercursoDTO;

        return Result.ok<IPercursoDTO>(perDTOResult);
      }
    } catch (e) {
      throw e;
    }
  }

  public async listPercursos(): Promise<Result<IPercursoDTO[]>> {
    try {
      const per = await this.percursoRepo.findAll();

      var pers: IPercursoDTO[] = new Array(per.length);

      for (var i = 0; i < per.length; i++) {

        const perOrError = PercursoMap.toDTO(per[i]) as IPercursoDTO;

        pers[i] = perOrError;
      }

      return Result.ok<IPercursoDTO[]>(pers);

    } catch (e) {
      throw e;
    }
  }

  public async deletePercursoById(id: string): Promise<Result<string>> {
    try {

      const percurso = await this.percursoRepo.deleteByDomainId(id);

      return Result.ok<string>(percurso);

    } catch (e) {
      throw e;
    }
  }
}