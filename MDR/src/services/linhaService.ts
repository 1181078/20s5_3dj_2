import { Container, Service, Inject } from 'typedi';
import config from '../../config.js';
import ILinhaDTO from '../dto/ILinhaDTO';
import ILinhaRepo from '../repos/IRepos/ILinhaRepo';
import ILinhaService from '../services/IServices/ILinhaService';
import { Result } from "../core/logic/Result";
import { LinhaMap } from "../mappers/LinhaMap";
import { Linha } from "../domain/linha/linha";

@Service()
export default class LinhaService implements ILinhaService {
  constructor(
    @Inject(config.repos.linha.name) private linhaRepo: ILinhaRepo
  ) {
  }

  public async createLinha(linhaDTO: ILinhaDTO): Promise<Result<ILinhaDTO>> {

    try {

      const linhaOrError = await Linha.create(linhaDTO);

      if (linhaOrError.isFailure) {
        return Result.fail<ILinhaDTO>(linhaOrError.errorValue());
      }

      const percursoRes = linhaOrError.getValue();

      await this.linhaRepo.save(percursoRes);

      const linhaDTOResult = LinhaMap.toDTO(percursoRes) as ILinhaDTO;

      return Result.ok<ILinhaDTO>(linhaDTOResult)

    } catch (e) {
      throw e;
    }
  }




  public async updateLinha(linhaDTO: ILinhaDTO): Promise<Result<ILinhaDTO>> {
    try {
      const per = await this.linhaRepo.findByDomainId(linhaDTO.domainId);

      if (per === null) {
        return Result.fail<ILinhaDTO>("Linha não encontrada");
      }
      else {
        per.nome = linhaDTO.nome;
        per.cor = linhaDTO.cor;
        per.percursoLinha = linhaDTO.percursoLinha;
        await this.linhaRepo.save(per);

        const perDTOResult = LinhaMap.toDTO(per) as ILinhaDTO;
        return Result.ok<ILinhaDTO>(perDTOResult)
      }
    } catch (e) {
      throw e;
    }
  }


  public async getLinhaById(id: string): Promise<Result<ILinhaDTO>> {

    try {

      const linha: Linha = await this.linhaRepo.findByDomainId(id);
      if (linha === null) {
        return Result.fail<ILinhaDTO>("Linha não encontrada");
      } else {
        const linhaDTOResult = LinhaMap.toDTO(linha) as ILinhaDTO;

        return Result.ok<ILinhaDTO>(linhaDTOResult);
      }
    } catch (e) {
      throw e;
    }
  }

  public async listLinhas(): Promise<Result<ILinhaDTO[]>> {
    try {
      const linha = await this.linhaRepo.findAll();

      var linhas: ILinhaDTO[] = new Array(linha.length);

      for (var i = 0; i < linha.length; i++) {

        const noOrError = LinhaMap.toDTO(linha[i]) as ILinhaDTO;

        linhas[i] = noOrError;
      }


      return Result.ok<ILinhaDTO[]>(linhas);

    } catch (e) {
      throw e;
    }
  }

  public async listLinhasSorted(ord: string): Promise<Result<ILinhaDTO[]>> {
    try {
      const linha = await this.linhaRepo.findAllSorted(ord);

      var linhas: ILinhaDTO[] = new Array(linha.length);

      for (var i = 0; i < linha.length; i++) {

        const noOrError = LinhaMap.toDTO(linha[i]) as ILinhaDTO;

        linhas[i] = noOrError;
      }

      return Result.ok<ILinhaDTO[]>(linhas);

    } catch (e) {
      throw e;
    }
  }

  public async deleteLinhaById(id: string): Promise<Result<string>> {
    try {

      const linha = await this.linhaRepo.deleteByDomainId(id);

      return Result.ok<string>(linha);

    } catch (e) {
      throw e;
    }
  }
}