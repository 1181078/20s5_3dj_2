import { Container, Service, Inject } from 'typedi';
import config from '../../config.js';
import ITipoViaturaDTO from '../dto/ITipoViaturaDTO';
import ITipoViaturaRepo from '../repos/IRepos/ITipoViaturaRepo';
import ITipoViaturaService from '../services/IServices/ITipoViaturaService';
import { Result } from "../core/logic/Result";
import { TipoViaturaMap } from "../mappers/TipoViaturaMap";
import { TipoViatura } from "../domain/tipoViatura/tipoViatura";

@Service()
export default class TipoViaturaService implements ITipoViaturaService {
  constructor(
    @Inject(config.repos.tipoViatura.name) private tipoViaturaRepo: ITipoViaturaRepo
  ) {
  }

  public async createTipoViatura(tipoViaturaDTO: ITipoViaturaDTO): Promise<Result<ITipoViaturaDTO>> {
    try {
      const tipoViaturaOrError = await TipoViatura.create(tipoViaturaDTO);

      if (tipoViaturaOrError.isFailure) {
        return Result.fail<ITipoViaturaDTO>(tipoViaturaOrError.errorValue());
      }

      const tipoViaturaRes = tipoViaturaOrError.getValue();

      await this.tipoViaturaRepo.save(tipoViaturaRes);

      const tipoViaturaDTOResult = TipoViaturaMap.toDTO(tipoViaturaRes) as ITipoViaturaDTO;
      return Result.ok<ITipoViaturaDTO>(tipoViaturaDTOResult)

    } catch (e) {
      throw e;
    }
  }




  public async updateTipoViatura(tipoViaturaDTO: ITipoViaturaDTO): Promise<Result<ITipoViaturaDTO>> {
    try {
      const tipo = await this.tipoViaturaRepo.findByDomainId(tipoViaturaDTO.domainId);

      if (tipo === null) {
        return Result.fail<ITipoViaturaDTO>("Tipo de Viatura não encontrado");
      }
      else {
        tipo.nome = tipoViaturaDTO.nome;
        tipo.autonomia = tipoViaturaDTO.autonomia;
        tipo.custo = tipoViaturaDTO.custo;
        tipo.velocidadeMedia = tipoViaturaDTO.velocidadeMedia;
        tipo.combustivel = tipoViaturaDTO.combustivel;
        tipo.consumo = tipoViaturaDTO.consumo;
        tipo.emissoes = tipoViaturaDTO.emissoes;
        await this.tipoViaturaRepo.save(tipo);

        const tipoDTOResult = TipoViaturaMap.toDTO(tipo) as ITipoViaturaDTO;
        return Result.ok<ITipoViaturaDTO>(tipoDTOResult)
      }
    } catch (e) {
      throw e;
    }
  }

  public async getTipoViaturaById(id: string): Promise<Result<ITipoViaturaDTO>> {
    try {

      const tipoViaturaRes: TipoViatura = await this.tipoViaturaRepo.findByDomainId(id);
      if (tipoViaturaRes === null) {
        return Result.fail<ITipoViaturaDTO>("Tipo de viatura não encontrado");
      } else {
        const tipoViaturaDTOResult = TipoViaturaMap.toDTO(tipoViaturaRes) as ITipoViaturaDTO;

        return Result.ok<ITipoViaturaDTO>(tipoViaturaDTOResult);
      }
    } catch (e) {
      throw e;
    }
  }

  public async listTiposViatura(): Promise<Result<ITipoViaturaDTO[]>> {
    try {
      const per = await this.tipoViaturaRepo.findAll();

      var tipos: ITipoViaturaDTO[] = new Array(per.length);

      for (var i = 0; i < per.length; i++) {

        const tipoOrError = TipoViaturaMap.toDTO(per[i]) as ITipoViaturaDTO;

        tipos[i] = tipoOrError;
      }

      return Result.ok<ITipoViaturaDTO[]>(tipos);

    } catch (e) {
      throw e;
    }
  }

  public async listTiposViaturaSorted(ord: string): Promise<Result<ITipoViaturaDTO[]>> {
    try {
      const per = await this.tipoViaturaRepo.findAllSorted(ord);

      var tipos: ITipoViaturaDTO[] = new Array(per.length);

      for (var i = 0; i < per.length; i++) {

        const tipoOrError = TipoViaturaMap.toDTO(per[i]) as ITipoViaturaDTO;

        tipos[i] = tipoOrError;
      }

      return Result.ok<ITipoViaturaDTO[]>(tipos);

    } catch (e) {
      throw e;
    }
  }

  public async deleteTipoViaturaById(id: string): Promise<Result<string>> {
    try {

      const tipoViaturaRes = await this.tipoViaturaRepo.deleteByDomainId(id);

      return Result.ok<string>(tipoViaturaRes);

    } catch (e) {
      throw e;
    }
  }
}


