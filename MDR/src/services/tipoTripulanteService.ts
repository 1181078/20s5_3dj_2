import { Container, Service, Inject } from 'typedi';
import config from '../../config.js';
import ITipoTripulanteDTO from '../dto/ITipoTripulanteDTO';
import ITipoTripulanteRepo from '../repos/IRepos/ITipoTripulanteRepo';
import ITipoTripulanteService from '../services/IServices/ITipoTripulanteService';
import { Result } from "../core/logic/Result";
import { TipoTripulanteMap } from "../mappers/TipoTripulanteMap";
import { TipoTripulante } from "../domain/tipoTripulante/tipoTripulante";

@Service()
export default class TipoTripulanteService implements ITipoTripulanteService {
  constructor(
    @Inject(config.repos.tipoTripulante.name) private tipoTripulanteRepo: ITipoTripulanteRepo
  ) {
  }

  public async createTipoTripulante(tipoTripulanteDTO: ITipoTripulanteDTO): Promise<Result<ITipoTripulanteDTO>> {
    try {
      const tipoTripulanteOrError = await TipoTripulante.create(tipoTripulanteDTO);

      if (tipoTripulanteOrError.isFailure) {
        return Result.fail<ITipoTripulanteDTO>(tipoTripulanteOrError.errorValue());
      }

      const tipoTripulanteRes = tipoTripulanteOrError.getValue();

      await this.tipoTripulanteRepo.save(tipoTripulanteRes);

      const tipoTripulanteDTOResult = TipoTripulanteMap.toDTO(tipoTripulanteRes) as ITipoTripulanteDTO;
      return Result.ok<ITipoTripulanteDTO>(tipoTripulanteDTOResult);

    } catch (e) {
      throw e;
    }
  }




  public async updateTipoTripulante(tipoTripulanteDTO: ITipoTripulanteDTO): Promise<Result<ITipoTripulanteDTO>> {
    try {
      const tipo = await this.tipoTripulanteRepo.findByDomainId(tipoTripulanteDTO.domainId);
      if (tipo === null) {
        return Result.fail<ITipoTripulanteDTO>("Tipo de Tripulante não encontrado");
      }
      else {
        tipo.desc = tipoTripulanteDTO.desc;
        await this.tipoTripulanteRepo.save(tipo);

        const tipoDTOResult = TipoTripulanteMap.toDTO(tipo) as ITipoTripulanteDTO;
        return Result.ok<ITipoTripulanteDTO>(tipoDTOResult)
      }
    } catch (e) {
      throw e;
    }
  }

  public async getTipoTripulanteById(id: string): Promise<Result<ITipoTripulanteDTO>> {
    try {

      const tipoTripulanteRes: TipoTripulante = await this.tipoTripulanteRepo.findByDomainId(id);
      if (tipoTripulanteRes === null) {
        return Result.fail<ITipoTripulanteDTO>("Tipo tripulante nao encontrado");
      } else {
        const tipoTripulanteDTOResult = TipoTripulanteMap.toDTO(tipoTripulanteRes) as ITipoTripulanteDTO;

        return Result.ok<ITipoTripulanteDTO>(tipoTripulanteDTOResult);
      }
    } catch (e) {
      throw e;
    }
  }

  public async listTiposTripulante(): Promise<Result<ITipoTripulanteDTO[]>> {
    try {
      const per = await this.tipoTripulanteRepo.findAll();

      var tipos: ITipoTripulanteDTO[] = new Array(per.length);

      for (var i = 0; i < per.length; i++) {

        const tipoOrError = TipoTripulanteMap.toDTO(per[i]) as ITipoTripulanteDTO;

        tipos[i] = tipoOrError;
      }

      return Result.ok<ITipoTripulanteDTO[]>(tipos);

    } catch (e) {
      throw e;
    }
  }

  public async listTiposTripulanteSorted(ord: string): Promise<Result<ITipoTripulanteDTO[]>> {
    try {
      const per = await this.tipoTripulanteRepo.findAllSorted(ord);

      var tipos: ITipoTripulanteDTO[] = new Array(per.length);

      for (var i = 0; i < per.length; i++) {

        const tipoOrError = TipoTripulanteMap.toDTO(per[i]) as ITipoTripulanteDTO;

        tipos[i] = tipoOrError;
      }

      return Result.ok<ITipoTripulanteDTO[]>(tipos);

    } catch (e) {
      throw e;
    }
  }

  public async deleteTipoTripulanteById(id: string): Promise<Result<string>> {
    try {

      const tipoTripulanteRes = await this.tipoTripulanteRepo.deleteByDomainId(id);

      return Result.ok<string>(tipoTripulanteRes);

    } catch (e) {
      throw e;
    }
  }
}