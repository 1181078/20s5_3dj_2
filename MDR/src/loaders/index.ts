import expressLoader from './express';
import dependencyInjectorLoader from './dependencyInjector';
import mongooseLoader from './mongoose';
import Logger from './logger';
import config from '../../config.js';
import gestaoCaminhosRoute from '../api/routes/gestaoCaminhosRoute';


export default async ({ expressApp }) => {
  const mongoConnection = await mongooseLoader();
  Logger.info('✌️ DB loaded and connected!');

  const noSchema = {
    name: 'noSchema',
    schema: '../persistence/schemas/noSchema',
  };
  
  const percursoSchema = {
    name: 'percursoSchema',
    schema: '../persistence/schemas/percursoSchema',
  };

  const tipoViaturaSchema = {
    name: 'tipoViaturaSchema',
    schema: '../persistence/schemas/tipoViaturaSchema',
  };

  const tipoTripulanteSchema = {
    name: 'tipoTripulanteSchema',
    schema: '../persistence/schemas/tipoTripulanteSchema',
  };

  const linhaSchema = {
    name: 'linhaSchema',
    schema: '../persistence/schemas/linhaSchema',
  };
    
  const userSchema = {
    // compare with the approach followed in repos and services
    name: 'userSchema',
    schema: '../persistence/schemas/userSchema',
  }

  const roleSchema = {
    // compare with the approach followed in repos and services
    name: 'roleSchema',
    schema: '../persistence/schemas/roleSchema',
  }

  const roleController = {
    name: config.controller.role.name,
    path: config.controller.role.path
  }

  const gestaoCaminhosController = {
    name: config.controller.gestao.name,
    path: config.controller.gestao.path
  }


  const roleRepo = {
    name: config.repos.role.name,
    path: config.repos.role.path
  }

  const userRepo = {
    name: config.repos.user.name,
    path: config.repos.user.path
  }

  const roleService = {
    name: config.services.role.name,
    path: config.services.role.path
  }
  
  const readFileController = {
    name: config.controller.readFile.name,
    path: config.controller.readFile.path
  }

  const noController = {
    name: config.controller.no.name,
    path: config.controller.no.path
  }

  const percursoController = {
    name: config.controller.percurso.name,
    path: config.controller.percurso.path
  }

  const tipoViaturaController = {
    name: config.controller.tipoViatura.name,
    path: config.controller.tipoViatura.path
  }

  const tipoTripulanteController = {
    name: config.controller.tipoTripulante.name,
    path: config.controller.tipoTripulante.path
  }

  const linhaController = {
    name: config.controller.linha.name,
    path: config.controller.linha.path
  }

  const noRepo = {
    name: config.repos.no.name,
    path: config.repos.no.path
  }
  const percursoRepo = {
    name: config.repos.percurso.name,
    path: config.repos.percurso.path
  }

  const tipoViaturaRepo = {
    name: config.repos.tipoViatura.name,
    path: config.repos.tipoViatura.path
  }

  const tipoTripulanteRepo = {
    name: config.repos.tipoTripulante.name,
    path: config.repos.tipoTripulante.path
  }

  const linhaRepo = {
    name: config.repos.linha.name,
    path: config.repos.linha.path
  }

  const noService = {
    name: config.services.no.name,
    path: config.services.no.path
  }
  const percursoService = {
    name: config.services.percurso.name,
    path: config.services.percurso.path
  }
  
  const tipoViaturaService = {
    name: config.services.tipoViatura.name,
    path: config.services.tipoViatura.path
  }

  const tipoTripulanteService = {
    name: config.services.tipoTripulante.name,
    path: config.services.tipoTripulante.path
  }

  const linhaService = {
    name: config.services.linha.name,
    path: config.services.linha.path
  }

  const readFileService = {
    name: config.services.readFile.name,
    path: config.services.readFile.path
  }

  const authService = {
    name: config.services.auth.name,
    path: config.services.auth.path
  }

  await dependencyInjectorLoader({
    mongoConnection,
    schemas: [
      noSchema,
      percursoSchema,
      tipoViaturaSchema,
      tipoTripulanteSchema,
      linhaSchema,
      roleSchema,
      userSchema
    ],
    controllers: [
      noController,
      percursoController,
      tipoViaturaController,
      tipoTripulanteController,
      linhaController,
      readFileController,
      roleController,
      gestaoCaminhosController

    ],
    repos: [
      noRepo,
      percursoRepo,
      tipoViaturaRepo,
      tipoTripulanteRepo,
      linhaRepo,
      roleRepo,
      userRepo
    ],
    services: [
      noService,
      percursoService,
      tipoViaturaService,
      tipoTripulanteService,
      linhaService,
      readFileService,
      roleService,
      authService
    ]
  });
  Logger.info('✌️ Schemas, Controllers, Repositories, Services, etc. loaded');

  await expressLoader({ app: expressApp });
  Logger.info('✌️ Express loaded');
};