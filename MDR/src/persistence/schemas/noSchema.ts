import { INoPersistence } from '../../dataschema/INoPersistence';
import mongoose from 'mongoose';
import { TempoViagemGrupo } from '../../domain/no/tempoViagemGrupo';

const NoSchema = new mongoose.Schema(
  {
    domainId: { type: String, unique: true },
    nome: { type: String, unique: true },
    latitude: { type: Number, unique: false },
    longitude: { type: Number, unique: false },
    nomeCurto: { type: String, unique: true },
    isEstacaoRecolha: { type: Boolean},
    isPontoRendicao: { type: Boolean},
    tempoViagem: { type: Array }  
  },
  {
    timestamps: true
  }
);

export default mongoose.model<INoPersistence & mongoose.Document>('No', NoSchema);
