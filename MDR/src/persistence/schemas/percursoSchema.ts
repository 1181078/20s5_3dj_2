import { IPercursoPersistence } from '../../dataschema/IPercursoPersistence';
import mongoose from 'mongoose';
import { NosPercurso } from '../../domain/percurso/nosPercurso';

const PercursoSchema = new mongoose.Schema(
  {
    domainId: { type: String, unique: true },
    nosPercurso: { type: Array },
  },
  {
    timestamps: true
  }
);

export default mongoose.model<IPercursoPersistence & mongoose.Document>('Percurso', PercursoSchema);
