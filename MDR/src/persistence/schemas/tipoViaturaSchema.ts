import { ITipoViaturaPersistence } from '../../dataschema/ITipoViaturaPersistence';
import mongoose from 'mongoose';

const TipoViaturaSchema = new mongoose.Schema(
  {
    domainId: { type: String, unique: true },
    nome: { type: String, unique: true },
    autonomia: { type: Number },
    custo: { type: Number },
    velocidadeMedia: { type: Number },
    combustivel: { type: Number },
    consumo: { type: Number },
    emissoes: { type: Number }
  },
  {
    timestamps: true
  }
);

export default mongoose.model<ITipoViaturaPersistence & mongoose.Document>('TipoViatura', TipoViaturaSchema);



