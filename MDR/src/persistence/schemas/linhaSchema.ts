import { ILinhaPersistence } from '../../dataschema/ILinhaPersistence';
import mongoose from 'mongoose';
import { PercursoLinha } from '../../domain/linha/percursoLinha';

const LinhaSchema = new mongoose.Schema(
  {
    domainId: { type: String, unique: true },
    nome: { type: String, unique: true },
    cor: { type: String },
    percursoLinha: { type: Array },
  },
  {
    timestamps: true
  }
);

export default mongoose.model<ILinhaPersistence & mongoose.Document>('Linha', LinhaSchema);
