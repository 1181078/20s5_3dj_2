import { PercursoLinha } from "../domain/linha/percursoLinha";

export interface ILinhaPersistence {
    domainId: string;
    nome: string;
    cor: string;
    percursoLinha : PercursoLinha[];
  }