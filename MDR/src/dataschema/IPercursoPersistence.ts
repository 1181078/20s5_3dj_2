import { NosPercurso } from "../domain/percurso/nosPercurso";

export interface IPercursoPersistence {
    domainId: string;
    nosPercurso : NosPercurso[];
  }