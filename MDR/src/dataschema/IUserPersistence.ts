export interface IUserPersistence {
	_id: string;
	email: string;
	password: string;
	salt: string;
	role: string
  }