import { Router, Request, Response, NextFunction } from 'express';
import { Container } from 'typedi';
import AuthService from '../../services/auth';
import IUserDTO from '../../dto/IUserDTO';
import AuthController from '../../controllers/authController';

import middlewares from '../middlewares';
import { celebrate, Joi } from 'celebrate';
import winston = require('winston');

import { Result } from "../../core/logic/Result";
import user from './userRoute';
import { checkJwt } from '../middlewares/checkJwt';

const route = Router();

export default (app: Router) => {
    app.use('/auth', route);

    
    const authServiceInstance = Container.get(AuthService);
    const ctrl = new AuthController(authServiceInstance);

    route.get('', (req, res, next) => ctrl.listUsers(req, res, next));

    route.post(
        '/signup',
        celebrate({
            body: Joi.object({
                email: Joi.string().required(),
                password: Joi.string().required(),
                role: Joi.string().required()
            }),
        }),
        async (req: Request, res: Response, next: NextFunction) => {

            const authServiceInstance = Container.get(AuthService);
            const userOrError = await authServiceInstance.SignUp(req.body as IUserDTO);
            if (userOrError.isFailure) {
                return res.status(401).send(userOrError.errorValue());
            }

            const { userDTO, token } = userOrError.getValue();

            return res.status(201).json(userDTO);
        },
    );

    route.post(
        '/signin',
        celebrate({
            body: Joi.object({
                email: Joi.string().required(),
                password: Joi.string().required(),
            }),
        }),
        async (req: Request, res: Response, next: NextFunction) => {
            try {
                const { email, password } = req.body;
                const authServiceInstance = Container.get(AuthService);
                const { role, token } = await authServiceInstance.SignIn(email, password);
                return res.json({ role, token }).status(200);
            } catch (e) {
                return next(e);
            }
        },
    );

    route.post(
        '/change-password',
        async (req: Request, res: Response, next: NextFunction) => {
            try {
                const id = res.locals.jwtPayload.userEmail;
                //Get parameters from the body
                const { oldPassword, newPassword } = req.body;
                if (!(oldPassword && newPassword)) {
                    res.status(400).send();
                  }
                await authServiceInstance.changePassword(id, oldPassword, newPassword );
                return res.status(200);
            } catch (e) {
                return next(e);
            }
        },
    );

    /**
     * @TODO Let's leave this as a place holder for now
     * The reason for a logout route could be deleting a 'push notification token'
     * so the device stops receiving push notifications after logout.
     *
     * Another use case for advance/enterprise apps, you can store a record of the jwt token
     * emitted for the session and add it to a black list.
     * It's really annoying to develop that but if you had to, please use Redis as your data store
     */
    route.post('/logout', [checkJwt], (req: Request, res: Response, next: NextFunction) => {
        const logger = Container.get('logger') as winston.Logger;
        logger.debug('Calling Sign-Out endpoint with body: %o', req.body)
        try {
            //@TODO AuthService.Logout(req.user) do some clever stuff
            return res.status(200).end();
        } catch (e) {
            logger.error('🔥 error %o', e);
            return next(e);
        }
    });
    
    route.get('/currentUser', (req, res, next) => Container.get(AuthService).getCurrentUser());

    route.get('/logout', (req, res, next) => Container.get(AuthService).SignOut());

    route.put('/user', [checkJwt], );

    route.delete('/delete', (req, res, next) => Container.get(AuthService).Delete());

};
