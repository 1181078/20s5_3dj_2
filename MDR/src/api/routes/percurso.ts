import { Router, Request, Response, NextFunction } from 'express';
import { celebrate, Joi } from 'celebrate';
import { Container } from 'typedi';
import  IPercursoController  from '../../controllers/IControllers/IPercursoController';

import config from "../../../config.js";

const route = Router();

export default (app: Router) => {
  app.use('/percursos', route);
  
  const ctrl = Container.get(config.controller.percurso.name) as IPercursoController;

  route.post('', 

  celebrate({​​

    body: Joi.object({​​

      domainId: Joi.string().required(),

      nosPercurso: Joi.array().items(Joi.object({​​

        noId: Joi.string(), duracao: Joi.number(), distancia: Joi.number()

      }​​))

    }​​)

  }​​), (req, res, next) => ctrl.createPercurso(req, res, next));

  route.put('', (req, res, next) => ctrl.updatePercurso(req, res, next));  

  route.get('', (req, res, next) => ctrl.listPercursos(req, res, next));

  route.get('/:id', (req, res, next) => ctrl.getPercursoById(req, res, next));

  route.delete('/:id', (req, res, next) => ctrl.deletePercursoById(req, res, next));


};