import { Router, Request, Response, NextFunction } from 'express';
import { celebrate, Joi } from 'celebrate';
import { Container } from 'typedi';
import INoController from '../../controllers/IControllers/INoController';

import config from "../../../config";
import { TempoViagemGrupo } from '../../domain/no/tempoViagemGrupo';
import IGestaoCaminhosController from '../../controllers/IControllers/IGestaoCaminhosController';
import gestaoCaminhosController from '../../controllers/gestaoCaminhosController';
const route = Router();

export default (app: Router) => {
  app.use('/gestaoCaminhos', route);

  const ctrl = Container.get(config.controller.gestao.name) as IGestaoCaminhosController;

  
  route.post('/aStar', (req, res, next) => ctrl.gerirCaminhoAstar(req, res, next));
  route.post('/todosMelhor', (req, res, next) => ctrl.gerirCaminhoTodosMelhor(req, res, next));
  route.post('/bestfs', (req, res, next) => ctrl.gerirCaminhoBestfs(req, res, next));
  route.post('/AG', (req, res, next) => ctrl.ag(req, res, next));


};