import { Router, Request, Response, NextFunction } from 'express';
import { celebrate, Joi } from 'celebrate';
import { Container } from 'typedi';
import ITipoViaturaController from '../../controllers/IControllers/ITipoViaturaController';
const route = Router();

import config from "../../../config.js";

export default (app: Router) => {
  app.use('/tipoViaturas', route);

  const ctrl = Container.get(config.controller.tipoViatura.name) as ITipoViaturaController;

  route.post('',
    celebrate({
      body: Joi.object({
        domainId: Joi.string().required(),
        nome: Joi.string().required(),
        autonomia: Joi.number().min(0).required(),
        custo: Joi.number().min(0).required(),
        velocidadeMedia: Joi.number().min(0).required(),
        combustivel: Joi.number().min(0).required(),
        consumo: Joi.number().min(0).required(),
        emissoes: Joi.number().min(0).required()
      })
    }), (req, res, next) => ctrl.createTipoViatura(req, res, next));

  route.put('', (req, res, next) => ctrl.updateTipoViatura(req, res, next));

  route.get('', (req, res, next) => ctrl.listTiposViatura(req, res, next));
  
  route.get('/sortedBy/:ord', (req, res, next) => ctrl.listTiposViaturaSorted(req, res, next));

  route.get('/:id', (req, res, next) => ctrl.getTipoViaturaById(req, res, next));

  route.delete('/:id', (req, res, next) => ctrl.deleteTipoViaturaById(req, res, next));

};