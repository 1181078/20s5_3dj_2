import { Router, Request, Response, NextFunction } from 'express';
import { celebrate, Joi } from 'celebrate';
import { Container } from 'typedi';
import INoController from '../../controllers/IControllers/INoController';

import config from "../../../config";
import { TempoViagemGrupo } from '../../domain/no/tempoViagemGrupo';
const route = Router();

export default (app: Router) => {
  app.use('/nos', route);

  const ctrl = Container.get(config.controller.no.name) as INoController;

  route.post('',
    celebrate({
      body: Joi.object({
        domainId: Joi.string().required(),
        nome: Joi.string().required(),
        latitude: Joi.number().min(-90).max(90).required(),
        longitude: Joi.number().min(-180).max(180).required(),
        nomeCurto: Joi.string().min(4).max(5).required(),
        isEstacaoRecolha: Joi.boolean().required(),
        isPontoRendicao: Joi.boolean().required(),
        tempoViagem: Joi.array().items(Joi.object({
          noId: Joi.string(),
          duracao: Joi.number().min(0)

        }))
      })
    }),
    (req, res, next) => ctrl.createNo(req, res, next));
  route.put('', (req, res, next) => ctrl.updateNo(req, res, next));

  route.get('', (req, res, next) => ctrl.listNos(req, res, next));

  route.get('/sortedBy/:ord', (req, res, next) => ctrl.listNosSorted(req, res, next));
  
  route.get('/pontoOuEstacao', (req, res, next) => ctrl.getNosPontoOuEstacao(req, res, next));

  route.get('/:id', (req, res, next) => ctrl.getNoById(req, res, next));

  route.delete('/:id', (req, res, next) => ctrl.deleteNoById(req, res, next));

};