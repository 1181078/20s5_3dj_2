import { Router, Request, Response, NextFunction } from 'express';
import { celebrate, Joi } from 'celebrate';
import { Container } from 'typedi';
import ITipoTripulanteController from '../../controllers/IControllers/ITipoTripulanteController';

import config from "../../../config.js";

const route = Router();


export default (app: Router) => {
  app.use('/tipoTripulantes', route);

  const ctrl = Container.get(config.controller.tipoTripulante.name) as ITipoTripulanteController;

  route.post('/add',
    celebrate({
      body: Joi.object({
        domainId: Joi.string().required(),
        desc: Joi.string().required()
      })

    }), (req, res, next) => ctrl.createTipoTripulante(req, res, next));

  route.put('', (req, res, next) => ctrl.updateTipoTripulante(req, res, next));

  route.get('', (req, res, next) => ctrl.listTiposTripulante(req, res, next));
  
  route.get('/sortedBy/:ord', (req, res, next) => ctrl.listTiposTripulanteSorted(req, res, next));

  route.get('/:id', (req, res, next) => ctrl.getTipoTripulanteById(req, res, next));

  route.delete('/:id', (req, res, next) => ctrl.deleteTipoTripulanteById(req, res, next));

};