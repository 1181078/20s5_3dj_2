import { Router, Request, Response, NextFunction } from 'express';
import  ILinhaController  from '../../controllers/IControllers/ILinhaController';
import { celebrate, Joi } from 'celebrate';
import { Container } from 'typedi';
import config from "../../../config.js";
import { PercursoLinha } from '../../domain/linha/percursoLinha';
const route = Router();

export default (app: Router) => {
  app.use('/linhas', route);

  const ctrl = Container.get(config.controller.linha.name) as ILinhaController;

  route.post('',
    celebrate({
      body: Joi.object({
        domainId: Joi.string().required(),
        nome: Joi.string().required(),
        cor: Joi.string().required(),
        percursoLinha: Joi.array().items(Joi.object({
          percursoLinhaId: Joi.string(), percurso: Joi.string(), orientation: Joi.string()
        }))
      })
    }),
    (req, res, next) => ctrl.createLinha(req, res, next));
  route.put('', (req, res, next) => ctrl.updateLinha(req, res, next));
  
  route.get('', (req, res, next) => ctrl.listLinhas(req, res, next));

  route.get('/sortedBy/:ord', (req, res, next) => ctrl.listLinhasSorted(req, res, next));

  route.get('/:id', (req, res, next) => ctrl.getLinhaById(req, res, next));

  route.delete('/:id', (req, res, next) => ctrl.deleteLinhaById(req, res, next));

};