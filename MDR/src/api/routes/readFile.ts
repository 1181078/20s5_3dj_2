import { Router, Request, Response, NextFunction } from 'express';
import { Container } from 'typedi';
import IReadFileController from '../../controllers/IControllers/IReadFileController';
import config from "../../../config.js";
const route = Router();

export default (app: Router) => {
  app.use('/readFile', route);
  const ctrl = Container.get(config.controller.readFile.name) as IReadFileController;

  //Decidir se por post ou get
  route.post('/import', (req, res, next) => ctrl.readFile(req, res, next));
  //route.put('', (req, res, next) => ctrl.updatePercurso(req, res, next));

};