import { Router } from 'express';
import auth from './routes/authRoute';
import user from './routes/userRoute';
import role from './routes/roleRoute';
import tipoTripulante from './routes/tipoTripulante';
import tipoViatura from './routes/tipoViatura';
import percurso from './routes/percurso';
import no from './routes/no'
import linha from './routes/linha'
import readFile  from './routes/readFile';
import gestaoCaminhosRoute from './routes/gestaoCaminhosRoute';


// guaranteed to get dependencies
export default () => {
	const app = Router();
	
	auth(app);
	user(app);
	role(app);
	tipoTripulante(app);
	tipoViatura(app);
	percurso(app);
	no(app);
	linha(app);
	readFile(app);
	gestaoCaminhosRoute(app);

	return app
}


