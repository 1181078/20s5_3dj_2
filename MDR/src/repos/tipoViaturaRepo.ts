import { Document, Model } from 'mongoose';
import { Service, Inject } from 'typedi';
import { TipoViatura } from '../domain/tipoViatura/tipoViatura';
import { TipoViaturaId } from '../domain/tipoViatura/tipoViaturaId';
import { TipoViaturaMap } from '../mappers/TipoViaturaMap';
import { ITipoViaturaPersistence } from '../dataschema/ITipoViaturaPersistence';
import ITipoViaturaRepo from "./IRepos/ITipoViaturaRepo";

@Service()
export default class TipoViaturaRepo implements ITipoViaturaRepo {
  private models: any;

  constructor(
    @Inject('tipoViaturaSchema') private tipoViaturaSchema: Model<ITipoViaturaPersistence & Document>,
  ) { }

  private createBaseQuery(): any {
    return {
      where: {},
    }
  }

  public async exists(tipoViaturaId: TipoViaturaId | string): Promise<boolean> {

    const idX = tipoViaturaId instanceof TipoViaturaId ? (<TipoViaturaId>tipoViaturaId).id.toValue() : tipoViaturaId;

    const query = { string: idX };
    const tipoViaturaDocument = await this.tipoViaturaSchema.findOne(query);

    return !!tipoViaturaDocument === true;
  }

  public async save(tipoViatura: TipoViatura): Promise<TipoViatura> {
    const query = { domainId: tipoViatura.id.toString() };

    var tipoViaturaDocument = await this.tipoViaturaSchema.findOne(query);

    try {
      if (tipoViaturaDocument === null) {
        const rawTipoViatura: any = TipoViaturaMap.toPersistence(tipoViatura);

        const tipoViaturaCreated = await this.tipoViaturaSchema.create(rawTipoViatura);

        return TipoViaturaMap.toDomain(tipoViaturaCreated);
      } else {
        // tipoViaturaDocument.nome = tipoViatura.nome;
        // tipoViaturaDocument.autonomia = tipoViatura.autonomia;
        // tipoViaturaDocument.custo = tipoViatura.custo;
        // tipoViaturaDocument.velocidadeMedia = tipoViatura.velocidadeMedia;
        // tipoViaturaDocument.combustivel = tipoViatura.combustivel;
        // tipoViaturaDocument.consumo = tipoViatura.consumo;
        // tipoViaturaDocument.emissoes = tipoViatura.emissoes;

        
        tipoViaturaDocument = Object.assign({nome: tipoViatura.nome }, tipoViaturaDocument)
        tipoViaturaDocument = Object.assign({autonomia: tipoViatura.autonomia }, tipoViaturaDocument)
        tipoViaturaDocument = Object.assign({custo:tipoViatura.custo }, tipoViaturaDocument)
        tipoViaturaDocument = Object.assign({velocidadeMedia: tipoViatura.velocidadeMedia }, tipoViaturaDocument)
        tipoViaturaDocument = Object.assign({combustivel: tipoViatura.combustivel }, tipoViaturaDocument)
        tipoViaturaDocument = Object.assign({consumo: tipoViatura.consumo }, tipoViaturaDocument)
        tipoViaturaDocument = Object.assign({emissoes: tipoViatura.emissoes }, tipoViaturaDocument)
        
        // await tipoViaturaDocument.save();

        return tipoViatura;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findByDomainId(tipoViaturaId: string): Promise<TipoViatura> {
    const query = { domainId: tipoViaturaId };
    const tipoViaturaRecord = await this.tipoViaturaSchema.findOne(query);

    if (tipoViaturaRecord != null) {
      return TipoViaturaMap.toDomain(tipoViaturaRecord);
    }
    else
      return null;
  }

  public async findAll() {
    const tipoViaturaRecord = await this.tipoViaturaSchema.find({});


    if (tipoViaturaRecord != null) {
      return TipoViaturaMap.toDomainArray(tipoViaturaRecord);
    }
    else
      return null;
  };

  public async findAllSorted(ord: string) {
    const tipoViaturaRecord = await this.tipoViaturaSchema.find({}).sort(ord);


    if (tipoViaturaRecord != null) {
      return TipoViaturaMap.toDomainArray(tipoViaturaRecord);
    }
    else
      return null;
  };

  public async deleteByDomainId (tipoViaturaId: string): Promise<string> {
    const query = { domainId: tipoViaturaId};
    const tipoViaturaRecord = await this.tipoViaturaSchema.findOne( query );
    await this.tipoViaturaSchema.deleteOne( query );

    if( tipoViaturaRecord != null) {
      return tipoViaturaRecord.domainId;
    }
    else
      return null;
  }

}


