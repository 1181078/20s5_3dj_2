import { Service, Inject } from 'typedi';

import IRoleRepo from "./IRepos/IRoleRepo";
import { Role } from "../domain/role/role";
import { RoleId } from "../domain/role/roleId";
import { RoleMap } from "../mappers/RoleMap";

import { Document, Model } from 'mongoose';
import { IRolePersistence } from '../dataschema/IRolePersistence';

@Service()
export default class RoleRepo implements IRoleRepo {
  private models: any;

  constructor(
    @Inject('roleSchema') private roleSchema : Model<IRolePersistence & Document>,
  ) {}

  private createBaseQuery (): any {
    return {
      where: {},
    }
  }

  public async exists (roleId: string): Promise<boolean> {

    const query = { domainId: roleId}; 
    const roleDocument = await this.roleSchema.findOne( query );

    return !!roleDocument === true;
  }

  public async save (role: Role): Promise<Role> {
    const query = { domainId: role.id.toString()}; 

    var roleDocument = await this.roleSchema.findOne( query );

    try {
      if (roleDocument === null ) {
        const rawRole: any = RoleMap.toPersistence(role);

        const roleCreated = await this.roleSchema.create(rawRole);

        return RoleMap.toDomain(roleCreated);
      } else {
        // roleDocument.name = role.name;
        
        roleDocument = Object.assign({name: role.name }, roleDocument)

        // await roleDocument.save();

        return role;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findByDomainId (roleId: string): Promise<Role> {
    const query = { name: roleId};
    const roleRecord = await this.roleSchema.findOne( query );

    if( roleRecord != null) {
      return RoleMap.toDomain(roleRecord);
    }
    else
      return null;
  }

  public async findById (roleId: string): Promise<Role> {
    const query = { id: roleId};
    const roleRecord = await this.roleSchema.findOne( query );

    if( roleRecord != null) {
      return RoleMap.toDomain(roleRecord);
    }
    else
      return null;
  }
}