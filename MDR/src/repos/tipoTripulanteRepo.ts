import { Document, Model } from 'mongoose';
import { Service, Inject } from 'typedi';
import { TipoTripulante } from '../domain/tipoTripulante/tipoTripulante';
import { TipoTripulanteId } from '../domain/tipoTripulante/tipoTripulanteId';
import { TipoTripulanteMap } from '../mappers/TipoTripulanteMap';
import { ITipoTripulantePersistence } from '../dataschema/ITipoTripulantePersistence';
import ITipoTripulanteRepo from "./IRepos/ITipoTripulanteRepo";

@Service()
export default class TipoTripulanteRepo implements ITipoTripulanteRepo{
    private models: any;

  constructor(
    @Inject('tipoTripulanteSchema') private tipoTripulanteSchema : Model<ITipoTripulantePersistence & Document>
  ) {}

    private createBaseQuery (): any {
        return {
            where: {},
        }
    }

    public async exists (tipoTripulanteId: TipoTripulanteId | string): Promise<boolean> {

        const idX = tipoTripulanteId instanceof TipoTripulanteId ? (<TipoTripulanteId>tipoTripulanteId).id.toValue() : tipoTripulanteId;
    
        const query = { string: idX}; 
        const tipoTripulanteDocument = await this.tipoTripulanteSchema.findOne( query );
    
        return !!tipoTripulanteDocument === true;
    }
    
      public async save (tipoTripulante: TipoTripulante): Promise<TipoTripulante> {
        const query = { domainId: tipoTripulante.id.toString()}; 
    
        var tipoTripulanteDocument = await this.tipoTripulanteSchema.findOne( query );
  
        try {
          if (tipoTripulanteDocument === null ) {
            const rawTipoTripulante: any = TipoTripulanteMap.toPersistence(tipoTripulante);
    
            const tipoTripulanteCreated = await this.tipoTripulanteSchema.create(rawTipoTripulante);
    
            return TipoTripulanteMap.toDomain(tipoTripulanteCreated);
          } else {
           // const newDocument = Object.assign({domainId: tipoTripulante.props.domainId, desc: tipoTripulante.desc}, tipoTripulanteDocument);
            
            // tipoTripulanteDocument.desc = tipoTripulante.desc;
            
        tipoTripulanteDocument = Object.assign({desc: tipoTripulante.desc }, tipoTripulanteDocument)
            // await tipoTripulanteDocument.save();
    
            return tipoTripulante;
          }
        } catch (err) {
          throw err;
        }
      }
    
      public async findByDomainId (tipoTripulanteId: string): Promise<TipoTripulante> {
        const query = { domainId: tipoTripulanteId};
        const tipoTripulanteRecord = await this.tipoTripulanteSchema.findOne( query );
    
        if( tipoTripulanteRecord != null) {
          return TipoTripulanteMap.toDomain(tipoTripulanteRecord);
        }
        else
          return null;
      }

      public async findAll(){
        const tipoTripulanteRecord = await this.tipoTripulanteSchema.find({});
        
        if( tipoTripulanteRecord != null) {
          return TipoTripulanteMap.toDomainArray(tipoTripulanteRecord);
        }
        else
          return null;
      };

      public async findAllSorted(ord: String){
        const tipoTripulanteRecord = await this.tipoTripulanteSchema.find({}).sort(ord);
    
        
        if( tipoTripulanteRecord != null) {
          return TipoTripulanteMap.toDomainArray(tipoTripulanteRecord);
        }
        else
          return null;
      };

      public async deleteByDomainId (tipoTripulanteId: string): Promise<string> {
        const query = { domainId: tipoTripulanteId};
        const tipoTripulanteRecord = await this.tipoTripulanteSchema.findOne( query );
        await this.tipoTripulanteSchema.deleteOne( query );
    
        if( tipoTripulanteRecord != null) {
          return tipoTripulanteRecord.domainId;
        }
        else
          return null;
      }

}


