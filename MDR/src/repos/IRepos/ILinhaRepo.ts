import { Repo } from "../../core/infra/Repo";
import { Linha } from "../../domain/linha/linha";
import { LinhaId } from "../../domain/linha/linhaId";

export default interface ILinhaRepo extends Repo<Linha> {
  save(linha: Linha): Promise<Linha>;
  findByDomainId (linhaId: LinhaId | string): Promise<Linha>;
  findAll();
  findAllSorted(ord: string);  
  deleteByDomainId (tipoTripulanteId: LinhaId | string): Promise<string>;
    
  //findByIds (linhaIds: LinhaID[]): Promise<Linha[]>;
  //saveCollection (linha: Linha[]): Promise<Linha[]>;
  //removeByIds (linhas: LinhaID[]): Promise<any>
}