import { Repo } from "../../core/infra/Repo";
import { Percurso } from "../../domain/percurso/percurso";
import { PercursoId } from "../../domain/percurso/percursoId";

export default interface IPercursoRepo extends Repo<Percurso> {
  save(percurso: Percurso): Promise<Percurso>;
  findByDomainId (percursoId: PercursoId | string): Promise<Percurso>;
  findAll();
  deleteByDomainId (tipoTripulanteId: PercursoId | string): Promise<string>;
    
  //findByIds (percursoIds: PercursoId[]): Promise<Percurso[]>;
  //saveCollection (percursos: Percurso[]): Promise<Percurso[]>;
  //removeByIds (percursos: PercursoId[]): Promise<any>
}
