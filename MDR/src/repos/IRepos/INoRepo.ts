import { Repo } from "../../core/infra/Repo";
import { No } from "../../domain/no/no";
import { NoId } from "../../domain/no/noId";

export default interface INoRepo extends Repo<No> {
  save(no: No): Promise<No>;
  findByDomainId (noId: NoId | string): Promise<No>;
  findAll();
  findAllSorted(ord: string);
  findPOuE();
  deleteByDomainId (tipoTripulanteId: NoId | string): Promise<string>;
    
  //findByIds (tipoViaturaIds: TipoViaturaId[]): Promise<TipoViatura[]>;
  //saveCollection (tipoViaturas: TipoViatura[]): Promise<TipoViatura[]>;
  //removeByIds (tipoViaturas: TipoViaturaId[]): Promise<any>;

}