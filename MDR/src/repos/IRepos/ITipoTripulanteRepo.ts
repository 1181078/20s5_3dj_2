import { Repo } from "../../core/infra/Repo";
import { TipoTripulante } from "../../domain/tipoTripulante/tipoTripulante";
import { TipoTripulanteId } from "../../domain/tipoTripulante/tipoTripulanteId";

export default interface ItipoTripulanteRepo extends Repo<TipoTripulante> {
  save(tipoTripulante: TipoTripulante): Promise<TipoTripulante>;
  findByDomainId (tipoTripulanteId: TipoTripulanteId | string): Promise<TipoTripulante>;
  findAll();
  findAllSorted(ord: string);
  deleteByDomainId (tipoTripulanteId: TipoTripulanteId | string): Promise<string>;
    
  //findByIds (tipoTripulanteIds: TipoTripulanteId[]): Promise<TipoTripulante[]>;
  //saveCollection (tipoTripulantes: TipoTripulante[]): Promise<TipoTripulante[]>;
  //removeByIds (tipoTripulantes: TipoTripulanteId[]): Promise<any>
}
