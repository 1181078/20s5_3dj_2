import { Repo } from "../../core/infra/Repo";
import { Role } from "../../domain/role/role";
import { RoleId } from "../../domain/role/roleId";

export default interface IRoleRepo {
  save(role: Role): Promise<Role>;
  findByDomainId (roleId: string): Promise<Role>;
  exists (roleId: string): Promise<boolean>;
    
  //findByIds (rolesIds: RoleId[]): Promise<Role[]>;
  //saveCollection (roles: Role[]): Promise<Role[]>;
  //removeByRoleIds (roles: RoleId[]): Promise<any>
}
