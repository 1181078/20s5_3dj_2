import { Repo } from "../../core/infra/Repo";
import { TipoViatura } from "../../domain/tipoViatura/tipoViatura";
import { TipoViaturaId } from "../../domain/tipoViatura/tipoViaturaId";

export default interface ItipoViaturaRepo extends Repo<TipoViatura> {
  save(tipoViatura: TipoViatura): Promise<TipoViatura>;
  findByDomainId (tipoViaturaId: TipoViaturaId | string): Promise<TipoViatura>;
  findAll();
  findAllSorted(ord: string);
  deleteByDomainId (tipoTripulanteId: TipoViaturaId | string): Promise<string>;
    
  //findByIds (tipoViaturaIds: TipoViaturaId[]): Promise<TipoViatura[]>;
  //saveCollection (tipoViaturas: TipoViatura[]): Promise<TipoViatura[]>;
  //removeByIds (tipoViaturas: TipoViaturaId[]): Promise<any>
}
