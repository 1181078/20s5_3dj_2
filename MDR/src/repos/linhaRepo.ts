import { Document, Model } from 'mongoose';
import { Service, Inject } from 'typedi';
import { Linha } from '../domain/linha/linha';
import { LinhaId } from '../domain/linha/linhaId';
import { LinhaMap } from '../mappers/LinhaMap';
import { ILinhaPersistence } from '../dataschema/ILinhaPersistence';
import ILinhaRepo from "./IRepos/ILinhaRepo";

@Service()
export default class LinhaRepo implements ILinhaRepo {
    private models: any;

    constructor(
        @Inject('linhaSchema') private linhaSchema: Model<ILinhaPersistence & Document>,
    ) { }

    private createBaseQuery(): any {
        return {
            where: {},
        }
    }

    public async exists(linhaId: LinhaId | string): Promise<boolean> {

        const idX = linhaId instanceof LinhaId ? (<LinhaId>linhaId).id.toValue() : linhaId;

        const query = { string: idX };
        const linhaDocument = await this.linhaSchema.findOne(query);

        return !!linhaDocument === true;
    }

    public async save(linha: Linha): Promise<Linha> {
        const query = { domainId: linha.id.toString() };

        var linhaDocument = await this.linhaSchema.findOne(query);

        try {
            if (linhaDocument === null) {
                const rawLinha: any = LinhaMap.toPersistence(linha);

                const linhaCreated = await this.linhaSchema.create(rawLinha);

                return LinhaMap.toDomain(linhaCreated);
            } else {
                // linhaDocument.domainId = linha.props.domainId;
                // linhaDocument.nome = linha.props.nome;
                // linhaDocument.cor = linha.props.cor;
                // linhaDocument.percursoLinha = linha.props.percursoLinha;

                linhaDocument = Object.assign({domainId: linha.props.domainId }, linhaDocument)
                linhaDocument = Object.assign({nome: linha.props.nome }, linhaDocument)
                linhaDocument = Object.assign({cor: linha.props.cor }, linhaDocument)
                linhaDocument = Object.assign({percursoLinha: linha.props.percursoLinha }, linhaDocument)


                // await linhaDocument.save();

                return linha;
            }
        } catch (err) {
            throw err;
        }
    }

    public async findByDomainId(linhaId: string): Promise<Linha> {
        const query = { domainId: linhaId };
        const noRecord = await this.linhaSchema.findOne(query);

        if (noRecord != null) {
            return LinhaMap.toDomain(noRecord);
        }
        else
            return null;
    }

    public async findAll(){
        const noRecord = await this.linhaSchema.find({});
        
        if( noRecord != null) {
               
          return LinhaMap.toDomainArray(noRecord);
        }
        else
          return null;
      };


      public async findAllSorted(ord: string){
        const noRecord = await this.linhaSchema.find({}).sort(ord);
    
        
        if( noRecord != null) {
          return LinhaMap.toDomainArray(noRecord);
        }
        else
          return null;
      };

      public async deleteByDomainId (linhaId: string): Promise<string> {
        const query = { domainId: linhaId};
        const linhaRecord = await this.linhaSchema.findOne( query );
        await this.linhaSchema.deleteOne( query );
    
        if( linhaRecord != null) {
          return linhaRecord.domainId;
        }
        else
          return null;
      }

}