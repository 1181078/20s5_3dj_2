import { Document, Model } from 'mongoose';
import { Service, Inject } from 'typedi';
import { No } from '../domain/no/no';
import { NoId } from '../domain/no/noId';
import { NoMap } from '../mappers/NoMap';
import { INoPersistence } from '../dataschema/INoPersistence';
import INoRepo from "./IRepos/INoRepo";

@Service()
export default class NoRepo implements INoRepo {
    private models: any;

    constructor(
        @Inject('noSchema') private noSchema: Model<INoPersistence & Document>,
    ) { }

    private createBaseQuery(): any {
        return {
            where: {},
        }
    }

    public async exists(noId: NoId | string): Promise<boolean> {

        const idX = noId instanceof NoId ? (<NoId>noId).id.toValue() : noId;

        const query = { string: idX };
        const noDocument = await this.noSchema.findOne(query);

        return !!noDocument === true;
    }

    public async save(no: No): Promise<No> {
        const query = { domainId: no.id.toString() };

        var noDocument = await this.noSchema.findOne(query);

        try {
            if (noDocument === null) {
                const rawNo: any = NoMap.toPersistence(no);

                const noCreated = await this.noSchema.create(rawNo);

                return NoMap.toDomain(noCreated);
            } else {
                // noDocument.domainId = no.domainId;
                // noDocument.nome = no.nome;
                // noDocument.latitude = no.latitude;
                // noDocument.longitude = no.longitude;
                // noDocument.nomeCurto = no.nomeCurto;
                // noDocument.isEstacaoRecolha = no.isEstacaoRecolha;
                // noDocument.isPontoRendicao = no.isPontoRendicao;
                // noDocument.tempoViagem = no.isTempoViagem;

                noDocument = Object.assign({domainId: no.domainId }, noDocument)
                noDocument = Object.assign({nome: no.nome }, noDocument)
                noDocument = Object.assign({latitude: no.latitude }, noDocument)
                noDocument = Object.assign({longitude: no.longitude }, noDocument)
                noDocument = Object.assign({nomeCurto: no.nomeCurto }, noDocument)
                noDocument = Object.assign({isEstacaoRecolha: no.isEstacaoRecolha }, noDocument)
                noDocument = Object.assign({isPontoRendicao: no.isPontoRendicao }, noDocument)
                noDocument = Object.assign({isTempoViagem: no.isTempoViagem }, noDocument)
              
                Object.preventExtensions(noDocument);

                // await noDocument.save();

                return no;
            }
        } catch (err) {
            throw err;
        }
    }

    public async findByDomainId(noId: string): Promise<No> {
        const query = { domainId: noId };
        const noRecord = await this.noSchema.findOne(query);

        if (noRecord != null) {
            return NoMap.toDomain(noRecord);
        }
        else
            return null;
    }

    public async findAll() {
        const noRecord = await this.noSchema.find({});


        if (noRecord != null) {
            return NoMap.toDomainArray(noRecord);
        }
        else
            return null;
    };


    public async findAllSorted(ord: string) {
        const noRecord = await this.noSchema.find({}).sort(ord);


        if (noRecord != null) {
            return NoMap.toDomainArray(noRecord);
        }
        else
            return null;
    };

    public async findPOuE() {
        const noRecord = await this.noSchema.find({$or:[{ isEstacaoRecolha: true}, { isPontoRendicao: true }]});

        if (noRecord != null) {
            return NoMap.toDomainArray(noRecord);
        }
        else
            return null;
    };

    public async deleteByDomainId (noId: string): Promise<string> {
        const query = { domainId: noId};
        const noRecord = await this.noSchema.findOne( query );
        await this.noSchema.deleteOne( query );
    
        if( noRecord != null) {
          return noRecord.domainId;
        }
        else
          return null;
      }

}