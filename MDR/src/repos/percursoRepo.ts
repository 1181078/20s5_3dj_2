import { Document, Model } from 'mongoose';
import { Service, Inject } from 'typedi';
import { Percurso } from '../domain/percurso/percurso';
import { PercursoId } from '../domain/percurso/percursoId';
import { PercursoMap } from '../mappers/PercursoMap';
import { IPercursoPersistence } from '../dataschema/IPercursoPersistence';
import IPercursoRepo from "./IRepos/IPercursoRepo";

@Service()
export default class PercursoRepo implements IPercursoRepo {
  private models: any;

  constructor(
    @Inject('percursoSchema') private percursoSchema: Model<IPercursoPersistence & Document>,
  ) { }

  private createBaseQuery(): any {
    return {
      where: {},
    }
  }

  public async exists(percursoId: PercursoId | string): Promise<boolean> {

    const idX = percursoId instanceof PercursoId ? (<PercursoId>percursoId).id.toValue() : percursoId;

    const query = { string: idX };
    const percursoDocument = await this.percursoSchema.findOne(query);

    return !!percursoDocument === true;
  }

  public async save(percurso: Percurso): Promise<Percurso> {
    const query = { domainId: percurso.id.toString()};

    var percursoDocument = await this.percursoSchema.findOne(query);

    try {
      if (percursoDocument === null) {
        const rawPercurso: any = PercursoMap.toPersistence(percurso);

        const percursoCreated = await this.percursoSchema.create(rawPercurso);

        return PercursoMap.toDomain(percursoCreated);
      } else {
        //  percursoDocument.domainId = percurso.props.domainId;
        //  percursoDocument.nosPercurso = percurso.nosPercurso;
         
         percursoDocument = Object.assign({domainId: percurso.props.domainId }, percursoDocument)
        
         percursoDocument = Object.assign({nosPercurso: percurso.nosPercurso }, percursoDocument)

        //  await percursoDocument.save();

        return percurso;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findByDomainId(percursoId: string): Promise<Percurso> {
    const query = { domainId: percursoId };
    const percursoRecord = await this.percursoSchema.findOne(query);

    if (percursoRecord != null) {
      return PercursoMap.toDomain(percursoRecord);
    }
    else
      return null;
  }

  public async findAll(){
    const percursoRecord = await this.percursoSchema.find({});

    
    if( percursoRecord != null) {
      
      return PercursoMap.toDomainArray(percursoRecord);
    }
    else
      return null;
  };

  public async deleteByDomainId (percursoId: string): Promise<string> {
    const query = { domainId: percursoId};
    const percursoRecord = await this.percursoSchema.findOne( query );
    await this.percursoSchema.deleteOne( query );

    if( percursoRecord != null) {
      return percursoRecord.domainId;
    }
    else
      return null;
  }


}

