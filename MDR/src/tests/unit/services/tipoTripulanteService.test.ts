import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import TipoTripulanteService from "../../../services/tipoTripulanteService";
import ITipoTripulanteRepo from "../../../repos/IRepos/ITipoTripulanteRepo";
import ITipoTripulanteDTO from '../../../dto/ITipoTripulanteDTO';
import {TipoTripulante} from '../../../domain/tipoTripulante/tipoTripulante';


describe('tipo tripulante service', function () {
	beforeEach(function() {
	});
	

	let tipoTripulanteSchemaClass = require("../../../persistence/schemas/tipoTripulanteSchema").default;
	Container.set('tipoTripulanteSchema', tipoTripulanteSchemaClass);
	
	let tipoTripulanteRepoClass = require("../../../repos/tipoTripulanteRepo").default;
	let tipoTripulanteRepoInstance = Container.get(tipoTripulanteRepoClass);
	Container.set(config.repos.tipoTripulante.name, tipoTripulanteRepoInstance);
	

    it('createTipoTripulante: returns json with description value', async function () {
		
		const body = {"domainId":"12345", "desc":'test_desc' } as ITipoTripulanteDTO;
		const resu = TipoTripulante.create(body).getValue();

		tipoTripulanteRepoInstance = Container.get(config.repos.tipoTripulante.name);
		sinon.stub(tipoTripulanteRepoInstance, "save").returns( Result.ok<TipoTripulante>(resu));

		const serv = new TipoTripulanteService(tipoTripulanteRepoInstance as ITipoTripulanteRepo);


		const res = await (await serv.createTipoTripulante(body)).getValue();

		sinon.assert.match(res, body);
	});

	
	it('listTipoTripulantes: returns json with array of types', async function () {
		

        const body = {"domainId":"12345", "desc":'test_desc' } as ITipoTripulanteDTO;
        const body2 = {"domainId":"123456", "desc":'test_desc2' } as ITipoTripulanteDTO;
        const body3 = {"domainId":"123457", "desc":'test_desc3' } as ITipoTripulanteDTO;
		const resu = TipoTripulante.create(body).getValue();
		const resu2 = TipoTripulante.create(body2).getValue();
		const resu3 = TipoTripulante.create(body3).getValue();

		
		tipoTripulanteRepoInstance = Container.get(config.repos.tipoTripulante.name);
		sinon.stub(tipoTripulanteRepoInstance, "findAll").returns( [resu, resu2, resu3]);

		const serv = new TipoTripulanteService(tipoTripulanteRepoInstance as ITipoTripulanteRepo);

		serv.createTipoTripulante(body);
		serv.createTipoTripulante(body2);
		serv.createTipoTripulante(body3);
		const res = await (await serv.listTiposTripulante()).getValue();

		sinon.assert.match(res, [body, body2, body3]);
	});

	it('listTipoTripulantes: returns json with sorted array of types', async function () {
		

        const body = {"domainId":"12345", "desc":'btest_desc' } as ITipoTripulanteDTO;
        const body2 = {"domainId":"123456", "desc":'atest_desc2' } as ITipoTripulanteDTO;
        const body3 = {"domainId":"123457", "desc":'ctest_desc3' } as ITipoTripulanteDTO;
		const resu = TipoTripulante.create(body).getValue();
		const resu2 = TipoTripulante.create(body2).getValue();
		const resu3 = TipoTripulante.create(body3).getValue();

		
		tipoTripulanteRepoInstance = Container.get(config.repos.tipoTripulante.name);
		sinon.stub(tipoTripulanteRepoInstance, "findAllSorted").returns( [resu2, resu, resu3]);

		const serv = new TipoTripulanteService(tipoTripulanteRepoInstance as ITipoTripulanteRepo);

		serv.createTipoTripulante(body);
		serv.createTipoTripulante(body2);
		serv.createTipoTripulante(body3);
		const res = await (await serv.listTiposTripulanteSorted("desc")).getValue();

		sinon.assert.match(res, [body2, body, body3]);
	});

});



// import * as sinon from 'sinon';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'typedi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import ITipoTripulanteService from "../../../services/IServices/ITipoTripulanteService";
// import tipoTripulanteService from "../../../services/tipoTripulanteService";
// import tipoTripulanteRepo from "../../../repos/tipoTripulanteRepo";
// import TipoTripulanteController from "../../../controllers/tipoTripulanteController";
// import ITipoTripulanteController from "../../../controllers/IControllers/ITipoTripulanteController";
// import ITipoTripulanteDTO from '../../../dto/ITipoTripulanteDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/tipoTripulanteService", () => {
// 	const serv = {
// 		createTipoTripulante: jest.fn()
// 	  };
// 	  return { TipoTripulanteService: jest.fn(() => serv) };
// 	});
	

// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(tipoTripulanteService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createTipoTripulante: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
		
// 	// 	const controller = new TipoTripulanteController(MockedServ.prototype);

// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.prototype.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createTipoTripulante.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "TipoTripulanteController", path: "../controllers/tipoTripulanteController"};

	

	
// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	require('../../../app');
		
// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.tipoTripulante.name) as ITipoTripulanteController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
		 
// 	// 	 ctrl.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});