import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import PercursoService from "../../../services/percursoService";
import IPercursoRepo from "../../../repos/IRepos/IPercursoRepo";
import IPercursoDTO from '../../../dto/IPercursoDTO';
import {Percurso} from '../../../domain/percurso/percurso';
import { NosPercurso } from '../../../domain/percurso/nosPercurso';


describe('tipo tripulante service', function () {
	beforeEach(function() {
	});
	

	let percursoSchemaClass = require("../../../persistence/schemas/percursoSchema").default;
	Container.set('percursoSchema', percursoSchemaClass);
	
	let percursoRepoClass = require("../../../repos/percursoRepo").default;
	let percursoRepoInstance = Container.get(percursoRepoClass);
	Container.set(config.repos.percurso.name, percursoRepoInstance);
	

    it('createPercurso: returns json with description value', async function () {
		
		const body = {
			"domainId": "Path:1",
			"nosPercurso": []
		} as unknown as IPercursoDTO;
		const resu = Percurso.create(body).getValue();

		percursoRepoInstance = Container.get(config.repos.percurso.name);
		sinon.stub(percursoRepoInstance, "save").returns( Result.ok<Percurso>(resu));

		const serv = new PercursoService(percursoRepoInstance as IPercursoRepo);


		const res = await (await serv.createPercurso(body)).getValue();

		sinon.assert.match(res, body);
	});

	
	it('listPercursos: returns json with array of types', async function () {
		
		const bodyT = [NosPercurso.create({noId: "no1", duracao: 34, distancia: 45}).getValue(), NosPercurso.create({noId: "no7", duracao: 34, distancia: 45}).getValue()];
		const bodyT2 = [NosPercurso.create({noId: "no2", duracao: 34, distancia: 45}).getValue()];
		const bodyT3 = [NosPercurso.create({noId: "no3", duracao: 34, distancia: 45}).getValue()];

		const body = {
			"domainId": "Path:1",
			"nosPercurso": bodyT
		} as IPercursoDTO;
		const body2 = {
			"domainId": "Path:2",
			"nosPercurso": bodyT2
		} as IPercursoDTO;
		const body3 = {
			"domainId": "Path:3",
			"nosPercurso": bodyT3
		} as IPercursoDTO;
		const resu = Percurso.create(body).getValue();
		const resu2 = Percurso.create(body2).getValue();
		const resu3 = Percurso.create(body3).getValue();

		
		percursoRepoInstance = Container.get(config.repos.percurso.name);
		sinon.stub(percursoRepoInstance, "findAll").returns( [resu, resu2, resu3]);

		const serv = new PercursoService(percursoRepoInstance as IPercursoRepo);

		serv.createPercurso(body);
		serv.createPercurso(body2);
		serv.createPercurso(body3);
		const res = await (await serv.listPercursos()).getValue();

		sinon.assert.match(res, [body, body2, body3]);
	});


});



// import * as sinon from 'sinon';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'typedi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import IPercursoService from "../../../services/IServices/IPercursoService";
// import percursoService from "../../../services/percursoService";
// import percursoRepo from "../../../repos/percursoRepo";
// import PercursoController from "../../../controllers/percursoController";
// import IPercursoController from "../../../controllers/IControllers/IPercursoController";
// import IPercursoDTO from '../../../dto/IPercursoDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/percursoService", () => {
// 	const serv = {
// 		createPercurso: jest.fn()
// 	  };
// 	  return { PercursoService: jest.fn(() => serv) };
// 	});
	

// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(percursoService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createPercurso: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
		
// 	// 	const controller = new PercursoController(MockedServ.prototype);

// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.prototype.createPercurso(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: IPercursoDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createPercurso.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "PercursoController", path: "../controllers/percursoController"};

	

	
// 	// it('createPercurso: returns json with description value', async function () {
// 	// 	require('../../../app');
		
// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.percurso.name) as IPercursoController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
		 
// 	// 	 ctrl.createPercurso(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: IPercursoDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});