import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import IPercursoService from "../../../services/IServices/IPercursoService";
import PercursoController from "../../../controllers/percursoController";
import IPercursoDTO from '../../../dto/IPercursoDTO';


describe('Percurso controller', function () {
	beforeEach(function () {
	});


	let percursoSchemaClass = require("../../../persistence/schemas/percursoSchema").default;
	Container.set('percursoSchema', percursoSchemaClass);

	let percursoRepoClass = require("../../../repos/percursoRepo").default;
	let percursoRepoInstance = Container.get(percursoRepoClass);
	Container.set(config.repos.percurso.name, percursoRepoInstance);

	let percursoServiceClass = require("../../../services/percursoService").default;
	let percursoServiceInstance = Container.get(percursoServiceClass);
	Container.set(config.services.percurso.name, percursoServiceInstance);

	it('createpercurso: returns json with percurso components', async function () {

		let body = {
			"domainId": "Path:1",
			"nosPercurso": [
				{​​"noId": "no3", "duracao": 34, "distancia": 45}​​,
				{​​"noId": "no4", "duracao": 34, "distancia": 45}
			]
		};

		let req: Partial<Request> = {};
		req.body = body;

		let res: Partial<Response> = {
			json: sinon.spy(),
			status: function (s) {this.statusCode = s; return this;}
		};
		let next: Partial<NextFunction> = () => { };

		percursoServiceInstance = Container.get(config.services.percurso.name);
		sinon.stub(percursoServiceInstance, "createPercurso").returns(Result.ok<IPercursoDTO>({
			"domainId": req.body.domainId,
			"nosPercurso": req.body.nosPercurso
		}));

		const ctrl = new PercursoController(percursoServiceInstance as IPercursoService);


		await ctrl.createPercurso(<Request>req, <Response>res, <NextFunction>next);

		sinon.assert.calledOnce(res.json);
		sinon.assert.calledWith(res.json, sinon.match(body));
	});

	it('listPercursos: returns json with array of percursos', async function () {

		let body = {
			"domainId": "Path:1",
			"nosPercurso": []
		};

		let body2 = {
			"domainId": "Path:2",
			"nosPercurso": []
		};

		let req: Partial<Request> = {};
		let req2: Partial<Request> = {};
		let req3: Partial<Request> = {};
		req.body = body;
		req2.body = body2;

		let res: Partial<Response> = {};
		let res2: Partial<Response> = {};
		let res3: Partial<Response> = {
			json: sinon.spy()
		};
		let next: Partial<NextFunction> = () => { };

		percursoServiceInstance = Container.get(config.services.percurso.name);
		sinon.stub(percursoServiceInstance, "listPercursos").returns(Result.ok<IPercursoDTO[]>([{
			"domainId": req.body.domainId,
			"nosPercurso": req.body.nosPercurso
		},
		{
			"domainId": req2.body.domainId,
			"nosPercurso": req2.body.nosPercurso
		}]));

		const ctrl = new PercursoController(percursoServiceInstance as IPercursoService);


		await ctrl.createPercurso(<Request>req, <Response>res, <NextFunction>next);
		await ctrl.createPercurso(<Request>req2, <Response>res2, <NextFunction>next);
		await ctrl.listPercursos(<Request>req3, <Response>res3, <NextFunction>next);

		sinon.assert.calledOnce(res3.json);
		sinon.assert.calledWith(res3.json, sinon.match([{
			"domainId": "Path:1",
			"nosPercurso": []
		
		}, {
			"domainId": "Path:2",
			"nosPercurso": []
		}]));
	});

});




// import * as sipercurson from 'sipercurson';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'percursodi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import ITipoTripulanteService from "../../../services/IServices/ITipoTripulanteService";
// import tipoTripulanteService from "../../../services/tipoTripulanteService";
// import tipoTripulanteRepo from "../../../repos/tipoTripulanteRepo";
// import TipoTripulanteController from "../../../controllers/tipoTripulanteController";
// import ITipoTripulanteController from "../../../controllers/IControllers/ITipoTripulanteController";
// import ITipoTripulanteDTO from '../../../dto/ITipoTripulanteDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/tipoTripulanteService", () => {
// 	const serv = {
// 		createTipoTripulante: jest.fn()
// 	  };
// 	  return { TipoTripulanteService: jest.fn(() => serv) };
// 	});


// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(tipoTripulanteService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createTipoTripulante: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: sipercurson.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };

// 	// 	const controller = new TipoTripulanteController(MockedServ.protopercurso);

// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sipercurson.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.protopercurso.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.protopercurso.createTipoTripulante.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "TipoTripulanteController", path: "../controllers/tipoTripulanteController"};




// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	require('../../../app');

// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.tipoTripulante.name) as ITipoTripulanteController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: sipercurson.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };

// 	// 	 ctrl.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});