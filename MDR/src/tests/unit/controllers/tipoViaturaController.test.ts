import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import ITipoViaturaService from "../../../services/IServices/ITipoViaturaService";
import TipoViaturaController from "../../../controllers/tipoViaturaController";
import ITipoViaturaDTO from '../../../dto/ITipoViaturaDTO';


describe('tipo viatura controller', function () {
	beforeEach(function () {
	});


	let tipoViaturaSchemaClass = require("../../../persistence/schemas/tipoViaturaSchema").default;
	Container.set('tipoViaturaSchema', tipoViaturaSchemaClass);

	let tipoViaturaRepoClass = require("../../../repos/tipoViaturaRepo").default;
	let tipoViaturaRepoInstance = Container.get(tipoViaturaRepoClass);
	Container.set(config.repos.tipoViatura.name, tipoViaturaRepoInstance);

	let tipoViaturaServiceClass = require("../../../services/tipoViaturaService").default;
	let tipoViaturaServiceInstance = Container.get(tipoViaturaServiceClass);
	Container.set(config.services.tipoViatura.name, tipoViaturaServiceInstance);

	it('createTipoViatura: returns json with type components', async function () {

		let body = {
			"domainId": "v:123",
			"nome": "iviat123",
			"autonomia": 151,
			"custo": 23,
			"velocidadeMedia": 100,
			"combustivel": 23,
			"consumo": 120,
			"emissoes": 330
		};

		let req: Partial<Request> = {};
		req.body = body;

		let res: Partial<Response> = {
			json: sinon.spy(),
			status: function (s) {this.statusCode = s; return this;}
		};
		let next: Partial<NextFunction> = () => { };

		tipoViaturaServiceInstance = Container.get(config.services.tipoViatura.name);
		sinon.stub(tipoViaturaServiceInstance, "createTipoViatura").returns(Result.ok<ITipoViaturaDTO>({
			"domainId": req.body.domainId,
			"nome": req.body.nome,
			"autonomia": req.body.autonomia,
			"custo": req.body.custo,
			"velocidadeMedia": req.body.velocidadeMedia,
			"combustivel": req.body.combustivel,
			"consumo": req.body.consumo,
			"emissoes": req.body.emissoes
		}));

		const ctrl = new TipoViaturaController(tipoViaturaServiceInstance as ITipoViaturaService);


		await ctrl.createTipoViatura(<Request>req, <Response>res, <NextFunction>next);
		sinon.assert.calledOnce(res.json);
		sinon.assert.calledWith(res.json, sinon.match(body));
	});

	it('listTipoViaturas: returns json with array of types', async function () {

		let body = {
			"domainId": "v:123",
			"nome": "iviat123",
			"autonomia": 151,
			"custo": 23,
			"velocidadeMedia": 100,
			"combustivel": 23,
			"consumo": 120,
			"emissoes": 330
		};
		let body2 = {
			"domainId": "v:321",
			"nome": "iviat321",
			"autonomia": 231,
			"custo": 423,
			"velocidadeMedia": 200,
			"combustivel": 50,
			"consumo": 70,
			"emissoes": 200
		};

		let req: Partial<Request> = {};
		let req2: Partial<Request> = {};
		let req3: Partial<Request> = {};
		req.body = body;
		req2.body = body2;

		let res: Partial<Response> = {};
		let res2: Partial<Response> = {};
		let res3: Partial<Response> = {
			json: sinon.spy()
		};
		let next: Partial<NextFunction> = () => { };

		tipoViaturaServiceInstance = Container.get(config.services.tipoViatura.name);
		sinon.stub(tipoViaturaServiceInstance, "listTiposViatura").returns(Result.ok<ITipoViaturaDTO[]>([{
			"domainId": req.body.domainId,
			"nome": req.body.nome,
			"autonomia": req.body.autonomia,
			"custo": req.body.custo,
			"velocidadeMedia": req.body.velocidadeMedia,
			"combustivel": req.body.combustivel,
			"consumo": req.body.consumo,
			"emissoes": req.body.emissoes
		},
		{
			"domainId": req2.body.domainId,
			"nome": req2.body.nome,
			"autonomia": req2.body.autonomia,
			"custo": req2.body.custo,
			"velocidadeMedia": req2.body.velocidadeMedia,
			"combustivel": req2.body.combustivel,
			"consumo": req2.body.consumo,
			"emissoes": req2.body.emissoes
		}]));

		const ctrl = new TipoViaturaController(tipoViaturaServiceInstance as ITipoViaturaService);


		await ctrl.createTipoViatura(<Request>req, <Response>res, <NextFunction>next);
		await ctrl.createTipoViatura(<Request>req2, <Response>res2, <NextFunction>next);
		await ctrl.listTiposViatura(<Request>req3, <Response>res3, <NextFunction>next);

		sinon.assert.calledOnce(res3.json);
		sinon.assert.calledWith(res3.json, sinon.match([{
			"domainId": "v:123",
			"nome": "iviat123",
			"autonomia": 151,
			"custo": 23,
			"velocidadeMedia": 100,
			"combustivel": 23,
			"consumo": 120,
			"emissoes": 330
		}, 
		{
			"domainId": "v:321",
			"nome": "iviat321",
			"autonomia": 231,
			"custo": 423,
			"velocidadeMedia": 200,
			"combustivel": 50,
			"consumo": 70,
			"emissoes": 200
		}]));
	});

	it('listTipoViaturas: returns json with sorted array of types', async function () {

		let body = {
			"domainId": "v:123",
			"nome": "iviat123",
			"autonomia": 151,
			"custo": 23,
			"velocidadeMedia": 100,
			"combustivel": 23,
			"consumo": 120,
			"emissoes": 330
		};
		let body2 = {
			"domainId": "v:321",
			"nome": "iviat321",
			"autonomia": 231,
			"custo": 423,
			"velocidadeMedia": 200,
			"combustivel": 50,
			"consumo": 70,
			"emissoes": 200
		};
		let req: Partial<Request> = {};
		let req2: Partial<Request> = {};
		let req3: Partial<Request> = {};
		req.body = body;
		req2.body = body2;
		req3.params = { "ord": "autonomia" };;

		let res: Partial<Response> = {};
		let res2: Partial<Response> = {};
		let res3: Partial<Response> = {
			json: sinon.spy()
		};
		let next: Partial<NextFunction> = () => { };

		tipoViaturaServiceInstance = Container.get(config.services.tipoViatura.name);
		sinon.stub(tipoViaturaServiceInstance, "listTiposViaturaSorted").returns(Result.ok<ITipoViaturaDTO[]>([{
			"domainId": req.body.domainId,
			"nome": req.body.nome,
			"autonomia": req.body.autonomia,
			"custo": req.body.custo,
			"velocidadeMedia": req.body.velocidadeMedia,
			"combustivel": req.body.combustivel,
			"consumo": req.body.consumo,
			"emissoes": req.body.emissoes
		},
		{
			"domainId": req2.body.domainId,
			"nome": req2.body.nome,
			"autonomia": req2.body.autonomia,
			"custo": req2.body.custo,
			"velocidadeMedia": req2.body.velocidadeMedia,
			"combustivel": req2.body.combustivel,
			"consumo": req2.body.consumo,
			"emissoes": req2.body.emissoes
		}]));

		const ctrl = new TipoViaturaController(tipoViaturaServiceInstance as ITipoViaturaService);


		await ctrl.createTipoViatura(<Request>req, <Response>res, <NextFunction>next);
		await ctrl.createTipoViatura(<Request>req2, <Response>res2, <NextFunction>next);
		await ctrl.listTiposViaturaSorted(<Request>req3, <Response>res3, <NextFunction>next);

		sinon.assert.calledOnce(res3.json);
		sinon.assert.calledWith(res3.json, sinon.match([{
			"domainId": "v:123",
			"nome": "iviat123",
			"autonomia": 151,
			"custo": 23,
			"velocidadeMedia": 100,
			"combustivel": 23,
			"consumo": 120,
			"emissoes": 330
		}, 
		{
			"domainId": "v:321",
			"nome": "iviat321",
			"autonomia": 231,
			"custo": 423,
			"velocidadeMedia": 200,
			"combustivel": 50,
			"consumo": 70,
			"emissoes": 200
		}]))
	});
});




// import * as sinon from 'sinon';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'typedi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import ITipoTripulanteService from "../../../services/IServices/ITipoTripulanteService";
// import tipoTripulanteService from "../../../services/tipoTripulanteService";
// import tipoTripulanteRepo from "../../../repos/tipoTripulanteRepo";
// import TipoTripulanteController from "../../../controllers/tipoTripulanteController";
// import ITipoTripulanteController from "../../../controllers/IControllers/ITipoTripulanteController";
// import ITipoTripulanteDTO from '../../../dto/ITipoTripulanteDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/tipoTripulanteService", () => {
// 	const serv = {
// 		createTipoTripulante: jest.fn()
// 	  };
// 	  return { TipoTripulanteService: jest.fn(() => serv) };
// 	});


// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(tipoTripulanteService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createTipoTripulante: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };

// 	// 	const controller = new TipoTripulanteController(MockedServ.prototype);

// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.prototype.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createTipoTripulante.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "TipoTripulanteController", path: "../controllers/tipoTripulanteController"};




// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	require('../../../app');

// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.tipoTripulante.name) as ITipoTripulanteController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };

// 	// 	 ctrl.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});