import { expect,assert } from "chai";
import * as sinon from 'sinon';
import { Linha } from "../../../domain/linha/linha";
import { PercursoLinha } from "../../../domain/linha/percursoLinha";
import { UniqueEntityID } from "../../../core/domain/UniqueEntityID";
import ILinhaDTO from "../../../dto/ILinhaDTO";


describe("Linha Tests", () => {
    it("Validate Linha values", () => {


        const id = new UniqueEntityID(1);

        const percursoLinhaa = PercursoLinha.create({ percursoLinhaId: "pl1", percurso: "percurso", orientation: "Go" }).getValue();

        let body = { "domainId": "1", "nome": "nome", "cor": "cor", "percursoLinha": [percursoLinhaa] } as ILinhaDTO;

        const goodLinha = Linha.create( body, id);
        // const badLinha = Linha.create({ domainId: "1", percursoLinha: percursoLinhaa, nome: "nome", cor: "cor" }, id);

        expect(goodLinha.isSuccess).to.equal(true);
        
        // expect(badLinha.isFailure).to.equal(true);


        var linha = Linha.create(body).getValue();

        //testar get domainID
        assert.equal(linha.props.domainId,"1");
        
        //testar get nome
        sinon.assert.match(linha.percursoLinha,[percursoLinhaa]);
        //testar get nome
        assert.equal(linha.nome, "nome");
        //testar get cor
        assert.equal(linha.cor, "cor");

        var linha2 = Linha.create(
            body
        ).getValue();


        const percursoLinhaa2 = PercursoLinha.create({ percursoLinhaId: "pl:1", percurso: "percurso", orientation: "Go" }).getValue();

        
        //testar set percursoLinha
        linha2.percursoLinha = [percursoLinhaa2];
        sinon.assert.match(linha2.percursoLinha, [percursoLinhaa2]);

        //testar set nome
        linha2.nome = "the name";
        assert.equal(linha2.nome,"the name");

        //testar set cor
        linha2.nome = "a cor";
        assert.equal(linha2.nome,"a cor");
    });

});