import { expect } from "chai";
import { TipoTripulante } from "../../../domain/tipoTripulante/tipoTripulante";
import { UniqueEntityID } from "../../../core/domain/UniqueEntityID";


describe("Tipo de Tripulante Tests", () => {
    it("Validate Percurso values", () => {


        const id = new UniqueEntityID(1);
        const goodTipoTripulante = TipoTripulante.create({
            domainId: "1",
            desc: "descricao"
        }, id);

        const badTipoTripulante = TipoTripulante.create({
            domainId: "1",
            desc: ""
        }, id);

        expect(goodTipoTripulante.isSuccess).to.equal(true);
        expect(badTipoTripulante.isFailure).to.equal(true);


        var tipoTripulante = TipoTripulante.create({ domainId: "1", 
        desc: "descricao" }).getValue();

        //testar get domainID
        expect(tipoTripulante.props.domainId).equal("1");

        //testar get nosPercurso
        expect(tipoTripulante.desc).equal("descricao");

        var tipoTripulante2 = TipoTripulante.create({
            domainId: "1",
            desc: "la description"
        }).getValue();



        //testar set nosPercurso
        tipoTripulante2.desc = "vive la france";
        expect(tipoTripulante2.desc).equal("vive la france");

        
    });

});

