import { expect } from "chai";
import { Role } from "../../../domain/role/role";
import IRoleDTO from "../../../dto/IRoleDTO";
import { UniqueEntityID } from "../../../core/domain/UniqueEntityID";


describe("Role Tests", () => {
    it("Validate role values", () => {


        const id = new UniqueEntityID(1);
        const goodRole = Role.create({ id: "id1", name: "Role" } as IRoleDTO);
        const badRole = Role.create({ id: "id2", name: "" } as IRoleDTO);

        expect(goodRole.isSuccess).to.equal(true);
        expect(badRole.isFailure).to.equal(true);


        var role = Role.create({
            id: "id3",
            name: "role_"
        } as IRoleDTO).getValue();

        
        //testar get name
        expect(role.name).equal("role_");

        var role2 = Role.create({
            id: "idrole2",
            name: "la description"
        } as IRoleDTO).getValue();



        //testar set role
        role2.name = "role2";
        expect(role2.name).equal("role2");


    });






});

