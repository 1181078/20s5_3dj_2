import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import TipoTripulanteService from "../../../services/tipoTripulanteService";
import ITipoTripulanteRepo from "../../../repos/IRepos/ITipoTripulanteRepo";
import TipoTripulanteRepo from "../../../repos/tipoTripulanteRepo";
import ITipoTripulanteDTO from '../../../dto/ITipoTripulanteDTO';
import {TipoTripulante} from '../../../domain/tipoTripulante/tipoTripulante';
import TipoTripulanteSchema from '../../../persistence/schemas/tipoTripulanteSchema';
import { ITipoTripulantePersistence } from '../../../dataschema/ITipoTripulantePersistence';
import { Document, Model } from 'mongoose';
import mongoose from 'mongoose';



describe('tipo tripulante repo', function () {
	beforeEach(function() {
	});
	

	let tipoTripulanteSchemaClass = require("../../../persistence/schemas/tipoTripulanteSchema").default;
	Container.set('tipoTripulanteSchema', tipoTripulanteSchemaClass);
		

    it('save: returns json with description value', async function () {
		
		const body = {"domainId":"12345", "desc":'test_desc' } as ITipoTripulanteDTO;
		const body2 = {"domainId":"12345", "desc":'test_desc' };
		const resu = TipoTripulante.create(body).getValue();

		const newS: Model<ITipoTripulantePersistence  & Document> = Container.get("tipoTripulanteSchema");
		sinon.stub(newS, "findOne").returns( Result.ok<ITipoTripulantePersistence>(body2));
		sinon.stub(newS, "create").returns( Result.ok<TipoTripulante>(resu));

		const repo = new TipoTripulanteRepo(newS);

		
		const res = await repo.save(resu);

		sinon.assert.match(res, resu);
	});

	
	it('listTipoTripulantes: returns json with array of types', async function () {
		

        const body = {"domainId":"12345", "desc":'test_desc' } as ITipoTripulanteDTO;
        const body2 = {"domainId":"123456", "desc":'test_desc2' } as ITipoTripulanteDTO;
        const body3 = {"domainId":"123457", "desc":'test_desc3' } as ITipoTripulanteDTO;
        const bodyb = {"domainId":"12345", "desc":'test_desc' } as ITipoTripulantePersistence;
        const body2b = {"domainId":"123456", "desc":'test_desc2' } as ITipoTripulantePersistence;
		const body3b= {"domainId":"123457", "desc":'test_desc3' } as ITipoTripulantePersistence;
		const resu = TipoTripulante.create(body).getValue();
		const resu2 = TipoTripulante.create(body2).getValue();
		const resu3 = TipoTripulante.create(body3).getValue();

		const arr = [bodyb, body2b, body3b] as unknown as ITipoTripulantePersistence;
		const resf = [resu, resu2, resu3] as TipoTripulante[];

		const newS: Model<ITipoTripulantePersistence  & Document> = Container.get("tipoTripulanteSchema");
		sinon.stub(newS, "find").returns( Result.ok<ITipoTripulantePersistence>(arr));

		const repo = new TipoTripulanteRepo(newS);
		
		const res = await repo.findAll();

		sinon.assert.match(resf, resf);
	});


	it('listTipoTripulantes: returns json with sorted array of types', async function () {
		

        const body = {"domainId":"12345", "desc":'ztest_desc' } as ITipoTripulanteDTO;
        const body2 = {"domainId":"123456", "desc":'xtest_desc2' } as ITipoTripulanteDTO;
        const body3 = {"domainId":"123457", "desc":'ytest_desc3' } as ITipoTripulanteDTO;
        const bodyb = {"domainId":"12345", "desc":'test_desc' } as ITipoTripulantePersistence;
        const body2b = {"domainId":"123456", "desc":'test_desc2' } as ITipoTripulantePersistence;
		const body3b= {"domainId":"123457", "desc":'test_desc3' } as ITipoTripulantePersistence;
		const resu = TipoTripulante.create(body).getValue();
		const resu2 = TipoTripulante.create(body2).getValue();
		const resu3 = TipoTripulante.create(body3).getValue();

		const arr = [bodyb, body2b, body3b] as unknown as ITipoTripulantePersistence;
		const resf = [resu2, resu3, resu] as TipoTripulante[];

		const newS: Model<ITipoTripulantePersistence  & Document> = Container.get("tipoTripulanteSchema");
		//sinon.stub(newS, "findAllSorted").returns( Result.ok<ITipoTripulantePersistence>(arr));

		const repo = new TipoTripulanteRepo(newS);
		
		const res = await repo.findAll();

		sinon.assert.match(resf, resf);
	});


});



// import * as sinon from 'sinon';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'typedi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import ITipoTripulanteService from "../../../services/IServices/ITipoTripulanteService";
// import tipoTripulanteService from "../../../services/tipoTripulanteService";
// import tipoTripulanteRepo from "../../../repos/tipoTripulanteRepo";
// import TipoTripulanteController from "../../../controllers/tipoTripulanteController";
// import ITipoTripulanteController from "../../../controllers/IControllers/ITipoTripulanteController";
// import ITipoTripulanteDTO from '../../../dto/ITipoTripulanteDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/tipoTripulanteService", () => {
// 	const serv = {
// 		createTipoTripulante: jest.fn()
// 	  };
// 	  return { TipoTripulanteService: jest.fn(() => serv) };
// 	});
	

// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(tipoTripulanteService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createTipoTripulante: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
		
// 	// 	const controller = new TipoTripulanteController(MockedServ.prototype);

// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.prototype.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createTipoTripulante.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "TipoTripulanteController", path: "../controllers/tipoTripulanteController"};

	

	
// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	require('../../../app');
		
// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.tipoTripulante.name) as ITipoTripulanteController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
		 
// 	// 	 ctrl.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});