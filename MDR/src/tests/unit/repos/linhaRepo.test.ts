import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import LinhaService from "../../../services/linhaService";
import ILinhaRepo from "../../../repos/IRepos/ILinhaRepo";
import LinhaRepo from "../../../repos/linhaRepo";
import ILinhaDTO from '../../../dto/ILinhaDTO';
import {Linha} from '../../../domain/linha/linha';
import LinhaSchema from '../../../persistence/schemas/linhaSchema';
import { ILinhaPersistence } from '../../../dataschema/ILinhaPersistence';
import { Document, Model } from 'mongoose';
import mongoose from 'mongoose';
import { PercursoLinha } from '../../../domain/linha/percursoLinha';



describe('tipo tripulante repo', function () {
	beforeEach(function() {
	});
	

	let linhaSchemaClass = require("../../../persistence/schemas/linhaSchema").default;
	Container.set('linhaSchema', linhaSchemaClass);
		

    it('save: returns json with description value', async function () {
		
		const bodyT = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT
		} as ILinhaDTO;

		const bodyb = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT
		};

		const resu = Linha.create(body).getValue();

		const newS: Model<ILinhaPersistence  & Document> = Container.get("linhaSchema");
		sinon.stub(newS, "findOne").returns( Result.ok<ILinhaPersistence>(bodyb));
		sinon.stub(newS, "create").returns( Result.ok<Linha>(resu));

		const repo = new LinhaRepo(newS);

		
		const res = await repo.save(resu);

		sinon.assert.match(res, resu);
	});

	
	it('listLinhas: returns json with array of types', async function () {
		
		const bodyT = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT
		} as ILinhaDTO;

		const bodyb = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT
		};

		const bodyT2 = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body2 = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT2
		} as ILinhaDTO;

		const body2b = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT2
		};

		const bodyT3 = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body3 = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT3
		} as ILinhaDTO;

		const body3b = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT3
		};
		const resu = Linha.create(body).getValue();
		const resu2 = Linha.create(body2).getValue();
		const resu3 = Linha.create(body3).getValue();

		const arr = [bodyb, body2b, body3b] as unknown as ILinhaPersistence;
		const resf = [resu, resu2, resu3] as Linha[];

		const newS: Model<ILinhaPersistence  & Document> = Container.get("linhaSchema");
		sinon.stub(newS, "find").returns( Result.ok<ILinhaPersistence>(arr));

		const repo = new LinhaRepo(newS);
		
		const res = await repo.findAll();

		sinon.assert.match(resf, resf);
	});


	it('listLinhas: returns json with sorted array of types', async function () {
		
		const bodyT = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT
		} as ILinhaDTO;

		const bodyb = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT
		};

		const bodyT2 = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body2 = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT2
		} as ILinhaDTO;

		const body2b = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT2
		};

		const bodyT3 = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body3 = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT3
		} as ILinhaDTO;

		const body3b = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT3
		};

		const resu = Linha.create(body).getValue();
		const resu2 = Linha.create(body2).getValue();
		const resu3 = Linha.create(body3).getValue();

		const arr = [bodyb, body2b, body3b] as unknown as ILinhaPersistence;
		const resf = [resu2, resu3, resu] as Linha[];

		const newS: Model<ILinhaPersistence  & Document> = Container.get("linhaSchema");
		//sinon.stub(newS, "findAllSorted").returns( Result.ok<ILinhaPersistence>(arr));

		const repo = new LinhaRepo(newS);
		
		const res = await repo.findAll();

		sinon.assert.match(resf, resf);
	});


});



// import * as sinon from 'sinon';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'typedi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import ILinhaService from "../../../services/IServices/ILinhaService";
// import linhaService from "../../../services/linhaService";
// import linhaRepo from "../../../repos/linhaRepo";
// import LinhaController from "../../../controllers/linhaController";
// import ILinhaController from "../../../controllers/IControllers/ILinhaController";
// import ILinhaDTO from '../../../dto/ILinhaDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/linhaService", () => {
// 	const serv = {
// 		createLinha: jest.fn()
// 	  };
// 	  return { LinhaService: jest.fn(() => serv) };
// 	});
	

// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(linhaService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createLinha: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
		
// 	// 	const controller = new LinhaController(MockedServ.prototype);

// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.prototype.createLinha(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: ILinhaDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createLinha.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "LinhaController", path: "../controllers/linhaController"};

	

	
// 	// it('createLinha: returns json with description value', async function () {
// 	// 	require('../../../app');
		
// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.linha.name) as ILinhaController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
		 
// 	// 	 ctrl.createLinha(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: ILinhaDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});