import { expect } from "chai";
import * as sinon from 'sinon';
import { No } from "../../../domain/no/no";
import { TempoViagemGrupo } from "../../../domain/no/tempoViagemGrupo";
import { UniqueEntityID } from "../../../core/domain/UniqueEntityID";


describe("Tipo de Tripulante Tests", () => {
    it("Validate Percurso values", () => {


        const id = new UniqueEntityID(1);

        var tempoViagemm = TempoViagemGrupo.create({
            noId: "noId1",
            duracao: 4,
        }).getValue();

        const goodNo = No.create({
            domainId: "id1",
            nome: "nome",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: true,
            isPontoRendicao: false,
            tempoViagem: [tempoViagemm]
        }, id);

        const badNo = No.create({
            domainId: "id1",
            nome: "",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: true,
            isPontoRendicao: false,
            tempoViagem: [tempoViagemm]
        }, id);

        const badNo2 = No.create({
            domainId: "id1",
            nome: "as",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: null,
            isPontoRendicao: false,
            tempoViagem: [tempoViagemm]
        }, id);

        const badNo3 = No.create({
            domainId: "id1",
            nome: "as",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: true,
            isPontoRendicao: null,
            tempoViagem: [tempoViagemm]
        }, id);

        const badNo4 = No.create({
            domainId: "id1",
            nome: "as",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: null,
            isPontoRendicao: null,
            tempoViagem: [tempoViagemm]
        }, id);

        expect(goodNo.isSuccess).to.equal(true);
        expect(badNo.isFailure).to.equal(true);
        expect(badNo2.isFailure).to.equal(true);
        expect(badNo3.isFailure).to.equal(true);
        expect(badNo4.isFailure).to.equal(true);
        


        var no1 = No.create({
            domainId: "id1",
            nome: "nome",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: true,
            isPontoRendicao: false,
            tempoViagem: [tempoViagemm]
        }, id).getValue();

        //testar get domainID
        expect(no1.props.domainId).equal("id1");

        //testar get nome
        expect(no1.nome).equal("nome");

        //testar get latitude
        expect(no1.latitude).equal(40);

        //testar get longitude
        expect(no1.longitude).equal(23);

        //testar get nomeCurto
        expect(no1.nomeCurto).equal("Pedro");

        //testar get isEstacaoRecolha
        expect(no1.isEstacaoRecolha).equal(true);

        //testar get isPontoRendicao
        expect(no1.isPontoRendicao).equal(false);

        //testar get isTempoViagem
        sinon.assert.match(no1.isTempoViagem, [tempoViagemm]);

        var no2 = No.create({
            domainId: "id1",
            nome: "nome",
            latitude: 40,
            longitude: 23,
            nomeCurto: "Pedro",
            isEstacaoRecolha: true,
            isPontoRendicao: false,
            tempoViagem: [tempoViagemm]
        }, id).getValue();



        var tempoViagemm2 = TempoViagemGrupo.create({
            noId: "noId1",
            duracao: 4,
        }).getValue();

        //testar set nome
        no2.nome = "fromage";
        expect(no2.nome).equal("fromage");

        //testar set latitude
        no2.latitude = 21;
        expect(no2.latitude).equal(21);

        //testar set longitude
        no2.longitude = 11;
        expect(no2.longitude).equal(11);

        //testar set nomeCurto
        no2.nomeCurto = "curto";
        expect(no2.nomeCurto).equal("curto");

        //testar set isEstacaoRecolha
        no2.isEstacaoRecolha = false;
        expect(no2.isEstacaoRecolha).equal(false);

        //testar set isPontoRendicao
        no2.isEstacaoRecolha = true;
        expect(no2.isEstacaoRecolha).equal(true);

        //testar set isTempoViagem
        no2.isTempoViagem = [tempoViagemm2];
        sinon.assert.match(no2.isTempoViagem, [tempoViagemm2]);
    });



});

