import { expect } from "chai";
import * as sinon from 'sinon';
import { Percurso } from "../../../domain/percurso/percurso";
import { NosPercurso } from "../../../domain/percurso/nosPercurso";
import { UniqueEntityID } from "../../../core/domain/UniqueEntityID";
import IPercursoDTO from "../../../dto/IPercursoDTO";
import { result } from "lodash";
import { Result } from "../../../core/logic/Result";
import { assert } from "console";


describe("Percurso Tests", () => {
    it("Validate Percurso values", () => {


        const id = new UniqueEntityID();



        const nosPercursos = NosPercurso.create({
            noId: "100",
            duracao: 100,
            distancia: 900
        }).getValue();

        const nosPercursosxyz = NosPercurso.create({
            noId: "100",
            duracao: 100,
            distancia: 900
        }).getValue();


        let body = { "domainId": "1", "nosPercurso": [nosPercursos] } as IPercursoDTO;

        const goodPercurso = Percurso.create(
            body, id);


        console.log(goodPercurso);

        expect(goodPercurso.isSuccess).to.equal(true);

        const nosPercursos1 = NosPercurso.create({ noId: "100", duracao: 100, distancia: 900 }).getValue();

        var percurso = Percurso.create(body).getValue();

        //testar get domainID
        sinon.assert.match(percurso.props.domainId, "1");

        //testar get nosPercurso
        sinon.assert.match(percurso.nosPercurso, [nosPercursos]);

        const nosPercursos2 = NosPercurso.create({ noId: "100", duracao: 100, distancia: 900 }).getValue();
        const nosPercursos3 = NosPercurso.create({ noId: "102", duracao: 102, distancia: 910 }).getValue();

        var percurso2 = Percurso.create(body).getValue();



        //testar set nosPercurso
        percurso2.nosPercurso = [nosPercursos3];
        sinon.assert.match(percurso2.nosPercurso, [nosPercursos3]);

    });






});

