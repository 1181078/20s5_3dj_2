import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import INoService from "../../../services/IServices/INoService";
import NoController from "../../../controllers/noController";
import INoDTO from '../../../dto/INoDTO';


describe('No controller', function () {
	beforeEach(function () {
	});


	let noSchemaClass = require("../../../persistence/schemas/noSchema").default;
	Container.set('noSchema', noSchemaClass);

	let noRepoClass = require("../../../repos/noRepo").default;
	let noRepoInstance = Container.get(noRepoClass);
	Container.set(config.repos.no.name, noRepoInstance);

	let noServiceClass = require("../../../services/noService").default;
	let noServiceInstance = Container.get(noServiceClass);
	Container.set(config.services.no.name, noServiceInstance);

	it('createno: returns json with no components', async function () {

		let body = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [{ 
				"noId": "no1",
				"duracao": "123"
		
			}]
		};

		let req: Partial<Request> = {};
		req.body = body;

		let res: Partial<Response> = {
			json: sinon.spy(),
			status: function (s) {this.statusCode = s; return this;}
		};
		let next: Partial<NextFunction> = () => { };

		noServiceInstance = Container.get(config.services.no.name);
		sinon.stub(noServiceInstance, "createNo").returns(Result.ok<INoDTO>({
			"domainId": req.body.domainId,
			"nome": req.body.nome,
			"latitude": req.body.latitude,
			"longitude": req.body.longitude,
			"nomeCurto": req.body.nomeCurto,
			"isEstacaoRecolha": req.body.isEstacaoRecolha,
			"isPontoRendicao": req.body.isPontoRendicao,
			"tempoViagem" : req.body.tempoViagem
		}));

		const ctrl = new NoController(noServiceInstance as INoService);


		await ctrl.createNo(<Request>req, <Response>res, <NextFunction>next);
		sinon.assert.calledOnce(res.json);
		sinon.assert.calledWith(res.json, sinon.match(body));
	});

	it('listNos: returns json with array of nos', async function () {

		let body = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [{ 
				"noId": "no1",
				"duracao": "123"
		
			}]
		};

		let body2 = {
			"domainId": "no2",
			"nome": "no222",
			"latitude": 41,
			"longitude": 63,
			"nomeCurto": "gome2",
			"isEstacaoRecolha": true,
			"isPontoRendicao": true,
			"tempoViagem" : [{ 
				"noId": "no2",
				"duracao": "1223"
		
			}]
		};

		let req: Partial<Request> = {};
		let req2: Partial<Request> = {};
		let req3: Partial<Request> = {};
		req.body = body;
		req2.body = body2;

		let res: Partial<Response> = {};
		let res2: Partial<Response> = {};
		let res3: Partial<Response> = {
			json: sinon.spy()
		};
		let next: Partial<NextFunction> = () => { };

		noServiceInstance = Container.get(config.services.no.name);
		sinon.stub(noServiceInstance, "listNos").returns(Result.ok<INoDTO[]>([{
			"domainId": req.body.domainId,
			"nome": req.body.nome,
			"latitude": req.body.latitude,
			"longitude": req.body.longitude,
			"nomeCurto": req.body.nomeCurto,
			"isEstacaoRecolha": req.body.isEstacaoRecolha,
			"isPontoRendicao": req.body.isPontoRendicao,
			"tempoViagem" : req.body.tempoViagem
		},
		{
			"domainId": req2.body.domainId,
			"nome": req2.body.nome,
			"latitude": req2.body.latitude,
			"longitude": req2.body.longitude,
			"nomeCurto": req2.body.nomeCurto,
			"isEstacaoRecolha": req2.body.isEstacaoRecolha,
			"isPontoRendicao": req2.body.isPontoRendicao,
			"tempoViagem" : req2.body.tempoViagem
		}]));

		const ctrl = new NoController(noServiceInstance as INoService);


		await ctrl.createNo(<Request>req, <Response>res, <NextFunction>next);
		await ctrl.createNo(<Request>req2, <Response>res2, <NextFunction>next);
		await ctrl.listNos(<Request>req3, <Response>res3, <NextFunction>next);

		sinon.assert.calledOnce(res3.json);
		sinon.assert.calledWith(res3.json, sinon.match([{
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [{ 
				"noId": "no1",
				"duracao": "123"
		
			}]
		}, {
			"domainId": "no2",
			"nome": "no222",
			"latitude": 41,
			"longitude": 63,
			"nomeCurto": "gome2",
			"isEstacaoRecolha": true,
			"isPontoRendicao": true,
			"tempoViagem" : [{ 
				"noId": "no2",
				"duracao": "1223"
		
			}]
		}]));
	});

	it('listNos: returns json with sorted array of nos', async function () {

		let body = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [{ 
				"noId": "no1",
				"duracao": "123"
		
			}]
		};

		let body2 = {
			"domainId": "no2",
			"nome": "no222",
			"latitude": 41,
			"longitude": 63,
			"nomeCurto": "gome2",
			"isEstacaoRecolha": true,
			"isPontoRendicao": true,
			"tempoViagem" : [{ 
				"noId": "no2",
				"duracao": "1223"
		
			}]
		};

		let req: Partial<Request> = {};
		let req2: Partial<Request> = {};
		let req3: Partial<Request> = {};
		req.body = body;
		req2.body = body2;
		req3.params = { "ord": "nome" };;

		let res: Partial<Response> = {};
		let res2: Partial<Response> = {};
		let res3: Partial<Response> = {
			json: sinon.spy()
		};
		let next: Partial<NextFunction> = () => { };

		noServiceInstance = Container.get(config.services.no.name);
		sinon.stub(noServiceInstance, "listNosSorted").returns(Result.ok<INoDTO[]>([{
			"domainId": req.body.domainId,
			"nome": req.body.nome,
			"latitude": req.body.latitude,
			"longitude": req.body.longitude,
			"nomeCurto": req.body.nomeCurto,
			"isEstacaoRecolha": req.body.isEstacaoRecolha,
			"isPontoRendicao": req.body.isPontoRendicao,
			"tempoViagem" : req.body.tempoViagem
		},
		{
			"domainId": req2.body.domainId,
			"nome": req2.body.nome,
			"latitude": req2.body.latitude,
			"longitude": req2.body.longitude,
			"nomeCurto": req2.body.nomeCurto,
			"isEstacaoRecolha": req2.body.isEstacaoRecolha,
			"isPontoRendicao": req2.body.isPontoRendicao,
			"tempoViagem" : req2.body.tempoViagem
		}]));

		const ctrl = new NoController(noServiceInstance as INoService);


		await ctrl.createNo(<Request>req, <Response>res, <NextFunction>next);
		await ctrl.createNo(<Request>req2, <Response>res2, <NextFunction>next);
		await ctrl.listNosSorted(<Request>req3, <Response>res3, <NextFunction>next);

		sinon.assert.calledOnce(res3.json);
		sinon.assert.calledWith(res3.json, sinon.match([{
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [{ 
				"noId": "no1",
				"duracao": "123"
		
			}]
		}, {
			"domainId": "no2",
			"nome": "no222",
			"latitude": 41,
			"longitude": 63,
			"nomeCurto": "gome2",
			"isEstacaoRecolha": true,
			"isPontoRendicao": true,
			"tempoViagem" : [{ 
				"noId": "no2",
				"duracao": "1223"
		
			}]
		}]));
	});
});




// import * as sinon from 'sinon';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'nodi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import ITipoTripulanteService from "../../../services/IServices/ITipoTripulanteService";
// import tipoTripulanteService from "../../../services/tipoTripulanteService";
// import tipoTripulanteRepo from "../../../repos/tipoTripulanteRepo";
// import TipoTripulanteController from "../../../controllers/tipoTripulanteController";
// import ITipoTripulanteController from "../../../controllers/IControllers/ITipoTripulanteController";
// import ITipoTripulanteDTO from '../../../dto/ITipoTripulanteDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/tipoTripulanteService", () => {
// 	const serv = {
// 		createTipoTripulante: jest.fn()
// 	  };
// 	  return { TipoTripulanteService: jest.fn(() => serv) };
// 	});


// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(tipoTripulanteService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createTipoTripulante: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };

// 	// 	const controller = new TipoTripulanteController(MockedServ.protono);

// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.protono.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.protono.createTipoTripulante.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "TipoTripulanteController", path: "../controllers/tipoTripulanteController"};




// 	// it('createTipoTripulante: returns json with description value', async function () {
// 	// 	require('../../../app');

// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.tipoTripulante.name) as ITipoTripulanteController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };

// 	// 	 ctrl.createTipoTripulante(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: ITipoTripulanteDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});