import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import ILinhaService from "../../../services/IServices/ILinhaService";
import LinhaController from "../../../controllers/linhaController";
import ILinhaDTO from '../../../dto/ILinhaDTO';


describe('Linha controller', function () {
	beforeEach(function () {
	});


	let linhaSchemaClass = require("../../../persistence/schemas/linhaSchema").default;
	Container.set('linhaSchema', linhaSchemaClass);

	let linhaRepoClass = require("../../../repos/linhaRepo").default;
	let linhaRepoInstance = Container.get(linhaRepoClass);
	Container.set(config.repos.linha.name, linhaRepoInstance);

	let linhaServiceClass = require("../../../services/linhaService").default;
	let linhaServiceInstance = Container.get(linhaServiceClass);
	Container.set(config.services.linha.name, linhaServiceInstance);

	it('createlinha: returns json with linha components', async function () {

		let body = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": [{
				"percursoLinhaId": "71123",
				"percurso": "a",
				"orientation": "b"
			},{
				"percursoLinhaId": "1234",
				"percurso": "a4",
				"orientation": "b4"
			}]
		};

		let req: Partial<Request> = {};
		req.body = body;

		let res: Partial<Response> = {
			json: sinon.spy(),
			status: function (s) {this.statusCode = s; return this;}
		};
		let next: Partial<NextFunction> = () => { };

		linhaServiceInstance = Container.get(config.services.linha.name);
		sinon.stub(linhaServiceInstance, "createLinha").returns(Result.ok<ILinhaDTO>({
			"domainId": req.body.domainId,
			"nome": req.body.nome,
			"cor": req.body.cor,
			"percursoLinha": req.body.percursoLinha
		}));

		const ctrl = new LinhaController(linhaServiceInstance as ILinhaService);
		await ctrl.createLinha(<Request>req, <Response>res, <NextFunction>next);
		sinon.assert.calledOnce(res.json);
		sinon.assert.calledWith(res.json, sinon.match(body));
	});

	it('listLinhas: returns json with array of linhas', async function () {

		let body = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": [{
				"percursoLinhaId": "71123",
				"percurso": "a",
				"orientation": "b"
			},{
				"percursoLinhaId": "1234",
				"percurso": "a4",
				"orientation": "b4"
			}]
		};

		let body2 = {
			"domainId": "nl:11",
			"nome": "n11",
			"cor": "bbb",
			"percursoLinha": [{
				"percursoLinhaId": "121",
				"percurso": "a3",
				"orientation": "b"
			},{
				"percursoLinhaId": "1231114",
				"percurso": "a5",
				"orientation": "b4"
			}]
		};

		let req: Partial<Request> = {};
		let req2: Partial<Request> = {};
		let req3: Partial<Request> = {};
		req.body = body;
		req2.body = body2;

		let res: Partial<Response> = {};
		let res2: Partial<Response> = {};
		let res3: Partial<Response> = {
			json: sinon.spy()
		};
		let next: Partial<NextFunction> = () => { };

		linhaServiceInstance = Container.get(config.services.linha.name);
		sinon.stub(linhaServiceInstance, "listLinhas").returns(Result.ok<ILinhaDTO[]>([{
			"domainId": req.body.domainId,
			"nome": req.body.nome,
			"cor": req.body.cor,
			"percursoLinha": req.body.percursoLinha
		},
		{
			"domainId": req2.body.domainId,
			"nome": req2.body.nome,
			"cor": req2.body.cor,
			"percursoLinha": req2.body.percursoLinha
		}]));

		const ctrl = new LinhaController(linhaServiceInstance as ILinhaService);


		await ctrl.createLinha(<Request>req, <Response>res, <NextFunction>next);
		await ctrl.createLinha(<Request>req2, <Response>res2, <NextFunction>next);
		await ctrl.listLinhas(<Request>req3, <Response>res3, <NextFunction>next);

		sinon.assert.calledOnce(res3.json);
		sinon.assert.calledWith(res3.json, sinon.match([{
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": [{
				"percursoLinhaId": "71123",
				"percurso": "a",
				"orientation": "b"
			},{
				"percursoLinhaId": "1234",
				"percurso": "a4",
				"orientation": "b4"
			}]
		}, {
			"domainId": "nl:11",
			"nome": "n11",
			"cor": "bbb",
			"percursoLinha": [{
				"percursoLinhaId": "121",
				"percurso": "a3",
				"orientation": "b"
			},{
				"percursoLinhaId": "1231114",
				"percurso": "a5",
				"orientation": "b4"
			}]
		}]));
	});

	it('listLinha: returns json with sorted array of types', async function () {
		

        let body = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": [{
				"percursoLinhaId": "71123",
				"percurso": "a",
				"orientation": "b"
			},{
				"percursoLinhaId": "1234",
				"percurso": "a4",
				"orientation": "b4"
			}]
		};

		let body2 = {
			"domainId": "nl:11",
			"nome": "n11",
			"cor": "bbb",
			"percursoLinha": [{
				"percursoLinhaId": "121",
				"percurso": "a3",
				"orientation": "b"
			},{
				"percursoLinhaId": "1231114",
				"percurso": "a5",
				"orientation": "b4"
			}]
		};
		let req: Partial<Request> = {};
		let req2: Partial<Request> = {};
		let req3: Partial<Request> = {};
		req.body=body;
		req2.body = body2;
		req3.params = {"ord":"desc"};;

        let res: Partial<Response> = {};
		let res2: Partial<Response> = {};
		let res3: Partial<Response> = {
			json: sinon.spy()
        };
		let next: Partial<NextFunction> = () => {};

		linhaServiceInstance = Container.get(config.services.linha.name);
		sinon.stub(linhaServiceInstance, "listLinhasSorted").returns( Result.ok<ILinhaDTO[]>( [{
			"domainId": req.body.domainId,
			"nome": req.body.nome,
			"cor": req.body.cor,
			"percursoLinha": req.body.percursoLinha
		},
		{
			"domainId": req2.body.domainId,
			"nome": req2.body.nome,
			"cor": req2.body.cor,
			"percursoLinha": req2.body.percursoLinha
		}] ));

		const ctrl = new LinhaController(linhaServiceInstance as ILinhaService);


		await ctrl.createLinha(<Request>req, <Response>res, <NextFunction>next);
		await ctrl.createLinha(<Request>req2, <Response>res2, <NextFunction>next);
		await ctrl.listLinhasSorted(<Request>req3, <Response>res3, <NextFunction>next);

		sinon.assert.calledOnce(res3.json);
		sinon.assert.calledWith(res3.json, sinon.match([{
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": [{
				"percursoLinhaId": "71123",
				"percurso": "a",
				"orientation": "b"
			},{
				"percursoLinhaId": "1234",
				"percurso": "a4",
				"orientation": "b4"
			}]
		}, {
			"domainId": "nl:11",
			"nome": "n11",
			"cor": "bbb",
			"percursoLinha": [{
				"percursoLinhaId": "121",
				"percurso": "a3",
				"orientation": "b"
			},{
				"percursoLinhaId": "1231114",
				"percurso": "a5",
				"orientation": "b4"
			}]
		}]));
	});
});




// import * as silinhan from 'silinhan';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'linhadi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import ILinhaService from "../../../services/IServices/ILinhaService";
// import linhaService from "../../../services/linhaService";
// import linhaRepo from "../../../repos/linhaRepo";
// import LinhaController from "../../../controllers/linhaController";
// import ILinhaController from "../../../controllers/IControllers/ILinhaController";
// import ILinhaDTO from '../../../dto/ILinhaDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/linhaService", () => {
// 	const serv = {
// 		createLinha: jest.fn()
// 	  };
// 	  return { LinhaService: jest.fn(() => serv) };
// 	});


// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(linhaService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createLinha: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: silinhan.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };

// 	// 	const controller = new LinhaController(MockedServ.protolinha);

// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: silinhan.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.protolinha.createLinha(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: ILinhaDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.protolinha.createLinha.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "LinhaController", path: "../controllers/linhaController"};




// 	// it('createLinha: returns json with description value', async function () {
// 	// 	require('../../../app');

// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.linha.name) as ILinhaController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: silinhan.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };

// 	// 	 ctrl.createLinha(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: ILinhaDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});