import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import LinhaService from "../../../services/linhaService";
import ILinhaRepo from "../../../repos/IRepos/ILinhaRepo";
import ILinhaDTO from '../../../dto/ILinhaDTO';
import { Linha } from '../../../domain/linha/linha';
import { PercursoLinha } from '../../../domain/linha/percursoLinha';


describe('tipo tripulante service', function () {
	beforeEach(function () {
	});


	let linhaSchemaClass = require("../../../persistence/schemas/linhaSchema").default;
	Container.set('linhaSchema', linhaSchemaClass);

	let linhaRepoClass = require("../../../repos/linhaRepo").default;
	let linhaRepoInstance = Container.get(linhaRepoClass);
	Container.set(config.repos.linha.name, linhaRepoInstance);


	it('createLinha: returns json with description value', async function () {

		const bodyT = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT
		} as ILinhaDTO;
		const resu = Linha.create(body).getValue();

		linhaRepoInstance = Container.get(config.repos.linha.name);
		sinon.stub(linhaRepoInstance, "save").returns(Result.ok<Linha>(resu));

		const serv = new LinhaService(linhaRepoInstance as ILinhaRepo);


		const res = await (await serv.createLinha(body)).getValue();

		sinon.assert.match(res, body);
	});


	it('listLinhas: returns json with array of types', async function () {

		const bodyT = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT
		} as ILinhaDTO;

		const bodyT2 = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body2 = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT2
		} as ILinhaDTO;

		const bodyT3 = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body3 = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT3
		} as ILinhaDTO;

		const resu = Linha.create(body).getValue();
		const resu2 = Linha.create(body2).getValue();
		const resu3 = Linha.create(body3).getValue();


		linhaRepoInstance = Container.get(config.repos.linha.name);
		sinon.stub(linhaRepoInstance, "findAll").returns([resu, resu2, resu3]);

		const serv = new LinhaService(linhaRepoInstance as ILinhaRepo);

		serv.createLinha(body);
		serv.createLinha(body2);
		serv.createLinha(body3);
		const res = await (await serv.listLinhas()).getValue();

		sinon.assert.match(res, [body, body2, body3]);
	});

	it('listLinhas: returns json with sorted array of types', async function () {

		const bodyT = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT
		} as ILinhaDTO;

		const bodyT2 = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body2 = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT2
		} as ILinhaDTO;

		const bodyT3 = [PercursoLinha.create({"percursoLinhaId": "71123","percurso": "a","orientation": "b"}).getValue(),
		PercursoLinha.create({"percursoLinhaId": "1234","percurso": "a4","orientation": "b4"}).getValue()];

		const body3 = {
			"domainId": "nl:7512341ee",
			"nome": "newlineTest7532341",
			"cor": "rrr",
			"percursoLinha": bodyT3
		} as ILinhaDTO;
		
		const resu = Linha.create(body).getValue();
		const resu2 = Linha.create(body2).getValue();
		const resu3 = Linha.create(body3).getValue();


		linhaRepoInstance = Container.get(config.repos.linha.name);
		sinon.stub(linhaRepoInstance, "findAllSorted").returns([resu2, resu, resu3]);

		const serv = new LinhaService(linhaRepoInstance as ILinhaRepo);

		serv.createLinha(body);
		serv.createLinha(body2);
		serv.createLinha(body3);
		const res = await (await serv.listLinhasSorted("desc")).getValue();

		sinon.assert.match(res, [body2, body, body3]);
	});

});



// import * as sinon from 'sinon';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'typedi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import ILinhaService from "../../../services/IServices/ILinhaService";
// import linhaService from "../../../services/linhaService";
// import linhaRepo from "../../../repos/linhaRepo";
// import LinhaController from "../../../controllers/linhaController";
// import ILinhaController from "../../../controllers/IControllers/ILinhaController";
// import ILinhaDTO from '../../../dto/ILinhaDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/linhaService", () => {
// 	const serv = {
// 		createLinha: jest.fn()
// 	  };
// 	  return { LinhaService: jest.fn(() => serv) };
// 	});


// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(linhaService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createLinha: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };

// 	// 	const controller = new LinhaController(MockedServ.prototype);

// 	// 	console.log(controller.listLinhas(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.prototype.createLinha(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: ILinhaDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createLinha.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "LinhaController", path: "../controllers/linhaController"};




// 	// it('createLinha: returns json with description value', async function () {
// 	// 	require('../../../app');

// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.linha.name) as ILinhaController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };

// 	// 	 ctrl.createLinha(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: ILinhaDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});