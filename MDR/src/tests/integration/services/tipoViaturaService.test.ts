import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import TipoViaturaService from "../../../services/tipoViaturaService";
import ITipoViaturaRepo from "../../../repos/IRepos/ITipoViaturaRepo";
import ITipoViaturaDTO from '../../../dto/ITipoViaturaDTO';
import {TipoViatura} from '../../../domain/tipoViatura/tipoViatura';


describe('tipo tripulante service', function () {
	beforeEach(function() {
	});
	

	let tipoViaturaSchemaClass = require("../../../persistence/schemas/tipoViaturaSchema").default;
	Container.set('tipoViaturaSchema', tipoViaturaSchemaClass);
	
	let tipoViaturaRepoClass = require("../../../repos/tipoViaturaRepo").default;
	let tipoViaturaRepoInstance = Container.get(tipoViaturaRepoClass);
	Container.set(config.repos.tipoViatura.name, tipoViaturaRepoInstance);
	

    it('createTipoViatura: returns json with description value', async function () {

		const body = {
			"domainId": "v:123",
			"nome": "civiat123",
			"autonomia": 151,
			"custo": 23,
			"velocidadeMedia": 100,
			"combustivel": 23,
			"consumo": 120,
			"emissoes": 330
		} as ITipoViaturaDTO;
		const resu = TipoViatura.create(body).getValue();

		tipoViaturaRepoInstance = Container.get(config.repos.tipoViatura.name);
		sinon.stub(tipoViaturaRepoInstance, "save").returns( Result.ok<TipoViatura>(resu));

		const serv = new TipoViaturaService(tipoViaturaRepoInstance as ITipoViaturaRepo);


		const res = await (await serv.createTipoViatura(body)).getValue();

		sinon.assert.match(res, body);
	});

	
	it('listTipoViaturas: returns json with array of types', async function () {
		
		const body = {
			"domainId": "v:123",
			"nome": "civiat123",
			"autonomia": 151,
			"custo": 23,
			"velocidadeMedia": 100,
			"combustivel": 23,
			"consumo": 120,
			"emissoes": 330
		} as ITipoViaturaDTO;
        const body2 = {
			"domainId": "v:125",
			"nome": "biviat125",
			"autonomia": 151,
			"custo": 23,
			"velocidadeMedia": 100,
			"combustivel": 23,
			"consumo": 120,
			"emissoes": 330
		} as ITipoViaturaDTO;
        const body3 = {
			"domainId": "v:125",
			"nome": "aiviat125",
			"autonomia": 151,
			"custo": 23,
			"velocidadeMedia": 100,
			"combustivel": 23,
			"consumo": 120,
			"emissoes": 330
		} as ITipoViaturaDTO;
		const resu = TipoViatura.create(body).getValue();
		const resu2 = TipoViatura.create(body2).getValue();
		const resu3 = TipoViatura.create(body3).getValue();

		
		tipoViaturaRepoInstance = Container.get(config.repos.tipoViatura.name);
		sinon.stub(tipoViaturaRepoInstance, "findAll").returns( [resu, resu2, resu3]);

		const serv = new TipoViaturaService(tipoViaturaRepoInstance as ITipoViaturaRepo);

		serv.createTipoViatura(body);
		serv.createTipoViatura(body2);
		serv.createTipoViatura(body3);
		const res = await (await serv.listTiposViatura()).getValue();

		sinon.assert.match(res, [body, body2, body3]);
	});

	it('listTipoViaturas: returns json with sorted array of types', async function () {
		

        const body = {
			"domainId": "v:123",
			"nome": "civiat123",
			"autonomia": 151,
			"custo": 23,
			"velocidadeMedia": 100,
			"combustivel": 23,
			"consumo": 120,
			"emissoes": 330
		} as ITipoViaturaDTO;
        const body2 = {
			"domainId": "v:125",
			"nome": "biviat125",
			"autonomia": 151,
			"custo": 23,
			"velocidadeMedia": 100,
			"combustivel": 23,
			"consumo": 120,
			"emissoes": 330
		} as ITipoViaturaDTO;
        const body3 = {
			"domainId": "v:125",
			"nome": "aiviat125",
			"autonomia": 151,
			"custo": 23,
			"velocidadeMedia": 100,
			"combustivel": 23,
			"consumo": 120,
			"emissoes": 330
		} as ITipoViaturaDTO;
		const resu = TipoViatura.create(body).getValue();
		const resu2 = TipoViatura.create(body2).getValue();
		const resu3 = TipoViatura.create(body3).getValue();

		
		tipoViaturaRepoInstance = Container.get(config.repos.tipoViatura.name);
		sinon.stub(tipoViaturaRepoInstance, "findAllSorted").returns( [resu3, resu2, resu]);

		const serv = new TipoViaturaService(tipoViaturaRepoInstance as ITipoViaturaRepo);

		serv.createTipoViatura(body);
		serv.createTipoViatura(body2);
		serv.createTipoViatura(body3);
		const res = await (await serv.listTiposViaturaSorted("desc")).getValue();

		sinon.assert.match(res, [body3, body2, body]);
	});

});



// import * as sinon from 'sinon';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'typedi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import ITipoViaturaService from "../../../services/IServices/ITipoViaturaService";
// import tipoViaturaService from "../../../services/tipoViaturaService";
// import tipoViaturaRepo from "../../../repos/tipoViaturaRepo";
// import TipoViaturaController from "../../../controllers/tipoViaturaController";
// import ITipoViaturaController from "../../../controllers/IControllers/ITipoViaturaController";
// import ITipoViaturaDTO from '../../../dto/ITipoViaturaDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/tipoViaturaService", () => {
// 	const serv = {
// 		createTipoViatura: jest.fn()
// 	  };
// 	  return { TipoViaturaService: jest.fn(() => serv) };
// 	});
	

// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(tipoViaturaService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createTipoViatura: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
		
// 	// 	const controller = new TipoViaturaController(MockedServ.prototype);

// 	// 	console.log(controller.listTiposViatura(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.prototype.createTipoViatura(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: ITipoViaturaDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createTipoViatura.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "TipoViaturaController", path: "../controllers/tipoViaturaController"};

	

	
// 	// it('createTipoViatura: returns json with description value', async function () {
// 	// 	require('../../../app');
		
// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.tipoViatura.name) as ITipoViaturaController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
		 
// 	// 	 ctrl.createTipoViatura(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: ITipoViaturaDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});