import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import NoService from "../../../services/noService";
import INoRepo from "../../../repos/IRepos/INoRepo";
import INoDTO from '../../../dto/INoDTO';
import {No} from '../../../domain/no/no';
import { TempoViagemGrupo } from '../../../domain/no/tempoViagemGrupo';


describe('tipo tripulante service', function () {
	beforeEach(function() {
	});
	

	let noSchemaClass = require("../../../persistence/schemas/noSchema").default;
	Container.set('noSchema', noSchemaClass);
	
	let noRepoClass = require("../../../repos/noRepo").default;
	let noRepoInstance = Container.get(noRepoClass);
	Container.set(config.repos.no.name, noRepoInstance);
	

    it('createNo: returns json with description value', async function () {

		const bodyT = TempoViagemGrupo.create({ 
			"noId": "no1",
			"duracao": 123
		});

        const body = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT.getValue()]
		} as INoDTO;


		const resu = No.create(body).getValue();

		noRepoInstance = Container.get(config.repos.no.name);
		sinon.stub(noRepoInstance, "save").returns( Result.ok<No>(resu));

		const serv = new NoService(noRepoInstance as INoRepo);


		const res = await (await serv.createNo(body)).getValue();

		sinon.assert.match(res, body);
	});

	
	it('listNos: returns json with array of types', async function () {

		const bodyT = TempoViagemGrupo.create({ 
			"noId": "no1",
			"duracao": 123
		});

        const body = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT.getValue()]
		} as INoDTO;

		const bodyT2 = TempoViagemGrupo.create({ 
			"noId": "no2",
			"duracao": 123
		});

        const body2 = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT2.getValue()]
		} as INoDTO;

		const bodyT3 = TempoViagemGrupo.create({ 
			"noId": "no1",
			"duracao": 123
		});

        const body3 = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT3.getValue()]
		} as INoDTO;

		const resu = No.create(body).getValue();
		const resu2 = No.create(body2).getValue();
		const resu3 = No.create(body3).getValue();

		
		noRepoInstance = Container.get(config.repos.no.name);
		sinon.stub(noRepoInstance, "findAll").returns( [resu, resu2, resu3]);

		const serv = new NoService(noRepoInstance as INoRepo);

		serv.createNo(body);
		serv.createNo(body2);
		serv.createNo(body3);
		const res = await (await serv.listNos()).getValue();

		sinon.assert.match(res, [body, body2, body3]);
	});

	it('listNos: returns json with sorted array of types', async function () {
		
		const bodyT = TempoViagemGrupo.create({ 
			"noId": "no1",
			"duracao": 123
		});

        const body = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT.getValue()]
		} as INoDTO;

		const bodyT2 = TempoViagemGrupo.create({ 
			"noId": "no2",
			"duracao": 123
		});

        const body2 = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT2.getValue()]
		} as INoDTO;

		const bodyT3 = TempoViagemGrupo.create({ 
			"noId": "no1",
			"duracao": 123
		});

        const body3 = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT3.getValue()]
		} as INoDTO;


		const resu = No.create(body).getValue();
		const resu2 = No.create(body2).getValue();
		const resu3 = No.create(body3).getValue();

		
		noRepoInstance = Container.get(config.repos.no.name);
		sinon.stub(noRepoInstance, "findAllSorted").returns( [resu2, resu, resu3]);

		const serv = new NoService(noRepoInstance as INoRepo);

		serv.createNo(body);
		serv.createNo(body2);
		serv.createNo(body3);
		const res = await (await serv.listNosSorted("desc")).getValue();

		sinon.assert.match(res, [body2, body, body3]);
	});

});



// import * as sinon from 'sinon';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'typedi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import INoService from "../../../services/IServices/INoService";
// import noService from "../../../services/noService";
// import noRepo from "../../../repos/noRepo";
// import NoController from "../../../controllers/noController";
// import INoController from "../../../controllers/IControllers/INoController";
// import INoDTO from '../../../dto/INoDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/noService", () => {
// 	const serv = {
// 		createNo: jest.fn()
// 	  };
// 	  return { NoService: jest.fn(() => serv) };
// 	});
	

// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(noService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createNo: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
		
// 	// 	const controller = new NoController(MockedServ.prototype);

// 	// 	console.log(controller.listNos(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.prototype.createNo(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: INoDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createNo.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "NoController", path: "../controllers/noController"};

	

	
// 	// it('createNo: returns json with description value', async function () {
// 	// 	require('../../../app');
		
// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.no.name) as INoController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
		 
// 	// 	 ctrl.createNo(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: INoDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});