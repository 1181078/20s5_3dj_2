import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import NoService from "../../../services/noService";
import INoRepo from "../../../repos/IRepos/INoRepo";
import NoRepo from "../../../repos/noRepo";
import INoDTO from '../../../dto/INoDTO';
import {No} from '../../../domain/no/no';
import NoSchema from '../../../persistence/schemas/noSchema';
import { INoPersistence } from '../../../dataschema/INoPersistence';
import { Document, Model } from 'mongoose';
import mongoose from 'mongoose';
import { TempoViagemGrupo } from '../../../domain/no/tempoViagemGrupo';



describe('tipo tripulante repo', function () {
	beforeEach(function() {
	});
	

	let noSchemaClass = require("../../../persistence/schemas/noSchema").default;
	Container.set('noSchema', noSchemaClass);
		

    it('save: returns json with description value', async function () {
		
		const bodyT = TempoViagemGrupo.create({ 
			"noId": "no1",
			"duracao": 123
		});

        const body = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT.getValue()]
		} as INoDTO;

		const bodyb = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT.getValue()]
		};
		const resu = No.create(body).getValue();

		const newS: Model<INoPersistence  & Document> = Container.get("noSchema");
		sinon.stub(newS, "findOne").returns( Result.ok<INoPersistence>(bodyb));
		sinon.stub(newS, "create").returns( Result.ok<No>(resu));

		const repo = new NoRepo(newS);

		
		const res = await repo.save(resu);

		sinon.assert.match(res, resu);
	});

	
	it('listNos: returns json with array of types', async function () {
		

        const bodyT = TempoViagemGrupo.create({ 
			"noId": "no1",
			"duracao": 123
		});

        const body = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT.getValue()]
		} as INoDTO;

		const bodyb = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT.getValue()]
		};

		const bodyT2 = TempoViagemGrupo.create({ 
			"noId": "no2",
			"duracao": 123
		});

        const body2 = {
			"domainId": "no2",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT2.getValue()]
		} as INoDTO;

		const body2b = {
			"domainId": "no2",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT2.getValue()]
		} as INoDTO;

		const bodyT3 = TempoViagemGrupo.create({ 
			"noId": "no1",
			"duracao": 123
		});

        const body3 = {
			"domainId": "no3",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT3.getValue()]
		} as INoDTO;

		
        const body3b = {
			"domainId": "no3",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT3.getValue()]
		} as INoDTO;

		const resu = No.create(body).getValue();
		const resu2 = No.create(body2).getValue();
		const resu3 = No.create(body3).getValue();

		const arr = [bodyb, body2b, body3b] as unknown as INoPersistence;
		const resf = [resu, resu2, resu3] as No[];

		const newS: Model<INoPersistence  & Document> = Container.get("noSchema");
		sinon.stub(newS, "find").returns( Result.ok<INoPersistence>(arr));

		const repo = new NoRepo(newS);
		
		const res = await repo.findAll();

		sinon.assert.match(resf, resf);
	});


	it('listNos: returns json with sorted array of types', async function () {
		

        const bodyT = TempoViagemGrupo.create({ 
			"noId": "no1",
			"duracao": 123
		});

        const body = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT.getValue()]
		} as INoDTO;

		const bodyb = {
			"domainId": "no1",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT.getValue()]
		};

		const bodyT2 = TempoViagemGrupo.create({ 
			"noId": "no2",
			"duracao": 123
		});

        const body2 = {
			"domainId": "no2",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT2.getValue()]
		} as INoDTO;

		const body2b = {
			"domainId": "no2",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT2.getValue()]
		} as INoDTO;

		const bodyT3 = TempoViagemGrupo.create({ 
			"noId": "no1",
			"duracao": 123
		});

        const body3 = {
			"domainId": "no3",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT3.getValue()]
		} as INoDTO;

		
        const body3b = {
			"domainId": "no3",
			"nome": "ano8",
			"latitude": 40,
			"longitude": 64,
			"nomeCurto": "gome",
			"isEstacaoRecolha": true,
			"isPontoRendicao": false,
			"tempoViagem" : [bodyT3.getValue()]
		} as INoDTO;

		const resu = No.create(body).getValue();
		const resu2 = No.create(body2).getValue();
		const resu3 = No.create(body3).getValue();

		const arr = [bodyb, body2b, body3b] as unknown as INoPersistence;
		const resf = [resu2, resu3, resu] as No[];

		const newS: Model<INoPersistence  & Document> = Container.get("noSchema");
		//sinon.stub(newS, "findAllSorted").returns( Result.ok<INoPersistence>(arr));

		const repo = new NoRepo(newS);
		
		const res = await repo.findAll();

		sinon.assert.match(resf, resf);
	});


});



// import * as sinon from 'sinon';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'typedi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import INoService from "../../../services/IServices/INoService";
// import noService from "../../../services/noService";
// import noRepo from "../../../repos/noRepo";
// import NoController from "../../../controllers/noController";
// import INoController from "../../../controllers/IControllers/INoController";
// import INoDTO from '../../../dto/INoDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/noService", () => {
// 	const serv = {
// 		createNo: jest.fn()
// 	  };
// 	  return { NoService: jest.fn(() => serv) };
// 	});
	

// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(noService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createNo: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
		
// 	// 	const controller = new NoController(MockedServ.prototype);

// 	// 	console.log(controller.listNos(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.prototype.createNo(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: INoDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createNo.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "NoController", path: "../controllers/noController"};

	

	
// 	// it('createNo: returns json with description value', async function () {
// 	// 	require('../../../app');
		
// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.no.name) as INoController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
		 
// 	// 	 ctrl.createNo(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: INoDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});