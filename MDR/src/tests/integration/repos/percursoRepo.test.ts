import * as sinon from 'sinon';

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from "../../../../config";


import { Result } from '../../../core/logic/Result';

import PercursoService from "../../../services/percursoService";
import IPercursoRepo from "../../../repos/IRepos/IPercursoRepo";
import PercursoRepo from "../../../repos/percursoRepo";
import IPercursoDTO from '../../../dto/IPercursoDTO';
import {Percurso} from '../../../domain/percurso/percurso';
import PercursoSchema from '../../../persistence/schemas/percursoSchema';
import { IPercursoPersistence } from '../../../dataschema/IPercursoPersistence';
import { Document, Model } from 'mongoose';
import mongoose from 'mongoose';
import { NosPercurso } from '../../../domain/percurso/nosPercurso';



describe('tipo tripulante repo', function () {
	beforeEach(function() {
	});
	

	let percursoSchemaClass = require("../../../persistence/schemas/percursoSchema").default;
	Container.set('percursoSchema', percursoSchemaClass);
		

    it('save: returns json with description value', async function () {
		
		const bodyT = [NosPercurso.create({noId: "no1", duracao: 34, distancia: 45}).getValue(), NosPercurso.create({noId: "no7", duracao: 34, distancia: 45}).getValue()];
		
		const body = {
			"domainId": "Path:1",
			"nosPercurso": bodyT
		} as IPercursoDTO;

		const body2 = {
			"domainId": "Path:1",
			"nosPercurso": bodyT
		};
		
		const resu = Percurso.create(body).getValue();

		const newS: Model<IPercursoPersistence  & Document> = Container.get("percursoSchema");
		sinon.stub(newS, "findOne").returns( Result.ok<IPercursoPersistence>(body2));
		sinon.stub(newS, "create").returns( Result.ok<Percurso>(resu));

		const repo = new PercursoRepo(newS);

		
		const res = await repo.save(resu);

		sinon.assert.match(res, resu);
	});

	
	it('listPercursos: returns json with array of types', async function () {
		

		const bodyT = [NosPercurso.create({noId: "no1", duracao: 34, distancia: 45}).getValue(), NosPercurso.create({noId: "no7", duracao: 34, distancia: 45}).getValue()];
		const bodyT2 = [NosPercurso.create({noId: "no2", duracao: 34, distancia: 45}).getValue()];
		const bodyT3 = [NosPercurso.create({noId: "no3", duracao: 34, distancia: 45}).getValue()];

		const body = {
			"domainId": "Path:1",
			"nosPercurso": bodyT
		} as IPercursoDTO;
		const body2 = {
			"domainId": "Path:2",
			"nosPercurso": bodyT2
		} as IPercursoDTO;
		const body3 = {
			"domainId": "Path:3",
			"nosPercurso": bodyT3
		} as IPercursoDTO;

		const bodyb = {
			"domainId": "Path:1",
			"nosPercurso": bodyT
		} as IPercursoPersistence;
		const body2b = {
			"domainId": "Path:2",
			"nosPercurso": bodyT2
		} as IPercursoPersistence;
		const body3b = {
			"domainId": "Path:3",
			"nosPercurso": bodyT3
		} as IPercursoPersistence;
		
		const resu = Percurso.create(body).getValue();
		const resu2 = Percurso.create(body2).getValue();
		const resu3 = Percurso.create(body3).getValue();

		const arr = [bodyb, body2b, body3b] as unknown as IPercursoPersistence;
		const resf = [resu, resu2, resu3] as Percurso[];

		const newS: Model<IPercursoPersistence  & Document> = Container.get("percursoSchema");
		sinon.stub(newS, "find").returns( Result.ok<IPercursoPersistence>(arr));

		const repo = new PercursoRepo(newS);
		
		const res = await repo.findAll();

		sinon.assert.match(resf, resf);
	});


	it('listPercursos: returns json with sorted array of types', async function () {
		

		const bodyT = [NosPercurso.create({noId: "no1", duracao: 34, distancia: 45}).getValue(), NosPercurso.create({noId: "no7", duracao: 34, distancia: 45}).getValue()];
		const bodyT2 = [NosPercurso.create({noId: "no2", duracao: 34, distancia: 45}).getValue()];
		const bodyT3 = [NosPercurso.create({noId: "no3", duracao: 34, distancia: 45}).getValue()];

		const body = {
			"domainId": "Path:1",
			"nosPercurso": bodyT
		} as IPercursoDTO;
		const body2 = {
			"domainId": "Path:2",
			"nosPercurso": bodyT2
		} as IPercursoDTO;
		const body3 = {
			"domainId": "Path:3",
			"nosPercurso": bodyT3
		} as IPercursoDTO;

		const bodyb = {
			"domainId": "Path:1",
			"nosPercurso": bodyT
		} as IPercursoPersistence;
		const body2b = {
			"domainId": "Path:2",
			"nosPercurso": bodyT2
		} as IPercursoPersistence;
		const body3b = {
			"domainId": "Path:3",
			"nosPercurso": bodyT3
		} as IPercursoPersistence;
		
		const resu = Percurso.create(body).getValue();
		const resu2 = Percurso.create(body2).getValue();
		const resu3 = Percurso.create(body3).getValue();

		const arr = [bodyb, body2b, body3b] as unknown as IPercursoPersistence;
		const resf = [resu2, resu3, resu] as Percurso[];

		const newS: Model<IPercursoPersistence  & Document> = Container.get("percursoSchema");
		//sinon.stub(newS, "findAllSorted").returns( Result.ok<IPercursoPersistence>(arr));

		const repo = new PercursoRepo(newS);
		
		const res = await repo.findAll();

		sinon.assert.match(resf, resf);
	});


});



// import * as sinon from 'sinon';

// import { mocked } from 'ts-jest/utils'

// import { Response, Request, NextFunction } from 'express';

// import { Container } from 'typedi';
// import config from "../../../../config";


// import { Result } from '../../../core/logic/Result';

// import IPercursoService from "../../../services/IServices/IPercursoService";
// import percursoService from "../../../services/percursoService";
// import percursoRepo from "../../../repos/percursoRepo";
// import PercursoController from "../../../controllers/percursoController";
// import IPercursoController from "../../../controllers/IControllers/IPercursoController";
// import IPercursoDTO from '../../../dto/IPercursoDTO';
// import { Model } from 'mongoose';
// import mongoose from '../../../loaders/mongoose';
// import express from 'express';


// jest.mock("../../../services/percursoService", () => {
// 	const serv = {
// 		createPercurso: jest.fn()
// 	  };
// 	  return { PercursoService: jest.fn(() => serv) };
// 	});
	

// describe('tipo tripulante controller', () => {

// 	// const MockedServ = mocked(percursoService, true);
// 	// ;

// 	// beforeEach(() => {
// 	// 	MockedServ.mockClear;

// 	// });


// 	// it('createPercurso: returns json with description value', async function () {

// 	// 	let body = { "desc": 'test_desc' };
// 	// 	let req: Partial<Request> = {};
// 	// 	req.body = body;

// 	// 	let res: Partial<Response> = {
// 	// 		json: sinon.spy()
// 	// 	};
// 	// 	let next: Partial<NextFunction> = () => { };
		
// 	// 	const controller = new PercursoController(MockedServ.prototype);

// 	// 	console.log(controller.listTiposTripulante(<Request>req, <Response>res, <NextFunction>next));
// 	// 	expect(MockedServ).toHaveBeenCalledTimes(1);

// 	// 	// let body = { "desc": 'test_desc' };
// 	// 	// let req: Partial<Request> = {};
// 	// 	// req.body = body;

// 	// 	// let res: Partial<Response> = {
// 	// 	// 	json: sinon.spy()
// 	// 	// };
// 	// 	// let next: Partial<NextFunction> = () => { };

// 	// 	// mockedFoo.prototype.createPercurso(<Request>req, <Response>res, <NextFunction>next);

// 	// 	// const exp: IPercursoDTO = ({ "domainId": "123", "desc": req.body.desc });

// 	// 	// console.log(res.json);		
// 	// 	// console.log(exp);		
// 	// 	// expect(mockedFoo.prototype.createPercurso.mock.calls).toHaveLength(1);

// 	//});

// 	// const m = {name: "PercursoController", path: "../controllers/percursoController"};

	

	
// 	// it('createPercurso: returns json with description value', async function () {
// 	// 	require('../../../app');
		
// 	// 	console.log(Container);

// 	// 	const ctrl = Container.get(config.controller.percurso.name) as IPercursoController;

// 	// 	let body = { "desc": 'test_desc' };
// 	//  	let req: Partial<Request> = {};
// 	//  	req.body = body;

// 	//  	let res: Partial<Response> = {
// 	//  		json: sinon.spy()
// 	//  	};
// 	// 	 let next: Partial<NextFunction> = () => { };
		 
// 	// 	 ctrl.createPercurso(<Request>req, <Response>res, <NextFunction>next);

// 	// 	 const exp: IPercursoDTO = ({ "domainId": "123", "desc": req.body.desc });
// 	// 	 expect(res.json).toEqual(exp);



// 	// }


// 	// 	);});