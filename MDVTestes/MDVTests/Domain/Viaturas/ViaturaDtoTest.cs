using System;
using MDV1.Domain.Viaturas;
using Xunit;

namespace MDVTestes.MDVTests.Domain.Viaturas{

    public class ViaturaDtoTest{

           
          public string Id= "newDTOViatura";
          public string matricula= "76-89-ZR";
          public string VIN= "VIN";
          public string tipoViatura= "carro";
          public string dataEntrada= "20/12/2020";


        [Fact]
        public void ViaturaDtoConstrutor()
        {
         
            
            var viaturaDTO = new ViaturaDto{Id = this.Id, matricula = this.matricula, VIN = this.VIN, tipoViatura = this.tipoViatura,
            dataEntrada = this.dataEntrada};

            Assert.NotNull(viaturaDTO);
            Assert.Equal(viaturaDTO.Id, this.Id);
            Assert.Equal(viaturaDTO.matricula, this.matricula);
            Assert.Equal(viaturaDTO.VIN,this.VIN);
            Assert.Equal(viaturaDTO.tipoViatura,this.tipoViatura);
            Assert.Equal(viaturaDTO.dataEntrada,this.dataEntrada);        
        }

        [Fact]
        public void ViaturaDtoConstrutorSets()
        {
         
            
            var viaturaDTO = new ViaturaDto{Id = this.Id, matricula = this.matricula, VIN = this.VIN, tipoViatura = this.tipoViatura,
            dataEntrada = this.dataEntrada};

            viaturaDTO.matricula = "AA-AA-AA";

            viaturaDTO.VIN = "NewVIN";

            viaturaDTO.tipoViatura = "autocarro";

            viaturaDTO.dataEntrada = "01/01/01";

            Assert.Equal(viaturaDTO.matricula, "AA-AA-AA");
            Assert.Equal(viaturaDTO.VIN,"NewVIN");
            Assert.Equal(viaturaDTO.tipoViatura,"autocarro");
            Assert.Equal(viaturaDTO.dataEntrada,"01/01/01");        
        }
       
    }
}