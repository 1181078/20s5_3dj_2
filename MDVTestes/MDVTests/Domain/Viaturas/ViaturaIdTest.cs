using System;
using MDV1.Domain.Viaturas;
using Xunit;

namespace MDVTestes.MDVTests.Domain.Viaturas{

    public class ViaturaIdTest{

               
          public string IdValue= "76-89-ZR";

        [Fact]
        public void ViaturaIdConstrutorGetsSets()
        {
                     
            var viaturaId = new ViaturaId(this.IdValue);

            Assert.NotNull(viaturaId);  
        }

        [Fact]
        public void AsStringTest(){

            var viaturaId = new ViaturaId(this.IdValue);

            string val; 
            val = viaturaId.AsString();
            Assert.Equal(this.IdValue,val);

        }
       
    }
}