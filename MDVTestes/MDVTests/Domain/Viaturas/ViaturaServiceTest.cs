
using MDV1.Domain.Viaturas;
using MDV1.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace MDVTestes.MDVTests.Domain.Viaturas{

    public class ViaturaServiceTest{

           
          public string matricula= "76-89-ZR";
          public string VIN= "VIN";
          public string tipoViatura= "carro";
          public string dataEntrada= "20/12/2020";

          public string matricula2= "90-12-MS";
          public string VIN2= "NIV";
          public string tipoViatura2= "mota";
          public string dataEntrada2= "23/01/2018";


        [Fact]
        public void ViaturaServiceConstrutor()
        {

            var mockViaturaRepo = new Mock<IViaturaRepository>();
            var mockUnitRepo = new Mock<IUnitOfWork>();
            
            var service = new ViaturaService(mockUnitRepo.Object, mockViaturaRepo.Object);

            Assert.NotNull(service);    
        }

        [Fact]
        public async void GetAllAsyncTest(){

            var mockViaturaRepo = new Mock<IViaturaRepository>();
            mockViaturaRepo.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(GetViaturas());
            var mockUnitRepo = new Mock<IUnitOfWork>();
            
            var service = new ViaturaService(mockUnitRepo.Object, mockViaturaRepo.Object);

            var viats = await service.GetAllAsync();

            var viatsDto = GetViaturasDto();

            Assert.Equal(viatsDto.Count(),viats.Count());
            Assert.Equal(viatsDto,viats);

        }

        [Fact]
        public async void GetByIdAsyncTest(){

            var id = new ViaturaId(this.matricula);
            var v1 = new Viatura(this.matricula,this.VIN,this.tipoViatura, this.dataEntrada);
            var v2 = new ViaturaDto{ Id = id.AsString(), matricula =  this.matricula, VIN = this.VIN,  tipoViatura = this.tipoViatura,  dataEntrada = this.dataEntrada};
            
            var mockViaturaRepo = new Mock<IViaturaRepository>();
            mockViaturaRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(v1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            
            var service = new ViaturaService(mockUnitRepo.Object, mockViaturaRepo.Object);

            var viat = await service.GetByIdAsync(id);

            Assert.Equal(viat,v2);

        }

        
        [Fact]
        public async void AddAsyncTest(){

            var id = new ViaturaId(this.matricula);
            var v1 = new Viatura(this.matricula,this.VIN,this.tipoViatura, this.dataEntrada);
            var v2 = new ViaturaDto{ Id = id.AsString(), matricula =  this.matricula, VIN = this.VIN,  tipoViatura = this.tipoViatura,  dataEntrada = this.dataEntrada};
            
            var mockViaturaRepo = new Mock<IViaturaRepository>();
            mockViaturaRepo.Setup(repo => repo.AddAsync(v1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());
            
            var service = new ViaturaService(mockUnitRepo.Object, mockViaturaRepo.Object);

            var viat = await service.AddAsync(v2);

            Assert.Equal(viat,v2);

        }


        [Fact]
        public async void UpdateAsyncTest(){

            var id = new ViaturaId(this.matricula);
            var v1 = new Viatura(this.matricula,this.VIN,this.tipoViatura, this.dataEntrada);
            var v2 = new ViaturaDto{ Id = id.AsString(), matricula =  this.matricula, VIN = this.VIN,  tipoViatura = this.tipoViatura,  dataEntrada = this.dataEntrada};
            var v2Alt = new ViaturaDto{ Id = id.AsString(), matricula =  this.matricula, VIN = "999",  tipoViatura = "newTipo",  dataEntrada = "00/00/00"};
            
            var mockViaturaRepo = new Mock<IViaturaRepository>();
            mockViaturaRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(v1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());
            
            var service = new ViaturaService(mockUnitRepo.Object, mockViaturaRepo.Object);

            var viat = await service.UpdateAsync(v2Alt);

            Assert.Equal(viat,v2Alt);

        }

        [Fact]
        public async void DeleteAsyncTest(){

            var id = new ViaturaId(this.matricula);
            var v1 = new Viatura(this.matricula,this.VIN,this.tipoViatura, this.dataEntrada);
            var v2 = new ViaturaDto{ Id = id.AsString(), matricula =  this.matricula, VIN = this.VIN,  tipoViatura = this.tipoViatura,  dataEntrada = this.dataEntrada};
            
            var mockViaturaRepo = new Mock<IViaturaRepository>();
            mockViaturaRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(v1);
            mockViaturaRepo.Setup(repo => repo.Remove(v1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());
            
            var service = new ViaturaService(mockUnitRepo.Object, mockViaturaRepo.Object);

            var viat = await service.DeleteAsync(id);

            Assert.Equal(viat,v2);

        }



        // [Fact]
        // public void MudarDataTest(){

        //     var viatura = new Viatura(this.matricula,this.VIN,this.tipoViatura,
        //     this.dataEntrada);

        //     string mudaData = "mudaData"; 
        //     var novaData = viatura.mudarData(mudaData);
        //     Assert.Equal(viatura.dataEntrada,mudaData);

        // }
       

       private List<Viatura> GetViaturas()
        {
            var viaturas = new List<Viatura>();
            viaturas.Add(new Viatura(this.matricula,this.VIN,this.tipoViatura, this.dataEntrada));
            viaturas.Add(new Viatura(this.matricula2,this.VIN2,this.tipoViatura2, this.dataEntrada2));
            
            return viaturas;
        }

        private List<ViaturaDto> GetViaturasDto()
        {
            List<ViaturaDto> viaturas = new List<ViaturaDto>();
            viaturas.Add(new ViaturaDto{ Id = new ViaturaId(this.matricula).AsString(), matricula = this.matricula, VIN = this.VIN,  tipoViatura = this.tipoViatura,  dataEntrada = this.dataEntrada});
            viaturas.Add(new ViaturaDto{ Id = new ViaturaId(this.matricula2).AsString(), matricula =  this.matricula2, VIN = this.VIN2,  tipoViatura = this.tipoViatura2,  dataEntrada = this.dataEntrada2});
            
            return viaturas;
        }
    }
}