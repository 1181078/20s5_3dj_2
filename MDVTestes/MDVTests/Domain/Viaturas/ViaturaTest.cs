using System;
using MDV1.Domain.Viaturas;
using Xunit;

namespace MDVTestes.MDVTests.Domain.Viaturas{

    public class ViaturaTest{

           
          public string matricula= "76-89-ZR";
          public string VIN= "VIN";
          public string tipoViatura= "carro";
          public string dataEntrada= "20/12/2020";


        [Fact]
        public void ViaturaConstrutorGetsSets()
        {
         
            
            var viatura = new Viatura(this.matricula,this.VIN,this.tipoViatura,
            this.dataEntrada);

            Assert.NotNull(viatura);
            Assert.Equal(viatura.matricula, this.matricula);
            Assert.Equal(viatura.VIN,this.VIN);
            Assert.Equal(viatura.tipoViatura,this.tipoViatura);
            Assert.Equal(viatura.dataEntrada,this.dataEntrada);        
        }

        [Fact]
        public void MudarTipoTest(){

            var viatura = new Viatura(this.matricula,this.VIN,this.tipoViatura,
            this.dataEntrada);

            string mudaTipo = "mudaTipo"; 
            viatura.mudarTipo(mudaTipo);
            Assert.Equal(viatura.tipoViatura,mudaTipo);

        }
        [Fact]
        public void MudarDataTest(){

            var viatura = new Viatura(this.matricula,this.VIN,this.tipoViatura,
            this.dataEntrada);

            string mudaData = "mudaData"; 
            var novaData = viatura.mudarData(mudaData);
            Assert.Equal(viatura.dataEntrada,mudaData);

        }
       
    }
}