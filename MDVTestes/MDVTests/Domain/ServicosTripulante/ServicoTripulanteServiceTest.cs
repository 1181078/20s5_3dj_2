using MDV1.Domain.ServicosTripulante;
using MDV1.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace MDVTestes.MDVTests.Domain.ServicosTripulante
{

    public class ServicoTripulanteServiceTest
    {



        public string domainId = "teste";
        public string nome = "teste";
        public string cor = "teste";
        public string[] blocks = { "teste", "teste" };

        public string domainId2 = "teste2";
        public string nome2 = "teste2";
        public string cor2 = "teste2";
        public string[] blocks2 = { "teste2", "teste2" };



        [Fact]
        public void ServicoTripulanteServiceConstrutor()
        {

            var mockRepo = new Mock<IServicoTripulanteRepository>();
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new ServicoTripulanteService(mockUnitRepo.Object, mockRepo.Object);

            Assert.NotNull(service);
        }

        [Fact]
        public async void GetAllAsyncTest()
        {

            var mockRepo = new Mock<IServicoTripulanteRepository>();
            mockRepo.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(GetST());
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new ServicoTripulanteService(mockUnitRepo.Object, mockRepo.Object);

            var st = await service.GetAllAsync();

            var Dto = GetSTDto();

            Assert.Equal(Dto.Count(), st.Count());
            

        }

        [Fact]
        public async void GetByIdAsyncTest()
        {

            var id = new ServicoTripulanteId(this.domainId);
            var st1 = new ServicoTripulante(
                this.domainId,
                this.nome,
                this.cor,
                this.blocks
            );
            var st2 = new ServicoTripulanteDto
            {
                Id = new ServicoTripulanteId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            };

            var mockRepo = new Mock<IServicoTripulanteRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(st1);
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new ServicoTripulanteService(mockUnitRepo.Object, mockRepo.Object);

            var serTri = await service.GetByIdAsync(id);

            Assert.True(serTri.Equal(st2));

        }


        [Fact]
        public async void AddAsyncTest()
        {

            var id = new ServicoTripulanteId(this.domainId);
            var st1 = new ServicoTripulante(
              this.domainId,
              this.nome,
              this.cor,
              this.blocks
          );
            var st2 = new ServicoTripulanteDto
            {
                Id = new ServicoTripulanteId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            };

            var mockRepo = new Mock<IServicoTripulanteRepository>();
            mockRepo.Setup(repo => repo.AddAsync(st1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new ServicoTripulanteService(mockUnitRepo.Object, mockRepo.Object);

            var serTri = await service.AddAsync(st2);

            Assert.True(serTri.Equal(st2));

        }

        [Fact]
        public async void UpdateAsyncTest()
        {

            var id = new ServicoTripulanteId(this.domainId);
            var st1 = new ServicoTripulante(
              this.domainId,
              this.nome,
              this.cor,
              this.blocks
          );
            var st2 = new ServicoTripulanteDto
            {
                Id = new ServicoTripulanteId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            };

            string[] novo = { "novo", "novo" };
            var st2Alt = new ServicoTripulanteDto
            {
                Id = new ServicoTripulanteId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome2,
                cor = this.cor2,
                blocks = this.blocks2
            };

            var mockRepo = new Mock<IServicoTripulanteRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(st1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new ServicoTripulanteService(mockUnitRepo.Object, mockRepo.Object);

            var serTri = await service.UpdateAsync(st2Alt);

            Assert.True(serTri.Equal(st2Alt));

        }

        [Fact]
        public async void DeleteAsyncTest()
        {

            var id = new ServicoTripulanteId(this.domainId);
            var st1 = new ServicoTripulante(
              this.domainId,
              this.nome,
              this.cor,
              this.blocks
          );
           var st2 = new ServicoTripulanteDto
            {
                Id = new ServicoTripulanteId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            };

            var mockRepo = new Mock<IServicoTripulanteRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(st1);
            mockRepo.Setup(repo => repo.Remove(st1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new ServicoTripulanteService(mockUnitRepo.Object, mockRepo.Object);

            var serTri = await service.DeleteAsync(id);

            Assert.True(serTri.Equal(st2));

        }

        private List<ServicoTripulante> GetST()
        {
            var st = new List<ServicoTripulante>();
            st.Add(new ServicoTripulante(
                this.domainId,
                this.nome,
                this.cor,
                this.blocks
                ));

            st.Add(new ServicoTripulante(
                this.domainId2,
                this.nome2,
                this.cor2,
                this.blocks2
                ));

            return st;
        }

        private List<ServicoTripulanteDto> GetSTDto()
        {
            List<ServicoTripulanteDto> st = new List<ServicoTripulanteDto>();
            st.Add(new ServicoTripulanteDto
            {
                Id = new ServicoTripulanteId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            });
            st.Add(new ServicoTripulanteDto
            {
                Id = new ServicoTripulanteId(this.domainId2).AsString(),
                domainId = this.domainId2,
                nome = this.nome2,
                cor = this.cor2,
                blocks = this.blocks2
            });
            return st;
        }
    }
}