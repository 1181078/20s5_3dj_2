using System;
using MDV1.Domain.ServicosTripulante;
using Xunit;

namespace MDVTestes.MDVTests.Domain.ServicosTripulante
{

    public class ServicoTripulanteTest
    {

        public string Id = "new";
        public string domainId = "teste";
        public string nome = "teste";
        public string cor = "teste";
        public string[] blocks = { "teste", "teste" };


        [Fact]
        public void ServicoTripulanteConstrutorGetsSets()
        {


            var st = new ServicoTripulante(
                 this.domainId,
                 this.nome,
                 this.cor,
                 this.blocks
                 );

            Assert.NotNull(st);
            Assert.Equal(st.domainId, this.domainId);
            Assert.Equal(st.nome, this.nome);
            Assert.Equal(st.cor, this.cor);
            Assert.Equal(st.blocks, this.blocks);
        }

        [Fact]
        public void Mudancas()
        {

            var st = new ServicoTripulante(
               this.domainId,
               this.nome,
               this.cor,
               this.blocks
               );

            string muda = "muda";
            string[] mudaB = { "muda", "muda", "muda" };
            st.mudaNome(muda);
            st.mudaCor(muda);
            st.mudaBlocks(mudaB);

            Assert.NotNull(st);
            Assert.Equal(muda, st.nome);
            Assert.Equal(muda, st.cor);
            Assert.Equal(mudaB, st.blocks);
        }
    }
}