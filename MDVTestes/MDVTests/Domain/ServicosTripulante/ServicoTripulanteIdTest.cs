using System;
using MDV1.Domain.ServicosTripulante;
using Xunit;

namespace MDVTestes.MDVTests.Domain.ServicosTripulante{

    public class ServicoTripulanteIdTest{

               
          public string IdValue= "teste";

        [Fact]
        public void ServicoTripulanteIdConstrutorGetsSets()
        {
                     
            var id = new ServicoTripulanteId(this.IdValue);

            Assert.NotNull(id);  
        }

        [Fact]
        public void AsStringTest(){

            var id = new ServicoTripulanteId(this.IdValue);

            string val; 
            val = id.AsString();
            Assert.Equal(this.IdValue,val);

        }
       
    }
}