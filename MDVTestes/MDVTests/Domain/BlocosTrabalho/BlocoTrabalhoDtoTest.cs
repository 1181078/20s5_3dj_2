using System;
using MDV1.Domain.BlocosTrabalho;
using Xunit;

namespace MDVTestes.MDVTests.Domain.BlocosTrabalho
{

    public class BlocoTrabalhoDtoTest
    {

        public string Id = "newDTOBlocoTrabalho";
        public string domainId = "teste";
        public int TempoInicio = 1;
        public int TempoFim = 2;
        public string NoInicio = "teste";
        public string NoFim = "teste";
        public string[] Viagens = { "teste", "teste" };

        [Fact]
        public void BlocoTrabalhoDtoConstructor()
        {
            var blocoDTO = new BlocoTrabalhoDto
            {
                Id = this.Id,
                domainId = this.domainId,
                TempoInicio = this.TempoInicio,
                TempoFim = this.TempoFim,
                NoInicio = this.NoInicio,
                NoFim = this.NoFim,
                Viagens = this.Viagens
            };
            Assert.NotNull(blocoDTO);
            Assert.Equal(blocoDTO.Id, this.Id);
            Assert.Equal(blocoDTO.domainId, this.domainId);
            Assert.Equal(blocoDTO.TempoInicio, this.TempoInicio);
            Assert.Equal(blocoDTO.TempoFim, this.TempoFim);
            Assert.Equal(blocoDTO.NoInicio, this.NoInicio);
            Assert.Equal(blocoDTO.NoFim, this.NoFim);
            Assert.Equal(blocoDTO.Viagens, this.Viagens);
        }
        [Fact]
        public void Sets()
        {
            var blocoDTO = new BlocoTrabalhoDto
            {
                Id = this.Id,
                domainId = this.domainId,
                TempoInicio = this.TempoInicio,
                TempoFim = this.TempoFim,
                NoInicio = this.NoInicio,
                NoFim = this.NoFim,
                Viagens = this.Viagens
            };

            string nova = "nova";
            string[] novoA = { nova, nova, nova };


            blocoDTO.domainId = nova;
            blocoDTO.TempoInicio = 2;
            blocoDTO.TempoFim = 3;
            blocoDTO.NoInicio = nova;
            blocoDTO.NoFim = nova;
            blocoDTO.Viagens = novoA;
            Assert.Equal( nova,blocoDTO.domainId);
            Assert.Equal(2,blocoDTO.TempoInicio);
            Assert.Equal(3,blocoDTO.TempoFim);
            Assert.Equal(nova,blocoDTO.NoInicio);
            Assert.Equal(nova,blocoDTO.NoFim);
            Assert.Equal(novoA,blocoDTO.Viagens);

        }
    }
}
