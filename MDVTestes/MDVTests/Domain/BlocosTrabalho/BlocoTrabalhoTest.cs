using System;
using MDV1.Domain.BlocosTrabalho;
using Xunit;

namespace MDVTestes.MDVTests.Domain.BlocosTrabalho
{

    public class BlocoTrabalhoTest
    {


        public string domainId = "teste";
        public int TempoInicio = 1;
        public int TempoFim = 2;
        public string NoInicio = "teste";
        public string NoFim = "teste";
        public string[] Viagens = { "teste", "teste" };

        [Fact]
        public void ViaturaConstrutorGetsSets()
        {


            var b1 = new BlocoTrabalho(
                 this.domainId,
                 this.TempoInicio,
                 this.TempoFim,
                 this.NoInicio,
                 this.NoFim,
                 this.Viagens
                 );

            Assert.NotNull(b1);
            Assert.Equal(b1.domainId, this.domainId);
            Assert.Equal(b1.TempoInicio, this.TempoInicio);
            Assert.Equal(b1.TempoFim, this.TempoFim);
            Assert.Equal(b1.NoInicio, this.NoInicio);
            Assert.Equal(b1.NoFim, this.NoFim);
            Assert.Equal(b1.Viagens, this.Viagens);
        }

        [Fact]
        public void Mudancas()
        {

            var b1 = new BlocoTrabalho(
               this.domainId,
               this.TempoInicio,
               this.TempoFim,
               this.NoInicio,
               this.NoFim,
               this.Viagens
               );

            string muda = "muda";
            string[] mudaV = { "muda", "muda", "muda"};
            b1.mudarTempoInicio(40);
            b1.mudarTempoFim(50);
            b1.mudarNoInicio(muda);
            b1.mudarNoFim(muda);
            b1.mudarViagens(mudaV);

            Assert.NotNull(b1);
            Assert.Equal(40,b1.TempoInicio);
            Assert.Equal(50,b1.TempoFim);
            Assert.Equal(muda,b1.NoInicio);
            Assert.Equal(muda,b1.NoFim);
            Assert.Equal(mudaV,b1.Viagens);
        }


    }
}
