using System;
using MDV1.Domain.BlocosTrabalho;
using Xunit;

namespace MDVTestes.MDVTests.Domain.BlocosTrabalho{

    public class BlocoTrabalhoIdTest{

               
          public string IdValue= "teste";

        [Fact]
        public void BlocoTrabalhoIdConstrutorGetsSets()
        {
                     
            var Id = new BlocoTrabalhoId(this.IdValue);

            Assert.NotNull(Id);  
        }

        [Fact]
        public void AsStringTest(){

            var Id = new BlocoTrabalhoId(this.IdValue);

            string val; 
            val = Id.AsString();
            Assert.Equal(this.IdValue,val);

        }
       
    }
}