using MDV1.Domain.BlocosTrabalho;
using MDV1.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace MDVTestes.MDVTests.Domain.BlocosTrabalho
{

    public class BlocoTrabalhoServiceTest
    {



        public string domainId = "teste";
        public int TempoInicio = 1;
        public int TempoFim = 2;
        public string NoInicio = "teste";
        public string NoFim = "teste";
        public string[] Viagens = { "teste", "teste" };

        public string domainId2 = "teste2";
        public int TempoInicio2 = 2;
        public int TempoFim2 = 3;
        public string NoInicio2 = "teste2";
        public string NoFim2 = "teste2";
        public string[] Viagens2 = { "teste2", "teste2" };


        [Fact]
        public void BlocoTrabalhoServiceConstrutor()
        {

            var mockBlocoRepo = new Mock<IBlocoTrabalhoRepository>();
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new BlocoTrabalhoService(mockUnitRepo.Object, mockBlocoRepo.Object);

            Assert.NotNull(service);
        }

        [Fact]
        public async void GetAllAsyncTest()
        {

            var mockRepo = new Mock<IBlocoTrabalhoRepository>();
            mockRepo.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(GetBlocos());
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new BlocoTrabalhoService(mockUnitRepo.Object, mockRepo.Object);

            var blocos = await service.GetAllAsync();

            var blocosDto = GetBlocosDto();

            Assert.Equal(blocosDto.Count(), blocos.Count());

        }

        [Fact]
        public async void GetByIdAsyncTest()
        {

            var id = new BlocoTrabalhoId(this.domainId);
            var b1 = new BlocoTrabalho(
                this.domainId,
                this.TempoInicio,
                this.TempoFim,
                this.NoInicio,
                this.NoFim,
                this.Viagens
                );
            var b2 = new BlocoTrabalhoDto
            {
                Id = new BlocoTrabalhoId(this.domainId).AsString(),
                domainId = this.domainId,
                TempoInicio = this.TempoInicio,
                TempoFim = this.TempoFim,
                NoInicio = this.NoInicio,
                NoFim = this.NoFim,
                Viagens = this.Viagens
            };

            var mockRepo = new Mock<IBlocoTrabalhoRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(b1);
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new BlocoTrabalhoService(mockUnitRepo.Object, mockRepo.Object);

            var bloco = await service.GetByIdAsync(id);

            Assert.True(bloco.Equal(b2));

        }


        [Fact]
        public async void AddAsyncTest()
        {

            var id = new BlocoTrabalhoId(this.domainId);
            var b1 = new BlocoTrabalho(
                this.domainId,
                this.TempoInicio,
                this.TempoFim,
                this.NoInicio,
                this.NoFim,
                this.Viagens
                );
            var b2 = new BlocoTrabalhoDto
            {
                Id = new BlocoTrabalhoId(this.domainId).AsString(),
                domainId = this.domainId,
                TempoInicio = this.TempoInicio,
                TempoFim = this.TempoFim,
                NoInicio = this.NoInicio,
                NoFim = this.NoFim,
                Viagens = this.Viagens
            };

            var mockRepo = new Mock<IBlocoTrabalhoRepository>();
            mockRepo.Setup(repo => repo.AddAsync(b1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new BlocoTrabalhoService(mockUnitRepo.Object, mockRepo.Object);

            var bloco = await service.AddAsync(b2);

            Assert.True(bloco.Equal(b2));

        }

        [Fact]
        public async void UpdateAsyncTest()
        {

            var id = new BlocoTrabalhoId(this.domainId);
            var b1 = new BlocoTrabalho(
                this.domainId,
                this.TempoInicio,
                this.TempoFim,
                this.NoInicio,
                this.NoFim,
                this.Viagens
                );
            var b2 = new BlocoTrabalhoDto
            {
                Id = new BlocoTrabalhoId(this.domainId).AsString(),
                domainId = this.domainId,
                TempoInicio = this.TempoInicio,
                TempoFim = this.TempoFim,
                NoInicio = this.NoInicio,
                NoFim = this.NoFim,
                Viagens = this.Viagens
            };
            
            string[] novo = {"novo","novo"};
            var b2Alt = new BlocoTrabalhoDto
            {
                Id = new BlocoTrabalhoId(this.domainId).AsString(),
                domainId = this.domainId,
                TempoInicio = 5,
                TempoFim = 6,
                NoInicio = "novo",
                NoFim = "novo",
                Viagens = novo
            };

            var mockRepo = new Mock<IBlocoTrabalhoRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(b1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new BlocoTrabalhoService(mockUnitRepo.Object, mockRepo.Object);

            var bloco = await service.UpdateAsync(b2Alt);

            Assert.True(bloco.Equal(b2Alt));

        }

        [Fact]
        public async void DeleteAsyncTest()
        {

            var id = new BlocoTrabalhoId(this.domainId);
            var b1 = new BlocoTrabalho(
                this.domainId,
                this.TempoInicio,
                this.TempoFim,
                this.NoInicio,
                this.NoFim,
                this.Viagens
                );
            var b2 = new BlocoTrabalhoDto
            {
                Id = new BlocoTrabalhoId(this.domainId).AsString(),
                domainId = this.domainId,
                TempoInicio = this.TempoInicio,
                TempoFim = this.TempoFim,
                NoInicio = this.NoInicio,
                NoFim = this.NoFim,
                Viagens = this.Viagens
            };

            var mockRepo = new Mock<IBlocoTrabalhoRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(b1);
            mockRepo.Setup(repo => repo.Remove(b1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new BlocoTrabalhoService(mockUnitRepo.Object, mockRepo.Object);

            var bloco = await service.DeleteAsync(id);

            Assert.True(bloco.Equal(b2));

        }

        private List<BlocoTrabalho> GetBlocos()
        {
            var blocos = new List<BlocoTrabalho>();
            blocos.Add(new BlocoTrabalho(
                this.domainId,
                this.TempoInicio,
                this.TempoFim,
                this.NoInicio,
                this.NoFim,
                this.Viagens
                ));

            blocos.Add(new BlocoTrabalho(
                this.domainId2,
                this.TempoInicio2,
                this.TempoFim2,
                this.NoInicio2,
                this.NoFim2,
                this.Viagens2
                ));

            return blocos;
        }

        private List<BlocoTrabalhoDto> GetBlocosDto()
        {
            List<BlocoTrabalhoDto> blocos = new List<BlocoTrabalhoDto>();
            blocos.Add(new BlocoTrabalhoDto
            {
                Id = new BlocoTrabalhoId(this.domainId).AsString(),
                domainId = this.domainId,
                TempoInicio = this.TempoInicio,
                TempoFim = this.TempoFim,
                NoInicio = this.NoInicio,
                NoFim = this.NoFim,
                Viagens = this.Viagens
            });
            blocos.Add(new BlocoTrabalhoDto
            {
                Id = new BlocoTrabalhoId(this.domainId2).AsString(),
                domainId = this.domainId2,
                TempoInicio = this.TempoInicio2,
                TempoFim = this.TempoFim2,
                NoInicio = this.NoInicio2,
                NoFim = this.NoFim2,
                Viagens = this.Viagens2
            });
            return blocos;
        }
    }
}