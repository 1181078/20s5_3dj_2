using System;
using MDV1.Domain.ServicosViatura;
using Xunit;

namespace MDVTestes.MDVTests.Domain.ServicosViatura{

    public class ServicoViaturaIdTest{

               
          public string IdValue= "teste";

        [Fact]
        public void ServicoViaturaIdConstrutorGetsSets()
        {
                     
            var id = new ServicoViaturaId(this.IdValue);

            Assert.NotNull(id);  
        }

        [Fact]
        public void AsStringTest(){

            var id = new ServicoViaturaId(this.IdValue);

            string val; 
            val = id.AsString();
            Assert.Equal(this.IdValue,val);

        }
       
    }
}