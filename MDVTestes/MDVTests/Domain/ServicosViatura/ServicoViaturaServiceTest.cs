using MDV1.Domain.ServicosViatura;
using MDV1.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace MDVTestes.MDVTests.Domain.ServicosViatura
{

    public class ServicoViaturaServiceTest
    {



        public string domainId = "teste";
        public string nome = "teste";
        public string cor = "teste";
        public string[] blocks = { "teste", "teste" };

        public string domainId2 = "teste2";
        public string nome2 = "teste2";
        public string cor2 = "teste2";
        public string[] blocks2 = { "teste2", "teste2" };



        [Fact]
        public void ServicoViaturaServiceConstrutor()
        {

            var mockRepo = new Mock<IServicoViaturaRepository>();
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new ServicoViaturaService(mockUnitRepo.Object, mockRepo.Object);

            Assert.NotNull(service);
        }

        [Fact]
        public async void GetAllAsyncTest()
        {

            var mockRepo = new Mock<IServicoViaturaRepository>();
            mockRepo.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(GetSV());
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new ServicoViaturaService(mockUnitRepo.Object, mockRepo.Object);

            var sv = await service.GetAllAsync();

            var Dto = GetSVDto();

            Assert.Equal(Dto.Count(), sv.Count());

        }

        [Fact]
        public async void GetByIdAsyncTest()
        {

            var id = new ServicoViaturaId(this.domainId);
            var sv1 = new ServicoViatura(
                this.domainId,
                this.nome,
                this.cor,
                this.blocks
            );
            var st2 = new ServicoViaturaDto
            {
                Id = new ServicoViaturaId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            };

            var mockRepo = new Mock<IServicoViaturaRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(sv1);
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new ServicoViaturaService(mockUnitRepo.Object, mockRepo.Object);

            var serVia = await service.GetByIdAsync(id);

            Assert.True(serVia.Equal(st2));

        }


        [Fact]
        public async void AddAsyncTest()
        {

            var id = new ServicoViaturaId(this.domainId);
            var sv1 = new ServicoViatura(
              this.domainId,
              this.nome,
              this.cor,
              this.blocks
          );
            var st2 = new ServicoViaturaDto
            {
                Id = new ServicoViaturaId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            };

            var mockRepo = new Mock<IServicoViaturaRepository>();
            mockRepo.Setup(repo => repo.AddAsync(sv1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new ServicoViaturaService(mockUnitRepo.Object, mockRepo.Object);

            var serVia = await service.AddAsync(st2);

            Assert.True(serVia.Equal(st2));

        }

        [Fact]
        public async void UpdateAsyncTest()
        {

            var id = new ServicoViaturaId(this.domainId);
            var sv1 = new ServicoViatura(
              this.domainId,
              this.nome,
              this.cor,
              this.blocks
          );
            var st2 = new ServicoViaturaDto
            {
                Id = new ServicoViaturaId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            };

            string[] novo = { "novo", "novo" };
            var st2Alt = new ServicoViaturaDto
            {
                Id = new ServicoViaturaId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome2,
                cor = this.cor2,
                blocks = this.blocks2
            };

            var mockRepo = new Mock<IServicoViaturaRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(sv1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new ServicoViaturaService(mockUnitRepo.Object, mockRepo.Object);

            var serVia = await service.UpdateAsync(st2Alt);
            
            Assert.True(serVia.Equal(st2Alt));

        }

        [Fact]
        public async void DeleteAsyncTest()
        {

            var id = new ServicoViaturaId(this.domainId);
            var sv1 = new ServicoViatura(
              this.domainId,
              this.nome,
              this.cor,
              this.blocks
          );
           var st2 = new ServicoViaturaDto
            {
                Id = new ServicoViaturaId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            };

            var mockRepo = new Mock<IServicoViaturaRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(sv1);
            mockRepo.Setup(repo => repo.Remove(sv1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new ServicoViaturaService(mockUnitRepo.Object, mockRepo.Object);

            var serVia = await service.DeleteAsync(id);

            Assert.True(serVia.Equal(st2));

        }

        private List<ServicoViatura> GetSV()
        {
            var sv = new List<ServicoViatura>();
            sv.Add(new ServicoViatura(
                this.domainId,
                this.nome,
                this.cor,
                this.blocks
                ));

            sv.Add(new ServicoViatura(
                this.domainId2,
                this.nome2,
                this.cor2,
                this.blocks2
                ));

            return sv;
        }

        private List<ServicoViaturaDto> GetSVDto()
        {
            List<ServicoViaturaDto> sv = new List<ServicoViaturaDto>();
            sv.Add(new ServicoViaturaDto
            {
                Id = new ServicoViaturaId(this.domainId).AsString(),
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            });
            sv.Add(new ServicoViaturaDto
            {
                Id = new ServicoViaturaId(this.domainId2).AsString(),
                domainId = this.domainId2,
                nome = this.nome2,
                cor = this.cor2,
                blocks = this.blocks2
            });
            return sv;
        }
    }
}