using System;
using MDV1.Domain.ServicosViatura;
using Xunit;

namespace MDVTestes.MDVTests.Domain.ServicosViatura
{

    public class ServicoViaturaTest
    {

        public string Id = "new";
        public string domainId = "teste";
        public string nome = "teste";
        public string cor = "teste";
        public string[] blocks = { "teste", "teste" };


        [Fact]
        public void ServicoViaturaConstrutorGetsSets()
        {


            var sv = new ServicoViatura(
                 this.domainId,
                 this.nome,
                 this.cor,
                 this.blocks
                 );

            Assert.NotNull(sv);
            Assert.Equal(sv.domainId, this.domainId);
            Assert.Equal(sv.nome, this.nome);
            Assert.Equal(sv.cor, this.cor);
            Assert.Equal(sv.blocks, this.blocks);
        }

        [Fact]
        public void Mudancas()
        {

            var sv = new ServicoViatura(
               this.domainId,
               this.nome,
               this.cor,
               this.blocks
               );

            string muda = "muda";
            string[] mudaB = { "muda", "muda", "muda" };
            sv.mudaNome(muda);
            sv.mudaCor(muda);
            sv.mudaBlocks(mudaB);

            Assert.NotNull(sv);
            Assert.Equal(muda, sv.nome);
            Assert.Equal(muda, sv.cor);
            Assert.Equal(mudaB, sv.blocks);
        }
    }
}