using System;
using MDV1.Domain.ServicosViatura;
using Xunit;

namespace MDVTestes.MDVTests.Domain.ServicosViatura
{

    public class ServicoViaturaDtoTest
    {

        public string Id = "new";
        public string domainId = "teste";
        public string nome = "teste";
        public string cor = "teste";
        public string[] blocks = { "teste", "teste" };

        [Fact]
        public void ServicoViaturaDtoConstructor()
        {
            var Dto = new ServicoViaturaDto
            {
                Id = this.Id,
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            };
            Assert.NotNull(Dto);
            Assert.Equal(Dto.Id, this.Id);
            Assert.Equal(Dto.domainId, this.domainId);
            Assert.Equal(Dto.nome, this.nome);
            Assert.Equal(Dto.cor, this.cor);
            Assert.Equal(Dto.blocks, this.blocks);
        }

        [Fact]
        public void Sets()
        {
           var Dto = new ServicoViaturaDto
            {
                Id = this.Id,
                domainId = this.domainId,
                nome = this.nome,
                cor = this.cor,
                blocks = this.blocks
            };

            string nova = "nova";
            string[] novoA = { nova, nova, nova };


            Dto.domainId = nova;
            Dto.nome = nova;
            Dto.cor = nova;
            Dto.blocks = novoA;
            Assert.Equal(Dto.domainId, nova);
            Assert.Equal(Dto.nome, nova);
            Assert.Equal(Dto.cor, nova);
            Assert.Equal(Dto.blocks, novoA);
        }
    }
}
