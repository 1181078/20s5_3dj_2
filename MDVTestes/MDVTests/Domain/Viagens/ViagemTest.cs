using System;
using MDV1.Domain.Viagens;
using Xunit;

namespace MDVTestes.MDVTests.Domain.Viagens
{

    public class ViagemTest
    {

        public string domainId = "teste";
        public bool vazio = false;
        public string lineId = "teste";
        public string pathId = "teste";
        public bool gerado = false;
        public int[] tempos = {111,222};
        [Fact]
        public void ViagemConstrutorGetsSets()
        {


            var viagem = new Viagem(

                this.domainId,
                this.vazio,
                this.lineId,
                this.pathId,
                this.gerado,
                this.tempos
                 );

            Assert.NotNull(viagem);
            Assert.Equal(viagem.domainId, this.domainId);
            Assert.Equal(viagem.vazio, this.vazio);
            Assert.Equal(viagem.lineId, this.lineId);
            Assert.Equal(viagem.pathId, this.pathId);
            Assert.Equal(viagem.gerado, this.gerado);
            Assert.Equal(viagem.tempos, this.tempos);
        }

        [Fact]
        public void Mudancas()
        {

             var viagem = new Viagem(

                this.domainId,
                this.vazio,
                this.lineId,
                this.pathId,
                this.gerado,
                this.tempos
                 );

            string muda = "muda";
            bool verdade  = true;
            int[] mudaI = {1,2,3};
            viagem.mudaVazio(verdade);
            viagem.mudaLine(muda);
            viagem.mudaPath(muda);
            viagem.mudaGerado(verdade);
            viagem.mudarTempos(mudaI);

            Assert.NotNull(viagem);
            Assert.Equal(verdade, viagem.vazio);
            Assert.Equal(muda, viagem.lineId);
            Assert.Equal(muda, viagem.pathId);
            Assert.Equal(verdade, viagem.gerado);
            Assert.Equal(mudaI, viagem.tempos);
        }
    }
}