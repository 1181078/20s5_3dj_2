using MDV1.Domain.Viagens;
using MDV1.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace MDVTestes.MDVTests.Domain.Viagens
{

    public class ViagemServiceTest
    {



        public string domainId = "teste";
        public bool vazio = false;
        public string lineId = "teste";
        public string pathId = "teste";
        public bool gerado = false;
        public int[] tempos = {111,222};

        public string domainId2 = "teste2";
        public bool vazio2 = false;
        public string lineId2 = "teste2";
        public string pathId2 = "teste2";
        public bool gerado2 = false;
        public int[] tempos2 = {1,11,2,22};



        [Fact]
        public void ViagemServiceConstrutor()
        {

            var mockRepo = new Mock<IViagemRepository>();
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new ViagemService(mockUnitRepo.Object, mockRepo.Object);

            Assert.NotNull(service);
        }

        [Fact]
        public async void GetAllAsyncTest()
        {

            var mockRepo = new Mock<IViagemRepository>();
            mockRepo.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(GetViagens());
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new ViagemService(mockUnitRepo.Object, mockRepo.Object);

            var viagens = await service.GetAllAsync();

            var Dto = GetViagensDto();

            Assert.Equal(Dto.Count(), viagens.Count());

        }

        [Fact]
        public async void GetByIdAsyncTest()
        {

            var id = new ViagemId(this.domainId);
            var v1 = new Viagem(
                this.domainId,
                this.vazio,
                this.lineId,
                this.pathId,
                this.gerado,
                this.tempos
            );
            var v2 = new ViagemDto
            {
                 Id = new ViagemId(this.domainId).AsString(),
                domainId = this.domainId,
                vazio = this.vazio,
                lineId = this.lineId,
                pathId = this.pathId,
                gerado = this.gerado,
                tempos = this.tempos
            };

            var mockRepo = new Mock<IViagemRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(v1);
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new ViagemService(mockUnitRepo.Object, mockRepo.Object);

            var tripulante = await service.GetByIdAsync(id);

            Assert.True(tripulante.Equal(v2));

        }


        [Fact]
        public async void AddAsyncTest()
        {

            var id = new ViagemId(this.domainId);
            var v1 = new Viagem(
                this.domainId,
                this.vazio,
                this.lineId,
                this.pathId,
                this.gerado,
                this.tempos
            );
            var v2 = new ViagemDto
            {
                  Id = new ViagemId(this.domainId).AsString(),
                domainId = this.domainId,
                vazio = this.vazio,
                lineId = this.lineId,
                pathId = this.pathId,
                gerado = this.gerado,
                tempos = this.tempos
            };

            var mockRepo = new Mock<IViagemRepository>();
            mockRepo.Setup(repo => repo.AddAsync(v1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new ViagemService(mockUnitRepo.Object, mockRepo.Object);

            var tripulante = await service.AddAsync(v2);
            
            Assert.True(tripulante.Equal(v2));

        }

        [Fact]
        public async void UpdateAsyncTest()
        {

            var id = new ViagemId(this.domainId);
            var v1 = new Viagem(
                 this.domainId,
                this.vazio,
                this.lineId,
                this.pathId,
                this.gerado,
                this.tempos
            );
            var v2 = new ViagemDto
            {
                 Id = new ViagemId(this.domainId).AsString(),
                domainId = this.domainId,
                vazio = this.vazio,
                lineId = this.lineId,
                pathId = this.pathId,
                gerado = this.gerado,
                tempos = this.tempos
            };

            string[] novo = { "novo", "novo" };
            var v2Alt = new ViagemDto
            {
                Id = new ViagemId(this.domainId).AsString(),
                domainId = this.domainId,
                vazio = this.vazio2,
                lineId = this.lineId2,
                pathId = this.pathId2,
                gerado = this.gerado2,
                tempos = this.tempos2
            };

            var mockRepo = new Mock<IViagemRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(v1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new ViagemService(mockUnitRepo.Object, mockRepo.Object);

            var tripulante = await service.UpdateAsync(v2Alt);

            Assert.True(tripulante.Equal(v2Alt));

        }

        [Fact]
        public async void DeleteAsyncTest()
        {

            var id = new ViagemId(this.domainId);
            var v1 = new Viagem(
                this.domainId,
                this.vazio,
                this.lineId,
                this.pathId,
                this.gerado,
                this.tempos
            );
            var v2 = new ViagemDto
            {
                  Id = new ViagemId(this.domainId).AsString(),
                domainId = this.domainId,
                vazio = this.vazio,
                lineId = this.lineId,
                pathId = this.pathId,
                gerado = this.gerado,
                tempos = this.tempos
            };

            var mockRepo = new Mock<IViagemRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(v1);
            mockRepo.Setup(repo => repo.Remove(v1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new ViagemService(mockUnitRepo.Object, mockRepo.Object);

            var tripulante = await service.DeleteAsync(id);
            
            Assert.True(tripulante.Equal(v2));

        }

        private List<Viagem> GetViagens()
        {
            var viagens = new List<Viagem>();
            viagens.Add(new Viagem(
                this.domainId,
                this.vazio,
                this.lineId,
                this.pathId,
                this.gerado,
                this.tempos
                ));

            viagens.Add(new Viagem(
                this.domainId2,
                this.vazio2,
                this.lineId2,
                this.pathId2,
                this.gerado2,
                this.tempos2
                ));

            return viagens;
        }

        private List<ViagemDto> GetViagensDto()
        {
            List<ViagemDto> viagens = new List<ViagemDto>();
            viagens.Add(new ViagemDto
            {
                Id = new ViagemId(this.domainId).AsString(),
                domainId = this.domainId,
                vazio = this.vazio,
                lineId = this.lineId,
                pathId = this.pathId,
                gerado = this.gerado,
                tempos = this.tempos
            });
            viagens.Add(new ViagemDto
            {
                Id = new ViagemId(this.domainId2).AsString(),
                domainId = this.domainId2,
                vazio = this.vazio2,
                lineId = this.lineId2,
                pathId = this.pathId2,
                gerado = this.gerado2,
                tempos = this.tempos2,
            });
            return viagens;
        }
    }
}