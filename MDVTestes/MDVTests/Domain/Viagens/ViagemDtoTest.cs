using System;
using MDV1.Domain.Viagens;
using Xunit;

namespace MDVTestes.MDVTests.Domain.Viagens
{

    public class ViagemDtoTest
    {

        public string Id = "new";
        public string domainId = "teste";
        public bool vazio = false;
        public string lineId = "teste";
        public string pathId = "teste";
        public bool gerado = false;
        public int[] tempos = { 111, 222 };

        [Fact]
        public void TripulanteDtoConstructor()
        {
            var Dto = new ViagemDto
            {
                Id = this.Id,
                domainId = this.domainId,
                vazio = this.vazio,
                lineId = this.lineId,
                pathId = this.pathId,
                gerado = this.gerado,
                tempos = this.tempos,
            };
            Assert.NotNull(Dto);
            Assert.Equal(Dto.Id, this.Id);
            Assert.Equal(Dto.domainId, this.domainId);
            Assert.Equal(Dto.vazio, this.vazio);
            Assert.Equal(Dto.lineId, this.lineId);
            Assert.Equal(Dto.pathId, this.pathId);
            Assert.Equal(Dto.gerado, this.gerado);
            Assert.Equal(Dto.tempos, this.tempos);
        }

        [Fact]
        public void Sets()
        {
            var Dto = new ViagemDto
            {
                Id = this.Id,
                domainId = this.domainId,
                vazio = this.vazio,
                lineId = this.lineId,
                pathId = this.pathId,
                gerado = this.gerado,
                tempos = this.tempos,
            };

            string nova = "nova";
            int[] novoInt = { 2, 2, 2 };
            bool verdade = true;
            Dto.domainId = nova;
            Dto.vazio = verdade;
            Dto.lineId = nova;
            Dto.pathId = nova;
            Dto.gerado = verdade;
            Dto.tempos = novoInt;
      
            Assert.Equal(Dto.domainId, nova);
            Assert.Equal(Dto.vazio, verdade);
            Assert.Equal(Dto.lineId, nova);
            Assert.Equal(Dto.pathId, nova);
            Assert.Equal(Dto.gerado, verdade);
            Assert.Equal(Dto.tempos, novoInt);
        }
    }
}
