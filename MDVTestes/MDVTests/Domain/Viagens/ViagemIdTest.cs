using System;
using MDV1.Domain.Viagens;
using Xunit;

namespace MDVTestes.MDVTests.Domain.Viagens{

    public class ViagemIdTest{

               
          public string IdValue= "teste";

        [Fact]
        public void ViagemIdConstrutorGetsSets()
        {
                     
            var id = new ViagemId(this.IdValue);

            Assert.NotNull(id);  
        }

        [Fact]
        public void AsStringTest(){

            var id = new ViagemId(this.IdValue);

            string val; 
            val = id.AsString();
            Assert.Equal(this.IdValue,val);

        }
       
    }
}