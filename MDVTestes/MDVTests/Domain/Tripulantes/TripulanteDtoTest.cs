using System;
using MDV1.Domain.Tripulantes;
using Xunit;

namespace MDVTestes.MDVTests.Domain.Tripulantes
{

    public class TripulanteDtoTest
    {

        public string Id = "new";
        public int numMecanografico = 111;
        public string nome = "teste";
        public string dataDeNascimento = "teste";
        public int numCartaoCidadao = 111;
        public int NIF = 111;
        public string tipoTripulante = "teste";
        public string dataDeEntrada = "teste";
        public string dataDeSaida = "teste";

        [Fact]
        public void TripulanteDtoConstructor()
        {
            var Dto = new TripulanteDto
            {
                Id = this.Id,
                numMecanografico = this.numMecanografico,
                nome = this.nome,
                dataDeNascimento = this.dataDeNascimento,
                numCartaoCidadao = this.numCartaoCidadao,
                NIF = this.NIF,
                tipoTripulante = this.tipoTripulante,
                dataDeEntrada = this.dataDeEntrada,
                dataDeSaida = this.dataDeSaida
            };
            Assert.NotNull(Dto);
            Assert.Equal(Dto.Id, this.Id);
            Assert.Equal(Dto.numMecanografico, this.numMecanografico);
            Assert.Equal(Dto.nome, this.nome);
            Assert.Equal(Dto.dataDeNascimento, this.dataDeNascimento);
            Assert.Equal(Dto.numCartaoCidadao, this.numCartaoCidadao);
            Assert.Equal(Dto.NIF,this.NIF);
            Assert.Equal(Dto.tipoTripulante,this.tipoTripulante);
            Assert.Equal(Dto.dataDeEntrada,this.dataDeEntrada);
            Assert.Equal(Dto.dataDeSaida,this.dataDeSaida);
        }

        [Fact]
        public void Sets()
        {
           var Dto = new TripulanteDto
            {
                Id = this.Id,
                numMecanografico = this.numMecanografico,
                nome = this.nome,
                dataDeNascimento = this.dataDeNascimento,
                numCartaoCidadao = this.numCartaoCidadao,
                NIF = this.NIF,
                tipoTripulante = this.tipoTripulante,
                dataDeEntrada = this.dataDeEntrada,
                dataDeSaida = this.dataDeSaida
            };

            string nova = "nova";
            int novoInt = 222;

            Dto.numMecanografico = novoInt;
            Dto.nome = nova;
            Dto.dataDeNascimento = nova;
            Dto.numCartaoCidadao = novoInt;
            Dto.NIF = novoInt;
            Dto.tipoTripulante = nova;
            Dto.dataDeEntrada = nova;
            Dto.dataDeSaida = nova;
            Assert.Equal(Dto.numMecanografico, novoInt);
            Assert.Equal(Dto.nome, nova);
            Assert.Equal(Dto.dataDeNascimento, nova);
            Assert.Equal(Dto.numCartaoCidadao, novoInt);
            Assert.Equal(Dto.NIF,novoInt);
            Assert.Equal(Dto.tipoTripulante,nova);
            Assert.Equal(Dto.dataDeEntrada,nova);
            Assert.Equal(Dto.dataDeSaida,nova);
        }
    }
}
