using MDV1.Domain.Tripulantes;
using MDV1.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace MDVTestes.MDVTests.Domain.Tripulantes
{

    public class TripulanteServiceTest
    {



        public int numMecanografico = 111;
        public string nome = "teste";
        public string dataDeNascimento = "teste";
        public int numCartaoCidadao = 111;
        public int NIF = 111;
        public string tipoTripulante = "teste";
        public string dataDeEntrada = "teste";
        public string dataDeSaida = "teste";

        public int numMecanografico2 = 222;
        public string nome2 = "teste2";
        public string dataDeNascimento2 = "teste2";
        public int numCartaoCidadao2 = 222;
        public int NIF2 = 222;
        public string tipoTripulante2 = "teste2";
        public string dataDeEntrada2 = "teste2";
        public string dataDeSaida2 = "teste2";



        [Fact]
        public void TripulanteServiceConstrutor()
        {

            var mockRepo = new Mock<ITripulanteRepository>();
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new TripulanteService(mockUnitRepo.Object, mockRepo.Object);

            Assert.NotNull(service);
        }

        [Fact]
        public async void GetAllAsyncTest()
        {

            var mockRepo = new Mock<ITripulanteRepository>();
            mockRepo.Setup(repo => repo.GetAllAsync())
                .ReturnsAsync(GetTripulantes());
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new TripulanteService(mockUnitRepo.Object, mockRepo.Object);

            var t = await service.GetAllAsync();

            var Dto = GetTripulantesDto();

            Assert.Equal(Dto.Count(), t.Count());
            Assert.Equal(Dto, t);

        }

        [Fact]
        public async void GetByIdAsyncTest()
        {

            var id = new TripulanteId(this.numMecanografico.ToString());
            var t1 = new Tripulante(
                this.numMecanografico,
                this.nome,
                this.dataDeNascimento,
                this.numCartaoCidadao,
                this.NIF,
                this.tipoTripulante,
                this.dataDeEntrada,
                this.dataDeSaida
            );
            var t2 = new TripulanteDto
            {
                 Id = new TripulanteId(this.numMecanografico.ToString()).AsString(),
                numMecanografico = this.numMecanografico,
                nome = this.nome,
                dataDeNascimento = this.dataDeNascimento,
                numCartaoCidadao = this.numCartaoCidadao,
                NIF = this.NIF,
                tipoTripulante = this.tipoTripulante,
                dataDeEntrada = this.dataDeEntrada,
                dataDeSaida = this.dataDeSaida
            };

            var mockRepo = new Mock<ITripulanteRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(t1);
            var mockUnitRepo = new Mock<IUnitOfWork>();

            var service = new TripulanteService(mockUnitRepo.Object, mockRepo.Object);

            var tripulante = await service.GetByIdAsync(id);

            Assert.Equal(tripulante, t2);

        }


        [Fact]
        public async void AddAsyncTest()
        {

            var id = new TripulanteId(this.numMecanografico.ToString());
            var t1 = new Tripulante(
                this.numMecanografico,
                this.nome,
                this.dataDeNascimento,
                this.numCartaoCidadao,
                this.NIF,
                this.tipoTripulante,
                this.dataDeEntrada,
                this.dataDeSaida
            );
            var t2 = new TripulanteDto
            {
                Id = new TripulanteId(this.numMecanografico.ToString()).AsString(),
                numMecanografico = this.numMecanografico,
                nome = this.nome,
                dataDeNascimento = this.dataDeNascimento,
                numCartaoCidadao = this.numCartaoCidadao,
                NIF = this.NIF,
                tipoTripulante = this.tipoTripulante,
                dataDeEntrada = this.dataDeEntrada,
                dataDeSaida = this.dataDeSaida
            };

            var mockRepo = new Mock<ITripulanteRepository>();
            mockRepo.Setup(repo => repo.AddAsync(t1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new TripulanteService(mockUnitRepo.Object, mockRepo.Object);

            var tripulante = await service.AddAsync(t2);

            Assert.Equal(tripulante, t2);

        }

        [Fact]
        public async void UpdateAsyncTest()
        {

            var id = new TripulanteId(this.numMecanografico.ToString());
            var t1 = new Tripulante(
                this.numMecanografico,
                this.nome,
                this.dataDeNascimento,
                this.numCartaoCidadao,
                this.NIF,
                this.tipoTripulante,
                this.dataDeEntrada,
                this.dataDeSaida
            );
            var t2 = new TripulanteDto
            {
                Id = new TripulanteId(this.numMecanografico.ToString()).AsString(),
                numMecanografico = this.numMecanografico,
                nome = this.nome,
                dataDeNascimento = this.dataDeNascimento,
                numCartaoCidadao = this.numCartaoCidadao,
                NIF = this.NIF,
                tipoTripulante = this.tipoTripulante,
                dataDeEntrada = this.dataDeEntrada,
                dataDeSaida = this.dataDeSaida
            };

            string[] novo = { "novo", "novo" };
            var t2Alt = new TripulanteDto
            {
                Id = new TripulanteId(this.numMecanografico.ToString()).AsString(),
                numMecanografico = this.numMecanografico,
                nome = this.nome2,
                dataDeNascimento = this.dataDeNascimento2,
                numCartaoCidadao = this.numCartaoCidadao2,
                NIF = this.NIF2,
                tipoTripulante = this.tipoTripulante2,
                dataDeEntrada = this.dataDeEntrada2,
                dataDeSaida = this.dataDeSaida2
            };

            var mockRepo = new Mock<ITripulanteRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(t1);
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new TripulanteService(mockUnitRepo.Object, mockRepo.Object);

            var tripulante = await service.UpdateAsync(t2Alt);

            Assert.Equal(tripulante, t2Alt);

        }

        [Fact]
        public async void DeleteAsyncTest()
        {

            var id = new TripulanteId(this.numMecanografico.ToString());
            var t1 = new Tripulante(
                this.numMecanografico,
                this.nome,
                this.dataDeNascimento,
                this.numCartaoCidadao,
                this.NIF,
                this.tipoTripulante,
                this.dataDeEntrada,
                this.dataDeSaida
            );
            var t2 = new TripulanteDto
            {
                Id = new TripulanteId(this.numMecanografico.ToString()).AsString(),
                numMecanografico = this.numMecanografico,
                nome = this.nome,
                dataDeNascimento = this.dataDeNascimento,
                numCartaoCidadao = this.numCartaoCidadao,
                NIF = this.NIF,
                tipoTripulante = this.tipoTripulante,
                dataDeEntrada = this.dataDeEntrada,
                dataDeSaida = this.dataDeSaida
            };

            var mockRepo = new Mock<ITripulanteRepository>();
            mockRepo.Setup(repo => repo.GetByIdAsync(id))
                .ReturnsAsync(t1);
            mockRepo.Setup(repo => repo.Remove(t1));
            var mockUnitRepo = new Mock<IUnitOfWork>();
            mockUnitRepo.Setup(repo => repo.CommitAsync());

            var service = new TripulanteService(mockUnitRepo.Object, mockRepo.Object);

            var tripulante = await service.DeleteAsync(id);

            Assert.Equal(tripulante, t2);

        }

        private List<Tripulante> GetTripulantes()
        {
            var t = new List<Tripulante>();
            t.Add(new Tripulante(
                this.numMecanografico,
                this.nome,
                this.dataDeNascimento,
                this.numCartaoCidadao,
                this.NIF,
                this.tipoTripulante,
                this.dataDeEntrada,
                this.dataDeSaida
                ));

            t.Add(new Tripulante(
                this.numMecanografico2,
                this.nome2,
                this.dataDeNascimento2,
                this.numCartaoCidadao2,
                this.NIF2,
                this.tipoTripulante2,
                this.dataDeEntrada2,
                this.dataDeSaida2
                ));

            return t;
        }

        private List<TripulanteDto> GetTripulantesDto()
        {
            List<TripulanteDto> t = new List<TripulanteDto>();
            t.Add(new TripulanteDto
            {
                Id = new TripulanteId(this.numMecanografico.ToString()).AsString(),
                numMecanografico = this.numMecanografico,
                nome = this.nome,
                dataDeNascimento = this.dataDeNascimento,
                numCartaoCidadao = this.numCartaoCidadao,
                NIF = this.NIF,
                tipoTripulante = this.tipoTripulante,
                dataDeEntrada = this.dataDeEntrada,
                dataDeSaida = this.dataDeSaida
            });
            t.Add(new TripulanteDto
            {
                Id = new TripulanteId(this.numMecanografico2.ToString()).AsString(),
                numMecanografico = this.numMecanografico2,
                nome = this.nome2,
                dataDeNascimento = this.dataDeNascimento2,
                numCartaoCidadao = this.numCartaoCidadao2,
                NIF = this.NIF2,
                tipoTripulante = this.tipoTripulante2,
                dataDeEntrada = this.dataDeEntrada2,
                dataDeSaida = this.dataDeSaida2
            });
            return t;
        }
    }
}