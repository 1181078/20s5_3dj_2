using System;
using MDV1.Domain.Tripulantes;
using Xunit;

namespace MDVTestes.MDVTests.Domain.Tripulantes
{

    public class TripulanteTest
    {

        public int numMecanografico = 111;
        public string nome = "teste";
        public string dataDeNascimento = "teste";
        public int numCartaoCidadao = 111;
        public int NIF = 111;
        public string tipoTripulante = "teste";
        public string dataDeEntrada = "teste";
        public string dataDeSaida = "teste";
        [Fact]
        public void TripulanteConstrutorGetsSets()
        {


            var t = new Tripulante(

                this.numMecanografico,
                this.nome,
                this.dataDeNascimento,
                this.numCartaoCidadao,
                this.NIF,
                this.tipoTripulante,
                this.dataDeEntrada,
                this.dataDeSaida
                 );

            Assert.NotNull(t);
            Assert.Equal(t.numMecanografico, this.numMecanografico);
            Assert.Equal(t.nome, this.nome);
            Assert.Equal(t.dataDeNascimento, this.dataDeNascimento);
            Assert.Equal(t.numCartaoCidadao, this.numCartaoCidadao);
            Assert.Equal(t.NIF, this.NIF);
            Assert.Equal(t.tipoTripulante, this.tipoTripulante);
            Assert.Equal(t.dataDeEntrada, this.dataDeEntrada);
            Assert.Equal(t.dataDeSaida, this.dataDeSaida);
        }

        [Fact]
        public void Mudancas()
        {

            var t = new Tripulante(
                this.numMecanografico,
                this.nome,
                this.dataDeNascimento,
                this.numCartaoCidadao,
                this.NIF,
                this.tipoTripulante,
                this.dataDeEntrada,
                this.dataDeSaida
                 );

            string muda = "muda";
            int mudaI = 222;
            t.mudaNome(muda);
            t.mudaNascimento(muda);
            t.mudaCartaoCidadao(mudaI);
            t.mudaNIF(mudaI);
            t.mudarTipo(muda);
            t.mudarDataDeEntrada(muda);
            t.mudarDataDeSaida(muda);

            Assert.NotNull(t);
            Assert.Equal(muda, t.nome);
            Assert.Equal(muda, t.dataDeNascimento);
            Assert.Equal(mudaI, t.numCartaoCidadao);
            Assert.Equal(mudaI, t.NIF);
            Assert.Equal(muda, t.tipoTripulante);
            Assert.Equal(muda, t.dataDeEntrada);
            Assert.Equal(muda, t.dataDeSaida);
        }
    }
}