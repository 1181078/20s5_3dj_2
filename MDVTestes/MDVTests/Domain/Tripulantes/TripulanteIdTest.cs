using System;
using MDV1.Domain.Tripulantes;
using Xunit;

namespace MDVTestes.MDVTests.Domain.Tripulantes{

    public class TripulanteIdTest{

               
          public string IdValue= "teste";

        [Fact]
        public void TripulanteIdConstrutorGetsSets()
        {
                     
            var id = new TripulanteId(this.IdValue);

            Assert.NotNull(id);  
        }

        [Fact]
        public void AsStringTest(){

            var id = new TripulanteId(this.IdValue);

            string val; 
            val = id.AsString();
            Assert.Equal(this.IdValue,val);

        }
       
    }
}